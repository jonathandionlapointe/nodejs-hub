'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const config = require('./config');
const EventEmitter = require('events');
const os = require('os');
const Promise = require('bluebird');
const url = require('url');
const util = require('util');

const signAsync = Promise.promisify(require('jsonwebtoken').sign);
const verifyAsync = Promise.promisify(require('jsonwebtoken').verify);
const readFileAsync = Promise.promisify(require('fs').readFile);
const randomBytesAsync = Promise.promisify(require('crypto').randomBytes);

// For now we haven't clarified the use of scopes, and these are assigned to any
// logged in users or devices.
const USER_SCOPES = [
    'alarm.arm',
    'alarm.create',
    'alarm.edit',
    'alarm.list',
    'alarm.manage_pins',
    'alarm.read',
    'alarm.remove',
    'alarm.sector.create',
    'alarm.sector.edit',
    'alarm.sector.remove',
    'alarm.zone.create',
    'alarm.zone.edit',
    'alarm.zone.remove',
    'capability.edit',
    'device.create',
    'device.edit',
    'device.list',
    'device.read',
    'device.remove',
    'event.subscribe',
    'gateway.manage',
    'scenario.create',
    'scenario.edit',
    'scenario.list',
    'scenario.read',
    'scenario.remove',
    'schedule.create',
    'schedule.edit',
    'schedule.list',
    'schedule.read',
    'schedule.remove',
    'zwave.exclude',
];

// For requests from the API, we additionally allow nonce requests.
const CLOUD_SCOPES = USER_SCOPES.concat([
    'auth.nonce',
]);

// Nothing uses these scopes yet.
//const SERVICE_SCOPES = [
//    'event.create',
//];

function Auth() {
    EventEmitter.call(this);

    // This is the example secret from hapi-auth-jwt.  Overwritten by auth.init().
    this.publicKey = 'BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc';
    this.privateKey = 'BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc';
    this.algorithm = 'HS256';
    this.nonces = {};
    this.tokenExpiry = 180 * 24 * 60 * 60; // 180 days (in seconds).
    this.nonceExpiry = 60 * 1000; // One minute (in milliseconds).
    this.apiToken = null;
    this.options = {};
}
util.inherits(Auth, EventEmitter);
module.exports = Auth;

Auth.views = {
    update(context) {
        context = context.getAppContext();
        return context.collection('users').createWithIndices([{
            name: 'users_username_idx',
            fields: ['username']
        }]);
    }
};

// Initialize the keys.
Auth.prototype.init = Promise.method(function (options) {
    this.options = options;

    if (options.tokenExpiry > 0) {
        this.tokenExpiry = options.tokenExpiry;
    }

    if (options.nonceExpiry > 0) {
        this.nonceExpiry = options.nonceExpiry;
    }

    if (options.keys) {
        return Promise.join(
            readPEM(options.keys.public),
            readPEM(options.keys.private),
            (publicKey, privateKey) => {
                this.algorithm = 'RS256';
                this.publicKey = publicKey;
                this.privateKey = privateKey;
                return this.generateApiToken();
            }
        ).catch(error => {
            this.emit('keyError', error);
            return this.initWithSecret(options);
        });
    } else {
        return this.initWithSecret(options);
    }
});

Auth.prototype.initWithSecret = Promise.method(function (options) {
    this.options = options;

    if (typeof options.secret === 'string') {
        this.algorithm = 'HS256';
        this.publicKey = options.secret;
        this.privateKey = options.secret;
        return this.generateApiToken();
    } else {
        throw new Error('unable to initialize auth keys');
    }
});

Auth.prototype.validateCredentials = Promise.method(function (context, user, password) {
    // Basic Auth is performed with username=username and password=nonce.
    const entry = this.nonces[password];

    // Delete the nonce immediately; it may only be used once, valid or invalid!
    delete this.nonces[password];

    const isUser = _.get(entry, 'user.username') === user;
    const isExpired = _.get(entry, 'expiry') < Date.now();
    if (!entry || !isUser || isExpired) {
        return false;
    }

    const scope = USER_SCOPES;

    return { scope, whoType: 'user', who: entry.user._id };
});

Auth.prototype.validateToken = function (context, token, whoType, who) {
    return Promise.try(() => {
        // Usually the token is already verified by hapi-auth-jwt, but in some
        // cases (e.g. SockJS auth) it still needs to be verified.
        if (typeof token === 'string') {
            return verifyAsync(token, this.publicKey, { algorithms: [ this.algorithm ] });
        }
        return token;
    }).then(token => {
        let scope;

        if (token.device) {
            whoType = 'device';
            who = token.device;
            scope = USER_SCOPES;
        } else if (token.user) {
            whoType = 'user';
            who = token.user;
            scope = USER_SCOPES;
        } else if (!token.api) {
            throw new Error('invalid token');
        } else {
            scope = CLOUD_SCOPES;
        }

        return { scope, whoType, who };
    });
};

Auth.prototype.sign = function (data) {
    return signAsync(data, this.privateKey, { algorithm: this.algorithm });
};

// Returns a Promise for an authentication token.
Auth.prototype.login = function (credentials) {
    // TODO create a unique record for this token that will allow us to
    // invalidate tokens before expiration (e.g. for a remote
    // sign-out-of-devices feature).

    // TODO invalidate the old token if used to get a new token.
    const tokenData = {
        [credentials.whoType]: credentials.who,
        exp: Date.now() / 1000 + this.tokenExpiry
    };
    return signAsync(tokenData, this.privateKey, { algorithm: this.algorithm });
};

Auth.prototype.deviceLogin = function (device) {
    return signAsync({ device }, this.privateKey, { algorithm: this.algorithm });
};

Auth.prototype.generateApiToken = function () {
    return signAsync({ api: true }, this.privateKey, { algorithm: this.algorithm })
        .then(token => { this.apiToken = token; });
};

// Generates a nonce for local authentication.
Auth.prototype.nonce = function (user) {
    return Promise.join(
        readFileAsync(this.options.cert, 'utf8').reflect(),
        randomBytesAsync(48),
        (certPromise, nonce) => {
            const servername = os.hostname() + '.local';
            const address = getIPv4LanAddress(this.options.netInterface);
            const prefix = this.options.prefix;
            let cert = null;
            if (certPromise.isFulfilled()) {
                cert = certPromise.value();
            }
            const url = getNonceUrl(address, prefix, cert);
            nonce = nonce.toString('base64');
            const entry = {
                user: _.pick(user, ['_id', 'username']),
                expiry: Date.now() + this.nonceExpiry,
            };
            this.nonces[nonce] = entry;
            return { nonce, servername, cert, url };
        }
    );
};

function getNonceUrl(address, prefix, cert) {
    if (cert) {
        return url.format({
            protocol: 'https',
            hostname: address,
            pathname: prefix
        });
    } else if (config.system.test) {
        return url.format({
            protocol: 'http',
            hostname: address,
            port: config.server.port,
        });
    } else {
        throw Boom.customCode(500, 'the server certificate could not be found', 'AUTH_CERT_NOT_FOUND');
    }
}

Auth.prototype.sync = function (context) {
    const results = [];
    return context.database.query('SELECT _id FROM users', [], row => results.push(row._id))
    .return(results)
    .mapSeries(id => this.syncUser(context, id).reflect());
};

Auth.prototype.syncUser = function (context, user) {
    return context.sdk.http.get('/users', { token: context.sdk.auth.token, who: user, whoType: 'user' }).then(response => {
        if (response.statusCode === 200) {
            return response.data;
        } else {
            throw Boom.notFound();
        }
    }).mapSeries(user => {
        user = _.omit(user, ['__v', 'permissions']);
        return context.collection('users').upsert(user).return(user);
    });
};

Auth.prototype.getUserByName = function (context, username) {
    let result = null;
    return context.database.query(`
    SELECT doc
      FROM users
     WHERE json_extract(doc, '$.username') = ?1
     LIMIT 1
    `, [username], row => result = row.doc)
    .then(() => {
        if (result) {
            return JSON.parse(result);
        }
        throw Boom.notFound();
    });
};

function readPEM(config) {
    return Promise.try(() => {
        if (typeof config === 'string') {
            return config;
        }
        if (typeof config.file !== 'string') {
            throw new Error('expected key file');
        }
        return readFileAsync(config.file);
    });
}

function getIPv4LanAddress(iface) {
    return _.result(_.find(os.networkInterfaces()[iface], { family: 'IPv4' }), 'address');
}
