'use strict';

const _ = require('lodash');

function GroupDatabase(context) {
    this.context = context;
}

GroupDatabase.initialize = function (context) {
    return context.collection('groups').create();
};

GroupDatabase.prototype.upsertGroups = function (context, groups) {
    groups = groups.map(serializeGroup);
    return context.collection('groups').upsertMany(groups);
};

GroupDatabase.prototype.addGroupMember = function (context, groupId, member) {
    member = _.pick(member, 'who', 'whoType');
    return context.withTransactionContext(context => {
        const groups = context.collection('groups');
        return groups.get(groupId)
        .then(group => {
            const isMember = _.find(group.members, member);
            if (!isMember) {
                group.members.push(member);
                return groups.update(group);
            }
        });
    });
};

GroupDatabase.prototype.getMemberGroups = function (member) {
    const results = [];
    return this.context.database.query(`
    SELECT doc
      FROM groups,
           json_each(json_extract(doc, '$.members'))
     WHERE json_extract(json_each.value, '$.who') = ?1 AND
           json_extract(json_each.value, '$.whoType') = ?2
    `, [member.who, member.whoType], row => results.push(row.doc))
    .return(results)
    .map(deserializeGroup);
};

GroupDatabase.prototype.getGroups = function (groupIds) {
    const results = [];
    return this.context.database.query(`
    SELECT doc
      FROM groups
     WHERE _id IN (SELECT value FROM json_each(?1))
    `, [JSON.stringify(groupIds)], row => results.push(row.doc))
    .return(results)
    .map(deserializeGroup);
};

GroupDatabase.prototype.getPermissions = function (whos, whoType, resource, type) {
    const results = [];
    return this.context.database.query(`
    SELECT doc
      FROM permissions
     WHERE json_extract(doc, '$.status') = 'ACTIVE' AND
           json_extract(doc, '$.type') = ?1 AND
           json_extract(doc, '$.resource') = ?2 AND
           json_extract(doc, '$.whoType') = ?3 AND
           json_extract(doc, '$.who') IN (SELECT value FROM json_each(?4))
    `, [type, resource, whoType, JSON.stringify(whos)], row => results.push(row.doc))
    .return(results)
    .map(deserializePermission);
};

GroupDatabase.prototype.getPermissionsByOwner = function (whos, whoType, type) {
    const results = [];
    return this.context.database.query(`
    SELECT doc
      FROM permissions
     WHERE json_extract(doc, '$.status') = 'ACTIVE' AND
           json_extract(doc, '$.whoType') = ?1 AND
           json_extract(doc, '$.who') IN (SELECT value FROM json_each(?2))
           ${type ? "AND json_extract(doc, '$.type') = ?3" : ''}
    `, [whoType, JSON.stringify(whos), type], row => results.push(row.doc))
    .return(results)
    .map(deserializePermission);
};

function serializeGroup(group) {
    group = _.cloneDeep(group);
    group.children.forEach(child => serializeDateRange(child.rights.range));
    return group;
}

function deserializeGroup(row) {
    row = JSON.parse(row);
    row.children.forEach(child => deserializeDateRange(child.rights.range));
    return row;
}

function deserializePermission(row) {
    const result = JSON.parse(row);
    deserializeDateRange(result.range);
    return result;
}

function serializeDateRange(range) {
    if (range) {
        if (range.to) {
            range.to = range.to.toISOString();
        }
        if (range.from) {
            range.from = range.from.toISOString();
        }
    }
}

function deserializeDateRange(range) {
    if (range) {
        if (range.to) {
            range.to = new Date(range.to);
        }
        if (range.from) {
            range.from = new Date(range.from);
        }
    }
}

module.exports = GroupDatabase;
