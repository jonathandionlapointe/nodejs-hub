'use strict';

const _ = require('lodash');
const config = require('./config');
const EventEmitter = require('events');
const Joi = require('joi');
const ObjectID = require('bson-objectid');
const Promise = require('bluebird');
const RouteRecognizer = require('route-recognizer');
const util = require('util');

function Events() {
    EventEmitter.call(this);
    this.setMaxListeners(1024); // TODO Tune this value.
    this.router = new RouteRecognizer();
    this.txCount = 0;
    this.maxTxCount = 5;
    this.backoffTimeFactor = 1; // Backoff tx*tx*factor seconds.
    this.started = false;
    this.notified = false;
    this.lastEmit = 0; // Date.now() (init to time zero).
    this.settleTime = 1000; // Events must stop for one second to 'settle'.
}
util.inherits(Events, EventEmitter);

Events.schema = Joi.object({
    _id: Joi.string().regex(/[0-9A-Fa-f]{24}/),
    type: Joi.string().valid(['create', 'read', 'update', 'delete']).required(),
    path: Joi.string().required(),
    resource: Joi.object(),
    timestamp: Joi.date().iso(),
    traceId: Joi.string(),
    request: Joi.string(),
    whoType: Joi.string(),
    who: Joi.string(),
});

Events.init = function (context) {
    return context.withTransactionContext(context =>
        Promise.mapSeries([`
        CREATE TABLE IF NOT EXISTS events (
            _id TEXT UNIQUE NOT NULL PRIMARY KEY,
            sent INTEGER NOT NULL,
            timestamp TEXT NOT NULL,
            doc TEXT NOT NULL
        ) WITHOUT ROWID
        `, `
        CREATE INDEX IF NOT EXISTS events_not_sent_idx
        ON events (_id) WHERE sent = 0
        `, `
        CREATE INDEX IF NOT EXISTS events_sent_idx
        ON events (timestamp) WHERE sent = 1
        `], sql => context.database.query(sql)));
};

Events.prototype.addResourceType = function (path, resourceType) {
    return this.router.add([
        { path: `${path}/:id`, handler: resourceType },
    ]);
};

Events.prototype.getEventResourceType = function (event) {
    const result = this.router.recognize(event.path);
    if (!result || result.length === 0) {
        return null;
    }
    return {
        type: result[0].handler,
        resource: result[0].params.id
    };
};

Events.prototype.init = function (context) {
    return Events.init(context)
    .then(() => setImmediate(run.bind(this, context.getServerContext())));
};

Events.prototype.stop = function () {
    this.started = false;
};

Events.prototype.notify = function (context, event) {
    this.emit('event', context.createEventContext(event), event);
    this.notified = true;

    if (this.started) {
        return;
    }
    this.started = true;

    setImmediate(run.bind(this, context.getServerContext()));
};

function run(context) {
    if (!this.started) {
        return;
    }

    this.notified = false;

    let queuedEvent = null;
    return context.database.query(`
    SELECT _id, doc
      FROM events
     WHERE sent = 0
     LIMIT 1
    `, [], row => queuedEvent = row)
    .catchReturn(null)
    .then(() => {
        if (!queuedEvent) {
            if (this.notified) {
                return setImmediate(run.bind(this, context));
            } else {
                this.started = false;
                return;
            }
        }

        const id = queuedEvent._id;
        const event = JSON.parse(queuedEvent.doc);

        setImmediate(send.bind(this, id, event, context));
    });
}

function send(id, event, context) {
    if (!this.started) {
        return;
    }

    return context.sdk.events.create(event).then(() => {
        this.emit('sent', event);
    }).catch(error => {
        // 401 means we haven't authenticated yet, we might need to
        // wait a bit longer for system login. Drop other 400-level
        // responses.
        if (error.statusCode < 500 && error.statusCode !== 401) {
            this.emit('drop', error, event);
        } else {
            throw error;
        }
    }).then(() => {
        return context.database.query(`
        UPDATE events
           SET sent = 1
         WHERE _id = ?1
        `, [id])
        .catch(error => {
            /* istanbul ignore next: pain to reproduce db failures */
            this.emit('error', error);
        });
    }).then(() => {
        this.txCount = 0;
        setImmediate(run.bind(this, context));
    }, error => {
        this.txCount = Math.min(this.maxTxCount, this.txCount);
        const backoff = this.txCount * this.txCount * this.backoffTimeFactor;
        this.emit('retry', { txCount: this.txCount, backoff });
        this.txCount++;
        return Promise.delay(backoff * 1000).then(() => setImmediate(send.bind(this, id, event, context)));
    });
}

Events.prototype.compact = function (context) {
    return context.database.query(`
    DELETE FROM events
     WHERE sent = 1 AND timestamp < COALESCE((
         SELECT timestamp FROM events
         WHERE sent = 1
         ORDER BY timestamp DESC
         LIMIT 1 OFFSET ?1
     ), ?2)
    `, [config.events.maxnum, new Date(Date.now() - config.events.lifetime * 60 * 1000).toISOString()]);
};

Events.prototype.onEvent = function (listener) {
    this.on('event', listener);
    this.emit('listening', /^.*$/);
};

Events.prototype.onEventPathMatch = function (pathRE, listener) {
    function actualListener(context, event) {
        var matches = event.path.match(pathRE);
        if (matches) {
            return listener(context, matches, event);
        }
    }
    this.on('event', actualListener);
    this.emit('listening', pathRE);
    return actualListener;
};

Events.prototype.removeEventListener = function (listener) {
    this.removeListener('event', listener);
};

Events.prototype.emitEvent = Promise.method(function (context, event) {
    _.defaults(event, {
        _id: ObjectID().toString(),
        timestamp: new Date().toISOString()
    });

    setIfNotNull(event, 'who', context.who);
    setIfNotNull(event, 'whoType', context.whoType);
    setIfNotNull(event, 'request', context.requestId);
    setIfNotNull(event, 'traceId', context.traceId);

    this.lastEmit = Date.now();

    if (!context.transaction) {
        if (context.server && context.server.log) context.server.log(['event', 'warn'], {
            message: 'Event emitted without a transaction',  event
        });
        this.notify(context, event);
    } else {
        context.txnEvents.push(event);
    }
    return context.database.query(`
    INSERT INTO events (_id, sent, timestamp, doc)
    VALUES (?1, ?2, ?3, ?4)
    `, [event._id, context.isSync, event.timestamp, JSON.stringify(event)])
    .return(event);

    function setIfNotNull(obj, k, v) {
        if (v !== null) {
            obj[k] = v;
        }
    }
});

Events.prototype.settle = function () {
    const settle = () => {
        const delta = Date.now() - this.lastEmit;
        if (delta < this.settleTime) {
            return Promise.delay(this.settleTime - delta).then(settle);
        } else {
            return Promise.resolve();
        }
    };
    return settle();
};

module.exports = Events;
