'use strict';

const _ = require('lodash');
const config = require('./config');
const EventEmitter = require('events');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const KeyPair = require('@2klic/hub-core').KeyPair;
const Promise = require('bluebird');
const util = require('util');

const readFileAsync = Promise.promisify(require('fs').readFile);
const writeFileAsync = Promise.promisify(require('fs').writeFile);

/**
 * @typedef Options
 * @type {object}
 * @property {boolean} noServer
 */

/**
 * @constructor
 * @param {Context} context
 * @param {Options} options
 */
function Provisioner(context, options) {
    EventEmitter.call(this);

    options = options || {};
    if (!options.noServer) {
        const server = context.server.select('api');
        server.route(ENDPOINTS);
        server.method('provisioner', () => this, { callback: false });
    }
}
util.inherits(Provisioner, EventEmitter);

Provisioner.prototype.systemInfo = Promise.method(function () {
    return { provisioner: true };
});

Provisioner.prototype.provision = function (context, options) {
    const settings = _.pick(jwt.decode(options.token), ['environment', 'logstash', 'api', 'test']);

    process.env.NODE_ENV = settings.environment;
    config.reload();

    return KeyPair.generate()
    .then(keys => {
        if (config.auth.keys.public.file && config.auth.keys.private.file) {
            return Promise.all([
                writeFileAsync(config.auth.keys.public.file, keys.publicKey, { encoding: 'ascii', mode: 0o644 }),
                writeFileAsync(config.auth.keys.private.file, keys.privateKey, { encoding: 'ascii', mode: 0o600 }),
            ]);
        }
    }).then(() => Promise.join(
        getConfigValue(config.remoteAccess.publicKey),
        getConfigValue(config.remoteAccess.localPassphrase),
        getConfigValue(config.auth.keys.public),
        (remotePublicKey, remoteLocalPassphrase, publicKey) => {

            this.emit('provisioning');

            context.sdk.base_url = context.sdk.http.url = settings.api;

            // FIXME use the SDK when the provisioning call is updated
            // to include the remoteAccess fields.  The old SDK call
            // was:
            //
            //     return this.sdk.devices.provision(config.default_name, macAddress, model._id);
            //
            // The logic of the provisioning call is inlined here,
            // with the remoteAccess fields added.
            return context.sdk.http.get(`/models/${options.model}`)
            .then(model => context.sdk.http.post('/provisioning', {
                name: config.default_name,
                model: model.data._id,
                publicKey: publicKey,
                lot: options.lot,
                branding: options.branding,
                defaultLanguage: options.defaultLanguage,
                remoteAccess: {
                    publicKey: remotePublicKey,
                    localPassphrase: remoteLocalPassphrase,
                },
            }, { token: options.token }).tap(result => result.model = model.data));
        }
    )).then(result => {
        settings.file = config.system.file;
        settings.provisioning = result.data.provisioning;
        settings.device = result.data.device;
        settings.lot = options.lot;
        settings.branding = options.branding;
        settings.defaultLanguage = options.defaultLanguage;
        settings.hostname = config.hostname
            .replace('{branding}', _.get(settings, 'branding', '2KLIC').toLowerCase())
            .replace('{model.productCode}', _.get(result.model, 'productCode', 'Dev').toLowerCase())
            .replace('{_id}', result.data.device._id);

        return writeFileAsync(config.system.file, JSON.stringify(settings), 'utf8')
        .then(() => {
            this.emit('provisioned', settings);
        });
    })
    .return(settings);
};

const GET_SYSTEM_INFO = {
    description: 'Get system info',
    tags: ['provisioner'],
    handler: function (request, reply) {
        const provisioner = request.server.methods.provisioner();
        return provisioner.systemInfo().asCallback(reply);
    }
};

const PROVISION = {
    description: 'Provision the controller',
    tags: ['provisioner'],
    validate: {
        payload: {
            token: Joi.string().required(),
            model: Joi.string().default(config.default_model),
            lot: Joi.string(),
            branding: Joi.string(),
            defaultLanguage: Joi.string(),
        }
    },
    handler: function (request, reply) {
        const provisioner = request.server.methods.provisioner();
        return provisioner.provision(request.context(), request.payload)
        .then(reply)
        .catch(error => {
            if (error.statusCode) {
                reply(error.body).code(error.statusCode);
            } else {
                reply(error);
            }
        });
    }
};

const FACTORY_RESET = {
    description: 'Stub',
    tags: ['provisioner'],
    handler: function (request, reply) {
        // If we are here, we are already in a factory default state, so do
        // nothing.
        reply();
    }
};

const ENDPOINTS = [
    { method: 'GET', path: '/system/info', config: GET_SYSTEM_INFO },
    { method: 'POST', path: '/provisioning', config: PROVISION },
    { method: 'POST', path: '/factory_reset', config: FACTORY_RESET },
];

/**
 * @param {string|{file:string}|{string:string}} config
 */
function getConfigValue(config) {
    if (typeof config === 'string') {
        config = { string: config };
    }
    if (config.string) {
        return Promise.resolve(config.string);
    } else if (config.file) {
        return readFileAsync(config.file).then(buf => buf.toString());
    } else {
        throw new TypeError('invalid config value (expected `string` or `file`)');
    }
}

module.exports = Provisioner;
