'use strict';

const _ = require('lodash');
const AsyncEventEmitter = require('./async-events');
const Boom = require('super-boom')();
const changelog = require('./changelog');
const config = require('./config');
const Promise = require('bluebird');
const diskinfo = Promise.promisifyAll(require('diskinfo'));
const fs = Promise.promisifyAll(require('fs'));
const Migration = require('../migration');
const os = require('os');
const path = require('path');
const permission = require('./permission');
const runCommand = require('./run-command');
const synchronizeData = require('./sync');
const util = require('util');
const version = require('../package.json').version;

const spawn = require('child_process').spawn;
const execAsync = Promise.promisify(require('child_process').exec);
const randomBytesAsync = Promise.promisify(require('crypto').randomBytes);

function System() {
    AsyncEventEmitter.call(this);

    // Scenarios use system events, so we can expect many
    // listeners. This number probably needs tuning.
    this.setMaxListeners(1024);

    this.device = null;
    this.group = null;
    this.shutdownPhases = [];
    this.debugCodes = {};

    this.addShutdownPhase('default');
}
util.inherits(System, AsyncEventEmitter);

System.views = {
    update(context) {
        return context.getAppContext().collection('system').create();
    }
};

System.prototype.addShutdownPhase = function (name, priority) {
    if (!Number.isSafeInteger(priority)) {
        priority = 100;
    }
    this.shutdownPhases.push({ name, priority });
    this.shutdownPhases.sort((a, b) => b.priority - a.priority);
};

System.prototype.health = function (server) {
    const ifaces = {};
    _.forEach(os.networkInterfaces(), (addrs, name) => {
        _.forEach(addrs, addr => {
            if (addr.internal) {
                return;
            }

            ifaces[name] = _.merge(
                ifaces[name] || {},
                {
                    [addr.family]: {
                        address: addr.address,
                        netmask: addr.netmask
                    },
                },
                _.omit(addr, ['address', 'family', 'internal', 'netmask'])
            );
        });
    });

    const services = _.get(server, 'plugins.rpc.services', []);

    return Promise.join(
        diskinfo.getDrivesAsync(),
        getInstalledVersions(server, services),
        getUpgradeUrgency(server, services),
        (drives, installedVersions, upgradeUrgency) => {
            const disks = {};
            _.forEach(drives, drive => {
                if (!drive.mounted.match(/^\/(dev|sys|run)/)) {
                    disks[drive.mounted] = _.omit(drive, ['mounted', 'filesystem']);
                }
            });

            return {
                version: version,
                installedVersion: _.get(installedVersions, '2klic-gateway', version), // TODO: remove deprecated
                installedServiceVersions: _.omit(installedVersions, '2klic-gateway'), // TODO: remove deprecated
                upgradeUrgency: upgradeUrgency, // TODO: remove deprecated
                nodeRelease: process.release.name,
                nodeVersion: process.version,
                services: _.map(services, x => _.omit(x, 'changelog')),
                cpus: os.cpus(),
                filesystem: disks,
                hostname: os.hostname(),
                loadavg: os.loadavg(),
                memory: {
                    total: os.totalmem(),
                    free: os.freemem(),
                },
                network: ifaces,
                serverLoad: server.load,
            };
        }
    );
};

const SERVICE_PACKAGE_NAMES = [
    '2klic-gateway',
    '2klic-screen',
    '2klic-service-camera',
    '2klic-service-network',
    '2klic-service-retrofit',
    '2klic-service-zwave',
];

function getInstalledVersions(server, services) {
    const results = {}; // TODO: remove deprecated
    return Promise.map(SERVICE_PACKAGE_NAMES, name =>
        runCommand('/usr/bin/dpkg-query', ['-W', '-f${Status}:${Version}', name], { splitLines: true })
        .catch(error => {
            server.log(['system', 'debug'], {
                message: 'Error checking installed package version',
                reason: error.message,
                code: error.code,
                stderr: error.stderr && error.stderr.toString()
            });
            return { stdout: null };
        }))
    .map(result => result.stdout)
    .map((lines, index) => {
        if (!lines) {
            return;
        }

        // Match the version field on fully installed packages.
        const matches = lines
            .map(line => line.match(/^install ok installed:(.*)$/))
            .filter(Boolean); // Look at only matched lines.
        if (matches.length === 0) {
            server.log(['system', 'warn'], SERVICE_PACKAGE_NAMES[index] +
                ' is installed, but no matching version line was returned by dpkg');
            return;
        }

        const installedVersion = matches[0][1]; // First group of first matched line.
        const name = SERVICE_PACKAGE_NAMES[index];
        let service = _.find(services, { name: `@2klic/${name}` });
        if (!service) {
            service = _.find(services, { name: name });
        }
        if (!service) {
            service = { name: `@2klic/${name}` };
            services.push(service);
        }
        if (name === '2klic-gateway') {
            service.version = version;
            service.changelog = config.changelog;
        }
        service.installedVersion = installedVersion;

        results[name] = installedVersion;
    }).return(results);
}

function getUpgradeUrgency(server, services) {
    const results = {}; // TODO: remove deprecated
    return Promise.map(SERVICE_PACKAGE_NAMES, name => {
        let service = _.find(services, { name: `@2klic/${name}` });
        if (!service) {
            service = _.find(services, { name: name });
        }
        if (!service) {
            if (name === '2klic-gateway') {
                service = { name: '@2klic/2klic-gateway', version, changelog: config.changelog };
                services.push(service);
            } else {
                return null;
            }
        }
        let filename = service.changelog;
        if (!filename) {
            filename = `/usr/share/doc/${name}/changelog.Debian.gz`;
        }
        server.log(['system', 'debug'], `Looking at changelog ${filename}`);
        return Promise.try(() => changelog.parse(filename, {
            version: { min: service.version },
            omitMerges: true,
            omitVersionBumps: true,
        })).catch(err => {
            server.log(['system', 'info'], `No changelog found for service ${name}`);
            return null;
        });
    })
    .map(logs => logs && _.reduce(logs, (result, entry) => {
        return _.reduce(entry.log, (result, line) => {
            const urgencyMatch = line.match(/^\[urgency=(\S+)\]/);
            if (urgencyMatch) {
                return maxUrgency(result, urgencyMatch[1].toLowerCase());
            }
            return result;
        }, maxUrgency(result, entry.urgency));
    }, null))
    .map((urgency, index) => {
        if (rankUrgency(urgency) >= 0) {
            const name = SERVICE_PACKAGE_NAMES[index];
            const service = _.find(services, { name: `@2klic/${name}` });
            if (service) {
                service.upgradeUrgency = urgency;
            }
            results[name] = urgency;
        }
    })
    .return(results);
}

function maxUrgency(result, value) {
    if (compareUrgency(result, value) > 0) {
        return value;
    }
    return result;
}

function compareUrgency(a, b) {
    a = rankUrgency(a);
    b = rankUrgency(b);
    if (a >= 0) {
        if (b >= 0) {
            return a - b;
        } else {
            return -1;
        }
    } else if (b >= 0) {
        return 1;
    }
}

function rankUrgency(urgency) {
    switch (urgency) {
    case 'low': return 0;
    case 'medium': return 50;
    case 'high': return 70;
    case 'critical': return 99;
    default: return -1;
    }
}

System.prototype.heartbeat = function (context, options) {
    options = options || {};
    const systemId = this.device._id;
    const start = Date.now();
    return this.health(context.server).tap(health => {
        const end = Date.now();
        health.heartbeatMillis = end - start;
        health.timestamp = new Date().toISOString();
        if (!options.suppressForward) {
            return context.sdk.http.put(`/devices/${systemId}/heartbeat`, health, {
                token: context.sdk.auth.token
            }).then(response => {
                if (response && response.statusCode === 200 && response.data.config) {
                    const config = _.merge(this.device.config, response.data.config);
                    context.server.log(['heartbeat', 'debug'], 'Boomp');
                    return this.update(context, { config });
                } else {
                    context.server.log(['heartbeat', 'warn'], {
                        message: `API responded to heartbeat with ${response.statusCode}`,
                        response: response,
                    });
                }
            }).catch(error => {
                context.server.log(['heartbeat', 'info'], {
                    message: 'Unable to send heartbeat',
                    reason: error,
                });
            });
        }
    }).tap(() => {
        this.emit('heartbeat');
        if (!options.suppressSync) {
            return this.sync(context)
            .catch(err => {
                context.server.log(['sync', 'info'], {
                    message: 'Sync failure',
                    reason: err.stack,
                });
            })
            .catchReturn();
        }
    });
};

System.prototype.sync = function (context) {
    return synchronizeData(context);
};

System.prototype.shutdown = function (context, reason) {
    reason = reason || 'normal';
    return Promise.try(() => {
        if (reason === 'factoryReset') {
            if (!_.get(this.options, 'factoryReset.enable')) {
                return Promise.reject(new Error('factory reset disabled'));
            }
            return context.emitEvent({
                type: 'create',
                path: '/factory_reset',
                resource: {},
            });
        }
    }).then(() => this.emitAsync('shutdownRequested', context.createShutdownContext('requested'), reason)).then(() => {
        return Promise.each(this.shutdownPhases, phase => {
            return this.emitAllAsync(`shutdown.${phase.name}`, context.createShutdownContext(phase.name), reason).catch(error => {
                // Ignored.
            });
        });
    }).then(() => {
        // Wait for events emitted by shutdown tasks to settle.
        return context.events.settle().timeout(10000).catch(error => {
            // Ignore.
        });
    }).then(() => {
        return this.emitAsync('shutdown', context.createShutdownContext('shutdown'), reason);
    });
};

System.prototype.provision = function (context) {
    if (!context.sdk) {
        throw new Error('no sdk');
    }

    const coll = context.collection('system');

    return coll.get('device').then(device => {
        if (!device && config.system.device) {
            // Either this is the first run after provisioning, or the database
            // must have been reset after initial provisioning. Recover the
            // saved, provisioned device and continue.
            device = { _id: 'device', device: config.system.device };
            return coll.insert(device)
            .then(() => Migration.setAsCurrent(context))
            .return(device);
        }
        if (device) {
            return Migration.migrateToCurrent(context)
            .return(device);
        }
    }).then(device => {
        if (device) {
            this.device = device.device;
            return;
        }

        throw new Error('not provisioned');
    }).then(() => {
        return coll.get('group');
    }).then(group => {
        if (group) {
            this.group = group.groupId;
            return;
        }

        return Promise.try(() => {
            if (config.system.provisioning.group) {
                return config.system.provisioning.group;
            }
            const id = config.system.provisioning._id;
            return context.sdk.http.get(`/provisioning/${id}`, { token: this.device.token })
            .then(response => response.data.group);
        }).then(groupId => {
            this.group = groupId;
            return permission.upsertGroups(context, [{
                _id: groupId,
                members: [{ whoType: 'gateway', who: this.device._id }],
                children: [],
            }])
            .then(() => coll.insert({ _id: 'group', groupId: groupId }));
        });
    }).then(() => {
        context.sdk.auth.token = this.device.token;
        return { device: this.device };
    });
};

System.prototype.updateToken = function (context, token) {
    context.sdk.auth.token = this.device.token = token;
    return context.collection('system').update({ device: this.device }, 'device')
    .then(() => {
        this.emit('config', this.device.config);
    });
};

System.prototype.update = function (context, device) {
    if (!this.device) {
        throw new Error('not provisioned');
    }

    if (!context.events) {
        throw new Error('no events instance');
    }

    device = _.omit(device, ['_id', '_rev', 'createdTs', 'mac', 'token', 'publicKey', 'remoteAccess']);

    if (_.isEqual(_.pick(this.device, Object.keys(device)), device)) {
        return Promise.resolve();
    }

    const oldConfig = _.merge({}, this.device.config);
    const newConfig = _.merge({}, this.device.config, _.get(device, 'config', {}));

    _.merge(this.device, device);
    return context.collection('system').update({ device: this.device }, 'device').then(() => {
        return context.emitEvent({
            type: 'update',
            path: `/devices/${this.device._id}`,
            resource: this.toJSON(),
        });
    }).then(() => {
        if (!_.isEqual(oldConfig, newConfig)) {
            this.emit('config', newConfig);
        }
    });
};

System.prototype.ping = function (context, latency) {
    return context.sdk.http.post(`/devices/${this.device._id}/ping`, { latency }, {
        token: context.sdk.auth.token
    }).then(result => {
        return result.data.frequency || 15;
    });
};

System.prototype.toJSON = function () {
    return _.omit(this.device, ['token', '__v', '_rev', 'config', 'publicKey', 'remoteAccess']);
};

System.prototype.handleCloudMessage = Promise.method(function (context, message) {
    this.emit('request', message);

    const methods = {
        create: 'POST',
        read: 'GET',
        update: 'PUT',
        delete: 'DELETE',
    };

    const method = methods[message.type];
    if (!method) {
        return;
    }

    // The message may be mutated. Create a copy that we can edit without those
    // changes being seen by the caller.
    message = _.merge({}, message);

    // For POST requests, use the list endpoint, not the single
    // resource endpoint. Remove the object ID (if present) that ends
    // the message path.
    const path = method === 'POST' ?
          message.path.replace(/\/[a-zA-Z0-9_]{24}$/, '') :
          message.path;

    return this.handleRequest(context, {
        message: message,
        method: method,
        path: path,
        resource: message.resource,
        requestId: message.request,
        traceId: message.traceId,
        who: message.who,
        whoType: message.whoType,
    });
});

System.prototype.handleRequest = function (context, options) {
    return new Promise(resolve => context.server.select('api').inject({
        method: options.method,
        url: `http://127.0.0.1:${config.server.port}${options.path}`,
        payload: options.resource,
        headers: {
            'x-request-id': options.requestId,
            'x-trace-id': options.traceId,
            'x-who': options.who,
            'x-who-type': options.whoType,
            'authorization': 'Bearer ' + _.get(context.server.plugins, 'auth.apiToken'),
        }
    }, resolve)).then(response => {
        let message = options.message;

        this.emit('response', response, message);

        if (response.statusCode === 307 || response.statusCode === 308) {
            options.path = response.headers.location;
            return this.handleRequest(context, options);
        }

        if (!options.requestId) {
            return;
        }

        message.resource = response.result || {};
        message.timestamp = new Date().toISOString();
        message.statusCode = response.statusCode;

        if (response.statusCode < 300) {
            if (options.method !== 'GET') {
                /* TODO we are planning to eventually use the events
                 * generated by create/update/delete messages (on
                 * success) to manage request success responses. When
                 * that day comes, the following commented out line of
                 * code should be enabled.
                 */
                //message = null;
            }
        } else if (response.statusCode === 304) {
            message.resource = 'No change';
        } else if (response.statusCode >= 400) {
            let error = response.result || {};
            if (typeof error === 'string') {
                error = { error: error };
            }
            if (!error.statusCode) {
                error.statusCode = response.statusCode;
            }
            message = _.omit(message, ['resource']);
            message.error = error;
        }

        if (message) {
            this.emit('message', message);
        }
    });
};

System.prototype.debugLink = Promise.method(function () {
    this.pruneDebugCodes();

    return randomBytesAsync(48)
    .then(bytes => bytes.toString('base64'))
    .then(code => {
        const expiry = new Date();
        expiry.setMinutes(expiry.getMinutes() + 5);

        this.debugCodes[code] = expiry;

        const address = getIPv4LanAddress(config.auth.netInterface);
        const url = `https://${address}${config.auth.prefix}/devices/${this.device._id}/debug/${encodeURIComponent(code)}`;

        return { url };
    });
});

function getIPv4LanAddress(iface) {
    return _.result(_.find(os.networkInterfaces()[iface], { family: 'IPv4' }), 'address');
}

System.prototype.debugData = Promise.method(function (context, code) {
    this.pruneDebugCodes();

    if (!this.debugCodes[code]) {
        throw Boom.notFound();
    }

    delete this.debugCodes[code];

    return fs.mkdtempAsync(`${os.tmpdir()}${path.sep}2klic-gateway-`)
    .then(tmpdir => {
        return Promise.join(
            this.health(context.server).then(health => fs.writeFileAsync(path.join(tmpdir, 'heartbeat.json'), JSON.stringify(health))),
            execAsync(`cp /var/log/2klic-*.log ${tmpdir}`).reflect(),
            fs.writeFileAsync(path.join(tmpdir, 'system.json'), JSON.stringify(safeSystemData())),
            () => {
                const proc = spawn('tar', ['cJ', '.'], { cwd: tmpdir });
                proc.on('close', () => {
                    execAsync(`rm -rf ${tmpdir}`).catchReturn();
                });
                return proc.stdout;
            }
        );
    });
});

function safeSystemData() {
    const data = config.system;
    return {
        environment: data.environment,
        logstash: data.logstash,
        api: data.api,
        test: data.test,
        file: data.file,
        provisioning: {
            _id: data.provisioning._id,
            mac: data.provisioning.mac,
            group: data.provisioning.group,
        },
        device: {
            _id: data.device._id,
            model: data.device.model,
            config: {
                channel: data.device.config.channel,
            },
            lastPing: data.device.lastPing,
            createdTs: data.device.createdTs,
            mode: data.device.mode,
            name: data.device.name,
        },
        hostname: data.hostname,
    };
}

System.prototype.pruneDebugCodes = function () {
    const now = new Date().toISOString();
    _.forEach(this.debugCodes, (expiry, k) => {
        if (expiry < now) {
            delete this.debugCodes[k];
        }
    });
};

module.exports = System;
