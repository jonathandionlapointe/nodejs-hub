/*eslint no-console: ["off"]*/
'use strict';

const _ = require('lodash');
const ArgumentParser = require('argparse').ArgumentParser;
const yamlConfig = require('node-yaml-config');
const fs = require('fs');
const os = require('os');
const path = require('path');
const pkg = require('../package.json');

const settings = [];
const config = module.exports = {};

function reload() {
    for (let key in config) delete config[key];
    settings.splice(0, settings.length);
    yamlConfig.reload('config.yaml');

    _.merge(config, yamlConfig.load('config.yaml'));

    config.hw = {};
    config.print = print;
    config.reload = reload;
    config.fixPorts = function (server) {
        if (config.server.port === 0 || config.server.port === '0') {
            const s = server.select('api');
            if (s && s.listener) {
                process.env.HUB_SERVER_PORT = config.server.port = s.listener.address().port;
            }
        }

        if (config.server.internalPort === 0 || config.server.internalPort === '0') {
            const s = server.select('internal');
            if (s && s.listener) {
                process.env.HUB_SERVER_INTERNAL_PORT = config.server.internalPort = s.listener.address().port;
            }
        }
    };

    const parser = new ArgumentParser({
        version: `${pkg.version} (${process.release.name} ${process.version})`,
        addHelp: true,
        description: pkg.description,
        usage: 'smart-controller [options]'
    });

    function option(path, description, options) {
        options = options || {};
        options.path = path;
        options.name = path
            .replace(/\.|_/g, '-')
            .replace(/([a-z])([A-Z])/g, (match, p1, p2) => p1 + '-' + p2.toLowerCase())
            .replace(/\[(.*?)\]/, '-$1');
        options.description = description;
        if (!options.noEnv) {
            options.env = options.env || ('HUB_' + options.name.replace(/-/g, '_').toUpperCase());
        }
        if (!options.noArg) {
            if (options.type === 'boolean') {
                parser.addArgument('--' + options.name, { help: description, action: 'storeTrue', defaultValue: _.get(config, path) });
                parser.addArgument('--no-' + options.name, { help: description, action: 'storeFalse', defaultValue: _.get(config, path) });
            } else if (options.type === 'array') {
                options.name = options.name.replace(/s$/, '');
                parser.addArgument('--' + options.name, { help: description, action: 'append' });
            } else {
                parser.addArgument('--' + options.name, { help: description, choices: options.choices });
            }
        }
        settings.push(options);
    }

    config.node_env = 'default';

    if (!config.mac) {
        let macAddress = null;
        _.forEach(os.networkInterfaces(), addrs => {
            if (macAddress) return;
            _.forEach(addrs, addr => {
                if (macAddress) return;
                if (addr.family === 'IPv4' && !addr.internal) {
                    macAddress = addr.mac;
                }
            });
        });
        config.mac = process.env.MAC || macAddress;
    }

    parser.addArgument('--print-env', {
        help: 'Just print the environment and exit',
        action: 'storeTrue',
    });

    option('node_env', 'Environment', { env: 'NODE_ENV', noArg: true });
    option('server.host', 'Server bind address');
    option('server.port', 'Server bind port');
    option('server.internalPort', 'Internal request port');
    option('default_name', 'Default device name on provisioning');
    option('default_model', 'Default model ID for provisioning');
    option('hostname', 'Hostname format');
    option('changelog', 'Changelog file');
    option('system.provisioning.mac', 'MAC address');
    option('system.api', 'Cloud API server URI');
    option('system.logstash', 'Logstash host:port');
    option('system.environment', 'API environment');
    option('system.file', 'Provisioning settings file');
    option('system.test', 'Whether test features are enabled (emulator, factory reset)');
    option('system.device._id', 'Controller ID', { noArg: true, noEnv: true });
    option('db.url', 'Database URL');
    option('db.journalMode', 'SQLite journal mode', { choices: ['DELETE', 'TRUNCATE', 'PERSIST', 'MEMORY', 'WAL', 'OFF']});
    option('db.pool.min', 'Database pool connection minimum', { type: 'int' });
    option('db.pool.max', 'Database pool connection maximum', { type: 'int' });
    option('events.lifetime', 'Event lifetime in minutes', { type: 'int' });
    option('events.maxnum', 'Maximum number of sent events to keep', { type: 'int' });
    option('rpc.shutdownTimeout', 'Shutdown prevention timeout for RPC clients', { type: 'int' });
    option('websocket.port', 'WebSocket bind port');
    option('auth.secret', 'Authentication token signing secret');
    option('auth.keys.public', 'Authentication public key');
    option('auth.keys.public.file', 'Authentication public key');
    option('auth.keys.private', 'Authentication private key');
    option('auth.keys.private.file', 'Authentication private key');
    option('auth.prefix', 'Path prefix of the TLS-proxied address');
    option('auth.cert', 'Path to the TLS proxy certificate');
    option('auth.netInterface', 'Network interface for LAN IP address');
    option('remoteAccess.publicKey', 'Remote access public key');
    option('remoteAccess.publicKey.file', 'Remote access public key file');
    option('remoteAccess.localPassphrase', 'Remote access passphrase');
    option('remoteAccess.localPassphrase.file', 'Remote access passphrase file');
    option('scenario.cooldown.minimum', 'Minimum scenario cooldown time in seconds', { type: 'int' });
    option('scenario.cooldown.maximum', 'Maximum scenario cooldown time in seconds', { type: 'int' });
    option('scenario.cooldown.default', 'Default scenario cooldown time in seconds', { type: 'int' });
    option('logger.joinTags', 'Characters if any to use to join log tags');
    option('logger.file.path', 'File to send log output to');
    option('logger.file.level', 'Log level for file output', { choices: ['trace', 'debug', 'info', 'warn', 'error', 'fatal']});
    option('logger.stdout.level', 'Log level for stdout', { choices: ['trace', 'debug', 'info', 'warn', 'error', 'fatal']});
    option('logger.stderr.level', 'Log level for stderr', { choices: ['trace', 'debug', 'info', 'warn', 'error', 'fatal']});
    option('logger.logstash.enable', 'Enable the logstash logger', { type: 'boolean' });
    option('logger.logstash.level', 'Log level for Logstash', { choices: ['trace', 'debug', 'info', 'warn', 'error', 'fatal']});
    option('hw.intercom', 'Intercom is enabled', { type: 'boolean', noArg: true, noEnv: true });
    option('hw.intercom.gpio.button.id', 'Intercom button GPIO', { type: 'int' });
    option('hw.intercom.gpio.button.pressed', 'Intercom button pressed value (0 or 1)', { type: 'int' });
    option('hw.intercom.gpio.light.id', 'Intercom light GPIO', { type: 'int' });

    function checkType(value, options) {
        if (options.type === 'int') {
            const n = Number.parseInt(value);
            if (Number.isNaN(n)) {
                console.log(`${options.path} must be an integer: ${value}`);
                process.exit(1);
            }
            return n;
        } else if (options.type === 'boolean') {
            if (typeof value === 'string') {
                value = value.match(/^t(rue|)$|^y(es|)$/);
            }
            return Boolean(value);
        } else if (options.choices && !_.includes(options.choices, value)) {
            console.log(`${options.path} must be one of [${options.choices.join(', ')}]: ${value}`);
            process.exit(1);
        }
        return value;
    }

    let args = null;
    if (!process.env.HUB_NO_ARGPARSE) {
        args = parser.parseArgs();
        _.forEach(args, (value, name) => {
            if (value === null) {
                return;
            }
            name = name.replace(/_/g, '-').replace(/^no-/, '');
            const options = _.find(settings, { name });
            if (!options) {
                return;
            }
            value = checkType(value, options);
            _.merge(config, _.set({}, options.path, value));
        });
    }

    _.forEach(settings, options => {
        if (!options.env) {
            return;
        }
        let value = process.env[options.env];
        if (typeof value === 'undefined') {
            return;
        }
        value = checkType(value, options);
        if (options.type === 'array') {
            value = value.split(',');
        }
        _.merge(config, _.set({}, options.path, value));
    });

    if (args && args.print_env) {
        _.forEach(settings, options => {
            const value = _.get(config, options.path);
            if (Array.isArray(value)) {
                console.log(options.env + '=' + value.join(','));
                return;
            }
            if (typeof value === 'undefined' || typeof value === 'object' || options.noEnv) {
                return;
            }
            console.log(options.env + '=' + value);
        });
        process.exit(0);
    }

    const file = config.system.file;
    try {
        config.system = JSON.parse(fs.readFileSync(file, 'utf8'));
    } catch (error) {
        config.system = { environment: process.env.NODE_ENV };
    }
    config.system.file = file;

    config.system.resetFile = path.join(
        path.dirname(config.system.file),
        path.basename(config.system.file, '.json') + '.factory-reset');
}

reload();

function print() {
    const table = {};
    _.forEach(settings, options => {
        let value = _.get(module.exports, options.path);
        if (Array.isArray(value)) {
            value = value.join(',');
        } else if (typeof value === 'undefined' || typeof value === 'object') {
            return;
        }
        if (options.type === 'boolean') {
            value = Boolean(value);
        }
        table[options.description] = value;
    });

    printTable(table);
}

function printTable(table, options) {
    options = _.merge({
        print: console.log,
        appendNewline: false,
    }, options);
    const width = _.max(_.map(_.keys(table), k => k.length)) + 1;
    _.forEach(table, (value, key) => {
        let line = key;
        if (typeof value !== 'string') {
            value = JSON.stringify(value);
        }
        if (value.length > 0) {
            line += ':' + ' '.repeat(width - key.length) + value;
        }
        if (options.appendNewline) {
            line += os.EOL;
        }
        options.print(line);
    });
}
