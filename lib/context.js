'use strict';

const _ = require('lodash');
const config = require('./config');
const later = require('later');
const Database = require('./sqldb');
const Promise = require('bluebird');

const nullLogger = {
    trace: _.noop,
    debug: _.noop,
    info: _.noop,
    warn: _.noop,
    error: _.noop,
    fatal: _.noop,
    child() { return nullLogger; }
};

/**
 * @class
 */
function Context() {}

Object.defineProperties(Context.prototype, {
    app:         { configurable: false, enumerable: false, value: null },
    log:         { configurable: false, enumerable: false, value: nullLogger },
    server:      { configurable: false, enumerable: false, value: null },
    request:     { configurable: false, enumerable: false, value: null },
    events:      { configurable: false, enumerable: false, value: null },
    sdk:         { configurable: false, enumerable: false, value: null },
    _collection: { configurable: false, enumerable: false, value: {} },
    Database:    { configurable: false, enumerable: false, value: Database },
    database:    { configurable: false, enumerable: false, value: new Database(config.db.url, config.db.pool) },
    transaction: { configurable: false, enumerable: true,  value: false },
    requestId:   { configurable: false, enumerable: true,  value: null },
    traceId:     { configurable: false, enumerable: true,  value: null },
    gatewayId:   { configurable: false, enumerable: true,  value: null },
    systemGroup: { configurable: false, enumerable: true,  value: null },
    plugin:      { configurable: false, enumerable: true,  value: null },
    who:         { configurable: false, enumerable: true,  value: null },
    whoType:     { configurable: false, enumerable: true,  value: null },
    scope:       { configurable: false, enumerable: false, value: null },
    isSync:      { configurable: false, enumerable: true,  value: false },
});

Context.prototype.collection = function (name) {
    let collection = this._collection[name];
    if (!collection) {
        if (!this.hasOwnProperty('_collection')) {
            Object.defineProperty(this, '_collection', { configurable: false, enumerable: false, value: Object.create(this._collection) });
        }
        Object.defineProperty(this._collection, name, { configurable: false, enumerable: true, value: this.database.collection(name) });
        collection = this._collection[name];
    }
    return collection;
};

Context.prototype.withTransactionContext = function (txnPromiser) {
    const txnEvents = this.transaction ? this.txnEvents : [];

    return this.database.transaction(txn => {
        const txnContext = createContext(this, {
            transaction: { configurable: false, enumerable: true, value: true },
            database:    { configurable: false, enumerable: false, value: txn },
            _collection: { configurable: false, enumerable: false, value: {} },
            txnEvents:   { configurable: false, enumerable: false, value: txnEvents },
        });
        return txnPromiser(txnContext);
    }).tap(() => {
        if (!this.transaction) {
            return Promise.mapSeries(txnEvents, event => this.events.notify(this, event));
        }
    });
};

Context.prototype.setTimeout = function (who, callback, timeout) {
    const args = Array.prototype.slice.call(arguments, 1);
    args.splice(2, 0, createTimerContext(this, who, 'timeout'));
    return setTimeout.apply(null, args);
};

Context.prototype.setInterval = function (who, callback, ms) {
    const args = Array.prototype.slice.call(arguments, 1);
    args.splice(2, 0, createTimerContext(this, who, 'interval'));
    return setInterval.apply(null, args);
};

Context.prototype.setSchedule = function (who, callback, schedule) {
    const args = Array.prototype.slice.call(arguments, 3);
    args.splice(0, 0, null, createTimerContext(this, who, 'schedule'));
    return later.setInterval(callback.bind.apply(callback, args), schedule);
};

Context.prototype.createServerContext = function (server) {
    if (server.realm.settings.bind) {
        return server.realm.settings.bind;
    }

    const context = createContext(this, {
        server:      { configurable: false, enumerable: false, value: server },
        sdk:         { configurable: false, enumerable: false, get() { return this.server.plugins['sdk']; } },
        events:      { configurable: false, enumerable: false, get() { return this.server.plugins['event']; } },
        systemGroup: { configurable: false, enumerable: true,  get() {
            const system = this.server.plugins['system'];
            if (system) {
                return system.group;
            } else {
                return _.get(config, 'system.provisioning.group');
            }
        } },
    });

    server.bind(context);

    server.ext('onRequest', function (request, reply) {
        createRequestContext.call(this, request);
        return reply.continue();
    });

    server.decorate('server', 'context', function () {
        const context = this.realm.settings.bind || this.root.realm.settings.bind;

        if (this.realm.plugin && this.realm.plugin !== context.plugin) {
            const pluginContext = createContext(context, {
                server: { configurable: false, enumerable: false, value: server },
                plugin: { configurable: false, enumerable: true, value: server.realm.plugin },
            });

            this.bind(pluginContext);

            return pluginContext;
        }

        return context;
    });

    server.decorate('request', 'context', function () {
        return this.app.context;
    });

    return context;
};

Context.prototype.createRemoteCallContext = function (params) {
    return createContext(this, {
        requestId: { configurable: false, enumerable: true, get() { return _.get(params, 'requestId', null); } },
        traceId:   { configurable: false, enumerable: true, get() { return _.get(params, 'traceId', null); } },
        who:       { configurable: false, enumerable: true, get() { return _.get(params, 'who', null); } },
        whoType:   { configurable: false, enumerable: true, get() { return _.get(params, 'whoType', null); } },
    });
};

Context.prototype.createDeviceContext = function (device) {
    return createContext(this, {
        who:     { configurable: false, enumerable: true,  value: device._id },
        whoType: { configurable: false, enumerable: true,  value: 'device' },
    });
};

Context.prototype.createClientContext = function (client) {
    return createContext(this, {
        client:  { configurable: false, enumerable: false, value: client },
        who:     { configurable: false, enumerable: true, get() { return _.get(this.client, 'credentials.who', null); } },
        whoType: { configurable: false, enumerable: true, get() { return _.get(this.client, 'credentials.whoType', null); } },
    });
};

Context.prototype.createEventContext = function (event) {
    return createContext(this, {
        who:     { configurable: false, enumerable: true, value: event._id },
        whoType: { configurable: false, enumerable: true, value: 'event' },
    });
};

Context.prototype.createStartupContext = function (who) {
    return createContext(this, {
        who:     { configurable: false, enumerable: true, value: who },
        whoType: { configurable: false, enumerable: true, value: 'startup' },
    });
};

Context.prototype.createShutdownContext = function (who) {
    return createContext(this, {
        who:     { configurable: false, enumerable: true, value: who },
        whoType: { configurable: false, enumerable: true, value: 'shutdown' },
    });
};

Context.prototype.createSynchronizationContext = function () {
    return createContext(this, {
        isSync:  { configurable: false, enumerable: true, value: true },
    });
};

Context.prototype.getAppContext = function () {
    for (let context = this;;) {
        const parent = Object.getPrototypeOf(context);
        if (!parent || parent === Context.prototype) {
            return context;
        }
        context = parent;
    }
};

Context.prototype.serviceContext = Context.prototype.getAppContext;

Context.prototype.getServerContext = function () {
    if (!this.server) {
        return null;
    }
    return this.getServerOrAppContext();
};

Context.prototype.getServerOrAppContext = function () {
    for (let context = this;;) {
        const parent = Object.getPrototypeOf(context);
        if (!parent || !parent.server) {
            return context;
        }
        context = parent;
    }
};

Context.prototype.emitEvent = function (event) {
    return this.events.emitEvent(this, event);
};

Object.freeze(Context.prototype);

/**
 * @typedef App
 * @type {Object}
 */

/**
 * Create the application context.
 *
 * TODO add intef
 * @param {App} app
 * @param {?bunyan.Logger} log
 * @returns {Context}
 */
exports.createAppContext = function (app, log) {
    return createContext(Context.prototype, {
        app:         { configurable: false, enumerable: false, value: app },
        log:         { configurable: false, enumerable: false, value: log },
        _collection: { configurable: false, enumerable: false, value: {} },
        gatewayId:   { configurable: false, enumerable: true,  get() { return config.system.device._id; } },
        systemGroup: { configurable: false, enumerable: true,  get() { return config.system.provisioning.group; } },
    });
};

function createRequestContext(request) {
    const context = createContext(this, {
        request:   { configurable: false, enumerable: false, value: request },
        server:    { configurable: false, enumerable: false, value: request.server },
        requestId: { configurable: false, enumerable: true,  value: request.headers['x-request-id'] },
        traceId:   { configurable: false, enumerable: true,  value: request.headers['x-trace-id'] },
        who:       { configurable: false, enumerable: true,  get() { return _.get(this.request, 'auth.credentials.who', null); } },
        whoType:   { configurable: false, enumerable: true,  get() { return _.get(this.request, 'auth.credentials.whoType', null); } },
        scope:     { configurable: false, enumerable: false, get() { return _.get(this.request, 'auth.credentials.scope', []); } },
    });

    request.app = request.app || {};
    request.app.context = context;

    return context;
}

function createTimerContext(context, who, whoType) {
    const rootContext = context.getServerOrAppContext();
    return createContext(rootContext, {
        who:     { configurable: false, enumerable: true, value: who },
        whoType: { configurable: false, enumerable: true, value: whoType },
    });
}

function createContext(prototype, properties) {
    const ctx = Object.create(prototype, properties);

    if (!ctx.hasOwnProperty('log') && prototype.log && _.some(properties, { enumerable: true })) {
        Object.defineProperty(ctx, 'log', {
            configurable: false, enumerable: false, value: prototype.log.child(ctx)
        });
    }

    // TODO it would be nice to seal or freeze the context here, but the
    // database collection getter currently only overrides the collections
    // property when a previously unaccessed database is accessed.
    return ctx;
}
