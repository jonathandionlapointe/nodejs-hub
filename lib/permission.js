'use strict';

const _ = require('lodash');
const Boom = require('boom');
const GroupDatabase = require('./group-database');
const GroupPermission = require('@2klic/groups_permissions');
const Promise = require('bluebird');
const RouteRecognizer = require('route-recognizer');

let groups = null;
let groupDb = null;

exports.views = {
    update(context) {
        context = context.getAppContext();
        return context.collection('permissions').createWithIndices([{
            name: 'permissions_who_idx',
            fields: ['who', 'whoType'],
            condition: { field: 'status', value: 'ACTIVE' },
        }, {
            name: 'permissions_resource_idx',
            fields: ['resource', 'type'],
            condition: { field: 'status', value: 'ACTIVE' },
        }])
        .then(() => {
            return GroupDatabase.initialize(context);
        }).then(() => {
            groupDb = new GroupDatabase(context);
            groups = new GroupPermission(groupDb);
        });
    }
};

let router = new RouteRecognizer();

exports.READ = 'read';
exports.WRITE = 'write';
exports.MANAGE = 'manage';
exports.SHARE = 'share';

exports.reset = function () {
    router = new RouteRecognizer();
};

exports.resourcePath = function (path, options) {
    if (typeof options === 'string') {
        options = { type: options };
    } else {
        options = options ? _.cloneDeep(options) : {};
    }
    if (typeof options.type !== 'string') {
        throw new Error('permission.resourcePath requires a type');
    }
    _.defaults(options, {
        param: 'id',
    });
    if (!path.match(new RegExp(`:${options.param}($|/)`))) {
        throw new Error(`path '${path}' does not contain param '${options.param}'`);
    }
    router.add([{ path, handler: options }]);
};

exports.pathPermission = function (path, access) {
    if (!access) {
        access = ['read'];
    } else if (typeof access === 'string') {
        access = [access];
    }

    if (path === '/factory_reset') {
        return null;
    }

    const route = _.first(router.recognize(path));
    const permission = {
        type: route ? route.handler.type : 'unknown',
        resource: route ? route.params[route.handler.param] : 'unknown',
    };
    access.forEach(type => permission[type] = true);
    return permission;
};

exports.find = function (context, permission) {
    return Promise.try(() => groups.getRightsForMember(permission, permission.type, permission.resource))
    .catch(error => {
        if (error.code === 'FORBIDDEN') {
            throw Boom.notFound();
        }
        throw error;
    });
};

exports.isValid = function (context, permission) {
    return exports.find(context, permission)
    .then(rights => {
        if (_.has(permission, 'read') && permission.read && !rights.read) return false;
        if (_.has(permission, 'write') && permission.write && !rights.write) return false;
        if (_.has(permission, 'share') && permission.share && !rights.share) return false;
        if (_.has(permission, 'manage') && permission.manage && !rights.manage) return false;
        return true;
    });
};

exports.findForType = function (context, credentials, type) {
    return Promise.try(() => groups.getMemberPermissions(credentials, type));
};

exports.get = function (context, id) {
    return context.collection('permissions').get(id);
};

exports.upsert = function (context, permission) {
    permission = serializePermission(context, permission);

    return context.collection('permissions')
    .upsert(permission)
    .return(permission);
};

exports.upsertMany = function (context, permissions) {
    permissions = permissions.map(serializePermission.bind(null, context));

    return context.collection('permissions')
    .upsertMany(permissions)
    .return(permissions);
};

exports.grant = function (context, auth, resource) {
    return exports.upsert(context, _.merge(resource, authPermission(auth), {
        read: true, write: true, manage: true, share: true, status: 'ACTIVE'
    }));
};

exports.revoke = function (context, auth, resource) {
    return exports.upsert(context, _.merge(resource, authPermission(auth), {
        status: 'REMOVED'
    }));
};

exports.revokeAll = function (context, resource) {
    return context.database.query(`
    UPDATE permissions
       SET doc = json_replace(doc, '$.status', 'REMOVED')
     WHERE json_extract(doc, '$.status') = 'ACTIVE' AND
           json_extract(doc, '$.type') = ?1 AND
           json_extract(doc, '$.resource') = ?2
    `, [resource.type, resource.resource]);
};

exports.upsertGroups = function (context, groups) {
    return groupDb.upsertGroups(context, groups);
};

exports.addGroupMember = function (context, groupId, member) {
    return groupDb.addGroupMember(context, groupId, member);
};

exports.systemGroupCredentials = function (context) {
    return { whoType: 'group', who: context.systemGroup };
};

exports.getSystemGroup = function (context) {
    return groupDb.getGroups([context.systemGroup])
    .then(groups => {
        if (groups && groups.length > 0) {
            return groups[0];
        }
        throw Boom.notFound();
    });
};

exports.sync = function (context, user) {
    return context.sdk.permissions.list(user)
    .then(r => r.data)
    .then(ps => exports.upsertMany(context, ps));
};

function serializePermission(context, permission) {
    const result = _.omit(permission, ['__v']);
    result._id = `${permission.whoType}-${permission.who}_on_${permission.type}-${permission.resource}`;

    if (permission.type === 'gateway' && permission.resource === context.gatewayId) {
        result.isOwnGateway = true;
    }

    return result;
}

function authPermission(auth) {
    return _.pick(auth, ['who', 'whoType']);
}
