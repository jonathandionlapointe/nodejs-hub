'use strict';

const EventEmitter = require('events');
const Promise = require('bluebird');
const util = require('util');

/** AsyncEventEmitter
 *
 * Subclasses EventEmitter with additional methods for managing
 * listener results that are promises.
 */
function AsyncEventEmitter() {
    EventEmitter.call(this);
    this._promises = {};
}
util.inherits(AsyncEventEmitter, EventEmitter);

/** onAsync(event, callback)
 *
 * Registers an asynchronous listener. Listeners registered with this
 * method will have their results waited on in the promise returned by
 * `emitAsync`.
 *
 * Returns the actual listener function that must be used to
 * unregister the listener with `removeListener`.
 */
AsyncEventEmitter.prototype.onAsync = function (event, callback) {
    const asyncCallback = Promise.method(callback);

    function fn() {
        if (!this._promises[event]) {
            callback.apply(this, arguments);
        } else {
            this._promises[event].push(asyncCallback.apply(this, arguments));
        }
    }

    this.on(event, fn);
    return fn;
};

/** emitAsync(event, ...)
 *
 * Emit an event. Listeners registered with `onAsync` that return
 * promises will have their results waited on.
 *
 * Returns a promise that resolves to the result of
 * `EventEmitter.emit`: true if there were any listeners, or false if
 * the event had no listeners. If any of the listeners fail, the
 * returned promise is rejected with the listener's rejection reason.
 */
AsyncEventEmitter.prototype.emitAsync = function (event) {
    this._promises[event] = [];
    const result = this.emit.apply(this, arguments);
    return Promise.all(this._promises[event]).return(result).finally(() => {
        this._promises[event] = null;
    });
};

/** emitAllAsync(event, ...)
 *
 * Emit an event. Listeners registered with `onAsync` that return
 * promises will have their results waited on.
 *
 * Returns a promise that resolves to the result of
 * `EventEmitter.emit`: true if there were any listeners, or false if
 * the event had no listeners.
 *
 * This method differs from `emitAsync` in that *all* listeners will
 * be called (assuming they were all registered with `onAsync`),
 * regardless of when/if any of them fail. If any of the listeners
 * fail, the returned promise will be rejected with an Error object
 * that has an `errors` property set to an array of the listener
 * rejection reasons.
 */
AsyncEventEmitter.prototype.emitAllAsync = function (event) {
    this._promises[event] = [];
    const result = this.emit.apply(this, arguments);
    return Promise.all(this._promises[event].map(promise => {
        return promise.reflect();
    })).then(results => {
        const errors = results
              .filter(result => !result.isFulfilled())
              .map(result => result.reason());
        if (errors.length > 0) {
            const error = new Error('emitAllAsync failure');
            error.errors = errors;
            throw error;
        }
        return result;
    });
};

module.exports = AsyncEventEmitter;
