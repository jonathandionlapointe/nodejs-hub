'use strict';

const _ = require('lodash');
const condition = require('./condition');
const Boom = require('super-boom')();
const ObjectID = require('bson-objectid');
const permission = require('./permission');
const Promise = require('bluebird');

function Device(model, doc) {
    this.model = model;
    this._doc = {};
    this.caps = {};
    this.parameters = {};
    this.status = 'NEW';

    // Initialize caps to hopefully sensible defaults.
    _.forEach(model.capabilities, capModel => {
        const cap = Device.defaultCap(capModel);
        this.caps[cap.name] = cap;
    });

    // Do the same for parameters.
    _.forEach(model.parameters, paramModel => {
        const param = Device.defaultParam(paramModel);
        this.parameters[paramModel.parameter] = param;
    });

    this._doc.protocolName = model.protocolId;
    this._updateDoc();

    _.merge(this._doc, doc || {});
    this._updateFromDoc();
}

Device.views = {
    update(context) {
        context = context.getAppContext();
        return context.collection('devices').createWithIndices([{
            name: 'devices_protocol_idx',
            fields: ['protocolName', 'protocolData.id'],
        }, {
            name: 'devices_parent_idx',
            fields: ['parent'],
        }])
        .then(() => context.collection('caps').createWithIndices([{
            name: 'caps_idx',
            fields: ['device', 'name'],
        }]))
        .then(() => context.collection('zones').createWithIndices([{
            name: 'zones_device_idx',
            fields: ['device', 'zoneStatus'],
        }]));
    }
};

let systemReady = false;
let protocols = {};

Device.registerProtocol = Promise.method(function (context, name, protocol) {
    protocols[name] = protocol;
    if (systemReady && typeof protocol.startup === 'function') {
        return protocol.startup(context);
    }
});

Device.unregisterProtocol = Promise.method(function (context, name) {
    if (_.has(protocols, name)) {
        const protocol = protocols[name];
        delete protocols[name];
        if (protocol && typeof protocol.shutdown === 'function') {
            return protocol.shutdown(context);
        }
    }
});

/** Start up all device protocols.
 *
 * @return {Promise<any[]>} an array of startup errors.
 */
Device.onSystemStartup = function (context) {
    if (systemReady) {
        return Promise.resolve([]);
    }
    return Promise.map(_.keys(protocols), name => {
        return Promise.try(() => {
            const protocol = protocols[name];
            if (typeof protocol.startup === 'function') {
                return protocol.startup(context);
            }
        }).reflect();
    }).then(results => {
        const errors = [];
        _.forEach(results, promise => {
            if (!promise.isFulfilled()) {
                errors.push(promise.reason());
            }
        });
        systemReady = true;
        return errors;
    });
};

/** Stop and unregister all device protocols.
 *
 * @return {Promise<any[]>} an array of shutdown errors.
 */
Device.onSystemShutdownDevices = function (context) {
    if (!systemReady) {
        return Promise.resolve([]);
    }
    systemReady = false;
    return Promise.map(_.keys(protocols), name => {
        return Device.unregisterProtocol(context, name).reflect();
    }).then(results => {
        const errors = [];
        _.forEach(results, promise => {
            if (!promise.isFulfilled()) {
                errors.push(promise.reason());
            }
        });
        return errors;
    });
};

/** Get the plugin responsible for a device protocol.
 *
 * @param {string} name - The protocol name.
 * @return The protocol plugin.
 */
Device.getProtocol = Promise.method(function (name) {
    const protocol = protocols[name];
    if (!protocol) {
        throw Boom.customCode(502, 'protocol not available', 'DEVICE_PROTOCOL_UNAVAILABLE', [], { protocol: name });
    }
    return protocol;
});

/** Check whether the given value falls inside the specified range.
 *
 * @param {NumberValue} cap - The value.
 * @param {Array<NumberValue>} range - `[min, max]`.
 */
Device.inRange = function (cap, range) {
    if (!range || range.length < 2) {
        return true;
    }
    const min = range[0];
    const max = range[1];
    return condition.matches(cap, {
        type: 'all',
        conditions: [
            _.merge({ type: '>=' }, min),
            _.merge({ type: '<=' }, max),
        ]
    });
};

/** Create sane defaults for a capability. */
Device.defaultCap = function (capModel) {
    const cap = (() => {
        switch (capModel.type) {
        case 'enum':
            return { value: capModel.range[0] };
        case 'float':
            return capModel.range && capModel.range.length > 0 && _.merge({}, capModel.range[0]) || {
                value: 0, unit: capModel.units[0]
            };
        case 'color':
            return { value: _.zipObject(_.map(capModel.range, component => [component, 0.0])) };
        case 'flags':
            return { value: _.zipObject(_.map(capModel.range, flag => [flag, false])) };
        case 'uri':
            return { value: 'http://www.example.com' };
        default:
            throw new TypeError('invalid capability type: ' + capModel.type);
        }
    })();
    cap.name = capModel.capabilityId;
    cap.timestamp = new Date().toISOString();
    return cap;
};

/** Create sane defaults for a parameter. */
Device.defaultParam = function (paramModel) {
    return {
        name: paramModel.parameter,
        value: paramModel.value || paramModel.default,
    };
};

/** Load a device from the database. */
Device.load = Promise.method(function (context, id) {
    let result = null;
    return context.database.query(`
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc)) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                WHERE device = ?1
           )
           AS caps
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE devices._id = ?1
    `, [id], row => result = loadRows(context, row))
    .then(() => {
        if (result) {
            return result;
        } else {
            throw Boom.notFound();
        }
    });
});

Device.readForRequest = Promise.method(function (context, auth, id, access) {
    let result = null;
    return permission.isValid(context, {
        type: 'device',
        resource: id,
        who: auth.credentials.who,
        whoType: auth.credentials.whoType,
        read: true,
        [access]: true
    })
    .then(hasAccess => {
        if (!hasAccess) {
            throw Boom.forbidden();
        }
    })
    .then(() => context.database.query(`
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc) ) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                WHERE device = ?1
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE devices._id = ?1
    `, [id], row => {
        result = loadRows(context, row);
    }))
    .then(() => {
        if (!result) {
            throw Boom.notFound();
        }

        return result;
    });
});

Device.listForRequest = Promise.method(function (context, auth) {
    const results = [];
    return permission.findForType(context, auth.credentials, 'device')
    .filter(p => p.read)
    .map(p => p.resource)
    .then(ids => context.database.query(`
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc) ) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                GROUP BY device
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE devices._id IN (SELECT value FROM json_each(?1))
    `, [JSON.stringify(ids)], row => {
        results.push(loadRows(context, row));
    }))
    .return(results)
    .all();
});

/** Find the device ID of a given protocol device ID.
 *
 * Looks for a device with the given protocolId. Returns the device
 * ID if any such device was found. The promise is rejected
 * otherwise.
 */
Device.fromProtocolId = Promise.method(function (context, protocolName, protocolId) {
    if (protocolId === null) {
        let result = null;
        return context.database.query(`
        SELECT _id
          FROM devices
         WHERE json_extract(doc, '$.protocolName') = ?1 AND
               json_extract(doc, '$.protocolData.id') IS NULL
         LIMIT 1
        `, [protocolName], row => result = row._id)
        .then(() => {
            if (result) {
                return result;
            } else {
                throw Boom.notFound();
            }
        });
    } else {
        let result = null;
        return context.database.query(`
        SELECT _id
          FROM devices
         WHERE json_extract(doc, '$.protocolName') = ?1 AND
               json_extract(doc, '$.protocolData.id') = ?2
         LIMIT 1
        `, [protocolName, protocolId], row => result = row._id)
        .then(() => {
            if (result) {
                return result;
            } else {
                throw Boom.notFound();
            }
        });
    }
});

/** Load a device with the given protocolId.
 *
 * Look for a device with the given protocolId, and loads it from
 * the database if found.
 */
Device.loadProtocolId = function (context, protocolName, protocolId) {
    return Device.fromProtocolId(context, protocolName, protocolId)
    .then(id => Device.load(context, id));
};

/** Load all devices for a given protocol. */
Device.loadProtocolAll = Promise.method(function (context, protocolName) {
    const results = [];
    return context.database.query(`
    WITH device_ids AS (
        SELECT _id
          FROM devices
         WHERE json_extract(devices.doc, '$.protocolName') = ?1
    )
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc)) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                WHERE device IN device_ids
                GROUP BY device
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE json_extract(devices.doc, '$.protocolName') = ?1
    `, [protocolName], row => results.push(row))
    .return(results)
    .map(row => loadRows(context, row));
});

/** Load a range of devices given protocol starting and ending IDs. */
Device.loadProtocolRange = Promise.method(function (context, protocolName, startkey, endkey) {
    const results = [];
    return context.database.query(`
    WITH device_ids AS (
        SELECT _id
          FROM devices
         WHERE json_extract(devices.doc, '$.protocolName') = ?1 AND
               json_extract(devices.doc, '$.protocolData.id') >= ?2 AND
               json_extract(devices.doc, '$.protocolData.id') <= ?3
    )
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc)) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                WHERE device IN device_ids
                GROUP BY device
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE json_extract(devices.doc, '$.protocolName') = ?1 AND
           json_extract(devices.doc, '$.protocolData.id') >= ?2 AND
           json_extract(devices.doc, '$.protocolData.id') <= ?3
    `, [protocolName, startkey, endkey], row => results.push(row))
    .return(results)
    .map(row => loadRows(context, row));
});

/** Load all devices from the database. */
Device.loadAll = Promise.method(function (context) {
    const results = [];
    return context.database.query(`
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc)) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                GROUP BY device
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
    `, [], row => {
        if (!context.scope || _.includes(context.scope, `device-${row.device._id}.read`)) {
            results.push(loadRows(context, row));
        }
    })
    .then(() => Promise.all(results));
});

/** Load a tree of device IDs, from the specified ID to all children.
 *
 * The resulting tree's leaves are each a tuple of the device ID, and
 * an array of its children.  So for instance, a device with one child
 * device would look like:
 *
 *     [parent, [[child, []]]]
 *
 * With this structure, turning the tree into a plain list of device
 * IDs is as simple as `_.flatten(tree)`.
 */
Device.getIdTree = function (context, id) {
    const results = [];
    return context.database.query(`
    SELECT _id
      FROM devices
     WHERE json_extract(doc, '$.parent') = ?1
    `, [id], row => results.push(row))
    .then(() => results)
    .mapSeries(row => {
        return Device.getIdTree(context, row._id);
    }).then(children => {
        return [id, children];
    });
};

/** Load the child devices (and their children).
 *
 * @return {Promise<this>} the device object.
 */
Device.prototype.loadChildren = Promise.method(function (context) {
    const results = [];
    return context.database.query(`
    WITH device_ids AS (
        SELECT _id
          FROM devices
         WHERE json_extract(doc, '$.parent') = ?1
    )
    SELECT devices.doc AS device,
           COALESCE(caps.docs, '[]') AS caps,
           json_extract(models.doc, '$.data') AS model
      FROM devices
           LEFT JOIN
           (
               SELECT json_group_array(json(caps.doc)) AS docs,
                      json_extract(caps.doc, '$.device') AS device
                 FROM caps
                WHERE device IN device_ids
                GROUP BY device
           )
           AS caps ON caps.device = devices._id
           JOIN
           cache models ON 'model:' || json_extract(devices.doc, '$.model') = models._id
     WHERE json_extract(devices.doc, '$.parent') = ?1
     GROUP BY devices._id
    `, [this._doc._id], row => results.push(loadRows(context, row)))
    .then(() => Promise.all(results))
    .mapSeries(child => child.loadChildren(context))
    .then(children => {
        this.children = children;
        return this;
    });
});

/** Find the root device of a particular device tree - but not
 *  including the System (gateway) device. */
Device.findRootId = function (context, id) {
    let result = null;
    return context.database.query(`
    SELECT json_extract(doc, '$.parent') AS parent
      FROM devices
     WHERE _id = ?1 AND
           parent IS NOT NULL AND
           parent != ?2
     LIMIT 1
    `, [id, context.gatewayId], row => result = row.parent)
    .then(() => {
        if (result) {
            return Device.findRootId(context, result);
        } else {
            return id;
        }
    });
};

/** Get the zones associated with a particular device ID. */
Device.getArmedZoneIds = function (context, id) {
    const results = [];
    return context.database.query(`
    SELECT _id
      FROM zones
     WHERE json_extract(doc, '$.device') = ?1 AND
           json_extract(doc, '$.zoneStatus') = 'armed'
    `, [id], row => results.push(row._id))
    .return(results);
};

function loadRows(context, row) {
    // TODO ensure cached models eventually get fetched, so that the cache can
    // eventually refresh after expiry!
    return Promise.try(() => {
        const model = typeof row.model === 'string' ? JSON.parse(row.model) : row.model;
        const doc = typeof row.device === 'string' ? JSON.parse(row.device) : row.device;
        const caps = typeof row.caps === 'string' ? JSON.parse(row.caps) : row.caps;
        const device = new Device(model, doc);
        _.forEach(caps, cap => {
            device._updateCapFromDoc(cap);
        });
        return device;
    });
}

/** Add a new device.
 *
 * @param context - The current context.
 * @param model - The base device model.
 * @param doc - The device document.
 * @returns {Promise<Device[] | string>} The created device and children
 * devices, or the new device ID if creation is a long-running process. In the
 * latter case, events must be emitted to notify the API and client of the final
 * result.
 */
Device.add = function (context, model, doc) {
    if (typeof model === 'string') {
        return context.sdk.models.getCached(context, model).then(model => {
            return Device.add(context, model, doc);
        });
    }

    doc = doc || {};
    if (!doc.protocolName) {
        doc.protocolName = model.protocolId;
    }
    if (!doc._id) {
        doc._id = new ObjectID().toString();
    }

    return Device.getProtocol(doc.protocolName).then(protocol => {
        const device = new Device(model, doc);
        return protocol.addDevice(context, device);
    })
    .tap(devices => {
        if (devices.length > 0) {
            devices[0]._updateDoc();
            devices[0]._doc = _.merge({}, doc, devices[0]._doc);
            devices[0]._updateFromDoc();
        }
    })
    .mapSeries(device => device._activate(context))
    .each(device => device.initialize(context))
    .then(devices => {
        if (devices.length === 0) {
            return doc._id;
        } else {
            return devices;
        }
    });
};

/** Initialize a device. Defers to the protocol to perform any
 * device-specific initialization routines. */
Device.prototype.initialize = function (context) {
    return Device.getProtocol(this._doc.protocolName).then(protocol => {
        if (typeof protocol.initializeDevice === 'function') {
            return protocol.initializeDevice(context, this);
        }
    });
};

/** Create a child device. */
Device.prototype.createChild = function (model, doc) {
    const child = new Device(model, doc);
    child.parent = this;
    return child;
};

/** Remove a device.
 *
 * @param options - Options. FIXME document me.
 */
Device.prototype.remove = function (context) {
    return Device.getIdTree(context, this._doc._id)
    .then(_.flatten)
    .map(id => Device.getArmedZoneIds(context, id))
    .then(_.flatten)
    .then(zoneIds => {
        if (zoneIds.length !== 0) {
            throw Boom.customCode(409,
                'Cannot remove device while it or a child device is associated with armed zones',
                'DEVICE_CANNOT_REMOVE_ARMED');
        }
        return Device.getProtocol(this._doc.protocolName);
    }).then(protocol => {
        return protocol.removeDevice(context, this);
    }).then(() => {
        return this.deactivate(context);
    });
};

/** Validate the given capability value and optional unit.
 *
 * Returns the capability object.
 */
Device.prototype.validateCapability = function (input, options) {
    options = options || {};
    const name = input.name;
    const unit = input.unit;
    const value = input.value;
    const cap = this.caps[name];
    const model = _.findWhere(this.model.capabilities, { capabilityId: name });
    if (!cap) {
        throw Boom.customCode(404, 'capability not found', 'DEVICE_CAP_NOT_FOUND', [], { capability: name });
    } else if (!options.ignoreMethod && !_.includes(model.methods, 'put')) {
        throw Boom.customCode(405, 'capability is read-only', 'DEVICE_CAP_READONLY');
    } else if (typeof value === 'undefined') {
        throw Boom.customCode(400, 'bad value', 'DEVICE_CAP_VALUE_INVALID', [], {
            capability: name, value: null
        });
    } else if ((model.type === 'float' && typeof value !== 'number') ||
               ((model.type === 'enum' || model.type === 'uri') && typeof value !== 'string')) {
        throw Boom.customCode(400, 'bad value', 'DEVICE_CAP_VALUE_INVALID', [], {
            capability: name, value
        });
    } else if (model.type === 'float' && !_.includes(model.units, unit)) {
        throw Boom.customCode(400, 'bad unit', 'DEVICE_CAP_UNIT_INVALID', [], {
            capability: name, unit, units: model.units
        });
    } else if ((model.type === 'enum' && !_.includes(model.range, value)) ||
               (model.type === 'float' && !Device.inRange({ value, unit }, model.range))) {
        throw Boom.customCode(400, 'out of range', 'DEVICE_CAP_OUT_OF_RANGE', [], {
            capability: name, value, unit, range: model.range
        });
    } else if (model.type === 'color') {
        input.value = _.merge({}, cap.value, _.pick(value, model.range));
        _.forEach(input.value, (colorValue, colorName) => {
            if (typeof colorValue !== 'number') {
                throw Boom.customCode(400, 'bad color value', 'DEVICE_CAP_COLOR_INVALID', {
                    capability: name, colorName, colorValue
                });
            }
            // Clamp the value to the range [0, 1].
            input.value[colorName] = Math.min(Math.max(colorValue, 0.0), 1.0);
        });
    } else if (model.type === 'flags') {
        input.value = _.merge({}, cap.value, _.pick(value, model.range));
        _.forEach(input.value, (flagValue, flag) => {
            if (typeof flagValue !== 'boolean') {
                throw Boom.customCode(400, 'bad flag value', 'DEVICE_CAP_FLAG_INVALID', {
                    capability:name, flag, flagValue
                });
            }
        });
    }
    return cap;
};

/** Attempt to set a capability.
 *
 * To update the changed capability or capabilities, the underlying
 * protocol's setCapability should call (and return the result of):
 *
 *     device.updateCap(context, name, value[, unit])
 *
 * @return A promise. If falsy, the capability value was not changed
 * (normally because the passed value is the same as the current
 * value). Otherwise, the updated capability is returned.
 */
Device.prototype.setCapability = Promise.method(function (context, name, value, unit) {
    // Capture arguments in an object so that validateCapability may modify them.
    const input = { name, value, unit };
    const cap = this.validateCapability(input);
    return Device.getProtocol(this._doc.protocolName).then(protocol => {
        return protocol.setCapability(context, this, input.name, input.value, input.unit);
    }).then(ok => {
        if (ok) {
            return this.save(context).return(cap);
        } else {
            return false;
        }
    });
});

/** Update the device parameters.
 *
 * To update the changed parameters, the underlying protocol's
 * setParameters should call (and return the result of):
 *
 *     device.updateParameters(context, parameters)
 *
 * @return A promise. If falsy, no parameters were updated (usually
 * because no new values were passed). Otherwise, the updated
 * parameters are returned.
 */
Device.prototype.setParameters = Promise.method(function (context, parameters) {
    if (_.isEmpty(parameters)) {
        return false;
    }

    // Make sure all parameters are { value } objects.
    _.mapValues(parameters, (param, name) => {
        if (typeof param !== 'object') {
            return { name, value: param };
        } else {
            return param;
        }
    });

    return Device.getProtocol(this._doc.protocolName).then(protocol => {
        return protocol.setParameters(context, this, parameters);
    }).then(ok => {
        if (ok) {
            return this.save(context).return(this.parameters);
        } else {
            return false;
        }
    });
});

/** Update the device metadata.
 *
 * @return A boolean promise. Resolves `true` if changes were made, or
 * `false` if the new data is the same as the old data. Rejected if it
 * was not possible to save the changes.
 */
Device.prototype.updateData = Promise.method(function (context, newData) {
    const sensitiveFields = [ // Fields we should not modify.
        'capabilities',
        'caps',         // Virtual field managed by updateCap/setCapability.
        'isEmulated',   // Read only.
        'gateway',      // Read only.
        'model',        // Read only.
        'parameters',   // Managed by setParameters.
        'parent',       // Read only.
        'protocolName', // Read only.
        'protocolData', // Managed by protocol.
        'status',       // Managed by protocol via updateStatus.
        'token',        // Autogenerated by the API on each update!
        'type',         // Always 'device'.
        '_id',          // Couch/Mongo ID, read-only.
        '_rev',         // Couch revision.
        '__v',          // Mongo version field.
    ];
    newData = _.omit(newData, sensitiveFields);
    const oldData = _.omit(this._doc, sensitiveFields);
    if (!_.isEqual(newData, oldData)) {
        const sensitiveData = _.pick(this._doc, sensitiveFields);
        this._doc = _.merge(newData, sensitiveData);
        this.dirty = true;
        return this.save(context).then(() => {
            return context.emitEvent({
                type: 'update',
                path: `/devices/${this._id}`,
                resource: newData,
            }).return(true);
        });
    }
    return false;
});

/** Update a capability value.
 *
 * This is an internal method for protocol-specific implementations to
 * update cap values. Generates an event unless `options.silent` is
 * truthy.
 *
 * If this originated from a user request, `options.request` should
 * be set to the request ID.
 *
 * @return {Promise<boolean>} Whether the cap was updated.
 */
Device.prototype.updateCap = Promise.method(function (context, name, value, unit) {
    const cap = this.caps[name] || { name };
    if (_.isEqual(cap.value, value) && cap.unit === unit) {
        // No change.
        return false;
    }

    if (!cap._id) {
        cap._id = new ObjectID().toString();
        this.caps[name] = cap;
    }

    cap.value = value;
    cap.unit = unit;
    cap.timestamp = new Date().toISOString();
    cap.dirty = true;

    return context.emitEvent({
        type: 'update',
        path: `/devices/${this._id}/caps/${name}`,
        resource: _.omit(cap, 'dirty'),
        timestamp: cap.timestamp,
    }).return(true);
});

/** Update parameters.
 *
 * This is an internal method for protocol-specific implementations to
 * update parameter values.
 *
 * @return If no parameters were changed, false. Otherwise, the
 * changed parameters.
 */
Device.prototype.updateParameters = Promise.method(function (context, parameters) {
    parameters = parameters || {};

    const edits = [];
    _.forEach(parameters, (newParam, key) => {
        const oldParam = this.parameters[key];
        if (!oldParam || !_.isEqual(newParam.value, oldParam.value)) {
            edits.push(key);
            this.parameters[key] = {
                name: key,
                value: newParam.value
            };
        }
    });

    if (edits.length === 0) {
        return false;
    }

    parameters = _.pick(this.parameters, edits);

    this.dirty = true;

    return context.emitEvent({
        type: 'update',
        path: `/devices/${this._id}`,
        resource: { parameters },
    }).return(parameters);
});

/** Update the device status.
 *
 * This is an internal method to update the status of a device.
 * Primarily used when a device enters or recovers from a failure
 * state.
 */
Device.prototype.updateStatus = Promise.method(function (context, newStatus) {
    if (newStatus === this.status) {
        return this;
    }

    this.status = newStatus;
    this.dirty = true;

    return context.emitEvent({
        type: 'update',
        path: `/devices/${this._id}`,
        resource: { status: newStatus },
    }).return(this);
});

/** Save a device.
 *
 * Saves the device and any changed caps, if a database has been
 * provided.
 */
Device.prototype.save = Promise.method(function (context, isNewDevice) {
    return Promise.try(() => {
        this._updateDoc();
        if (!isNewDevice) {
            if (this.dirty) {
                return context.collection('devices').update(this._doc, this._id).then(() => {
                    this.dirty = false;
                });
            }
        } else {
            if (!this._doc._id) {
                this._id = this._doc._id = ObjectID().toString();
            }
            return context.collection('devices').upsert(this._doc).then(body => {
                this._doc._rev = body.rev;
            });
        }
    }).then(() => {
        return Promise.mapSeries(_.pairs(this.caps), (capname) => {
            const cap = capname[1];
            const name = capname[0];
            const doc = {
                _id: cap._id,
                type: 'cap',
                device: this._id,
                name: name,
                value: cap.value,
                unit: cap.unit,
                timestamp: cap.timestamp
            };
            if (!isNewDevice) {
                if (cap.dirty) {
                    return context.collection('caps').upsert(doc).then(() => {
                        cap.dirty = false;
                    });
                }
            } else {
                if (!doc._id) {
                    cap._id = doc._id = ObjectID().toString();
                }
                cap.device = this._id;
                return context.collection('caps').upsert(doc);
            }
        });
    });
});

/** Munge the device object into JSON suitable for consumption by API
 * endpoints.
 */
Device.prototype.toJSON = function () {
    const caps = {};
    _.forEach(this.caps, (cap, name) => {
        caps[name] = _.omit(cap, ['_rev', 'type', 'dirty']);
    });
    return _.merge({}, _.omit(this._doc, ['_rev', 'type']), { caps });
};

/** Set the device status to `ACTIVE` and generate the creation event. */
Device.prototype._activate = function (context) {
    this.dirty = true;
    if (!this.status || this.status === 'NEW') {
        this.status = 'ACTIVE';
    }
    this._doc.gateway = context.gatewayId;
    if (!this._doc.parent) {
        this._doc.parent = context.gatewayId;
    }

    const credentials = permission.systemGroupCredentials(context);

    return this.save(context, true)
    .then(() => {
        if (this._doc.protocolName === 'ui') {
            const member = { whoType: 'device', who: this._id };
            return permission.addGroupMember(context, credentials.who, member);
        }
    })
    .then(() => permission.grant(context, credentials, {
        type: 'device',
        resource: this._id,
    }))
    .then(() => {
        return context.emitEvent({
            type: 'create',
            path: `/devices/${this._id}`,
            resource: this.toJSON(),
            timestamp: new Date().toISOString(),
        }).return(this);
    });
};

/** Delete the device and generate the delete event.
 *
 * This is an internal method for use by protocols when a
 * device has been used without any user interaction - NOT
 * during the course of handling the removeDevice callback.
 */
Device.prototype.deactivate = Promise.method(function (context) {
    // First set the device status to REMOVED to indicate this
    // device is no longer usable, in case something goes wrong
    // during the remainder of the removal process.
    //
    // TODO scan for REMOVED devices at start-up and re-attempt
    // removal.
    this.status = 'REMOVED';
    this.dirty = true;
    return this.save(context).then(() => {
        if (!this.children) {
            return this.loadChildren(context);
        }
    }).then(() => {
        return Promise.mapSeries(this.children, device => {
            return device.deactivate(context);
        });
    }).then(() => {
        return Promise.mapSeries(_.map(this.caps), cap => {
            return context.collection('caps').delete(cap._id);
        });
    }).then(() => {
        return context.collection('devices').delete(this._id);
    }).then(() => {
        return context.emitEvent({
            type: 'delete',
            path: `/devices/${this._id}`,
            resource: {},
            timestamp: new Date().toISOString(),
        });
    });
});

Device.prototype._updateDoc = function () {
    _.merge(this._doc, {
        _id: this._id,
        type: 'device',
        model: this.model._id,
        parameters: this.parameters,
        protocolData: this.protocolData,
        status: this.status,
    });
    if (this.parent instanceof Device) {
        this._doc.parent = this.parent._id;
    }
};

Device.prototype._updateFromDoc = function () {
    this._id = this._doc._id;
    this.status = this._doc.status;
    _.forEach(this._doc.parameters, (param, key) => {
        // Make sure we can load the old { param: value } as
        // { param: { value } }.
        if (typeof param !== 'object') {
            param = { name: key, value: param };
        }
        this.parameters[key] = param;
    });
    this.protocolData = this._doc.protocolData;
    this.dirty = false;
};

Device.prototype._updateCapFromDoc = function (doc) {
    // If this capability was removed from the device model (as stored
    // in the cache/API), `this.caps[name]` will not exist. Ignore it,
    // but don't purge it from the DB - it may be re-added in a
    // subsequent version of the device model.
    const cap = this.caps[doc.name];
    if (cap) {
        _.merge(cap, doc);
        cap.dirty = false;
    }
};

module.exports = Device;
