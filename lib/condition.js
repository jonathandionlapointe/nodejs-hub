'use strict';

const util = require('util');
const _ = require('lodash');

/**
 * Value conditions.
 *
 * Tools for comparing values.
 *
 * @module lib/condition
 */

/**
 * The default epsilon value for numerical equality comparisons.
 * @static
 * @constant {number}
 * @default
 */
const DEFAULT_EPSILON = 10e-8;

/**
 * Checks if the given value matches the condition.
 * @static
 * @param {Value} cap - The value.
 * @param {Condition|boolean} cond - The condition.
 * @returns {boolean}
 * @throws {TypeError} The value type must match the condition type.
 * @throws {ConversionError} The value unit must be compatible with the condition's unit.
 */
function matches(cap, cond) {
    if (typeof cond === 'boolean') {
        return cond;
    }
    switch (cond.type) {
    case 'always': return true;
    case 'never': return false;
    case 'exact':
        assertEnumCap(cap);
        return cap.value === cond.value;
    case 'not': return !matches(cap, cond.condition);
    case 'all': return _.all(cond.conditions, cond => matches(cap, cond));
    case 'any': return _.any(cond.conditions, cond => matches(cap, cond));
    default:
        assertNumberCap(cap);
        return matchRelative(cap, cond.type, cond);
    }
}

/**
 * Extract all unique `exact` values from a condition.  Relative
 * comparisons are ignored.
 *
 * @static
 * @param {(Condition)} cond - The condition to extract values from.
 * @param {?string[]} results - The output array.
 * @returns {string[]}
 */
function exactValues(cond, results) {
    if (!results) {
        results = [];
    }

    if (cond.type === 'exact') {
        results.push(cond.value);
    } else if (cond.type === 'all' || cond.type == 'any') {
        _.forEach(cond.conditions, cond => exactValues(cond, results));
    } else if (cond.type === 'not') {
        exactValues(cond.condition, results);
    }

    return results;
}

/**
 * Recursively simplifies a condition according to the following rules,
 * where `1 -> true` and `0 -> false`.
 *
 *     1 -> 1
 *     0 -> 0
 *     A -> A
 *     not 1 -> 0
 *     not 0 -> 1
 *     not(not A) -> A
 *     all(A, all(B, C)) -> all(A, B, C)
 *     any(A, any(B, C)) -> any(A, B, C)
 *     all(A, B, 1) -> all(A, B)
 *     any(A, B, 0) -> any(A, B)
 *     all(A, 0) -> 0
 *     any(A, 1) -> 1
 *     all(A) -> A
 *     any(B) -> B
 *     all() -> 1
 *     any() -> 0
 *     all(not A, not B) -> not any(A, B)
 *     any(not A, not B) -> not all(A, B)
 *
 * @static
 * @param {(Condition)} cond - The condition to simplify.
 * @returns {(Condition)}
 */
function simplify(cond) {
    if (cond.type === 'always' || cond.type === 'never') {
        return cond;
    } else if (cond.type === 'not') {
        // Simplify sub-term.
        const term = simplify(cond.condition);

        // Apply rules: not 1 -> 0, not 0 -> 1
        if (term.type === 'always') {
            return { type: 'never' };
        } else if (term.type === 'never') {
            return { type: 'always' };
        }

        // Apply rule: not(not A) -> A
        if (term.type === 'not') {
            return term.condition;
        } else {
            return {
                type: 'not',
                condition: term
            };
        }
    } else if (cond.type === 'all') {
        const terms = _.reduce(cond.conditions, (result, input) => {
            if (result.type === 'never') {
                return result;
            }

            // Simplify sub-term.
            const term = simplify(input);

            // Apply rule: all(A, 0) -> 0
            if (term.type === 'never') {
                return term;
            }

            // Apply rule: all(A, 1) -> all(A)
            if (term.type === 'always') {
                return result;
            }

            // Apply rule: all(A, all(B, C)) -> all(A, B, C)
            if (term.type === 'all') {
                return result.concat(term.conditions);
            } else {
                return result.concat(term);
            }
        }, []);

        if (terms.type === 'never') {
            return terms;
        }

        // Apply rule: all() -> true
        if (terms.length === 0) {
            return { type: 'always' };
        }

        // Apply rule: all(A) -> A
        if (terms.length === 1) {
            return terms[0];
        }

        // Apply rule: all(not A, not B) -> not any(A, B)
        if (_.all(terms, term => term.type === 'not')) {
            return {
                type: 'not',
                condition: {
                    type: 'any',
                    conditions: _.map(terms, term => term.condition)
                }
            };
        } else {
            return {
                type: 'all',
                conditions: terms
            };
        }
    } else if (cond.type === 'any') {
        const terms = _.reduce(cond.conditions, (result, input) => {
            if (result.type === 'always') {
                return result;
            }

            // Simplify sub-term.
            const term = simplify(input);

            // Apply rule: any(A, 1) -> 1
            if (term.type === 'always') {
                return term;
            }

            // Apply rule: any(A, 0) -> any(A)
            if (term.type === 'never') {
                return result;
            }

            // Apply rule: any(A, any(B, C)) -> any(A, B, C)
            if (term.type === 'any') {
                return result.concat(term.conditions);
            } else {
                return result.concat(term);
            }
        }, []);

        if (terms.type === 'always') {
            return terms;
        }

        // Apply rule: any() -> false
        if (terms.length === 0) {
            return { type: 'never' };
        }

        // Apply rule: any(A) -> A
        if (terms.length === 1) {
            return terms[0];
        }

        // Apply rule: any(not A, not B) -> not all(A, B)
        if (_.all(terms, term => term.type === 'not')) {
            return {
                type: 'not',
                condition: {
                    type: 'all',
                    conditions: _.map(terms, term => term.condition)
                }
            };
        } else {
            return {
                type: 'any',
                conditions: terms
            };
        }
    } else {
        return cond;
    }
}

function assertEnumCap(cap) {
    if (typeof cap.value !== 'string') {
        throw new TypeError('expected enum value');
    }
}

function assertNumberCap(cap) {
    if (typeof cap.value !== 'number') {
        throw new TypeError('expected number value');
    }
    if (typeof cap.unit !== 'string') {
        throw new TypeError('expected unit on number value');
    }
}

function matchRelative(lhs, op, rhs) {
    const n = lhs.unit === rhs.unit ? lhs.value : convert(lhs, rhs.unit);
    const m = rhs.value;
    const d = n - m;
    const epsilon = rhs.epsilon === void 0 ? DEFAULT_EPSILON : rhs.epsilon;
    switch (op) {
    case '<':  return d < -epsilon;
    case '<=': return d < +epsilon;
    case '>':  return d > +epsilon;
    case '>=': return d > -epsilon;
    case '=':  return Math.abs(d) < epsilon;
    case '!=': return Math.abs(d) > epsilon;
    default: throw new Error('invalid condition type');
    }
}

const CONVERSION_TABLE = {
    // Temperature scales: Celsius (C), Fahrenheit (F), and Kelvin (K).
    C: { C: undefined,           F: x => (x - 32) * 5 / 9,     K: x => x - 273.15         },
    F: { C: x => x * 9 / 5 + 32, F: undefined,                 K: x => x * 9 / 5 - 459.67 },
    K: { C: x => x + 273.15,     F: x => (x + 459.67) * 5 / 9, K: undefined               },
};

function convert(value, unit) {
    try {
        return CONVERSION_TABLE[unit][value.unit](value.value);
    } catch (error) {
        throw new ConversionError(value, unit);
    }
}

/**
 * Error thrown if a value comparison fails due to incompatible units.
 * @static
 * @constructor
 * @param {Value} input - The input value.
 * @param {string} targetUnit - The incompatible target unit.
 */
function ConversionError(input, targetUnit) {
    Error.call(this);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.input = input ? {
        value: input.value,
        unit: input.unit,
    } : {};
    this.targetUnit = targetUnit;
    this.message = `Unable to convert ${JSON.stringify(this.input)} to unit ${targetUnit}`;
}
util.inherits(ConversionError, Error);

module.exports = {
    matches,
    exactValues,
    simplify,
    ConversionError,
    DEFAULT_EPSILON,
};

/**
 * @typedef NumberValue
 * @type {object}
 * @property {number} value - The value.
 * @property {string} unit - The unit of the value.
 */

/**
 * @typedef EnumValue
 * @type {object}
 * @property {string} value - The value.
 */

/**
 * @typedef Value
 * @type {(NumberValue|EnumValue)}
 */

/**
 * An `always` condition matches anything.
 * @typedef AlwaysCondition
 * @type {object}
 * @property {string} type - "always"
 */

/**
 * A `never` condition matches nothing.
 * @typedef NeverCondition
 * @type {object}
 * @property {string} type - "never"
 */

/**
 * An `exact` condition matches a string (enum-style) value exactly.
 * @typedef ExactCondition
 * @type {object}
 * @property {string} type - "exact"
 * @property {string} value - The value to match.
 */

/**
 * A `not` condition performs a boolean negation on another condition.
 * @typedef NotCondition
 * @type {object}
 * @property {string} type - "not"
 * @property {Condition} condition - The condition to negate.
 */

/**
 * An `all` condition matches if all sub-conditions match.
 * @typedef AllCondition
 * @type {object}
 * @property {string} type - "all"
 * @property {Array<Condition>} conditions - The conditions to match.
 */

/**
 * An `any` condition matches if any of its sub-conditions match.
 * @typedef AnyCondition
 * @type {object}
 * @property {string} type - "any"
 * @property {Array<Condition>} conditions - The conditions to match.
 */

/**
 * Matches based on the comparison type. For equality/inequality
 * comparisons, the optional epsilon property is the distance by
 * which the value must match (defaults to
 * [DEFAULT_EPSILON]{@link module:lib/conditions.DEFAULT_EPSILON}).
 * @typedef CompareCondition
 * @type {object}
 * @property {string} type - "<" | "<=" | ">" | ">=" | "=" | "!="
 * @property {number} value
 * @property {string} unit
 * @property {number=} epsilon
 */

/**
 * @typedef Condition
 * @type {(AlwaysCondition|NeverCondition|ExactCondition|CompareCondition|NegateCondition|AllCondition|AnyCondition)}
 */
