'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const Joi = require('joi');
const later = require('later');
const ObjectID = require('bson-objectid');
const permission = require('./permission');
const Promise = require('bluebird');
const time = require('time');

const views = {
    update(context) {
        return context.getAppContext().collection('schedules').create();
    }
};

function objectid() {
    return Joi.string().regex(/^[0-9A-Fa-f]{24}$/);
}

const schema = Joi.object().keys({
    _id: objectid(),
    _rev: Joi.string().strip(),
    type: Joi.string().strip(),
    name: Joi.string().required(),
    schedules: Joi.string().required(),
    timezone: Joi.string().required(),
    occurrenceType: Joi.string().required(),
    frequency: Joi.string().required(),
    gateway: Joi.string(),
    status: Joi.string(),
    createdTs: Joi.string(),
});

function list(context, scope) {
    const results = [];
    return context.database.query('SELECT doc FROM schedules', [], row => results.push(row.doc))
    .return(results)
    .map(JSON.parse);
}

function listForRequest(context, auth) {
    const results = [];
    return permission.findForType(context, auth.credentials, 'schedule')
    .filter(p => p.read)
    .map(p => p.resource)
    .then(ids => context.database.query(`
    SELECT doc
      FROM schedules
     WHERE _id IN (SELECT value FROM json_each(?1))
    `, [JSON.stringify(ids)], row => results.push(row.doc)))
    .return(results)
    .map(JSON.parse);
}

function readForRequest(context, auth, id, access) {
    return permission.isValid(context, {
        type: 'schedule',
        resource: id,
        who: auth.credentials.who,
        whoType: auth.credentials.whoType,
        read: true,
        [access]: true,
    })
    .then(hasAccess => {
        if (!hasAccess) {
            throw Boom.forbidden();
        }
    })
    .then(() => context.collection('schedules').get(id))
    .tap(result => {
        if (!result) {
            throw Boom.notFound();
        }
    });
}

function create(context, schedule) {
    schedule._id = schedule._id || ObjectID().toString();
    schedule.type = 'schedule';
    schedule.gateway = context.gatewayId;

    try {
        // Validate the timezone.
        if (schedule.timezone) {
            new time.Date().setTimezone(schedule.timezone);
        }
    } catch (error) {
        return Promise.reject(Boom.customCode(400, 'unknown timezone', 'SCHEDULE_UNKNOWN_TIMEZONE', [], { timezone: schedule.timezone }));
    }

    return context.collection('schedules').insert(_.omit(schedule, ['_rev'])).then(result => {
        if (result.ok) {
            schedule._rev = result.rev;
            const credentials = permission.systemGroupCredentials(context);
            return permission.grant(context, credentials, {
                type: 'schedule',
                resource: schedule._id,
            }).then(() => context.emitEvent({
                type: 'create',
                path: `/schedules/${schedule._id}`,
                resource: schedule,
            })).return(schedule);
        } else {
            context.server.log(['schedule', 'error'], 'Unable to insert schedule into database');
            throw Boom.badImplementation('unable to update database');
        }
    });
}

function read(context, id) {
    return context.collection('schedules').get(id).tap(doc => {
        if (!doc) {
            throw Boom.notFound();
        }
    });
}

function update(context, schedule, id) {
    return context.collection('schedules').get(id).then(doc => {
        if (!doc) {
            throw Boom.notFound();
        }

        schedule.type = 'schedule';
        schedule._rev = doc._rev;
        schedule._id = id;

        return context.collection('schedules').update(schedule, id).then(result => {
            if (!result.ok) {
                context.server.log(['schedule', 'error'], 'Unable to update schedule in database');
                throw Boom.badImplementation('unable to update database');
            }

            schedule._rev = result.rev;

            return context.emitEvent({
                type: 'update',
                path: `/schedules/${schedule._id}`,
                resource: schedule,
            }).return(schedule);
        });
    });
}

function remove(context, id) {
    return context.collection('schedules').get(id).then(doc => {
        if (!doc) {
            throw Boom.notFound();
        }

        return context.collection('schedules').delete(id).then(result => {
            if (result.ok) {
                return context.emitEvent({
                    type: 'delete',
                    path: `/schedules/${id}`,
                    resource: {},
                });
            } else {
                context.server.log(['schedule', 'error'], 'Unable to remove rule from database');
                throw Boom.badImplementation('unable to update database');
            }
        });
    });
}

function isValid(context, id, date) {
    return read(context, id).then(schedule => {
        if (!date) {
            date = new time.Date();
        } else {
            date = new time.Date(date);
        }

        if (schedule.timezone) {
            date.setTimezone(schedule.timezone);

            // Later doesn't support arbitrary timezones, so do a
            // relative conversion to UTC.
            date.setTimezone('UTC', true);
        }

        const sched = later.schedule(later.parse.text(schedule.schedules));
        return sched.isValid(date);
    });
}

module.exports = {
    views, schema,
    listForRequest, readForRequest,
    list, create, read, update, remove,
    isValid,
};
