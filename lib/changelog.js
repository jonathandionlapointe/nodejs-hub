'use strict';

const _ = require('lodash');
const EventEmitter = require('events');
const fs = require('fs');
const moment = require('moment');
const Promise = require('bluebird');
const readline = require('readline');
const semver = require('semver');
const util = require('util');
const zlib = require('zlib');

function parse(input, options) {
    let filename = null;

    options = setDefaults(options);
    if (typeof input === 'string') {
        filename = input;
        input = openInputStream(input);
    } else if (typeof options.filename === 'string') {
        filename = options.filename;
    }

    return new Promise((resolve, reject) => {
        const entries = [];
        const builder = createEntryBuilder();
        const parser = createLineParser(input, filename);
        input.on('error', err => {
            reject(err);
            resolve = _.noop;
        });
        parser.on('data', data => {
            switch (data.type) {
            case 'header':
                builder.setHeader(data);
                break;
            case 'log':
                if (entryIncluded(data.log, options)) {
                    builder.addLog(data.log);
                }
                break;
            case 'footer':
                builder.setFooter(data);
                if (versionInRange(builder.data.version, options.version)) {
                    builder.setFilename(filename);
                    entries.push(builder.data);
                }
                builder.clear();
            }
        });
        parser.on('end', () => {
            resolve(entries);
        });
    });
}
exports.parse = parse;

function setDefaults(options) {
    options = _.merge({
        version: { min: null, max: null },
        omitMerges: false,
        omitVersionBumps: false,
    }, options);

    options.version = _.mapValues(options.version, v => {
        if (typeof v === 'string') {
            return semver.parse(v);
        }
        return v;
    });

    return options;
}

function openInputStream(path) {
    const stream = fs.createReadStream(path);
    if (path.endsWith('.gz')) {
        const unzip = zlib.createGunzip();
        stream.on('error', err => unzip.emit('error', err));
        return stream.pipe(unzip);
    }
    return stream;
}

function versionInRange(version, range) {
    if (range.min) {
        if (version.compare(range.min) <= 0) {
            return false;
        }
    }
    if (range.max) {
        if (version.compare(range.max) >= 0) {
            return false;
        }
    }
    return true;
}

function entryIncluded(log, options) {
    if (options.omitMerges && log.startsWith('Merge branch')) {
        return false;
    }
    if (options.omitVersionBumps && log.startsWith('Version bump')) {
        return false;
    }
    return true;
}

function EntryBuilder() {
    this.clear();
}
exports.EntryBuilder = EntryBuilder;

EntryBuilder.prototype.setHeader = function (data) {
    this.data.line = data.line || 0;
    _.merge(this.data, data.header);
    return this;
};

EntryBuilder.prototype.setFilename = function (filename) {
    this.data.filename = filename;
    return this;
};

EntryBuilder.prototype.setSourcePackage = function (sourcePackage) {
    this.data.sourcePackage = sourcePackage;
    return this;
};

EntryBuilder.prototype.setVersion = function (version) {
    if (typeof version === 'string') {
        version = semver.parse(version);
    }
    this.data.version = version;
    return this;
};

EntryBuilder.prototype.setDistribution = function (distribution) {
    this.data.distribution = distribution;
    return this;
};

EntryBuilder.prototype.setUrgency = function (urgency) {
    this.data.urgency = urgency;
    return this;
};

EntryBuilder.prototype.addLog = function (log) {
    this.data.log.push(log);
    return this;
};

EntryBuilder.prototype.setFooter = function (data) {
    this.data.author = data.author;
    this.data.date = data.date || new Date();
    return this;
};

EntryBuilder.prototype.setAuthorName = function (name) {
    this.data.author.name = name;
    return this;
};

EntryBuilder.prototype.setAuthorEmail = function (email) {
    this.data.author.email = email;
    return this;
};

EntryBuilder.prototype.setDate = function (date) {
    this.data.date = date || new Date();
    return this;
};

EntryBuilder.prototype.clear = function () {
    this.data = {
        filename: null,
        line: -1,
        sourcePackage: null,
        version: null,
        distribution: 'unstable',
        urgency: 'low',
        log: [],
        author: { name: null, email: null },
        date: new Date(),
    };
    return this;
};

EntryBuilder.prototype.toString = function () {
    assertNotNull(this.data, 'sourcePackage');
    assertNotNull(this.data, 'version');
    assertNotNull(this.data, 'author.name');
    assertNotNull(this.data, 'author.email');
    if (_.isEmpty(this.data.log)) {
        throw new TypeError('Entry must have at least one log');
    }
    const header = `${this.data.sourcePackage} (${this.data.version.raw}) ${this.data.distribution}; urgency=${this.data.urgency}`;
    const lines = this.data.log.map(s => `  * ${s}`).join('\n');
    const date = moment(this.data.date).format('ddd, DD MMM Y HH:mm:ss ZZ');
    const footer = ` -- ${this.data.author.name} <${this.data.author.email}>  ${date}`;
    return `${header}\n\n${lines}\n\n${footer}\n`;
};

function createEntryBuilder() {
    return new EntryBuilder();
}
exports.createEntryBuilder = createEntryBuilder;

function entryToString(entry) {
    return EntryBuilder.prototype.toString.call({ data: entry });
}
exports.entryToString = entryToString;

function assertNotNull(object, attribute) {
    if (_.isNull(_.get(object, attribute, null))) {
        throw new TypeError(`${attribute} is required`);
    }
}

function LineParser(stream) {
    this.line = 0;
    this.rl = readline.createInterface({
        input: stream
    });
    this.rl.on('line', this.onLine.bind(this));
    this.rl.on('close', this.onClose.bind(this));
}
util.inherits(LineParser, EventEmitter);
exports.LineParser = LineParser;

LineParser.prototype.onLine = function (line) {
    this.line += 1;

    const data = this.parseLine(line);
    if (data) {
        data.line = this.line;
        this.emit('data', data);
    }
};

LineParser.prototype.parseLine = function (line) {
    const header = line.match(/^(\S+) \((.+)\) (\S+); urgency=(\S+)/);
    if (header) {
        return {
            type: 'header',
            header: {
                sourcePackage: header[1],
                version: semver.parse(header[2]) || { raw: header[2] },
                distribution: header[3],
                urgency: header[4],
            }
        };
    }

    const logLine = line.match(/^ +\* (.*)/);
    if (logLine) {
        return {
            type: 'log',
            log: logLine[1],
        };
    }

    const author = line.match(/^ +-- (.*) <(.*)> {2}(.*)$/);
    if (author) {
        return {
            type: 'footer',
            author: {
                name: author[1],
                email: author[2],
            },
            date: new Date(author[3])
        };
    }

    return null;
};

LineParser.prototype.onClose = function () {
    this.emit('end');
};

function createLineParser(stream) {
    return new LineParser(stream);
}
exports.createLineParser = createLineParser;
