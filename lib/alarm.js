'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const crypto = require('crypto');
const Device = require('./device');
const EventEmitter = require('events');
const Joi = require('joi');
const ObjectID = require('bson-objectid');
const permission = require('./permission');
const Promise = require('bluebird');
const schedule = require('./schedule');
const util = require('util');

const jwt = Promise.promisifyAll(require('jsonwebtoken'));

const randomBytesAsync = Promise.promisify(crypto.randomBytes);

// Key used to sign arming tokens with jwt.
const privateKey = crypto.randomBytes(20).toString('hex');

const DEFAULT_GENERATED_PIN_LENGTH = 6;

const Alarm = new EventEmitter();

Alarm.views = {
    update(context) {
        context = context.getAppContext();
        return context.collection('alarms').createWithIndices([])
        .then(() => context.collection('zones').createWithIndices([{
            name: 'sectors_idx',
            fields: ['alarmSystem'],
        }]))
        .then(() => context.collection('sectors').createWithIndices([{
            name: 'zones_idx',
            fields: ['alarmSystem'],
        }, {
            name: 'zones_device_idx',
            fields: ['device', 'zoneStatus'],
        }]))
        .then(() => context.collection('pins').createWithIndices([{
            name: 'pins_idx',
            fields: ['alarm', 'pin'],
        }]));
    }
};

Alarm.minimumDelay = 1;

Alarm.systems = {};

function System(doc) {
    this._doc = {
        type: 'alarmSystem',
        status: 'disarmed',
        timestamp: new Date().toISOString()
    };
    this.zones = {};
    this.sectors = {};
    this.access = {};
    this.tokens = {};
    this.eventQueue = [];

    _.merge(this._doc, doc);
}
Alarm.System = System;

System.create = function (context, doc) {
    doc = _.omit(doc, ['status', 'type']);

    const system = new System({});
    _.merge(system._doc, { _id: ObjectID().toString() }, doc, {
        gateway: context.gatewayId,
        type: 'alarmSystem',
    });
    return context.collection('alarms').insert(system._doc).then(result => {
        const credentials = permission.systemGroupCredentials(context);
        return permission.grant(context, credentials, {
            type: 'alarm',
            resource: system._doc._id,
        }).then(() => context.emitEvent({
            type: 'create',
            path: `/alarms/${system._doc._id}`,
            resource: system.toJSON(),
        })).then(() => system.start(context));
    });
};

System.get = function (id) {
    const system = Alarm.systems[id];
    if (!system) {
        throw Boom.customCode(404, 'alarm system not found', 'ALARM_SYSTEM_NOT_FOUND');
    }
    return system;
};

System.startAll = function (context) {
    return loadSystems(context).each(system => system.start(context));
};

System.prototype.start = function (context) {
    return Promise.all(_.map(this.zones, zone => zone.start(context, this))).then(() => {
        _.forEach(this.sectors, sector => {
            sector.system = this;
        });
        Alarm.systems[this._doc._id] = this;
        Alarm.emit('register', this);
        return this;
    });
};

System.prototype.stop = function (context) {
    delete Alarm.systems[this._doc._id];
    _.forEach(this.zones, zone => zone.stop(context));
    Alarm.emit('unregister', this);
};

System.prototype.generateToken = function (context, pin) {
    let entry = null;
    return context.database.query(`
    SELECT doc
      FROM pins
     WHERE json_extract(doc, '$.alarm') = ?1 AND
           json_extract(doc, '$.pin') = ?2
    `, [this._doc._id, pin], row => entry = row.doc)
    .then(() => {
        if (!entry) {
            throw Boom.customCode(400, 'Invalid PIN', 'ALARM_PIN_INVALID');
        }

        return JSON.parse(entry);
    }).tap(entry => {
        if (entry.expiry && entry.expiry !== 'never') {
            const expiry = new Date(entry.expiry);
            if (expiry < new Date()) {
                throw Boom.customCode(400, 'Invalid PIN', 'ALARM_PIN_INVALID');
            }
        }

        if (entry.schedule) {
            return schedule.isValid(context, entry.schedule)
            .then(isValid => {
                if (!isValid) {
                    throw Boom.customCode(400, 'Invalid PIN', 'ALARM_PIN_INVALID');
                }
            });
        }
    }).then(entry => {
        const accessId = entry._id;
        const exp = Math.floor(Date.now() / 1000) + 60;
        const token = jwt.sign({ accessId, exp }, privateKey);
        this.tokens[token] = _.omit(entry, 'pin', 'alarm');
        return token;
    });
};

System.prototype.verifyToken = function (token) {
    const tokenEntry = this.tokens[token];
    delete this.tokens[token];
    return jwt.verifyAsync(token, privateKey).then(token => {
        const now = Date.now() / 1000;
        const expiry = token.exp;
        if (!tokenEntry || expiry < now) {
            throw Boom.customCode(403, 'invalid token', 'ALARM_TOKEN_INVALID');
        }
        return tokenEntry;
    }).catch(() => {
        throw Boom.customCode(403, 'invalid token', 'ALARM_TOKEN_INVALID');
    });
};

System.prototype.update = function (context, doc) {
    const sensitiveFields = [ // Fields we should not modify here.
        'type',   // [obsolete] Tag for Couch views.
        'status', // Managed by arming.
        'timestamp', // Updated by arming.
        'users',  // [obsolete] Managed by PIN access.
        '_id',    // Couch/Mongo ID, read-only.
        '_rev',   // [obsolete] Couch revision.
        '__v',    // Mongo version field.
    ];

    if (this.txn) {
        throw errorArmingConflict();
    }

    doc = _.omit(doc, sensitiveFields);
    const oldDoc = _.omit(this._doc, sensitiveFields);
    if (!_.isEqual(doc, oldDoc)) {
        this._doc = _.merge(this._doc, doc);
        return context.collection('alarms').update(this._doc, this._doc._id).then(() => {
            return context.emitEvent({
                type: 'update',
                path: `/alarms/${this._doc._id}`,
                resource: _.omit(this._doc, ['_rev', 'type']),
            }).return(this);
        });
    }
};

System.prototype.arm = function (context, status, options) {
    if (this.txn) {
        throw errorArmingConflict();
    }

    if (!_.includes(['disarmed', 'armed', 'perimeter', 'sector', 'suspend'], status)) {
        throw errorInvalidStatus(status);
    }

    options = _.merge({
        bypassed: [],
        sectors: [],
        timeout: 5*60,
        rearm: false,
        zones: [],
        skipExitDelays: status === 'perimeter',
    }, options);

    options.arming = status !== 'suspend';

    switch (status) {
    case 'disarmed':  break;
    case 'armed':     this.expectStatus(['disarmed', 'sector']); break;
    case 'perimeter': this.expectStatus(['disarmed']); break;
    case 'sector':    this.expectStatus(['disarmed', 'sector', 'armed']); break;
    case 'suspend':   this.expectStatus(['disarmed']); break;
    }

    // Set a transaction object. This will also pause incoming events to zones.
    this.txn = { commits: [], errors: [] };

    const spec = armingSpecification[status](this, options);
    _.forEach(spec.commands, cmd => {
        const result = zoneTxn(cmd.zone, cmd.command, options);
        result.zone = (typeof cmd.zone === 'object') ? cmd.zone.toJSON() : cmd.zone;
        if (result.error) {
            this.txn.errors.push(result);
        } else if (result.commit) {
            this.txn.commits.push(result);
        } else {
            // Anything without error or something to commit can be ignored.
        }
    });

    return commitArming(this, context, status, spec.sectors, options.access).finally(() => {
        // Flush any events that were queued.
        this.txn = undefined;
        const queue = this.eventQueue;
        this.eventQueue = [];
        return Promise.mapSeries(queue, args => {
            // Replace possibly stale transaction context.
            if (args[0].transaction) {
                args[0] = context;
            }
            return Promise.try(() => zoneEvent.apply(null, args))
            .catch(error => {
                context.log && context.log.warn(error);
            });
        });
    });
};

System.prototype.delete = function (context) {
    this.expectStatus('disarmed');

    this.stop(context);

    return context.database.query(`
        DELETE FROM sectors
        WHERE json_extract(doc, '$.alarmSystem') = ?1
        `, [this._doc._id])
    .then(() => context.database.query(`
        DELETE FROM zones
        WHERE json_extract(doc, '$.alarmSystem') = ?1
        `, [this._doc._id]))
    .then(() => context.database.query(`
        DELETE FROM pins
        WHERE json_extract(doc, '$.alarm') = ?1
        `, [this._doc._id]))
    .then(() => context.database.query(`
        DELETE FROM alarms
        WHERE _id = ?1
        `, [this._doc._id]))
    .then(() => {
        return context.emitEvent({
            type: 'delete',
            path: `/alarms/${this._doc._id}`,
            resource: {}
        });
    });
};

System.prototype.expectStatus = function (status) {
    if (typeof status === 'string') {
        status = [status];
    }

    if (_.includes(status, this._doc.status)) {
        return this;
    }

    throw Boom.customCode(409, 'action not allowed with current system status', 'ALARM_STATUS_ERROR', [], {
        expectedStatus: status,
        actualStatus: this._doc.status,
    });
};

System.prototype.toJSON = function () {
    return _.merge(_.omit(this._doc, ['_rev', 'type']), {
        zones: _.map(this.zones, zone => zone.toJSON()),
        sectors: _.map(this.sectors, sector => sector.toJSON()),
    });
};

function Access(system, doc) {
    this._doc = _.merge({
        pin: null,
        name: 'New PIN',
        schedule: null,
        expiry: 'never',
        user: null,
    }, doc);
    this.system = system;
    system.access[this._doc._id] = this;
}
Alarm.Access = Access;

Access.schema = Joi.object({
    pin: Joi.alternatives().try(
        Joi.string().regex(/^[0-9]{4,12}$/),
        Joi.object().keys({
            length: Joi.number().min(4).max(12).default(DEFAULT_GENERATED_PIN_LENGTH),
        })
    ).default({ length: DEFAULT_GENERATED_PIN_LENGTH }),
    name: Joi.string().required(),
    schedule: Joi.alternatives().try(Joi.string(), Joi.valid(null)).optional(),
    expiry: Joi.alternatives().try(Joi.string().isoDate(), Joi.string().valid('never')).optional(),
    user: Joi.alternatives().try(Joi.string(), Joi.valid(null)).optional(),
});

// Generate a new unused PIN. To guarantee its usability for creating/updating
// an access record, this must be called from a transaction context.
Access.generatePIN = function (context, length) {
    const pins = [];
    return context.database.query(
        "SELECT json_extract(doc, '$.pin') AS pin FROM pins",
        row => pins.push(row.pin)
    ).then(() => {
        if (!Number.isSafeInteger(length)) {
            length = DEFAULT_GENERATED_PIN_LENGTH;
        } else {
            length = Math.max(4, Math.min(length, 12));
        }

        return generatePIN();

        function generatePIN() {
            return randomBytesAsync(length)
            .then(bytes => {
                const pin = _.map(bytes, x => (x * 10 / 256) | 0).join('');
                if (_.includes(pins, pin)) {
                    return generatePIN();
                }
                return pin;
            });
        }
    });
};

Access.create = function (context, system, doc) {
    return context.withTransactionContext(context => {
        return new Promise((resolve, reject) => {
            Joi.validate(doc, Access.schema, (err, value) => {
                if (err) reject(err);
                resolve(value);
            });
        }).tap(doc => {
            if (typeof doc.pin === 'object') {
                return Access.generatePIN(context, doc.pin.length)
                .then(pin => {
                    doc.pin = pin;
                });
            } else {
                // Verify PIN uniqueness.
                let exists = false;
                return context.database.query(`
                SELECT 1
                FROM pins
                WHERE json_extract(doc, '$.alarm') = ?1
                AND json_extract(doc, '$.pin') = ?2
                `, [system._doc._id, doc.pin], row => exists = true)
                .then(() => {
                    if (exists) {
                        throw Boom.customCode(400, 'Invalid PIN', 'ALARM_PIN_INVALID');
                    }
                });
            }
        }).then(doc => {
            return new Access(system, _.merge(doc, {
                _id: new ObjectID().toString(),
                alarm: system._doc._id,
            }));
        }).tap(access => {
            return context.collection('pins').insert(access._doc);
        }).tap(access => {
            return context.emitEvent({
                type: 'create',
                path: `/alarms/${system._doc._id}/access/${access._doc._id}`,
                resource: access.toJSON(),
            });
        });
    });
};

Access.get = function (systemId, id) {
    const system = System.get(systemId);
    const access = system.access[id];
    if (!access) {
        throw Boom.notFound();
    }
    return access;
};

Access.getByUser = function (systemId, userId) {
    const system = System.get(systemId);
    for (const id in system.access) {
        const access = system.access[id];
        if (access._doc.user === userId) {
            return access;
        }
    }
    throw Boom.notFound();
};

Access.prototype.update = function (context, doc) {
    return context.withTransactionContext(context => {
        return new Promise((resolve, reject) => {
            // Input can be a partial object; fill in properties.
            doc = _.merge({}, this._doc, _.omit(doc, '_id', 'alarm'));

            Joi.validate(doc, Access.schema, { stripUnknown: true }, (err, value) => {
                if (err) reject(err);
                else resolve(value);
            });
        }).tap(doc => {
            if (typeof doc.pin === 'object') {
                return Access.generatePIN(context, doc.pin.length)
                .then(pin => {
                    doc.pin = pin;
                });
            } else {
                let isDuplicate = false;
                return context.database.query(`
                SELECT _id
                FROM pins
                WHERE json_extract(doc, '$.alarm') = ?1
                AND json_extract(doc, '$.pin') = ?2
                `, [this._doc.alarm, doc.pin], row => isDuplicate = isDuplicate || row._id !== this._doc._id)
                .then(() => {
                    if (isDuplicate) {
                        throw Boom.customCode(400, 'Invalid PIN', 'ALARM_PIN_INVALID');
                    }
                });
            }
        }).then(doc => {
            _.merge(this._doc, doc);
            return context.collection('pins').update(this._doc, this._doc._id);
        }).then(() => {
            return context.emitEvent({
                type: 'update',
                path: `/alarms/${this._doc.alarm}/access/${this._doc._id}`,
                resource: this.toJSON(),
            });
        }).return(this);
    });
};

Access.prototype.delete = function (context) {
    return context.collection('pins').delete(this._doc._id)
    .then(() => {
        delete this.system.access[this._doc._id];
    }).then(() => context.emitEvent({
        type: 'delete',
        path: `/alarms/${this._doc.alarm}/access/${this._doc._id}`,
        resource: {},
    }));
};

Access.prototype.toJSON = function () {
    return _.omit(this._doc, 'alarm', 'pin');
};

function Sector(doc) {
    this._doc = {
        type: 'sector',
        name: '',
        armed: false,
        zones: [],
        timestamp: new Date().toISOString(),
    };
    _.merge(this._doc, doc);
}
Alarm.Sector = Sector;

Sector.create = function (context, system, doc) {
    doc = doc || {};

    system.expectStatus('disarmed');

    const sector = new Sector({});
    _.merge(sector._doc, { _id: ObjectID().toString() }, doc, {
        type: 'sector',
        alarmSystem: system._doc._id,
    });
    return context.collection('sectors').insert(sector._doc).then(() => {
        return context.emitEvent({
            type: 'create',
            path: `/alarms/${system._doc._id}/sectors/${sector._doc._id}`,
            resource: sector.toJSON(),
        }).then(() => {
            system.sectors[sector._doc._id] = sector;
            sector.system = system;
            return sector;
        });
    });
};

Sector.get = function (systemId, id) {
    const system = System.get(systemId);
    const sector = system.sectors[id];
    if (!sector) {
        throw Boom.customCode(404, 'sector not found', 'SECTOR_NOT_FOUND');
    }
    return sector;
};

Sector.prototype.include = function (context, zone) {
    this.system.expectStatus('disarmed');

    if (_.includes(this._doc.zones, zone._doc._id)) {
        return this._doc.zones;
    }

    this._doc.zones = _.union(this._doc.zones, [zone._doc._id]);
    return context.collection('sectors').update(this._doc, this._doc._id).then(() => {
        return context.emitEvent({
            type: 'update',
            path: `/alarms/${this.system._doc._id}/sectors/${this._doc._id}`,
            resource: { zones: this._doc.zones },
        }).return(this._doc.zones);
    });
};

Sector.prototype.exclude = function (context, zone) {
    this.system.expectStatus('disarmed');

    if (_.includes(this._doc.zones, zone._doc._id)) {
        this._doc.zones = _.without(this._doc.zones, zone._doc._id);
        return context.collection('sectors').update(this._doc, this._doc._id).then(() => {
            return context.emitEvent({
                type: 'update',
                path: `/alarms/${this.system._doc._id}/sectors/${this._doc._id}`,
                resource: { zones: this._doc.zones }
            }).return(this._doc.zones);
        });
    }

    throw Boom.customCode(404, 'sector does not include that zone', 'SECTOR_ZONE_NOT_FOUND');
};

Sector.prototype.update = function (context, doc) {
    this.system.expectStatus('disarmed');

    _.merge(this._doc, _.pick(doc, ['name']));
    return context.collection('sectors').update(this._doc, this._doc._id).then(() => {
        return context.emitEvent({
            type: 'update',
            path: `/alarms/${this.system._doc._id}/sectors/${this._doc._id}`,
            resource: this.toJSON(),
        }).return(this);
    });
};

Sector.prototype.delete = function (context) {
    this.system.expectStatus('disarmed');

    return context.collection('sectors').delete(this._doc._id).then(() => {
        delete this.system.sectors[this._doc._id];
        return context.emitEvent({
            type: 'delete',
            path: `/alarms/${this.system._doc._id}/sectors/${this._doc._id}`,
            resource: {}
        });
    });
};

Sector.prototype.toJSON = function () {
    return _.omit(this._doc, ['_rev', 'type']);
};

function Zone(doc) {
    this._doc = {
        type: 'zone',
        exitDelay: 0,
        entryDelay: 0,
        perimeter: false,
        is24hours: false,
        sensorStatus: 'normal',
        zoneStatus: 'disarmed',
        timestamp: new Date().toISOString(),
    };
    _.merge(this._doc, doc);
}
Alarm.Zone = Zone;

Zone.BadStateError = function (zone, event) {
    Error.call(this);
    Error.captureStackTrace(this, this.constructor);
    this.name = 'Alarm.Zone.BadStateError';
    this.message = event;
    this.zone = zone;
};
util.inherits(Zone.BadStateError, Error);

Zone.create = function (context, system, doc) {
    doc = doc || {};

    system.expectStatus('disarmed');

    return Device.load(context, doc.device).then(device => {
        const cap = device.caps[doc.capability];
        const model = _.findWhere(device.model.capabilities, { capabilityId: doc.capability });
        if (!cap || model.type !== 'enum' || !_.includes(model.range, doc.normalState)) {
            throw Boom.customCode(400, 'invalid capability for zone', 'ZONE_BAD_CAP', [], {
                capability: doc.capability || null,
                normalState: doc.normalState || null,
                range: model ? model.range : undefined
            });
        }

        // Cannot create 24/7 zones if the cap is not in its normalState.
        if (doc.is24hours && cap.value !== doc.normalState) {
            throw Boom.customCode(400, 'new zone capability value must be in normal state', 'ZONE_BAD_STATE', [], {
                capability: doc.capability,
                normalState: doc.normalState,
                value: cap.value,
            });
        }

        // Only one zone can exist for a specific device/capability in an alarm system.
        const dup = _.findWhere(system.zones, { _doc: { device: doc.device, capability: doc.capability }});
        if (dup) {
            throw Boom.customCode(409, 'a zone for this device capability already exists', 'ZONE_DUPLICATE', [], {
                zone: dup.toJSON()
            });
        }

        const zone = new Zone({});
        _.merge(zone._doc, { _id: ObjectID().toString() }, doc, {
            type: 'zone',
            alarmSystem: system._doc._id,
            zoneStatus: doc.is24hours ? 'armed': 'disarmed',
            sensorStatus: cap.value === doc.normalState ? 'normal' : 'tripped',
        });

        return context.collection('zones').insert(zone._doc).then(() => {
            return context.emitEvent({
                type: 'create',
                path: `/alarms/${system._doc._id}/zones/${zone._doc._id}`,
                resource: zone.toJSON(),
            }).then(() => {
                system.zones[zone._doc._id] = zone;
                return zone.start(context, system).return(zone);
            });
        });
    });
};

Zone.get = function (systemId, id) {
    const system = System.get(systemId);
    const zone = system.zones[id];
    if (!zone) {
        throw Boom.customCode(404, 'zone not found', 'ZONE_NOT_FOUND');
    }
    return zone;
};

Zone.prototype.start = function (context, system) {
    // Update self based on device caps, set up timeouts for any
    // delays, and register listeners for device events.
    this.system = system;
    return Device.load(context, this._doc.device).then(device => {
        const pathRE = new RegExp(`^/devices/${this._doc.device}(/caps/${this._doc.capability})?`);
        this.listener = context.events.onEventPathMatch(pathRE, (context, matches, event) => {
            return context.withTransactionContext(context => {
                return zoneEvent(context, this, matches, event);
            })
            .catch(error => {
                Alarm.emit('error', error);
            });
        });

        setZoneTimeout(context, this);

        return zoneSync(context, this, device);
    });
};

Zone.prototype.stop = function (context) {
    if (this.listener) {
        context.events.removeEventListener(this.listener);
        this.listener = undefined;
    }

    if (this.timer) {
        clearTimeout(this.timer);
        this.timer = null;
    }
};

Zone.prototype.update = function (context, doc) {
    this.system.expectStatus('disarmed');

    if (this._doc.is24hours) {
        // Nothing can be edited on 24-hour zones.
        throw Boom.customCode(405, '24/7 zones cannot be modified', 'ZONE_24_READONLY');
    }

    _.merge(this._doc, _.pick(doc, ['perimeter', 'entryDelay', 'exitDelay']));
    return context.collection('zones').update(this._doc, this._doc._id).then(() => {
        return context.emitEvent({
            type: 'update',
            path: `/alarms/${this.system._doc._id}/zones/${this._doc._id}`,
            resource: this.toJSON(),
        }).return(this);
    });
};

Zone.prototype.delete = function (context) {
    this.system.expectStatus('disarmed');

    this.stop(context);

    // Remove this zone from any sectors.
    return Promise.all(_.map(this.system.sectors, sector => {
        return Promise.try(() => sector.exclude(context, this)).catch(error => {
            // Ignore errors or 'not_found' errors in exclusion.
        });
    })).then(() => {
        return context.collection('zones').delete(this._doc._id);
    }).then(() => {
        delete this.system.zones[this._doc._id];
        return context.emitEvent({
            type: 'delete',
            path: `/alarms/${this.system._doc._id}/zones/${this._doc._id}`,
            resource: {}
        });
    });
};

Zone.prototype.toJSON = function () {
    return _.omit(this._doc, ['_rev', 'type']);
};

function loadSystems(context) {
    const results = [];
    return context.database.query(`
    SELECT alarms.doc AS alarm,
           COALESCE(zones.docs, '[]') AS zones,
           COALESCE(sectors.docs, '[]') AS sectors,
           COALESCE(pins.docs, '[]') AS pins
      FROM alarms
           LEFT JOIN
           (
               SELECT json_group_array(json(zones.doc)) AS docs,
                      json_extract(zones.doc, '$.alarmSystem') AS system
                 FROM zones
                GROUP BY system
           )
           AS zones ON alarms._id = zones.system
           LEFT JOIN
           (
               SELECT json_group_array(json(sectors.doc)) AS docs,
                      json_extract(sectors.doc, '$.alarmSystem') AS system
                 FROM sectors
                GROUP BY system
           )
           AS sectors ON alarms._id = sectors.system
           LEFT JOIN
           (
               SELECT json_group_array(json(pins.doc)) AS docs,
                      json_extract(pins.doc, '$.alarm') AS system
                 FROM pins
                GROUP BY system
           )
           AS pins ON alarms._id = pins.system
    `, [], row => results.push(row))
    .return(results)
    .map(loadSystemFromRows);
}

function loadSystemFromRows(row) {
    row.zones = JSON.parse(row.zones);
    row.sectors = JSON.parse(row.sectors);
    row.pins = JSON.parse(row.pins);

    row.alarm = JSON.parse(row.alarm);

    const system = new System(row.alarm);
    _.forEach(row.zones, zone => {
        system.zones[zone._id] = new Zone(zone);
    });
    _.forEach(row.sectors, sector => {
        system.sectors[sector._id] = new Sector(sector);
    });
    _.forEach(row.pins, pin => {
        system.access[pin._id] = new Access(system, pin);
    });

    return system;
}

function zoneEvent(context, zone, matches, event) {
    if (zone.system.txn) {
        // Currently in a transaction, queue up for later.
        const args = Array.prototype.slice.call(arguments);
        zone.system.eventQueue.push(args);
        return;
    }

    if (matches[1]) { // This is a cap event.
        if (zone._doc.sensorStatus === 'trouble') {
            // We shouldn't see cap changes in a failed state?
            return;
        }
        if (event.resource.value === zone._doc.normalState) {
            return sensorStatus(context, zone, 'normal');
        } else {
            return sensorStatus(context, zone, 'tripped');
        }
    } else { // This is a general device event.
        if (event.type === 'delete') {
            zone.stop(context);

            // Remove the zone from any sectors.
            return Promise.all(_.map(zone.system.sectors, sector => {
                return Promise.try(() => sector.exclude(context, zone));
            })).catch(error => {
                // Ignore errors or 'not_found' errors in exclusion.
            }).then(() => context.collection('zones').delete(zone._doc._id)).then(() => {
                delete zone.system.zones[zone._doc._id];
                return context.emitEvent({
                    type: 'delete',
                    path: `/alarms/${zone.system._doc._id}/zones/${zone._doc._id}`,
                    resource: {},
                });
            });
        } else if (event.type === 'update' && event.resource.status) {
            if (event.resource.status === 'FAILED') {
                return sensorStatus(context, zone, 'trouble');
            } else if (event.resource.status === 'ACTIVE') {
                return Device.load(context, zone._doc.device).then(device => {
                    if (device.caps[zone._doc.capability].value === zone._doc.normalState) {
                        return sensorStatus(context, zone, 'normal');
                    } else {
                        return sensorStatus(context, zone, 'tripped');
                    }
                });
            }
        }
    }
}

function zoneSync(context, zone, device) {
    if (device.status === 'FAILED') {
        if (zone.sensorStatus !== 'trouble') {
            return sensorStatus(context, zone, 'trouble');
        }
    } else {
        const cap = device.caps[zone._doc.capability];
        if (zone._doc.sensorStatus === 'trouble') {
            if (cap.value === zone._doc.normalState) {
                return sensorStatus(context, zone, 'normal');
            } else {
                return sensorStatus(context, zone, 'tripped');
            }
        } else if (zone._doc.sensorStatus === 'normal') {
            if (cap.value !== zone._doc.normalState) {
                return sensorStatus(context, zone, 'tripped');
            }
        } else if (cap.value === zone._doc.normalState) {
            return sensorStatus(context, zone, 'normal');
        }
    }
    return Promise.resolve(zone);
}

function sensorStatus(context, zone, status) {
    if (status === zone._doc.sensorStatus) {
        return zone;
    }

    zone._doc.sensorStatus = status;
    zone._doc.timestamp = new Date().toISOString();
    return context.collection('zones').update(zone._doc, zone._doc._id).then(() => {
        const event = {
            _id: ObjectID().toString(),
            type: 'update',
            path: '/alarms/' + zone._doc.alarmSystem + '/zones/' + zone._doc._id,
            resource: { sensorStatus: status },
            timestamp: zone._doc.timestamp,
        };
        return context.emitEvent(event);
    }).then(event => {
        const ctx = context.createEventContext(event);

        if (zone._doc.is24hours) {
            switch (zone._doc.zoneStatus) {
            case 'armed':
                if (status !== 'normal') {
                    return zoneStatus(ctx, zone, 'alarm');
                }
                break;
            case 'alarm':
                break;
            case 'suspended':
                if (status === 'normal' && zone._doc.rearm) {
                    return zoneStatus(ctx, zone, 'armed');
                }
                break;
            }
        } else {
            switch (zone._doc.zoneStatus) {
            case 'disarmed':
                break;
            case 'bypassed':
                break;
            case 'exitDelay':
            case 'entryDelay':
                if (status === 'trouble') {
                    return zoneStatus(ctx, zone, 'alarm');
                }
                break;
            case 'armed':
                if (status === 'trouble') {
                    return zoneStatus(ctx, zone, 'alarm');
                } else if (status === 'tripped') {
                    return zoneStatus(ctx, zone, 'entryDelay');
                }
                break;
            case 'alarm':
                break;
            }
        }
        return zone;
    });
}

function zoneStatus(context, zone, status, options) {
    options = options || {};

    // Use negated comparisons so coerced NaN results are handled correctly.
    if (status === 'exitDelay' && (!(zone._doc.exitDelay >= Alarm.minimumDelay) || options.skipExitDelays)) {
        status = 'armed';
    } else if (status === 'entryDelay' && !(zone._doc.entryDelay >= Alarm.minimumDelay)) {
        status = 'alarm';
    }

    if (zone.timer) {
        clearTimeout(zone.timer);
        zone.timer = undefined;
    }

    if (status === zone._doc.zoneStatus) {
        if (!zone.system.txn) {
            setZoneTimeout(context, zone);
        }
        return zone;
    }

    zone._doc.zoneStatus = status;
    zone._doc.timestamp = new Date().toISOString();
    zone._doc.delayUntil = undefined;

    if (status === 'entryDelay' || status === 'exitDelay') {
        const date = new Date(zone._doc.timestamp);
        date.setMilliseconds(date.getMilliseconds() + Number(zone._doc[status]) * 1000);
        zone._doc.delayUntil = date.toISOString();
    } else if (status === 'suspended') {
        const date = new Date(zone._doc.timestamp);
        date.setMilliseconds(date.getMilliseconds() + Number(options.timeout) * 1000);
        zone._doc.delayUntil = date.toISOString();
        zone._doc.rearm = options.rearm;
    }

    return context.collection('zones').update(zone._doc, zone._doc._id).then(() => {
        if (!options.arming) {
            return context.emitEvent({
                type: 'update',
                path: '/alarms/' + zone._doc.alarmSystem + '/zones/' + zone._doc._id,
                resource: { zoneStatus: status },
                timestamp: zone._doc.timestamp
            });
        }
    }).then(() => {
        if (!zone.system.txn) {
            setZoneTimeout(context, zone);
        }

        return zone;
    });
}

function setZoneTimeout(context, zone) {
    if (zone._doc.delayUntil && !zone.timer) {
        const date = new Date(zone._doc.delayUntil).getTime();
        const t = Math.max(1, date - Date.now());
        zone.timer = context.getServerContext().setTimeout(zone._doc.zoneStatus, zoneTimeout, t, zone);
    }
}

function zoneTimeout(context, zone) {
    zone.timer = undefined;
    zone._doc.delayUntil = undefined;

    return context.withTransactionContext(context => {
        switch (zone._doc.zoneStatus) {
        case 'exitDelay':
            if (zone._doc.sensorStatus === 'normal') {
                return zoneStatus(context, zone, 'armed');
            } else {
                return zoneStatus(context, zone, 'alarm');
            }
        case 'entryDelay':
            return zoneStatus(context, zone, 'alarm');
        case 'suspended':
            if (zone._doc.sensorStatus === 'normal') {
                return zoneStatus(context, zone, 'armed');
            } else {
                return zoneStatus(context, zone, 'alarm');
            }
        default:
            Alarm.emit('error', new Zone.BadStateError(zone, 'timeout'));
        }
    });
}

function zoneTxn(zone, command, options) {
    if (typeof zone !== 'object') {
        return { error: 'not_found' };
    }
    if (zone._doc.is24hours) {
        if (command === 'disarm' && zone._doc.zoneStatus === 'alarm' && zone._doc.sensorStatus === 'normal') {
            return { commit: ctx => zoneStatus(ctx, zone, 'armed', options) };
        } else if (command === 'disarm' || command === 'arm') {
            return { };
        } else if (command === 'suspend' && zone._doc.zoneStatus === 'alarm') {
            return { commit: ctx => zoneStatus(ctx, zone, 'suspended', options) };
        } else {
            return { error: 'command', command };
        }
    } else {
        switch (zone._doc.zoneStatus) {
        case 'disarmed':
            if (command === 'disarm') {
                return { };
            } else if (command === 'arm' && zone._doc.sensorStatus === 'normal') {
                return { commit: ctx => zoneStatus(ctx, zone, 'exitDelay', options) };
            } else if (command === 'arm') {
                return { error: 'sensorStatus' };
            } else if (command === 'bypass') {
                return { commit: ctx => zoneStatus(ctx, zone, 'bypassed', options) };
            } else {
                return { error: 'command', command };
            }
        case 'bypassed':
            if (command === 'disarm') {
                return { commit: ctx => zoneStatus(ctx, zone, 'disarmed', options) };
            } else {
                return { error: 'command', command };
            }
        case 'exitDelay':
            if (command === 'disarm') {
                return { commit: ctx => zoneStatus(ctx, zone, 'disarmed', options) };
            } else if (command === 'arm') {
                return { };
            } else {
                return { error: 'command', command };
            }
        case 'armed':
            if (command === 'disarm') {
                return { commit: ctx => zoneStatus(ctx, zone, 'disarmed', options) };
            } else {
                return { error: 'command', command };
            }
        case 'entryDelay':
            if (command === 'disarm') {
                return { commit: ctx => zoneStatus(ctx, zone, 'disarmed', options) };
            } else {
                return { error: 'command', command };
            }
        case 'alarm':
            if (command === 'disarm') {
                return { commit: ctx => zoneStatus(ctx, zone, 'disarmed', options) };
            } else {
                return { error: 'command', command };
            }
        }
    }
}

const armingSpecification = {
    disarmed: function (system, options) {
        return { commands: _.map(system.zones, function (zone) {
            return { command: 'disarm', zone: zone };
        }) };
    },
    armed: function (system, options) {
        return { commands: _.map(system.zones, function (zone) {
            return { command: _.includes(options.bypassed, zone._doc._id) ? 'bypass' : 'arm', zone: zone };
        }) };
    },
    perimeter: function (system, options) {
        var commands = [];
        _.forEach(system.zones, function (zone) {
            if (zone._doc.perimeter) {
                commands.push({ command: _.includes(options.bypassed, zone._doc._id) ? 'bypass' : 'arm', zone: zone });
            }
        });
        return { commands: commands };
    },
    sector: function (system, options) {
        // Partition zones into three groups:
        //
        //   - zones that are armed and are not changing
        //   - zones that are going to be armed
        //   - zones that are going to be disarmed
        //
        // Zones can only be disarmed if they do not belong to one
        // of the first two groups.

        // Start by grouping the sectors according to the above.
        var sectors = _.reduce(system.sectors, function (result, sector) {
            var input = _.findWhere(options.sectors, { '_id': sector._doc._id });
            if ('undefined' === typeof input) {
                if (sector._doc.armed) {
                    result.armed.push(sector); // Armed and not changing
                }
            } else if (sector._doc.armed === input.armed) {
                if (input.armed) {
                    result.armed.push(sector); // Armed and not changing
                }
            } else if (input.armed) {
                result.arm.push(sector); // Going to arm
            } else {
                result.disarm.push(sector); // Going to disarm
            }
            return result;
        }, {
            arm: [],    // Going to arm
            disarm: [], // Going to disarm
            armed: []   // Armed and not changing
        });

        function collectZones(accum, sector) {
            _.forEach(sector._doc.zones, function (id) {
                accum[id] = system.zones[id];
            });
            return accum;
        }

        function subtractZones(from, zones) {
            _.forEach(zones, function (zone, id) {
                delete from[id];
            });
        }

        // Get all zones in the arming and disarming sectors, and
        // the already armed sectors.
        var arm    = _.reduce(sectors.arm, collectZones, {});
        var disarm = _.reduce(sectors.disarm, collectZones, {});
        var armed  = _.reduce(sectors.armed, collectZones, {});

        // Remove unchanging zones from the two change groups.
        subtractZones(arm, armed);
        subtractZones(disarm, _.merge({}, arm, armed));

        // Make a list of commands.
        var commands = [];
        _.forEach(arm, function (zone, id) {
            commands.push({ command: _.includes(options.bypassed, id) ? 'bypass': 'arm', zone: zone });
        });
        _.forEach(disarm, function (zone) {
            commands.push({ command: 'disarm', zone: zone });
        });

        return { commands: commands, sectors: sectors };
    },
    suspend: function (system, options) {
        return { commands: _.map(options.zones, function (id) {
            return { command: 'suspend', zone: system.zones[id] || id || null };
        }) };
    }
};

const commitArming = Promise.method(function (system, context, status, sectors, access) {
    if (system.txn.errors.length > 0) {
        throw errorArmingErrors(system.txn.errors);
    }

    if (system.txn.commits.length === 0) {
        // Nothing to commit! No changes.
        return false;
    }

    return context.withTransactionContext(context => Promise.all(_.map(system.txn.commits, entry => entry.commit(context))).then(() => {
        if (status === 'sector') {
            // Check if a sector arm actually ended up arming or disarming all zones.
            const result = _.reduce(system.zones, (result, zone) => {
                if (zone._doc.is24hours) {
                    return result;
                } else if (zone._doc.zoneStatus === 'disarmed') {
                    return [false, result[1]];
                } else {
                    return [result[0], false];
                }
            }, [true, true]);
            if (result[0]) {
                status = 'armed';
            } else if (result[1]) {
                status = 'disarmed';
            }
        }

        if (status === 'suspend') {
            // Events are emitted by the individual zones being suspended.
            _.forEach(system.zones, zone => setZoneTimeout(context, zone));
            return system;
        }

        system._doc.status = status;
        return context.collection('alarms').update(system._doc, system._doc._id).then(() => {
            let changedSectors = [];
            if (status === 'sector') {
                sectors = sectors || { arm: [], disarm: [] };
                _.forEach(sectors.arm, sector => { sector._doc.armed = true; });
                _.forEach(sectors.disarm, sector => { sector._doc.armed = false; });
                changedSectors = sectors.arm.concat(sectors.disarm);
            } else if (status === 'armed') {
                _.forEach(system.sectors, sector => { sector._doc.armed = true; });
                changedSectors = _.map(system.sectors);
            } else if (status === 'disarmed') {
                _.forEach(system.sectors, sector => { sector._doc.armed = false; });
                changedSectors = _.map(system.sectors);
            }

            if (changedSectors.length > 0) {
                return Promise.map(changedSectors, sector => context.collection('sectors').update(sector._doc, sector._doc._id));
            }
        }).then(() => {
            system._doc.timestamp = new Date().toISOString();
            return context.emitEvent({
                type: 'update',
                path: `/alarms/${system._doc._id}`,
                resource: system.toJSON(),
                access: access,
            }).then(() => {
                _.forEach(system.zones, zone => setZoneTimeout(context, zone));
                return system;
            });
        });
    }));
});

function errorInvalidStatus(invalidStatus) {
    return Boom.customCode(
        400, 'Invalid status', 'ALARM_INVALID_STATUS', [], {
            status: invalidStatus
        }
    );
}

function errorArmingConflict() {
    return Boom.customCode(
        409, 'Another user is arming the system', 'ALARM_ARMING_CONFLICT', [], {}
    );
}

function errorArmingErrors(zoneErrors) {
    return Boom.customCode(
        409, 'Some zones cannot be armed', 'ALARM_ZONES_CANNOT_ARM', [], {
            errors: zoneErrors
        }
    );
}

module.exports = Alarm;
