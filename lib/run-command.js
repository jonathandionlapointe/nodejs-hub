'use strict';

const os = require('os');
const Promise = require('bluebird');
const spawn = require('child_process').spawn;

/** Run a command, and return its output.
 *
 * @param {!string} command The executable
 * @param {!string[]} args The arguments to pass
 * @param {?{ ignoreStdout: ?boolean, ignoreStderr: ?boolean, splitLines: ?(boolean|string) }} options
 *
 * @return {Promise<{ stdout: (Buffer|string[]), stderr: (Buffer|string[]) }>}
 *
 * Rejected if the command exits with a non-zero exit code. The error
 * includes the properties:
 *
 * - message {string} "non-zero exit code"
 * - code {number} the exit code
 * - stdout {Buffer} the data written to stdout
 * - stderr {Buffer} the data written to stderr
 *
 * The input options are:
 *
 * - ignoreStdout: don't collect data written to stdout
 * - ignoreStderr: don't collect data written to stderr
 * - splitLines: return output as an array of lines. Either
 *   a boolean or a string to use as the "end-of-line"
 *   delimeter.
 */
function runCommand(command, args, options) {
    options = options || {};
    let stdout = new Buffer(0);
    let stderr = new Buffer(0);
    const process = spawn(command, args);
    if (!options.ignoreStdout) {
        process.stdout.on('data', data => { stdout = Buffer.concat([stdout, data]); });
    }
    if (!options.ignoreStderr) {
        process.stderr.on('data', data => { stderr = Buffer.concat([stderr, data]); });
    }
    return new Promise((resolve, reject) => {
        process.on('error', reject);
        process.on('close', (code, signal) => {
            if (code === null) {
                const error = new Error('killed by signal');
                error.code = signal;
                error.stdout = stdout;
                error.stderr = stderr;
                return reject(error);
            } else if (code !== 0) {
                const error = new Error('non-zero exit code');
                error.code = code;
                error.stdout = stdout;
                error.stderr = stderr;
                return reject(error);
            }
            if (options.splitLines) {
                let eol = os.EOL;
                if (typeof options.splitLines === 'string') {
                    eol = options.splitLines;
                }
                stdout = stdout.toString().split(eol);
                stderr = stderr.toString().split(eol);
            }
            return resolve({ stdout, stderr });
        });
    });
}

module.exports = runCommand;
