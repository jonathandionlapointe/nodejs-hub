'use strict';

const _ = require('lodash');
const Alarm = require('./alarm');
const Device = require('./device');
const permission = require('./permission');
const Promise = require('bluebird');
const scenario = require('../plugins/scenario/scenario');
const schedule = require('./schedule');

module.exports = function (context) {
    return getSyncData(context)
    .then(data => {
        context = context.createSynchronizationContext();
        return context.withTransactionContext(context => {
            return saveSyncData(context, data);
        });
    });
};

function getSyncData(context) {
    const sdk = context.sdk;
    return Promise.props({
        groups: resultData(sdk.http.get('/groups', { token: sdk.auth.token }), 'group'),
        permissions: resultData(sdk.permissions.list(), 'permission'),
        models: resultData(sdk.models.list(), 'model'),
        devices: resultData(sdk.devices.list(), 'device'),
        locations: resultData(sdk.locations.list(), 'location'),
        alarms: resultData(sdk.alarm.list(), 'alarm'),
        scenarios: resultData(sdk.scenarios.list(), 'scenario'),
        schedules: resultData(sdk.schedules.list(), 'schedule'),
        templates: resultData(sdk.templates.list(), 'template'),
    });

    function resultData(resultPromise, label, fallback) {
        if (fallback === undefined) {
            fallback = [];
        }
        return resultPromise.then(r => r.data, err => {
            context.server.log(['sync', 'debug'], {
                message: `Failed to fetch ${label} data`,
                reason: err.stack,
            });
            return fallback;
        });
    }
}

function saveSyncData(context, data) {
    // NOTE this relies on data being marked with a "status" property or
    // something that indicates whether a resource has been removed or has
    // expired. Fully deleted/missing entries will never be flushed on this
    // side!
    return context.withTransactionContext(context => {
        return Promise.each([
            () => permission.upsertGroups(context, data.groups).catch(err => {
                context.server.log(['sync', 'debug'], {
                    message: 'Failed to sync groups',
                    reason: err.stack
                });
            }),
            () => permission.upsertMany(context, data.permissions).catch(err => {
                context.server.log(['sync', 'debug'], {
                    message: 'Failed to sync permissions',
                    reason: err.stack
                });
            }),
            () => Promise.each(data.models, m => syncCachedData(context, 'model', m)),
            () => Promise.each(data.locations, loc => syncCachedData(context, 'location', loc, '/locations')),
            () => Promise.each(data.devices, d => syncDevice(context, d)),
            () => Promise.each(data.alarms, a => syncAlarm(context, a)),
            () => Promise.each(data.schedules, s => syncSchedule(context, s)),
            () => Promise.each(data.scenarios, s => syncScenario(context, s)),
            () => Promise.each(data.templates, t => syncCachedData(context, 'template', t, '/templates')),
        ], f => f());
    });
}

function syncDevice(context, doc) {
    if (doc._id === context.gatewayId) {
        const system = context.server.plugins['system'];
        return system.update(context, doc)
        .catch(err => {
            context.server.log(['sync', 'debug'], {
                message: 'Failed to sync gateway device',
                reason: err.stack,
            });
        });
    } else {
        return Device.load(context, doc._id)
        .then(device => {
            // NOTE This may trigger an event, which will be forwarded to the
            // API. The API/API clients may not expect this in the case where
            // this synchronization is happening after the fact (for example
            // after being offline for some time).
            return device.updateData(context, doc);
        })
        .catch(err => {
            context.server.log(['sync', 'debug'], {
                message: 'Failed to sync device',
                reason: err.stack,
            });
        });
    }
}

function syncCachedData(context, type, data, prefix) {
    const cache = context.server.plugins['cache'];

    return cache.get(context, type, data._id, () => Promise.resolve(null))
    .then(existingData => {
        return cache.inject(context, type, data._id, data)
        .catch(err => {
            context.server.log(['sync', 'debug'], {
                message: 'Failed to sync ' + type,
                reason: err.stack,
            });
        })
        .then(() => {
            if (!prefix) {
                return;
            }
            if (!existingData) {
                return context.emitEvent({
                    type: 'create',
                    path: `${prefix}/${data._id}`,
                    resource: data,
                });
            } else if (!_.isEqual(existingData, data)) {
                return context.emitEvent({
                    type: 'update',
                    path: `${prefix}/${data._id}`,
                    resource: data,
                });
            }
        });
    });
}

function syncAlarm(context, doc) {
    return Promise.try(() => Alarm.System.get(doc._id))
    .then(alarm => {
        return alarm.update(context, doc);
    })
    .catch(err => {
        context.server.log(['sync', 'debug'], {
            message: 'Failed to sync alarm system',
            reason: err.stack,
        });
    });
}

function syncSchedule(context, doc) {
    return schedule.update(context, doc, doc._id)
    .catch(err => schedule.create(context, doc))
    .catch(err => {
        context.server.log(['sync', 'debug'], {
            message: 'Failed to sync schedule',
            reason: err.stack,
        });
    });
}

function syncScenario(context, doc) {
    return scenario.validate(context, doc)
    .then(() => scenario.update(context, doc, doc._id))
    .catch(err => scenario.create(context, doc))
    .catch(err => {
        context.server.log(['sync', 'debug'], {
            message: 'Failed to sync scenario',
            reason: err.stack,
        });
    });
}
