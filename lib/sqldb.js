'use strict';

const _ = require('lodash');
const anyDB = require('any-db');
const begin = require('any-db-transaction');
const config = require('./config');
const ObjectID = require('bson-objectid');
const Promise = require('bluebird');

function Database(url, options) {
    this.conn = anyDB.createPool(url, options);
}

Database.isConstraintError = function (error) {
    return error.code === 'SQLITE_CONSTRAINT';
};

Database.isBusyError = function (error) {
    return error.code === 'SQLITE_BUSY';
};

Database.prototype.collection = function (name) {
    return new Collection(this, name);
};

Database.prototype.init = function () {
    return this.query('VACUUM')
    .then(() => this.query('PRAGMA journal_mode = ' + config.db.journalMode));
};

Database.prototype.transaction = retried(Promise.method(function (txnPromiser) {
    const txn = begin(this.conn, { autoRollback: false }, 'BEGIN IMMEDIATE');
    const db = new Transaction(this, txn);
    return Promise.try(() => txnPromiser(db))
    .tap(() => {
        if (!db.isDone) {
            return db.commit();
        }
    })
    .catch(error => {
        if (!db.isDone) {
            return db.rollback().catchReturn().throw(error);
        }
        throw error;
    });
}));

Database.prototype.query = retried(function (sql, args, callback) {
    if (typeof args === 'function') {
        callback = args;
        args = [];
    } else if (!args) {
        args = [];
    }
    return new Promise((resolve, reject) => {
        const q = this.conn.query(sql, args);
        if (!q) {
            // If we are in a transaction and it is closed, txn.query will not
            // return a query. We avoid rejecting the promise here because the
            // error is emitted on the transaction itself and will show up on
            // commit.
            return;
        }
        q.on('error', error => {
            reject(error);
            resolve = null;
        });
        q.on('end', () => {
            resolve && resolve();
        });
        if (typeof callback === 'function') {
            q.on('data', callback);
        } else {
            // Without a data listener, the stream will not emit the 'end'
            // event, so we must put a no-op listener here.
            q.on('data', _.noop);
        }
    });
});

function retried(query) {
    return retryUntilNotBusy;
    function retryUntilNotBusy() {
        return query.apply(this, arguments)
        .catch(error => {
            if (Database.isBusyError(error)) {
                process.stdout.write('!');
                return immediateAsync().then(() => retryUntilNotBusy.apply(this, arguments));
            }
            throw error;
        });
    }
}

function immediateAsync() {
    return new Promise(setImmediate);
}

function Collection(db, name) {
    this.db = db;
    this.name = name;
}

Collection.prototype.create = function () {
    return this.db.query(`
    CREATE TABLE IF NOT EXISTS "${this.name}" (
        _id TEXT UNIQUE NOT NULL PRIMARY KEY,
        doc TEXT NOT NULL
    ) WITHOUT ROWID`)
    .return(this);
};

Collection.prototype.createIndex = function (name, fields, condition) {
    let query = `CREATE INDEX IF NOT EXISTS "${name}" ON "${this.name}" (`;
    query += fields.map(field => `json_extract(doc, '$.${field}')`).join(', ');
    query += ')';
    if (condition) {
        query += ' WHERE ' + `json_extract(doc, '$.${condition.field}') = '${condition.value}'`;
    }
    return this.db.query(query);
};

Collection.prototype.createWithIndices = function (indices) {
    return this.create()
    .return(indices)
    .mapSeries(index => {
        return this.createIndex(index.name, index.fields, index.condition);
    })
    .return(this);
};

Collection.prototype.drop = function () {
    return this.db.query(`DROP TABLE IF EXISTS "${this.name}"`);
};

Collection.prototype.dropIndex = function (name) {
    return this.db.query(`DROP INDEX IF EXISTS "${name}"`);
};

Collection.prototype.exists = function () {
    let exists = false;
    return this.db.query(
        // SQLite version:
        'SELECT 1 FROM sqlite_master' +
        ' WHERE type = ?1' +
        ' AND   name = ?2', ['table', this.name], row => {
            exists = true;
        }
        // Postgres version:
        // 'SELECT EXISTS (' +
        // ' SELECT 1 FROM information_schema.tables' +
        // ' WHERE table_name = ?1)', [this.name], row => {
        //     exists = row.exists;
        // }
    )
    .then(() => exists);
};

Collection.prototype.delete = function (id) {
    return this.db.query(
        'DELETE FROM "' + this.name + '" WHERE _id = ?1', [id]
    ).return({ ok: true });
};

Collection.prototype.get = function (id, params) {
    let result = { doc: undefined };
    return this.db.query(
        'SELECT doc FROM "' + this.name + '"' +
        ' WHERE _id = ?1 LIMIT 1', [id],
        (row) => { result.doc = row.doc; }
    ).then(() => result.doc && JSON.parse(result.doc));
};

Collection.prototype.list = function (params) {
    params = params || {};
    const result = { total_rows: 0, offset: 0, rows: [] };
    return this.db.query(
        'SELECT _id, doc FROM "' + this.name + '" ORDER BY _id' + (params.limit ? ' LIMIT ?1' : ''),
        params.limit ? [params.limit] : [],
        row => {
            result.total_rows += 1;
            result.rows.push({
                id: row._id,
                doc: JSON.parse(row.doc),
            });
        })
    .return(result);
};

Collection.prototype.update = function (doc, id) {
    if (!id) {
        id = doc._id || ObjectID().toString();
    }
    doc = _.merge({}, doc, {
        _id: id,
    });

    return this.db.query(
        'UPDATE "' + this.name + '"' +
        ' SET doc = ?1 WHERE _id = ?2',
        [JSON.stringify(doc), id])
    .return({ ok: true, id: id });
};

Collection.prototype.upsert = function (doc) {
    const id = doc._id || ObjectID().toString();
    doc = _.merge({}, doc, {
        _id: id,
    });
    return this.db.query(
        'INSERT OR REPLACE INTO "' + this.name + '"' +
        ' (_id, doc) VALUES ($1, $3)', [id, JSON.stringify(doc)]
    )
    .return({ ok: true, id: id });
};

Collection.prototype.upsertMany = function (docs) {
    docs = docs.map(doc => {
        const id = doc._id || ObjectID().toString();
        return _.merge({}, doc, { _id: id });
    });
    return this.db.query(`
    INSERT OR REPLACE INTO "${this.name}" (_id, doc)
    SELECT json_extract(value, '$._id'), value
      FROM json_each(?1)
    `, [JSON.stringify(docs)])
    .return({ ok: true, ids: _.map(docs, '_id') });
};

Collection.prototype.insert = function (doc) {
    const id = doc._id || ObjectID().toString();
    doc = _.merge({}, doc, {
        _id: id,
    });
    return this.db.query(
        'INSERT INTO "' + this.name + '"' +
        ' (_id, doc) VALUES (?1, ?2)', [id, JSON.stringify(doc)]
    )
    .return({ ok: true, id: id });
};

Collection.prototype.follow = function () {
    return {
        follow() { },
        pause() { },
        resume() { },
        on() { },
    };
};

function Transaction(db, conn) {
    this.conn = conn;
    this.isDone = false;
    this.commit = Promise.promisify(conn.commit, { context: conn });
    this.rollback = Promise.promisify(conn.rollback, { context: conn });

    conn.on('close', () => {
        this.isDone = true;
    });
}

Transaction.prototype.query = function (sql, args, callback) {
    return Database.prototype.query.apply(this, arguments);
};

Transaction.prototype.info = function () {
    return Database.prototype.info.call(this);
};

Transaction.prototype.collection = function (name) {
    return new Collection(this, name);
};

Transaction.prototype.transaction = function (txnPromiser) {
    return Database.prototype.transaction.apply(this, arguments);
};

module.exports = Database;
