/*
 * Migration: convert device protocol IDs.
 *
 * The protocol-specific device IDs have been converted to text values that are
 * searchable via SQL queries.
 */

'use strict';

const Promise = require('bluebird');

exports.id = module.id;

exports.forwards = function (context) {
    return context.withTransactionContext(context => {
        return updateDevices(context, 'retrofit', doc => {
            const uid = doc.protocolData.id[0];
            let type = doc.protocolData.id[1];
            const index = doc.protocolData.id[2];
            switch (type) {
            case 0: type = '0_master'; break;
            case 1: type = '1_input_'; break;
            case 2: type = '2_output'; break;
            default: throw new TypeError('invalid retrofit type: ' + type);
            }
            if (typeof index === 'number') {
                doc.protocolData.id = `${uid}.${type}.${hex2(index)}`;
            } else {
                doc.protocolData.id = `${uid}.${type}`;
            }
            doc.protocolData.uid = uid;
            doc.protocolData.type = type;
            doc.protocolData.index = index;
            return doc;
        }).then(() => updateDevices(context, 'lc16', doc => {
            const uid = doc.protocolData.id[0];
            let type = doc.protocolData.id[1];
            const channel = doc.protocolData.id[2];
            switch (type) {
            case 0: type = '0_master'; break;
            case 1: type = '1_dimmer'; break;
            case 2: type = '2_switch'; break;
            case 3: type = '3_rgb___'; break;
            default: throw new TypeError('invalid lc16 type: ' + type);
            }
            if (typeof channel === 'number') {
                doc.protocolData.id = `${hex2(uid)}.${type}.${hex2(channel)}`;
            } else {
                doc.protocolData.id = `${hex2(uid)}.${type}`;
            }
            doc.protocolData.uid = uid;
            doc.protocolData.type = type;
            doc.protocolData.channel = channel;
            return doc;
        })).then(() => updateDevices(context, 'zwave', doc => {
            const nodeId = doc.protocolData.id[0];
            const endpointId = doc.protocolData.id[1];
            doc.protocolData.id = `${hex2(nodeId)}.${hex2(endpointId)}`;
            doc.protocolData.nodeId = nodeId;
            doc.protocolData.endpointId = endpointId;
            return doc;
        }));
    });
};

function updateDevices(context, protocolName, updateFun) {
    const results = [];
    return context.database.query(`
    SELECT _id, doc
      FROM devices
     WHERE json_extract(doc, '$.protocolName') = ?1
     `, [protocolName], row => results.push(row))
    .return(results)
    .each(row => {
        return Promise.try(() => updateFun(JSON.parse(row.doc)))
        .then(doc => {
            if (!doc) {
                return;
            }
            doc = JSON.stringify(doc);
            return context.database.query(`
            UPDATE devices
               SET doc = ?1
             WHERE _id = ?2
            `, [doc, row._id]);
        });
    });
}

function hex2(n) {
    if (typeof n !== 'number' || isNaN(n) || n >= 256) {
        throw new TypeError(`cannot convert '${n}' to two-digit hex`);
    }
    const s = n.toString(16);
    if (n < 16) {
        return '0' + s;
    } else {
        return s;
    }
}
