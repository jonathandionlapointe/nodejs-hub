/*
 * Migration: new provisioning process.
 *
 * The new process saves static configuration to a JSON file. We must ensure
 * this file exists.
 */

'use strict';

const _ = require('lodash');
const config = require('../../../lib/config');
const os = require('os');
const Promise = require('bluebird');

const writeFileAsync = Promise.promisify(require('fs').writeFile);

exports.id = module.id;

exports.forwards = function (context) {
    return context.collection('system').get('device')
    .then(device => {
        if (!device) {
            // Not provisioned.
            return;
        }

        if (config.system.provisioning) {
            // Already migrated.
            return;
        }

        const settings = settingsForEnv(process.env.NODE_ENV);
        settings.environment = process.env.NODE_ENV || 'prod';
        settings.file = config.system.file;
        settings.device = device;
        return writeFileAsync(config.system.file, JSON.stringify(settings))
        .then(() => {
            // Need a restart to ensure settings are propagated throughout the
            // server properly.
            process.exit();
        });
    });
};

function settingsForEnv(env) {
    switch (env) {
    case 'prod':
    case 'prod_test':
        return {
            logstash: 'elk-stack.production.05f60b30.svc.dockerapp.io:9998',
            api: 'https://api.2klic.io/v2',
            provisioning: {
                mac: activeMacAddress(),
            },
        };
    case 'staging':
    case 'staging_test':
        return {
            logstash: 'elk-stack.stage.39cf3fad.svc.dockerapp.io:9999',
            api: 'https://api.stage.2klic.io/v2',
            provisioning: {
                mac: activeMacAddress(),
            },
        };
    case 'qa':
    case 'qa_test':
        return {
            logstash: 'elk-stack.qa.8c6c00a9.svc.dockerapp.io:9997',
            api: 'https://api.qa.2klic.io/v2',
            provisioning: {
                mac: activeMacAddress(),
            }
        };
    case 'dev_cloud':
    case 'test_cloud':
        return {
            logstash: 'elk-stack.stage.39cf3fad.svc.dockerapp.io:9998',
            api: 'http://api:3003',
            provisioning: {
                mac: '01:23:45:67:89:ab',
            }
        };
    case 'dev':
    case 'test':
        return {
            logstash: '127.0.0.1:9998',
            api: 'http://127.0.0.1:3003',
            provisioning: {
                mac: '01:23:45:67:89:bc',
            },
        };
    case 'test2':
        return {
            logstash: '127.0.0.1:9998',
            api: 'http://127.0.0.1:3003',
            provisioning: {
                mac: '01:23:45:67:89:ab',
            },
        };
    case 'local_stage_copy':
        return {
            logstash: '127.0.0.1:9998',
            api: 'http://127.0.0.1:3003',
            provisioning: {
                mac: '01:23:45:67:89:ab',
            },
        };
    default:
        return {
            logstash: 'elk-stack.stage.39cf3fad.svc.dockerapp.io:9998',
            api: 'https://api.2klic.io/v2',
            provisioning: {
                mac: activeMacAddress(),
            },
        };
    }
}

function activeMacAddress() {
    if (process.env.MAC) {
        return process.env.MAC;
    }

    let macAddress = null;
    _.forEach(os.networkInterfaces(), addrs => {
        if (macAddress) return;
        _.forEach(addrs, addr => {
            if (macAddress) return;
            if (addr.family === 'IPv4' && !addr.internal) {
                macAddress = addr.mac;
            }
        });
    });

    return macAddress;
}
