'use strict';

const path = require('path');

/* Each migration module should be named YYYY/MM/ID_COMMENT.js, where
 * YYYY and MM are the year and month the migration was authored, ID
 * is the story/bug identifier, and COMMENT is a short description of
 * what is being migrated.
 */
const migrations = [
    require('./2016/10/131594535_device-parameters'),
    require('./2016/10/131594731_ssh-proxy'),
    require('./2016/11/scenario-schema'),
    require('./2017/02/provisioning'),
    require('./2017/03/device-protocol-ids'),
];

/** Set the current migration as the last executed migration.
 *
 * This should be called post-successful-provisioning, as in that case
 * no migrations need to be run, because there is no data in the
 * database.
 */
function setAsCurrent(context) {
    return commitMigration(context.collection('system'), migrations.length, { insert: true });
}

/** Migrate to the current migration.
 *
 * Should be called after no-op provisioning, to migrate any data
 * since the last update.
 */
function migrateToCurrent(context, firstIndex) {
    const db = context.collection('system');
    return db.get('migration').then(doc => doc.index).catchReturn(0).then(index => {
        if (!firstIndex) {
            firstIndex = index;
        }
        if (index > migrations.length) {
            throw new Error('backwards migration is not supported');
        }
        if (index === migrations.length) {
            // All migrations are complete.
            context.server.log(['info', 'migration'], `All migrations up-to-date (${index - firstIndex} applied)`);
            return;
        }
        const migration = migrations[index];
        context.server.log(['info', 'migration'], {
            message: `Applying migration ${index - firstIndex + 1} of ${migrations.length - firstIndex}`,
            index: index,
            total: migrations.length,
            id: path.basename(migration.id, '.js'),
        });
        // TODO consider creating a 'migration context' for running each migration.
        return migration.forwards(context).then(() => {
            return commitMigration(db, index + 1);
        }).then(() => {
            // Run the next migration.
            return migrateToCurrent(context, firstIndex);
        });
    });
}

function commitMigration(db, index, options) {
    if (options && options.insert) {
        return db.insert({ index, _id: 'migration' });
    } else {
        return db.update({ index }, 'migration');
    }
}

module.exports = {
    setAsCurrent, migrateToCurrent,
    currentMigration: migrations.length,
};
