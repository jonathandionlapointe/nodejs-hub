/*
 * Migration: updates to scenario schema.
 *
 * Instead of raw booleans for "match-all" alarmSystemStatus and
 * zoneStatus triggers, use { type: "always" } and { type: "never" }.
 * Additionally, make the capability trigger `device` and `capability`
 * fields behave in the same manner.
 */

'use strict';

const _ = require('lodash');

exports.id = module.id;

exports.forwards = function (context) {
    return context.withTransactionContext(context => {
        const results = [];
        return context.database.query('SELECT doc FROM scenarios', [], row => results.push(row.doc))
        .return(results)
        .catchReturn([])
        .map(JSON.parse)
        .each(rule => {
            if (mapTrigger(rule.trigger)) {
                return context.collection('scenarios').update(rule, rule._id)
                .then(() => context.emitEvent({
                    type: 'update',
                    path: `/scenarios/${rule._id}`,
                    resource: _.omit(rule, ['_rev']),
                }));
            }
        });
    });
};

function mapTrigger(trigger) {
    if (trigger.type === 'capability') {
        trigger.device = { type: 'exact', value: trigger.device };
        trigger.capability = { type: 'exact', value: trigger.capability };
    } else if (trigger.type === 'alarmSystemStatus') {
        trigger.alarmSystem = mapBooleanToCondition(trigger.alarmSystem);
        trigger.alarmSystemStatus = mapBooleanToCondition(trigger.alarmSystemStatus);
    } else if (trigger.type === 'zoneStatus') {
        trigger.alarmSystem = mapBooleanToCondition(trigger.alarmSystem);
        trigger.zone = mapBooleanToCondition(trigger.zone);
        trigger.zoneStatus = mapBooleanToCondition(trigger.zoneStatus);
    } else {
        return false;
    }
    return true;
}

function mapBooleanToCondition(cond) {
    if (typeof cond === 'boolean') {
        if (cond) {
            return { type: 'always' };
        } else {
            return { type: 'never' };
        }
    } else if (cond.type === 'all' || cond.type === 'any') {
        cond.conditions = _.map(cond.conditions, mapBooleanToCondition);
        return cond;
    } else if (cond.type === 'not') {
        cond.condition = mapBooleanToCondition(cond.condition);
        return cond;
    } else {
        return cond;
    }
}

exports.mapTrigger = mapTrigger;
exports.mapBooleanToCondition = mapBooleanToCondition;
