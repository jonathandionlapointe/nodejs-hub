/*
 * https://www.pivotaltracker.com/story/show/131594535
 *
 * Migration: update old parameter data
 *
 * Change all `{ param: value }` objects to `{ param: { value } }`
 * objects.
 */

'use strict';

const _ = require('lodash');

exports.id = module.id;

exports.forwards = function (context) {
    const results = [];
    return context.withTransactionContext(context => {
        return context.database.query('SELECT doc FROM devices', [], row => results.push(row.doc))
        .return(results)
        .catchReturn([])
        .map(JSON.parse)
        .each(doc => {
            if (typeof doc.parameters !== 'object') {
                return;
            }
            let updated = false;
            doc.parameters = _.mapValues(doc.parameters, (value, name) => {
                if (typeof value !== 'object') {
                    updated = true;
                    return { name, value };
                }
                return value;
            });
            if (updated) {
                return context.collection('devices').update(doc, doc._id)
                .then(() => context.emitEvent({
                    type: 'update',
                    path: `/devices/${doc._id}`,
                    resource: {
                        parameters: doc.parameters
                    }
                }));
            }
        });
    });
};
