/*
 * https://www.pivotaltracker.com/story/show/131594731
 *
 * Migration: send ssh-proxy keys and passphrases
 *
 * Already provisioned hubs that have the ssh-proxy installed during
 * upgrade must have their public keys and local passphrase sent to
 * the API.
 */

'use strict';

const config = require('../../../lib/config');
const Promise = require('bluebird');
const readFileAsync = Promise.promisify(require('fs').readFile);

exports.id = module.id;

exports.forwards = function (context) {
    return Promise.join(
        getConfigValue(config.remoteAccess.publicKey),
        getConfigValue(config.remoteAccess.localPassphrase),
        (publicKey, localPassphrase) => {
            return context.emitEvent({
                type: 'update',
                path: `/devices/${context.gatewayId}`,
                resource: {
                    remoteAccess: { publicKey, localPassphrase }
                }
            });
        }
    );
};

/**
 * @param {string|{file:string}|{string:string}} config
 */
function getConfigValue(config) {
    if (typeof config === 'string') {
        config = { string: config };
    }
    if (config.string) {
        return Promise.resolve(config.string);
    } else if (config.file) {
        return readFileAsync(config.file).then(buf => buf.toString());
    } else {
        throw new TypeError('invalid config value (expected `string` or `file`)');
    }
}
