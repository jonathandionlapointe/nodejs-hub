pipeline {
    agent any

    triggers {
        pollSCM 'H/15 * * * *'
        //bitbucketPush()
    }

    stages {
        stage('Start') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
                sh '''
                git clone . build
                cd build
                make git-info.json
                npm install dateformat
                npm version --no-git-tag-version `<package.json jq -r .version`-$BUILD_NUMBER-$BRANCH_NAME
                '''
                stash includes: 'build/**/*', name: 'src'
            }
        }
        stage('Build') {
            steps {
                parallel(
                    'amd64': {
                        node('amd64') {
                            unstash 'src'
                            sh "make -C build package"
                            stash includes: 'build/build/**/*', name: 'amd64_debs'
                        }
                    },
                    'armhf': {
                        node('armhf') {
                            unstash 'src'
                            sh "make -C build package"
                            stash includes: 'build/build/**/*', name: 'armhf_debs'
                        }
                    }
                )
            }
        }
        stage('Test') {
            steps {
                parallel(
                    'amd64': {
                        node('amd64') {
                            sh "make -C build cover"
                            publishHTML([
                                allowMissing: false,
                                alwaysLinkToLastBuild: false,
                                keepAll: false,
                                reportDir: 'build/coverage',
                                reportFiles: 'lcov-report/index.html',
                                reportName: 'Coverage Report',
                                reportTitles: ''
                            ])
                            deleteDir()
                        }
                    },
                    'armhf': {
                        node('armhf') {
                            sh "make -C build unit_test"
                            deleteDir()
                        }
                    }
                )
            }
        }
        stage('Deploy') {
            steps {
                unstash 'amd64_debs'
                unstash 'armhf_debs'
                sshagent(['2fa71134-45ba-4e3a-a576-9e3a317578e0']) {
                    sh 'ssh -p 2222 root@repo.services.2klic.io mkdir -p build/$BUILD_TAG'
                    sh 'scp -r -P 2222 build/build/. root@repo.services.2klic.io:build/$BUILD_TAG/.'
                    sh 'ssh -p 2222 root@repo.services.2klic.io ./publish_all.sh build/$BUILD_TAG'
                }
            }
        }
    }

    post {
        always {
            deleteDir()
        }
        success {
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
        unstable {
            bitbucketStatusNotify(buildState: 'FAILED')
        }
        failure {
            bitbucketStatusNotify(buildState: 'FAILED')
        }
    }
}
