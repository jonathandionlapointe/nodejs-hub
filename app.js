'use strict';

const _ = require('lodash');
const bunyan = require('bunyan');
const bunyanTcp = require('bunyan-logstash-tcp');
const config = require('./lib/config');
const context = require('./lib/context');
const EventEmitter = require('events');
const fs = require('fs');
const Hapi = require('hapi');
const HapiSwagger = require('hapi-swagger');
const Inert = require('inert');
const Promise = require('bluebird');
const Provisioner = require('./lib/provisioner');
const util = require('util');
const Vision = require('vision');

const swaggerOptions = {
    apiVersion: require('./package.json').version,
    info: {
        title: '2KLIC IoT Management API for Developers',
        description: 'Welcome to api.2klic.io, the Internet of Things Management Platform. This platform is private and only accessible with a valid token obtained from the 2KLIC developer registration page. Please contact 2KLIC for any requests.',
        contact: 'webmaster@2klic.com',
    },
};

function App() {
    EventEmitter.call(this);

    this.hardware = _.merge({
        intercom: false,
    }, config.hw);
    this.server = null;
    this.options = {};

    const streams = [];

    if (config.logger.enable) {
        if (config.logger.stdout) {
            streams.push({
                stream: process.stdout,
                level: config.logger.stdout.level || 'debug'
            });
        }

        if (config.logger.stderr) {
            streams.push({
                stream: process.stderr,
                level: config.logger.stderr.level || 'error'
            });
        }

        if (config.logger.file) {
            streams.push({
                path: config.logger.file.path,
                level: config.logger.file.level || 'info'
            });
        }

        if (config.logger.logstash.enable && config.system.logstash) {
            const server = config.system.logstash.split(':');
            streams.push({
                level: config.logger.logstash.level || 'warn',
                type: 'raw',
                stream: (bunyanTcp.createStream({
                    host: server[0],
                    port: server[1],
                    max_connect_retries: -1,
                    retry_interval: 1000
                }).on('error', error => this.emit('logging-error', error)))
            });
        }
    }

    this.log = bunyan.createLogger({
        src: config.logger.bunyan.source_info,
        name: '2klic',
        serializers: bunyan.stdSerializers,
        streams: streams
    }).child({
        env: process.env.NODE_ENV,
        app: 'NODEJS-HUB'
    });

    this.context = context.createAppContext(this, this.log);
}
util.inherits(App, EventEmitter);

App.prototype.start = function (options) {
    if (this.server) {
        throw new Error('already started');
    }

    // TODO some features are enabled based on `options`, and others
    // based on the global `config`; at least in the app module (in
    // this method) we should use only `options`. It's currently
    // confusing.
    this.options = options = _.merge({
        cloudMessaging: {
            enable: true,
            service: 'pubnub'
        },
        emulator: { enable: false },
        factoryReset: { enable: true },
        good: { enable: true },
        server: {
            load: { sampleInterval: 5000 }
        },
    }, options);

    this.server = Promise.promisifyAll(new Hapi.Server(options.server));

    this.server.connection({
        host: config.server.host,
        port: process.env.APP_PORT || config.server.port,
        routes: {
            cors: {
                origin: ['*'],
                additionalHeaders: [
                    'x-app-key', 'x-unsafe-auth', 'x-api-version', 'x-app-type'
                ],
            },
        },
        labels: ['api'],
    });

    this.server.connection({
        host: '127.0.0.1',
        port: config.server.internalPort,
        labels: ['internal'],
        routes: {
            cors: {
                origin: ['http://127.0.0.1:8100', 'http://127.0.0.1:9000', 'http://localhost:8100', 'http://localhost:9000', 'http://localhost', 'http://127.0.0.1', 'app://localhost']
            },
        }
    });

    this.context.createServerContext(this.server);

    return this.context.database.init()
    /* istanbul ignore next */
    .then(() => this.server.registerAsync({
        register: require('@2klic/2klic-hapi-bunyan'),
        options: {
            includeTags: true,
            mergeData: false,
            joinTags: config.logger.joinTags,
            logger: this.log
        },
    })).then(() => {
        return this.server.registerAsync([
            Inert, Vision,
            { register: HapiSwagger, options: swaggerOptions },
            { register: require('./plugins/sdk') },
            { register: require('./plugins/cache') },
        ]);
    }).then(() => {
        if (!config.system.device) {
            const db = this.context.collection('system');
            return db.exists()
            .then(exists => {
                if (exists) return db.get('device');
            })
            .then(device => {
                if (device) {
                    config.system.device = device;
                    return;
                }

                const provisioner = new Provisioner(this.server.context());

                provisioner.once('provisioned', () => {
                    this.server.log(['app'], 'Provisioning complete');
                    this.shutdown();
                });

                this.server.log(['app'], 'Starting in provisioning mode!');
            });
        }
    }).then(() => {
        if (!config.system.device) {
            return;
        }

        const rpcOptions = _.merge({}, config.rpc, {
            websocketServer: this.server.select('internal'),
            server: this.server.select('api'),
        });

        return this.server.registerAsync([
            { register: require('./plugins/system'), options: this.options },
            { register: require('./plugins/rpc'), options: rpcOptions },
            { register: require('./plugins/auth'), options: config.auth },
            { register: require('./plugins/sockjs') },
            { register: require('./plugins/event') },
            { register: require('./plugins/device') },
            { register: require('./plugins/scenario') },
            { register: require('./plugins/alarm') },
            { register: require('./plugins/api-crud'), options: { type: 'location' } },
            { register: require('./plugins/api-crud'), options: { type: 'template' } },
        ]);
    }).then(() => {
        if (!config.system.device) {
            return;
        }

        if (options.cloudMessaging.enable) {
            if (options.cloudMessaging.service === 'pubnub') {
                return this.server.registerAsync({
                    register: require('./plugins/pubnub'),
                    options: {},
                });
            } else {
                throw new Error('unknown cloudMessaging service: ' +
                                options.cloudMessaging.service);
            }
        }
    }).then(() => {
        if (config.system.device && this.hardware.intercom) {
            return this.server.registerAsync([
                { register: require('./plugins/intercom'), options: this.hardware.intercom },
                { register: require('./plugins/sip'), options: this.hardware.sip },
            ]);
        }
    }).then(() => {
        if (!config.system.device) {
            return;
        }

        if (!options.emulator.enable) {
            return;
        }

        return this.server.registerAsync([
            { register: require('./plugins/emu') }
        ]);
    }).then(() => this.server.startAsync()).then(() => {
        config.fixPorts(this.server);

        if (!config.system.device) {
            return;
        }

        const system = this.server.plugins.system;

        system.on('shutdown', (context, reason) => {
            if (_.get(this.options, 'factoryReset.enable') && reason === 'factoryReset') {
                try {
                    fs.writeFileSync(config.system.resetFile, '');
                } catch (err) {
                    this.log.error(err, { msg: 'Unable to create reset file', path: config.system.resetFile });
                }
                this.stop(reason);
            } else {
                this.stop(reason);
            }
        });
    }).then(() => {
        if (!config.system.device) {
            return;
        }

        // Start pinging the API.
        let latency = -1;
        let frequency = 15;

        const pingLoop = () => {
            if (!this.isRunning()) {
                return;
            }

            const system = this.server.plugins.system;
            const start = Date.now();
            system.ping(this.context, latency).tap(frequency => {
                const end = Date.now();
                latency = end - start;
                this.server.log(['ping', 'debug'], {
                    message: 'Ping! Pong!',
                    latency, frequency
                });
            }).catch(error => {
                if (this.isRunning()) {
                    this.server.log(['ping', 'info'], {
                        message: 'Unable to send ping',
                        reason: error,
                    });
                }
                latency = -1;
                return frequency;
            }).then(frequency_ => {
                if (isNaN(frequency_)) {
                    frequency_ = 15;
                }
                // Bound the ping frequency to 1s <= freq <= 60m
                frequency = Math.min(Math.max(1, frequency_), 60 * 60);
                return Promise.delay(frequency * 1000);
            }).then(() => {
                // TODO IIUC chained promises create an ever-growing
                // 'function name' for stack traces. Do not chain
                // here, to try to avoid creating a 'leak' that keeps
                // consuming more and more memory.
                pingLoop();
            });
        };

        this.server.plugins.system.once('startup', pingLoop);
    }).catch(error => {
        this.server = null;
        throw error;
    });
};

App.prototype.stop = function (reason, prestop) {
    if (!this.server) {
        throw new Error('not started');
    }

    reason = reason || 'normal';

    return this.server.stopAsync().then(() => {
        if (typeof prestop === 'function') {
            return Promise.try(prestop).catch(error => {
                this.server.log(['pre-stop', 'error'], error);
            });
        }
    }).finally(() => {
        this.server = null;
    }).then(() => {
        this.emit('stop', reason);
    }).catch(error => {
        this.emit('stop', error.message);
    });
};

App.prototype.shutdown = function (reason) {
    if (this.server.plugins['system']) {
        return this.server.plugins['system'].shutdown(this.server.context(), reason);
    } else {
        return this.stop(reason);
    }
};

App.prototype.isRunning = function () {
    return Boolean(this.server);
};

module.exports = App;
