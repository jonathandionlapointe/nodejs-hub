#!/usr/bin/env node
'use strict';

const _ = require('lodash');
const Boom = require('boom');
const constants = require('constants');
const crypto = require('crypto');
const EventEmitter = require('events');
const fs = require('fs');
const Hapi = require('hapi');
const Joi = require('joi');
const ptys = require('./ptys');
const SerialPort = require('serialport');
const util = require('util');

const DEFAULTS = {
    model: '2klic_sr10z',
    version: '1.3.0',
    inputs: 10,
    outputs: 4,
};

const UNUSED = 'U';
const ERROR = 'E';
const OPEN = 'O';
const CLOSED = 'C';

function RetrofitModule(options) {
    EventEmitter.call(this);

    this.options = _.merge({}, DEFAULTS, options);
    if (!this.options.uuid) {
        this.options.uuid = RetrofitModule.generateUuid();
    }
    this.host = null;
    this.slaveFd = null;
    this.slave = null;
    this.state = this.initialState();
}
util.inherits(RetrofitModule, EventEmitter);

RetrofitModule.DEFAULTS = DEFAULTS;
RetrofitModule.UNUSED = UNUSED;
RetrofitModule.ERROR = ERROR;
RetrofitModule.OPEN = OPEN;
RetrofitModule.CLOSED = CLOSED;

RetrofitModule.generateUuid = function () {
    const bits = crypto.randomBytes(96 / 8);
    return bits.slice(0, 6).toString('hex').toUpperCase() + '-'
         + bits.slice(6, 9).toString('hex').toUpperCase() + '-'
         + bits.slice(9, 12).toString('hex').toUpperCase();
};

RetrofitModule.prototype.initialState = function () {
    return {
        id: '00',
        inputs: _.map(_.range(this.options.inputs), () => ERROR),
        normals: _.map(_.range(this.options.inputs), () => UNUSED),
        outputs: _.map(_.range(this.options.outputs), () => OPEN),
    };
};

RetrofitModule.prototype.open = function () {
    if (this.host) {
        return;
    }

    const host = new SerialPort('/dev/ptmx', {
        baudrate: 9600,
        lock: false,
        parser: SerialPort.parsers.readline(/[\r\n]+/, 'ascii'),
    }, err => {
        if (err) {
            this.emit('error', err);
            return;
        }

        try {
            ptys.grantpt(host.fd);
            ptys.unlockpt(host.fd);

            this.hostPty = ptys.ptsname(host.fd);

            // Keep the slave open too, this prevents any other open slave from
            // causing an EIO when it closes the pty (EIO would happen the last
            // slave closes).
            this.slaveFd = fs.openSync(this.hostPty, constants.O_RDWR | constants.O_NOCTTY);

            host.on('data', this.onHostData.bind(this));
            host.on('close', this.onHostClose.bind(this));
            host.on('error', this.onHostError.bind(this));

            this.host = host;
            this.emit('open');
        } catch (error) {
            host.close();
            if (this.slaveFd) {
                fs.closeSync(this.slaveFd);
                this.slaveFd = null;
            }
            this.emit('error', error);
        }
    });
};

RetrofitModule.prototype.close = function () {
    if (!this.host) {
        return;
    }

    this.host.removeListener('data', this.onHostData);
    this.host.removeListener('close', this.onHostClose);
    this.host.removeListener('error', this.onHostError);
    this.host.close();
    this.host = null;

    fs.closeSync(this.slaveFd);
    this.slaveFd = null;

    this.emit('close');
};

RetrofitModule.prototype.onHostData = function (line) {
    const start = line.indexOf(':');
    if (start < 0) {
        return;
    }

    line = line.substr(start);
    let response = null;
    let broadcast = false;
    let cmd = null;
    let respond = true;
    const match = line.match(/^:ID([0-9A-F][0-9A-F])(....)(.*)$/);
    if (match) {
        const id = match[1];
        const args = match[3].trim();
        cmd = match[2];

        if (cmd === 'QERY') {
            response = this.options.uuid;
            broadcast = true;
        } else if (cmd === 'RSET') {
            respond = false;
            broadcast = true;
            const inputs = this.state.inputs;
            this.state = this.initialState();
            this.state.inputs = inputs;
        } else if (id === this.state.id) {
            if (cmd === 'CONN') {
                response = this.connect();
            } else if (cmd === 'SAVE') {
                response = this.save();
            } else if (cmd === 'STAT') {
                response = this.status();
            } else if (cmd === 'GETI') {
                response = this.normals();
            } else if (cmd === 'VSN ') {
                response = this.version();
            } else if (cmd === 'RELY' && args.length === this.options.outputs) {
                response = this.relay(_.toArray(args));
            } else if (cmd === 'STID' && args.match(/^[0-9A-F][0-9A-F]$/)) {
                response = this.setId(args);
            } else if (cmd === 'SETI' && args.length === this.options.inputs) {
                response = this.setNormals(_.toArray(args));
            }
        } else {
            response = 'sending to slave';
            broadcast = true;
            respond = false;
        }
    }

    if (!response) {
        this.host.write(`:ID${this.state.id}NACK\r\n\r\n`);
    } else {
        if (this.slave && broadcast) {
            this.slave.write(line + '\r\n');
        }

        if (respond) {
            response = `:ID${this.state.id}${cmd}${response}\r\n\r\n`;
            this.host.write(response);
        }
    }
};

RetrofitModule.prototype.onHostClose = function () {
    this.close();
};

RetrofitModule.prototype.onHostError = function (error) {
    this.host = null;
    this.emit('error', new Error(error));
};

RetrofitModule.prototype.connectSlave = function (tty) {
    if (this.slave) {
        throw new Error('slave already connected');
    }

    const slave = new SerialPort(tty, {
        baudrate: 9600,
        lock: false,
        parser: SerialPort.parsers.readline(/[\r\n]+/, 'ascii'),
    }, err => {
        if (err) {
            this.onSlaveError(err);
            return;
        }

        slave.on('data', this.onSlaveData.bind(this));
        slave.on('close', this.onSlaveClose.bind(this));
        slave.on('error', this.onSlaveError.bind(this));

        this.slave = slave;
        this.emit('connectSlave');
    });

};

RetrofitModule.prototype.disconnectSlave = function () {
    if (!this.slave) {
        return;
    }

    this.slave.removeListener('data', this.onSlaveData);
    this.slave.removeListener('close', this.onSlaveClose);
    this.slave.close();
    this.slave = null;

    this.emit('disconnectSlave');
};

RetrofitModule.prototype.onSlaveClose = function () {
    this.disconnectSlave();
};

RetrofitModule.prototype.onSlaveError = function (error) {
    this.slave = null;
    this.emit('error', new Error(error));
};

RetrofitModule.prototype.onSlaveData = function (line) {
    if (this.host) {
        this.host.write(line + '\r\n\r\n');
    }
};

RetrofitModule.prototype.connect = function () {
    return 'OK';
};

RetrofitModule.prototype.save = function () {
    // Maybe persist state to file.
    return 'OK';
};

RetrofitModule.prototype.status = function () {
    return _.map(_.zip(this.state.inputs, this.state.normals), input => {
        if (input[1] === UNUSED) {
            return UNUSED;
        }
        return input[0];
    }).join('');
};

RetrofitModule.prototype.normals = function () {
    return this.state.normals.join('');
};

RetrofitModule.prototype.version = function () {
    return [
        this.options.model,
        hex2(this.options.inputs),
        hex2(this.options.outputs),
        this.options.version,
    ].join(' ');
};

RetrofitModule.prototype.relay = function (outputs) {
    _.forEach(_.zip(this.state.outputs, outputs), (output, i) => {
        if (output[0] !== output[1]) {
            this.emit('output', i, output[1]);
        }
    });
    this.state.outputs = outputs;
    return 'OK';
};

RetrofitModule.prototype.setId = function (id) {
    if (this.state.id !== id) {
        this.emit('id', id);
    }

    this.state.id = id;
    return 'OK';
};

RetrofitModule.prototype.setNormals = function (normals) {
    _.forEach(_.zip(this.state.normals, normals), (normal, i) => {
        if (normal[0] !== normal[1]) {
            this.emit('normal', i, normal[1]);
        }
    });

    this.state.normals = normals;
    return 'OK';
};

RetrofitModule.prototype.changeInput = function (n, value) {
    if (n < 0 || n >= this.options.inputs) {
        throw new RangeError('input index out of range');
    }

    if (!_.includes([ERROR, OPEN, CLOSED], value)) {
        throw new RangeError('input value out of range');
    }

    const before = this.status();
    this.state.inputs[n] = value;
    const after = this.status();
    this.updateInputs(before, after);
};

RetrofitModule.prototype.updateInputs = function (before, after) {
    if (!_.isEqual(before, after) && this.host) {
        this.host.write(`:ID${this.state.id}STAT${after}\r\n\r\n`);
    }
};

module.exports = RetrofitModule;

function hex2(n) {
    if (n < 16) {
        return '0' + n.toString(16).toUpperCase();
    }
    return n.toString(16).toUpperCase();
}

RetrofitModule.createChain = function (num, uuids, host, callback) {
    if (!callback) {
        callback = host;
        host = null;
    }
    if (!callback) {
        callback = uuids;
        uuids = null;
    }
    if (!callback) {
        callback = num;
        num = 1;
    }
    if (!uuids) {
        uuids = [];
        for (let i = 0; i < num; i++) {
            uuids.push(RetrofitModule.generateUuid());
        }
    }
    if (num > uuids.length) {
        num = uuids.length;
    }

    const modules = [];
    create();

    function create() {
        const m = new RetrofitModule({ uuid: uuids.pop() });
        m.open();
        m.on('error', error);
        m.once('open', () => {
            if (modules.length > 0) {
                modules[modules.length - 1].connectSlave(m.hostPty);
            }

            modules.push(m);

            if (modules.length < num) {
                create();
            } else if (callback) {
                if (host) {
                    m.connectSlave(host);
                }

                modules.forEach(m => m.removeListener('error', error));

                callback(null, modules);
            }
        });
    }

    function error(err) {
        close();
        if (callback) {
            callback(err);
        }
    }

    function close() {
        modules.forEach(m => {
            m.disconnectSlave();
            m.close();
        });
    }
};

RetrofitModule.serve = function (modules, callback) {
    const server = new Hapi.Server({ debug: false });
    server.connection({
        host: process.env.RETROFIT_HOST || '127.0.0.1',
        port: process.env.RETROFIT_PORT || 1970,
    });

    server.app = { modules, uri: 'http:' };

    server.method('module', function (id, next) {
        const m = _.find(server.app.modules, { options: { uuid: id } });
        if (!m) {
            return next(Boom.notFound());
        }
        next(null, m);
    });

    server.method('input', function (module, index, next) {
        index = parseInt(index);
        if (isNaN(index) || index < 1 || index > module.options.inputs) {
            return next(Boom.notFound());
        }
        return next(index - 1);
    });

    server.method('output', function (module, index, next) {
        index = parseInt(index);
        if (isNaN(index) || index < 1 || index > module.options.outputs) {
            return next(Boom.notFound());
        }
        return next(index - 1);
    });

    server.route(SERVICE_ENDPOINTS);
    server.start(err => {
        if (err) {
            if (callback) {
                callback(err);
            }
            return;
        }
        callback(null, server);
    });
};

const SERVICE_ENDPOINTS = [
    { method: 'GET', path: '/', config: { handler: function (request, reply) {
        reply({
            tty: request.server.app.modules[0].hostPty,
            links: {
                modules: `${request.server.app.uri}/modules`
            }
        });
    }}},
    { method: 'GET', path: '/modules', config: { handler: function (request, reply) {
        reply(request.server.app.modules.map(m => ({
            model: m.options.model,
            version: m.options.version,
            id: parseInt(m.state.id, 16),
            inputs: m.state.inputs.map((input, index) => ({
                value: input,
                links: {
                    self: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}`,
                    setOpen: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=O`,
                    setClosed: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=C`,
                    setError: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=E`,
                },
            })),
            outputs: m.state.outputs.map((output, index) => ({
                value: output,
                links: { self: `${request.server.app.uri}/modules/${m.options.uuid}/outputs/${index + 1}` },
            })),
            links: {
                self: `${request.server.app.uri}/modules/${m.options.uuid}`,
                inputs: `${request.server.app.uri}/modules/${m.options.uuid}/inputs`,
                outputs: `${request.server.app.uri}/modules/${m.options.uuid}/outputs`,
            }
        })));
    }}},
    { method: 'GET', path: '/modules/{id}', config: { pre: ['module(params.id)'], handler: function (request, reply) {
        const m = request.pre.module;
        reply({
            model: m.options.model,
            version: m.options.version,
            id: parseInt(m.state.id, 16),
            inputs: m.state.inputs.map((input, index) => ({
                value: input,
                links: {
                    self: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}`,
                    setOpen: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=O`,
                    setClosed: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=C`,
                    setError: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=E`,
                },
            })),
            outputs: m.state.outputs.map((output, index) => ({
                value: output,
                links: { self: `${request.server.app.uri}/modules/${m.options.uuid}/outputs/${index + 1}` },
            })),
            links: {
                self: `${request.server.app.uri}/modules/${m.options.uuid}`,
                inputs: `${request.server.app.uri}/modules/${m.options.uuid}/inputs`,
                outputs: `${request.server.app.uri}/modules/${m.options.uuid}/outputs`,
            }
        });
    }}},
    { method: 'GET', path: '/modules/{id}/inputs', config: { pre: ['module(params.id)'], handler: function (request, reply) {
        const m = request.pre.module;
        reply(m.state.inputs.map((input, index) => ({
            value: input,
            links: {
                self: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}`,
                module: `${request.server.app.uri}/modules/${m.options.uuid}`,
                setOpen: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=O`,
                setClosed: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=C`,
                setError: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=E`,
            },
        })));
    }}},
    { method: 'GET', path: '/modules/{id}/outputs', config: { pre: ['module(params.id)'], handler: function (request, reply) {
        const m = request.pre.module;
        reply(m.state.outputs.map((input, index) => ({
            value: input,
            links: {
                self: `${request.server.app.uri}/modules/${m.options.uuid}/outputs/${index + 1}`,
                module: `${request.server.app.uri}/modules/${m.options.uuid}`,
            },
        })));
    }}},
    { method: 'GET', path: '/modules/{id}/inputs/{index}', config: {
        validate: {
            query: {
                value: Joi.allow(OPEN, CLOSED, ERROR),
                method: Joi.allow('GET', 'PUT')
            }
        }, pre: ['module(params.id)', 'input(pre.module, params.index)'], handler: function (request, reply) {
            const m = request.pre.module;
            const index = request.pre.input;
            if (request.query.method === 'PUT' && request.query.value) {
                m.changeInput(index, request.query.value);
            }
            reply({
                value: m.state.inputs[index],
                links: {
                    self: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}`,
                    module: `${request.server.app.uri}/modules/${m.options.uuid}`,
                    setOpen: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=O`,
                    setClosed: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=C`,
                    setError: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=E`,
                },
            });
        }
    }},
    { method: 'GET', path: '/modules/{id}/outputs/{index}', config: { pre: ['module(params.id)', 'output(pre.module, params.index)'], handler: function (request, reply) {
        const m = request.pre.module;
        const index = request.pre.output;
        reply({
            value: m.state.outputs[index],
            links: {
                self: `${request.server.app.uri}/modules/${m.options.uuid}/outputs/${index + 1}`,
                module: `${request.server.app.uri}/modules/${m.options.uuid}`,
            },
        });
    }}},
    { method: 'PUT', path: '/modules/{id}/inputs/{index}', config: {
        validate: {
            payload: {
                value: Joi.allow(OPEN, CLOSED, ERROR).required(),
            }
        },
        pre: ['module(params.id)', 'input(pre.module, params.index)'],
        handler: function (request, reply) {
            const m = request.pre.module;
            const index = request.pre.input;
            m.changeInput(index, request.payload.value);
            reply({
                value: m.state.inputs[index],
                links: {
                    self: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}`,
                    module: `${request.server.app.uri}/modules/${m.options.uuid}`,
                    setOpen: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=O`,
                    setClosed: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=C`,
                    setError: `${request.server.app.uri}/modules/${m.options.uuid}/inputs/${index + 1}?method=PUT&value=E`,
                },
            });
        }
    }},
];


if (require.main === module) {
    const args = process.argv.slice(2);

    let chainLength = 1;
    if (args.length > 0) {
        chainLength = parseInt(args[0]);
        if (isNaN(chainLength)) {
            chainLength = 1;
        } else {
            args.shift();
            chainLength = Math.min(Math.max(1, chainLength), 4);
        }
    }

    let host = null;
    if (args.length > 0) {
        host = args.shift();
    }

    const uuids = [
        '6BECCDAB8D99-27BCA9-F14C94',
        '3AD203E307BB-1CD718-1BDA67',
        '28E4A5696404-4C1D74-4D2D63',
        '36B1DD746800-773B48-7E1990',
    ];

    RetrofitModule.createChain(chainLength, uuids, host, (err, modules) => {
        if (err) {
            process.stderr.write(err.stack);
            process.stderr.write('\n');
            process.exit(1);
        }

        RetrofitModule.serve(modules, (err, server) => {
            if (err) {
                return exit(err);
            }

            server.on('error', exit);

            modules.forEach(m => {
                m.on('error', exit);
                m.on('id', id => process.stdout.write(`ID ${m.options.uuid} ${id}\n`));
                m.on('output', (n, v) => process.stdout.write(`RELAY ${m.options.uuid} ${n} ${v}\n`));
                m.on('normal', (n, v) => process.stdout.write(`NORMAL ${m.options.uuid} ${n} ${v}\n`));
                process.stdout.write(`UUID ${m.options.uuid}\n`);
            });

            process.stdout.write(`TTY ${modules[0].hostPty}\n`);

            process.stdout.write(`LISTEN ${server.info.uri}\n`);

            ['SIGHUP', 'SIGINT', 'SIGTERM'].forEach(signal => process.on(signal, () => exit()));

            function exit(err) {
                if (!err) {
                    process.stdout.write('\n');
                }
                if (server) {
                    server.stop(() => reallyExit(err));
                } else {
                    reallyExit(err);
                }
            }

            function reallyExit(err) {
                modules.forEach(m => {
                    m.disconnectSlave();
                    m.close();
                });
                if (err) {
                    process.stderr.write(err.stack);
                    process.stderr.write('\n');
                    process.exit(1);
                } else {
                    process.exit();
                }
            }
        });
    });
}
