'use strict';

const ffi = require('ffi');

const libc = ffi.Library(process.env.LIBC_PATH || 'libc', {
    grantpt: ['int', ['int']],
    unlockpt: ['int', ['int']],
    ptsname: ['string', ['int']],
});

exports.grantpt = function (fd) {
    if (libc.grantpt(fd) !== 0) {
        throw new Error('grantpt');
    }
};

exports.unlockpt = function (fd) {
    if (libc.unlockpt(fd) !== 0) {
        throw new Error('unlockpt');
    }
};

exports.ptsname = function (fd) {
    const name = libc.ptsname(fd);
    if (!name) {
        throw new Error('ptsname');
    }
    return name;
};
