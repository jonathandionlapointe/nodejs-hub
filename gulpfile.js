var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
// Restart the server when changes in files are detected
gulp.task('default', function () {
    nodemon({ script: 'index.js', ext: 'html js json' });
});