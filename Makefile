all :

.PHONY : help
help :
	@echo "Available targets:"
	@grep -P '^[a-zA-Z_-]+ *:.*?## .*$$' $(MAKEFILE_LIST) \
	| sort -s -k 1,1 \
	| awk 'BEGIN {FS = ":.*?## "}; {printf " \033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY : all clean distclean
all clean distclean :

TESTS := \
	lib/async-events \
	lib/sqldb \
	lib/context \
	lib/events \
	lib/provisioner \
	lib/system \
	lib/condition \
	lib/group-database \
	lib/permission \
	lib/device \
	lib/alarm \
	lib/auth \
	lib/run-command \
	mock-api \
	plugins/cache \
	plugins/sdk \
	plugins/event \
	plugins/system \
	plugins/auth \
	plugins/device \
	plugins/sockjs \
	plugins/alarm \
	plugins/scenario \
	plugins/api-crud \
	app

MOCHA ?= ./node_modules/.bin/mocha
_MOCHA ?= ./node_modules/.bin/_mocha
ISTANBUL ?= ./node_modules/.bin/istanbul

MOCHA_REPORTER ?= dot
MOCHA_TIMEOUT ?= 30000
MOCHA_ARGS ?=
MOCHA_FLAGS ?= $(MOCHA_ARGS) -R $(MOCHA_REPORTER) -t $(MOCHA_TIMEOUT)

TEST_N ?= 100

TEST_FILES := $(TESTS:%=test/%.js)

NAME := 2klic-smart-controller
VERSION := $(shell jq -r .version < package.json)
ARCH := $(shell dpkg-architecture -qDEB_BUILD_ARCH 2>/dev/null || echo amd64)

PKG_NODE := node8
PKG_OS := linux

ifeq ($(ARCH),amd64)
	PKG_ARCH := x64
else ifeq ($(ARCH),armhf)
	PKG_ARCH := armv7
else
	$(error "Unknown ARCH")
endif

PKG_TARGET = $(PKG_NODE)-$(PKG_OS)-$(PKG_ARCH)

DEB_TARGETS := \
	build/$(NAME)_$(VERSION)_$(ARCH).changes

PACKAGE_TARGETS := $(DEB_TARGETS)

clean :
	rm -rf gateway build

distclean : clean
	rm -rf node_modules

all : gateway retrofit-emulator ## Build binary version of application

gateway : git-info.json enclose-config.js $(glob lib/* plugins/**/*) index.js app.js
	@if ! command -v pkg ; then \
		echo 'pkg is required!'; \
		echo 'http://github.com/zeit/pkg'; \
		exit 1; \
	fi
	npm install --build-from-source
	pkg -t $(PKG_TARGET) -c enclose-config.js -o gateway index.js

retrofit-emulator : tools/retrofit-emulator.js tools/ptys.js
	pkg -t $(PKG_TARGET) -o retrofit-emulator tools/retrofit-emulator.js

git-info.json :
	scripts/git-info.sh > $@

.PHONY : install
install : gateway config.prod.yaml
	install -d $(DESTDIR)/usr/lib/2klic
	install -t $(DESTDIR)/usr/lib/2klic gateway
	install -m 0644 config.prod.yaml $(DESTDIR)/usr/lib/2klic/config.yaml
	install -d $(DESTDIR)/usr/lib/2klic/scripts
	install -t $(DESTDIR)/usr/lib/2klic/scripts \
		scripts/heartbeat.sh \
		scripts/update-network.sh

config.prod.yaml : config.yaml
	npm run strip-config

build/%.changes : ../%.changes
	mkdir -p build
	for f in `<$< awk 'BEGIN { files = 0; } { if (files) print $$5; } /^Files:/ { files = 1; }'`; do \
	    test -f build/$$f || mv ../$$f build/; \
	done
	mv $< $@

../%.changes :
	NODE_ENV=test dpkg-buildpackage -rfakeroot -b -uc

$(DEB_TARGETS) : debian/changelog

.PHONY : package
package : $(PACKAGE_TARGETS) ## Build .deb packages

.PHONY : unit_test
unit_test : ## Run unit tests
	unset HUB_LOGGER_STDOUT_LEVEL && \
	NODE_ENV=test HUB_NO_ARGPARSE=y \
	HUB_LOGGER_STDERR_LEVEL=fatal \
	HUB_SERVER_PORT=0 HUB_SERVER_INTERNAL_PORT=0 \
	HUB_AUTH_NET_INTERFACE=`ip route | awk '/^default via/ { print $$5; exit }'` \
	$(MOCHA) $(MOCHA_FLAGS) $(TEST_FILES)

.PHONY : test_until_failure
test_until_failure : ## Run unit tests repeatedly until test failure
	@bash -c '\
	let x=0; while $(MAKE) test; do \
	    let x=$$((x + 1)); \
	done; \
	echo "Successful runs: $$x"; \
	false'

.PHONY : test_reliability
test_reliability : ## Run unit tests TEST_N times (default 100)
	@bash -c '\
	let n=0 t=0 f=0; \
	while [ $$n -lt $(TEST_N) ]; do \
		echo "Run $$((n + 1)) of $(TEST_N), $$f failures"; \
		if $(MAKE) test; then \
			let t=$$((t + 1)); \
		else \
			let f=$$((f + 1)); \
		fi; \
		let n=$$((n + 1)); \
	done; \
	echo "$$t/$$n successful test runs"; \
	test $$f -eq 0'

.PHONY : cover
cover : ## Run unit tests with coverage output
	unset HUB_LOGGER_STDOUT_LEVEL && \
	NODE_ENV=test HUB_NO_ARGPARSE=y \
	HUB_LOGGER_STDERR_LEVEL=fatal \
	HUB_SERVER_PORT=0 HUB_SERVER_INTERNAL_PORT=0 \
	HUB_AUTH_NET_INTERFACE=`ip route | awk '/^default via/ { print $$5; }'` \
	$(ISTANBUL) cover $(_MOCHA) -- $(MOCHA_FLAGS) $(TEST_FILES)
