/*eslint no-console: ["off"], quotes: ["off"]*/
/**
 * Copyright 2014-2015 2Klic Inc.
 * Author: Joel Lavoie <jl@2klic.com>
 */
'use strict';

const _ = require('lodash');
const config = require('./lib/config');
const App = require('./app.js');
const fs = require('fs');

if (config.system.environment !== process.env.NODE_ENV) {
    process.env.NODE_ENV = config.system.environment;
    config.reload();
}

if (fs.existsSync(config.system.resetFile)) {
    console.error(
        'Application is in factory reset!\n' +
        'Drop all databases and remove the reset to continue:\n' +
        '\n' +
        '  ' + config.system.resetFile + '\n');
    process.exit(1);
}

config.print();

const options = {
    server: {
        debug: {
            request: ['debug']
        }
    },
    emulator: { enable: config.system.test },
    factoryReset: { enable: true },
};

const app = new App();

app.on('error', error => {
    console.error('Application error:', error.stack || error);
    if (app.isRunning()) {
        return app.shutdown(error.message);
    } else {
        exit();
    }
});

app.on('stop', reason => {
    const db = _.get(app, 'context.database.conn');
    if (db && typeof db.close === 'function') {
        console.log('Closing database connections...');
        db.close(() => exit(reason));
    } else {
        exit(reason);
    }
});

app.on('logging-error', error => { console.error('Logging error:', error); });
app.on('bootlog-error', error => { console.error('Bootlog error:', error); });

app.start(options).then(() => {
    _.forEach(['SIGINT', 'SIGTERM', 'SIGHUP'], signal => {
        process.on(signal, () => {
            console.log(`Received ${signal}. Shutting down...`);
            app.shutdown().catch(error => {
                console.error(`Shutdown rejected with message: ${error.message}`);
            });
        });
    });
    console.log('Server started!');
}).catch(error => {
    exit(error);
});

function exit(reason) {
    if (_.get(process.env, 'WRITE_MODULE_CACHE', '').match(/^[yt]/)) {
        try {
            fs.writeFileSync('modules.json', JSON.stringify(_.map(require('module')._cache, m => m.filename), null, '  '));
        } catch (err) {
            console.error('Unable to write modules.json:', err.stack);
        }
    }
    if (reason instanceof Error) {
        console.error(reason.stack || reason);
        process.exit(1);
    } else if (reason) {
        console.log(`Shutting down with reason: ${reason}`);
        process.exit(reason === 'normal' ? 0 : 1);
    } else {
        process.exit(1);
    }
}
