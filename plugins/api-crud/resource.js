'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const permission = require('../../lib/permission');
const Promise = require('bluebird');

exports.controller = function (options) {
    return _.mapValues({
        list: {
            description: `Get a list of ${options.type} resources`,
            handler: _list,
        },
        create: {
            description: `Create a new ${options.type} resource`,
            handler: _create,
        },
        get: {
            description: `Get a specific ${options.type} resource`,
            handler: _get,
        },
        update: {
            description: `Update a specific ${options.type} resource`,
            handler: _update,
        },
        delete: {
            description: `Delete a specific ${options.type} resource`,
            handler: _delete,
        },
    }, route => _.merge(route, {
        tags: options.tags,
        auth: options.auth,
        plugins: { 'api-crud': options },
    }));
};

exports.endpoints = function (controller, options) {
    return [
        { method: 'GET',    path: `${options.prefix}`,      config: controller.list },
        { method: 'POST',   path: `${options.prefix}`,      config: controller.create },
        { method: 'GET',    path: `${options.prefix}/{id}`, config: controller.get },
        { method: 'PUT',    path: `${options.prefix}/{id}`, config: controller.update },
        { method: 'PATCH',  path: `${options.prefix}/{id}`, config: controller.update },
        { method: 'DELETE', path: `${options.prefix}/{id}`, config: controller.delete },
    ];
};

function _list(request, reply) {
    const options = request.route.settings.plugins['api-crud'];
    const cache = request.server.plugins.cache;
    const context = request.context();
    return Promise.join(
        cache.itemsOfType(context, options.type),
        permission.findForType(context, request.auth.credentials, options.type),
        (items, permissions) => items.filter(item => {
            return _.find(permissions, p => p.resource === item._id && p.read);
        }))
        .asCallback(reply);
}

function _create(request, reply) {
    const options = request.route.settings.plugins['api-crud'];
    const cache = request.server.plugins.cache;
    const context = request.context();
    return context.sdk.http.post(options.prefix, request.payload, auth(context)).tap(response => {
        if (response.statusCode === 200) {
            return cache.inject(context, options.type, response.data._id, response.data)
                .then(() => permission.grant(context, request.auth.credentials, { type: options.type, resource: response.data._id }))
                .catch(error => {
                    request.server.log([options.type, 'debug'], {
                        message: `Error caching new ${options.type}`,
                        reason: error,
                    });
                });
        }
    }).then(response => {
        reply(response.data).code(response.statusCode);
    }).catch(error => {
        request.server.log([options.type, 'debug'], {
            message: `Error creating new ${options.type}`,
            reason: error,
        });
        reply(Boom.serverUnavailable());
    });
}

function _get(request, reply) {
    const options = request.route.settings.plugins['api-crud'];
    const cache = request.server.plugins.cache;
    const context = request.context();
    return context.sdk.http.get(`${options.prefix}/${request.params.id}`, auth(context)).tap(response => {
        if (response.statusCode === 200) {
            return cache.inject(context, options.type, request.params.id, response.data)
                .catch(error => {
                    request.server.log([options.type, 'debug'], {
                        message: `Error caching existing ${options.type}`,
                        reason: error,
                    });
                });
        }
    }).catch(error => {
        request.server.log([options.type, 'debug'], {
            message: `Error retrieving existing ${options.type}`,
            reason: error,
        });
        return permission.isValid(context, {
            whoType: request.auth.credentials.whoType,
            who: request.auth.credentials.who,
            type: options.type,
            resource: request.params.id,
            read: true,
        })
        .then(valid => {
            if (!valid) {
                throw Boom.notFound();
            }
        })
        .then(() => cache.get(context, options.type, request.params.id, () => { throw Boom.notFound(); }))
        .then(resource => ({ statusCode: 200, data: resource }));
    }).then(response => {
        reply(response.data).code(response.statusCode);
    }).catch(reply);
}

function _update(request, reply) {
    const options = request.route.settings.plugins['api-crud'];
    const cache = request.server.plugins.cache;
    const context = request.context();
    return context.sdk.http[request.method](`${options.prefix}/${request.params.id}`, request.payload, auth(context)).tap(response => {
        if (response.statusCode === 200) {
            return cache.inject(context, options.type, request.params.id, response.data)
                .catch(error => {
                    request.server.log([options.type, 'debug'], {
                        message: `Error caching updated ${options.type}`,
                        reason: error,
                    });
                });
        }
    }).then(response => {
        reply(response.data).code(response.statusCode);
    }).catch(error => {
        request.server.log([options.type, 'debug'], {
            message: `Error updating ${options.type}`,
            reason: error,
        });
        reply(Boom.serverUnavailable());
    });
}

function _delete(request, reply) {
    const options = request.route.settings.plugins['api-crud'];
    const context = request.context();
    return context.sdk.http.delete(`${options.prefix}/${request.params.id}`, auth(context)).tap(response => {
        if (response.statusCode < 300) {
            return permission.revokeAll(context, { type: options.type, resource: request.params.id })
                .catch(error => {
                    request.server.log([options.type, 'debug'], {
                        message: `Error revoking ${options.type} permissions`,
                        reason: error,
                    });
                });
        }
    }).then(response => {
        reply(response.data).code(response.statusCode);
    }).catch(error => {
        request.server.log([options.type, 'debug'], {
            message: `Error deleting ${options.type}`,
            reason: error,
        });
        reply(Boom.serverUnavailable());
    });
}

function auth(context) {
    return {
        token: context.sdk.auth.token,
        [context.whoType]: context.who,
    };
}
