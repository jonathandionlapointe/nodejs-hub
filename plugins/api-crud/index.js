'use strict';

const _ = require('lodash');
const permission = require('../../lib/permission');
const resource = require('./resource');

exports.register = function (server, options, next) {
    if (!options.type) {
        return next(new Error('api-crud requires a type option (e.g. "location")'));
    }

    server.context();

    _.defaults(options, {
        prefix: `/${options.type}s`,
        tags: ['api'],
        auth: { strategy: 'token' },
    });

    permission.resourcePath(`${options.prefix}/:id`, options.type);

    server.dependency(['cache', 'sdk'], function (server, next) {
        server.log(['api-crud', 'debug'], `Registering API CRUD for ${options.type} resources`);

        const controller = resource.controller(options);
        server.route(resource.endpoints(controller, options));

        return next();
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json'),
    multiple: true,
};
