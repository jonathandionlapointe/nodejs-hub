'use strict';

// Generalized object caching. IDs must be unique by type.

const _ = require('lodash');

const views = exports.views = {
    update(context) {
        return context.getAppContext().collection('cache').createWithIndices([{
            name: 'cache_type_idx',
            fields: ['type']
        }]);
    }
};

exports.register = function (server, options, next) {
    server.context();

    server.dependency([], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Cache plugin');

        return views.update(server.context()).asCallback(done);
    });

    function get(context, type, id, fulfill, options) {
        const docId = `${type}:${id}`;

        options = _.merge({
            noCache: false,
            lifetime: null,
        }, options);

        const now = Date.now() / 1000;

        function fetch() {
            return fulfill().tap(function (data) {
                if (data) {
                    return inject(context, type, id, data, options);
                }
            });
        }

        if (!options.noCache) {
            return context.collection('cache').get(docId).catch(fetch).then(function (doc) {
                if ('undefined' === typeof doc) {
                    return fetch();
                } else if (doc.expiry < now) {
                    return fetch().catchReturn(doc.data);
                }
                return doc.data;
            });
        }
        return fetch();
    }

    function inject(context, type, id, data, options) {
        const _id = `${type}:${id}`;

        options = _.merge({
            noCache: false,
            lifetime: null,
        }, options);

        const now = Date.now() / 1000;
        const expiry = options.lifetime ? now + options.lifetime : undefined;

        return context.collection('cache').upsert({ _id, type, data, expiry });
    }

    function itemsOfType(context, type) {
        const results = [];
        return context.database.query(`
        SELECT json_extract(doc, '$.data') AS data
          FROM cache
         WHERE json_extract(doc, '$.type') = ?1
        `, [type], row => results.push(row.data))
        .return(results)
        .map(JSON.parse);
    }

    server.expose({ get: get, inject, itemsOfType });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
