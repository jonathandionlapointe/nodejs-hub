'use strict';

const Events = require('../../../lib/events');

module.exports = function (server) {
    const events = server.plugins.event;

    return {
        create: {
            description: 'Create an event',
            notes: 'Returns the created event',
            tags: ['api'],
            auth: {
                strategy: 'token',
                scope: ['event.create'],
            },
            validate: {
                payload: Events.schema,
                options: {
                    allowUnknown: true
                }
            },
            handler: function (request, reply) {
                server.log(['info', 'event'], request.payload);
                return events.emitEvent(request.context(), request.payload)
                .then(() => reply().code(204))
                .catch(error => reply(error));
            }
        }
    };
};
