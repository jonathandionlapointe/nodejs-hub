// Load modules

module.exports = function(plugin){

    var Event = require('./controllers/event-ctrl.js')(plugin);

    // API Server Endpoints
    var endpoints = [

        { method: 'POST',   path: '/events',       config: Event.create }

    ];

    return { endpoints:endpoints };

};