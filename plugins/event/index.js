'use strict';

const Events = require('../../lib/events');

exports.register = function (server, options, next) {
    const events = new Events(server.context());

    events.on('listening', pathRE => {
        server.log(['event', 'debug'], 'Listener on ' + pathRE);
    });
    events.on('sent', event => {
        server.log(['event', 'debug'], `Event ${event._id} forwarded to API`);
    });
    events.on('drop', (error, event) => {
        server.log(['event', 'error'], `Event got a ${error.statusCode} response; dropped; body: ${JSON.stringify(error.body)}`);
    });
    events.on('error', error => {
        server.log(['event', 'error'], 'Event could not be deleted, duplicate(s) will follow');
    });
    events.on('retry', info => {
        server.log(['event', 'info'], `Event forwarding failed, retrying in ${info.backoff}s`);
    });

    server.dependency(['sdk'], function (server, next) {
        server.log(['plugin', 'debug'], 'Registering the Event plugin');

        events.route = () => {
            var Routes = require('./routes')(server);
            server.route(Routes.endpoints);
        };

        server.on('stop', () => {
            events.stop();
        });

        return events.init(server.context()).asCallback(next);
    });

    server.plugins.event = events;

    return next();
};

exports.register.attributes = {
    name: 'event'
};
