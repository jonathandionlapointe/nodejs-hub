/*jslint node:true */
'use strict';

const _ = require('lodash');
const Alarm = require('../../lib/alarm');
const Boom = require('super-boom')();
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const routes = require('./routes');

exports.register = function (server, options, next) {
    server.context();

    Alarm.on('error', error => {
        server.log(['alarm', 'error'], error);
    });

    Alarm.on('register', system => {
        server.log(['alarm', 'debug'], `Alarm system ${system._doc._id} starting`);
    });

    Alarm.on('unregister', system => {
        server.log(['alarm', 'debug'], `Alarm system ${system._doc._id} stopping`);
    });

    server.method('alarm.system.list', function (auth, next) {
        return permission.findForType(this, auth.credentials, 'alarm')
        .map(perm => Alarm.systems[perm.resource])
        .filter(_.identity) // Remove missing entries, if any.
        .asCallback(next);
    });

    server.method('alarm.system.read', function (auth, systemId, next) {
        return checkPermission(this, auth, systemId, 'read')
        .then(valid => {
            const system = Alarm.System.get(systemId);
            if (!valid) throw Boom.notFound();
            return system;
        })
        .asCallback(next);
    });

    server.method('alarm.system.write', function (auth, systemId, next) {
        return checkPermission(this, auth, systemId, 'write')
        .then(valid => {
            const system = Alarm.System.get(systemId);
            if (!valid) throw Boom.forbidden();
            return system;
        })
        .asCallback(next);
    });

    server.method('alarm.system.manage', function (auth, systemId, next) {
        return checkPermission(this, auth, systemId, 'manage')
        .then(valid => {
            const system = Alarm.System.get(systemId);
            if (!valid) throw Boom.forbidden();
            return system;
        })
        .asCallback(next);
    });

    server.method('alarm.sector.read', function (auth, systemId, sectorId, next) {
        return checkPermission(this, auth, systemId, 'read')
        .then(valid => {
            const sector = Alarm.Sector.get(systemId, sectorId);
            if (!valid) throw Boom.notFound();
            return sector;
        })
        .asCallback(next);
    });

    server.method('alarm.sector.write', function (auth, systemId, sectorId, next) {
        return checkPermission(this, auth, systemId, 'write')
        .then(valid => {
            const sector = Alarm.Sector.get(systemId, sectorId);
            if (!valid) throw Boom.forbidden();
            return sector;
        })
        .asCallback(next);
    });

    server.method('alarm.zone.read', function (auth, systemId, zoneId, next) {
        return checkPermission(this, auth, systemId, 'read')
        .then(valid => {
            const zone = Alarm.Zone.get(systemId, zoneId);
            if (!valid) throw Boom.notFound();
            return zone;
        })
        .asCallback(next);
    });

    server.method('alarm.zone.write', function (auth, systemId, zoneId, next) {
        return checkPermission(this, auth, systemId, 'write')
        .then(valid => {
            const zone = Alarm.Zone.get(systemId, zoneId);
            if (!valid) throw Boom.forbidden();
            return zone;
        })
        .asCallback(next);
    });

    server.method('alarm.user', function (systemId, userId, next) {
        return Promise.try(() => Alarm.Access.getByUser(systemId, userId)).asCallback(next);
    });

    server.dependency(['system', 'device', 'event'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Alarm plugin');

        server.route(routes.endpoints);

        const event = server.plugins.event;
        event.addResourceType('/alarms', 'alarm');
        event.addResourceType('/alarms/:system/zones', 'zone');
        event.addResourceType('/alarms/:system/sector', 'sector');
        permission.resourcePath('/alarms/:id', 'alarm');
        permission.resourcePath('/alarms/:id/*path', 'alarm');

        const system = server.plugins.system;
        system.onAsync('shutdownRequested', (context, reason) => {
            if (reason === 'factoryReset') {
                return;
            }

            _.forEach(Alarm.systems, system => {
                if (system._doc.status !== 'disarmed') {
                    throw Boom.customCode(
                        400, 'Cannot shut down while armed',
                        'ALARM_SHUTDOWN_WHILE_ARMED'
                    );
                }
                _.forEach(system.zones, zone => {
                    if (zone._doc.zoneStatus === 'alarm') {
                        throw Boom.customCode(
                            400, 'Cannot shut down during an alarm',
                            'ALARM_SHUTDOWN_DURING_ALARM'
                        );
                    }
                });
            });
        });

        const ctx = server.context();
        return Alarm.views.update(ctx)
        .then(() => Alarm.System.startAll(ctx))
        .then(systems => {
            if (systems.length === 0) {
                return Alarm.System.create(ctx);
            }
        }).asCallback(done);
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};

function checkPermission(context, auth, systemId, access) {
    return permission.isValid(context, {
        whoType: auth.credentials.whoType,
        who: auth.credentials.who,
        type: 'alarm',
        resource: systemId,
        [access]: true,
    });
}
