'use strict';

const _ = require('lodash');
const Alarm = require('../../../lib/alarm');

exports.getAll = {
    description: 'Get a list of zones',
    notes: 'Returns a list of zones managed by an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.read'],
    },
    pre: ['alarm.system.read(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.read'];
        reply(_.map(system.zones, zone => zone.toJSON()));
    }
};

exports.create = {
    description: 'Create a new zone',
    notes: 'Returns the newly created zone. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.zone.create'],
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        return request.context().withTransactionContext(context => {
            return Alarm.Zone.create(context, system, request.payload);
        })
        .then(zone => zone.toJSON())
        .asCallback(reply);
    }
};

exports.getOne = {
    description: 'Get a zone',
    notes: 'Returns a zone',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.read'],
    },
    pre: ['alarm.zone.read(auth, params.systemId, params.zoneId)'],
    handler: function (request, reply) {
        reply(request.pre['alarm.zone.read'].toJSON());
    }
};

exports.update = {
    description: 'Update a zone',
    notes: 'Returns the updated zone. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.zone.edit'],
    },
    pre: ['alarm.zone.write(auth, params.systemId, params.zoneId)'],
    handler: function (request, reply) {
        const zone = request.pre['alarm.zone.write'];
        return request.context().withTransactionContext(context => {
            return zone.update(context, request.payload);
        })
        .then(zone => zone.toJSON())
        .asCallback(reply);
    }
};

exports.delete = {
    description: 'Deletes a zone',
    notes: 'Removes the zone from the system. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.zone.remove'],
    },
    pre: ['alarm.zone.write(auth, params.systemId, params.zoneId)'],
    handler: function (request, reply) {
        const zone = request.pre['alarm.zone.write'];
        return request.context().withTransactionContext(context => {
            return zone.delete(context);
        })
        .then(() => reply().code(204))
        .catch(reply);
    }
};
