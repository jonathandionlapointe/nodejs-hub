'use strict';

const _ = require('lodash');
const Alarm = require('../../../lib/alarm');
const Boom = require('boom');
const Joi = require('joi');
const Promise = require('bluebird');
const schedule = require('../../../lib/schedule');

exports.getAll = {
    description: 'Get a list of alarm systems',
    notes: 'Returns a list of alarm systems managed by this controller',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.list'],
    },
    pre: ['alarm.system.list(auth)'],
    handler: function (request, reply) {
        reply(_.map(request.pre['alarm.system.list'], system => system.toJSON()));
    }
};

exports.create = {
    description: 'Create and provision a new alarm system',
    notes: 'Returns a newly provisioned alarm system. The new system is disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.create'],
    },
    handler: function (request, reply) {
        return Alarm.System.create(request.context(), request.payload)
        .then(system => system.toJSON())
        .asCallback(reply);
    }
};

exports.getOne = {
    description: 'Get an alarm system',
    notes: 'Returns the specified alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.read'],
    },
    pre: ['alarm.system.read(auth, params.systemId)'],
    handler: function (request, reply) {
        reply(request.pre['alarm.system.read'].toJSON());
    }
};

exports.arm = {
    description: 'Arm or disarm an alarm system',
    notes: 'Attempts to arm or disarm a system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.arm'],
    },
    validate: {
        query: {
            armToken: Joi.string().default('invalid token')
        }
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        if (request.payload.status) {
            return system.verifyToken(request.query.armToken).then(access => {
                const options = _.pick(request.payload, ['bypassed', 'sectors', 'zones', 'timeout', 'rearm', 'skipExitDelays']);
                options.access = access;
                const status = request.payload.status;
                return request.context().withTransactionContext(context => {
                    return system.arm(context, status, options);
                });
            }).then(system => {
                if (system) {
                    reply(system.toJSON());
                } else {
                    reply().code(304);
                }
            }).catch(reply);
        } else {
            return system.update(request.context(), request.payload).then(() => {
                reply(_.omit(system._doc, ['_rev', 'type']));
            }).catch(reply);
        }
    }
};

exports.delete = {
    description: 'Delete an alarm system',
    notes: 'Removes the system and all of its zones and sectors. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.remove'],
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        return request.pre['alarm.system.write'].delete(request.context())
        .then(() => reply().code(204))
        .catch(reply);
    }
};

exports.viewAccess = {
    description: 'List access PINs for an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.manage(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.manage'];
        reply(_.map(system.access, access => access.toJSON()));
    }
};

exports.grantAccess = {
    description: 'Grant access PIN for an alarm system',
    tags: ['api'],
    validate: {
        payload: Alarm.Access.schema,
        options: {
            stripUnknown: true,
        },
    },
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.manage(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.manage'];
        return Promise.try(() => {
            if (request.payload.schedule) {
                return schedule.readForRequest(
                    request.context(), request.auth,
                    request.payload.schedule, 'read');
            }
        }).then(() => Alarm.Access.create(request.context(), system, request.payload))
        .then(access => access.toJSON())
        .asCallback(reply);
    }
};

exports.updateAccess = {
    description: 'Update an access PIN for an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.manage(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.manage'];
        const access = system.access[request.params.accessId];
        if (!access) {
            return reply(Boom.notFound());
        }
        return Promise.try(() => {
            if (request.payload.schedule) {
                return schedule.readForRequest(
                    request.context(), request.auth,
                    request.payload.schedule, 'read');
            }
        }).then(() => access.update(request.context(), request.payload))
        .then(access => access.toJSON())
        .asCallback(reply);
    }
};

exports.revokeAccess = {
    description: 'Remove an access PIN from an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.manage(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.manage'];
        const access = system.access[request.params.accessId];
        if (!access) {
            return reply(Boom.notFound());
        }
        return access.delete(request.context())
        .then(() => reply().code(204))
        .catch(reply);
    }
};

exports.getUsers = {
    description: 'Get users for an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.read(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.read'];
        return reply(_.map(_.filter(system.access, '_doc.user'), '_doc.user'));
    }
};

exports.createUser = {
    description: 'Create a user for an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    validate: {
        payload: {
            _id: Joi.string().required(),
        },
        options: {
            allowUnknown: true,
        },
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        try {
            Alarm.Access.getByUser(system._doc._id, request.payload._id); // throws Boom.notFound if missing.
            return reply().code(304); // Already exists, don't modify.
        } catch (err) {
            return Alarm.Access.create(request.context(), system, {
                pin: {}, // Generate PIN with default length.
                name: request.payload.name || 'New User',
                expiry: new Date(0).toISOString(), // Disable PIN by default.
                user: request.payload._id,
            })
            .then(access => ({ _id: access._doc.user }))
            .asCallback(reply);
        }
    }
};

exports.updateUser = {
    description: 'Update a user for an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        try {
            const access = Alarm.Access.getByUser(system._doc._id, request.params.userId);
            if (request.payload.name) {
                return access.update(request.context(), { name: request.payload.name })
                .then(access => ({ _id: access._doc.user }))
                .asCallback(reply);
            } else {
                return reply().code(304);
            }
        } catch (err) {
            reply(err);
        }
    }
};

exports.deleteUser = {
    description: 'Remove a user from an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        try {
            const access = Alarm.Access.getByUser(system._doc._id, request.params.userId);
            return access.delete(request.context())
            .then(() => reply().code(204))
            .catch(reply);
        } catch (err) {
            reply(err);
        }
    }
};

exports.setUserPin = {
    description: 'Set a PIN for a user',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.manage_pins'],
    },
    validate: {
        payload: {
            pin: Joi.string().required()
        }
    },
    pre: [
        ['alarm.user(params.systemId, params.userId)']
    ],
    handler: function (request, reply) {
        const access = request.pre['alarm.user'];
        return access.update(request.context(), { pin: request.payload.pin, expiry: 'never' })
        .then(() => reply().code(204))
        .catch(reply);
    }
};

exports.generateToken = {
    description: 'Generate an arming token',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.arm'],
    },
    validate: {
        payload: {
            pin: Joi.string().required()
        }
    },
    pre: ['alarm.system.read(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.read'];
        return system.generateToken(request.context(), request.payload.pin)
        .then(token => ({ armingToken: token }))
        .asCallback(reply);
    }
};
