'use strict';

const _ = require('lodash');
const Alarm = require('../../../lib/alarm');

exports.getAll = {
    description: 'Get a list of sectors',
    notes: 'Returns a list of sectors managed by an alarm system',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.read'],
    },
    pre: ['alarm.system.read(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.read'];
        reply(_.map(system.sectors, sector => sector.toJSON()));
    }
};

exports.create = {
    description: 'Create a new sector',
    notes: 'Returns the newly created sector. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.sector.create'],
    },
    pre: ['alarm.system.write(auth, params.systemId)'],
    handler: function (request, reply) {
        const system = request.pre['alarm.system.write'];
        return Alarm.Sector.create(request.context(), system, request.payload)
        .then(sector => sector.toJSON())
        .asCallback(reply);
    }
};

exports.getOne = {
    description: 'Get a sector',
    notes: 'Returns a sector',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.read'],
    },
    pre: ['alarm.sector.read(auth, params.systemId, params.sectorId)'],
    handler: function (request, reply) {
        reply(request.pre['alarm.sector.read'].toJSON());
    }
};

exports.update = {
    description: 'Update a sector',
    notes: 'Returns the updated sector. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.sector.edit'],
    },
    pre: ['alarm.sector.write(auth, params.systemId, params.sectorId)'],
    handler: function (request, reply) {
        return request.pre['alarm.sector.write'].update(request.context(), request.payload)
        .then(sector => sector.toJSON())
        .asCallback(reply);
    }
};

exports.delete = {
    description: 'Delete a sector',
    notes: 'The zones of the sector are not modified. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.sector.remove'],
    },
    pre: ['alarm.sector.write(auth, params.systemId, params.sectorId)'],
    handler: function (request, reply) {
        return request.pre['alarm.sector.write'].delete(request.context())
        .then(() => reply().code(204))
        .catch(reply);
    }
};

exports.includeZone = {
    description: 'Include a zone in this sector',
    notes: 'Returns the new list of zones included in this sector. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.sector.edit'],
    },
    pre: [
        [
            'alarm.sector.write(auth, params.systemId, params.sectorId)',
            'alarm.zone.read(auth, params.systemId, params.zoneId)'
        ],
    ],
    handler: function (request, reply) {
        const sector = request.pre['alarm.sector.write'];
        const zone = request.pre['alarm.zone.read'];
        return sector.include(request.context(), zone).asCallback(reply);
    }
};

exports.excludeZone = {
    description: 'Exclude a zone from this sector',
    notes: 'Returns the new list of zones included in this sector. The system must be disarmed.',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['alarm.sector.edit'],
    },
    pre: [
        [
            'alarm.sector.write(auth, params.systemId, params.sectorId)',
            'alarm.zone.read(auth, params.systemId, params.zoneId)'
        ],
    ],
    handler: function (request, reply) {
        const sector = request.pre['alarm.sector.write'];
        const zone = request.pre['alarm.zone.read'];
        return sector.exclude(request.context(), zone).asCallback(reply);
    }
};
