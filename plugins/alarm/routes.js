'use strict';

const alarm = require('./controllers/alarm-ctrl');
const sector = require('./controllers/sector-ctrl');
const zone = require('./controllers/zone-ctrl');

exports.endpoints = [
    // Alarm system interface.
    { method: 'GET',    path: '/alarms', config: alarm.getAll },
    { method: 'POST',   path: '/alarms', config: alarm.create },
    { method: 'GET',    path: '/alarms/{systemId}', config: alarm.getOne },
    { method: 'PUT',    path: '/alarms/{systemId}', config: alarm.arm },
    { method: 'DELETE', path: '/alarms/{systemId}', config: alarm.delete },

    // Access/PIN interface.
    { method: 'GET',    path: '/alarms/{systemId}/access', config: alarm.viewAccess },
    { method: 'POST',   path: '/alarms/{systemId}/access', config: alarm.grantAccess },
    { method: 'PUT',    path: '/alarms/{systemId}/access/{accessId}', config: alarm.updateAccess },
    { method: 'DELETE', path: '/alarms/{systemId}/access/{accessId}', config: alarm.revokeAccess },
    { method: 'POST',   path: '/alarms/{systemId}/auth/token', config: alarm.generateToken },

    // Deprecated PIN management interface.
    { method: 'GET',    path: '/alarms/{systemId}/users', config: alarm.getUsers },
    { method: 'POST',   path: '/alarms/{systemId}/users', config: alarm.createUser },
    { method: 'DELETE', path: '/alarms/{systemId}/users', config: alarm.deleteUser },
    { method: 'PUT',    path: '/alarms/{systemId}/users/{userId}', config: alarm.updateUser },
    { method: 'DELETE', path: '/alarms/{systemId}/users/{userId}', config: alarm.deleteUser },
    { method: 'POST',   path: '/alarms/{systemId}/users/{userId}/pin', config: alarm.setUserPin },
    { method: 'PUT',    path: '/alarms/{systemId}/users/{userId}/pin', config: alarm.setUserPin },

    // Sector interface.
    { method: 'GET',    path: '/alarms/{systemId}/sectors', config: sector.getAll },
    { method: 'POST',   path: '/alarms/{systemId}/sectors', config: sector.create },
    { method: 'GET',    path: '/alarms/{systemId}/sectors/{sectorId}', config: sector.getOne },
    { method: 'PUT',    path: '/alarms/{systemId}/sectors/{sectorId}', config: sector.update },
    { method: 'DELETE', path: '/alarms/{systemId}/sectors/{sectorId}', config: sector.delete },
    { method: 'POST',   path: '/alarms/{systemId}/sectors/{sectorId}/zones/{zoneId}', config: sector.includeZone },
    { method: 'DELETE', path: '/alarms/{systemId}/sectors/{sectorId}/zones/{zoneId}', config: sector.excludeZone },

    // Zone interface.
    { method: 'GET',    path: '/alarms/{systemId}/zones', config: zone.getAll },
    { method: 'POST',   path: '/alarms/{systemId}/zones', config: zone.create },
    { method: 'GET',    path: '/alarms/{systemId}/zones/{zoneId}', config: zone.getOne },
    { method: 'PUT',    path: '/alarms/{systemId}/zones/{zoneId}', config: zone.update },
    { method: 'DELETE', path: '/alarms/{systemId}/zones/{zoneId}', config: zone.delete }
];
