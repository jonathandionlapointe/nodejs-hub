'use strict';

const Joi = require('joi');
const schedule = require('../../../lib/schedule');

exports.list = {
    description: 'Get a list of schedules',
    notes: 'Returns a list of schedules',
    auth: {
        strategy: 'token',
        scope: ['schedule.list'],
    },
    pre: ['schedule.list(auth)'],
    handler: function (request, reply) {
        reply(request.pre['schedule.list']);
    }
};

exports.create = {
    description: 'Creates a new schedule',
    auth: {
        strategy: 'token',
        scope: ['schedule.create'],
    },
    validate: {
        payload: schedule.schema
    },
    handler: function (request, reply) {
        const context = request.context();
        return schedule.create(context, request.payload)
        .asCallback(reply);
    }
};

exports.read = {
    description: 'Get a schedule',
    notes: 'Returns the specified schedule',
    auth: {
        strategy: 'token',
        scope: ['schedule.read'],
    },
    validate: {
        params: { id: Joi.string().required() },
    },
    pre: ['schedule.read(auth, params.id)'],
    handler: function (request, reply) {
        reply(request.pre['schedule.read']);
    }
};

exports.update = {
    description: 'Update a schedule',
    notes: 'Updates the specified schedule',
    auth: {
        strategy: 'token',
        scope: ['schedule.edit'],
    },
    validate: {
        params: { id: Joi.string().required() },
        payload: schedule.schema
    },
    pre: ['schedule.write(auth, params.id)'],
    handler: function (request, reply) {
        return schedule.update(request.context(), request.payload, request.params.id)
        .asCallback(reply);
    }
};

exports.delete = {
    description: 'Removes a schedule',
    auth: {
        strategy: 'token',
        scope: ['schedule.remove'],
    },
    validate: {
        params: { id: Joi.string().required() },
    },
    pre: ['schedule.write(auth, params.id)'],
    handler: function (request, reply) {
        return schedule.remove(request.context(), request.params.id)
        .then(() => reply().code(204))
        .catch(reply);
    }
};
