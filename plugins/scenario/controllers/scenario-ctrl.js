'use strict';

const _ = require('lodash');
const config = require('../../../lib/config');
const Joi = require('joi');
const scenario = require('../scenario');

function objectid() {
    return Joi.string().regex(/^[0-9A-Fa-f]{24}$/);
}

function rule() {
    return Joi.object().keys({
        type: Joi.string().strip(),
        _id: objectid(),
        _rev: Joi.string().strip(),
        errors: Joi.any().strip(),
        enabled: Joi.boolean().default(true),
        name: Joi.string(),
        cooldown: Joi.number().default(config.scenario.cooldown.default),
        trigger: trigger().required(),
        action: action().required(),
        schedule: objectid(),
        gateway: Joi.string(),
        status: Joi.string(),
        createdTs: Joi.string(),
    });
}

function trigger() {
    return Joi.alternatives().try([
        Joi.object().keys({
            type: Joi.string().valid('capability').required(),
            device: select().required(),
            capability: select().required(),
            condition: condition().required(),
        }),
        Joi.object().keys({
            type: Joi.string().valid('startup', 'shutdown').required(),
        }),
        Joi.object().keys({
            type: Joi.string().valid('schedule').required(),
            schedule: objectid().required(),
        }),
        Joi.object().keys({
            type: Joi.string().valid('alarmSystemStatus').required(),
            alarmSystem: select().required(),
            alarmSystemStatus: select().required(),
        }),
        Joi.object().keys({
            type: Joi.string().valid('zoneStatus').required(),
            alarmSystem: select().required(),
            zone: select().required(),
            zoneStatus: select().required(),
        }),
    ]);
}

function action() {
    return Joi.alternatives().try([
        Joi.object({
            type: Joi.string().valid('setCapability').required(),
            device: objectid().required(),
            capability: Joi.string().required(),
            value: Joi.alternatives().try(Joi.string()).required(),
        }),
        Joi.object({
            type: Joi.string().valid('setCapability').required(),
            device: objectid().required(),
            capability: Joi.string().required(),
            value: Joi.alternatives().try(Joi.number()).required(),
            unit: Joi.string().required(),
        }),
        Joi.object({
            type: Joi.string().valid('sequence').required(),
            actions: Joi.array().min(1).items(Joi.lazy(action)),
        })
    ]);
}

function condition() {
    return Joi.alternatives().try([
        always(), never(),
        exact(), comparison(),
        not(condition), all(condition), any(condition)
    ]);
}

function select() {
    return Joi.alternatives().try([
        always(), never(),
        exact(),
        not(select), any(select)
    ]);
}

function always() {
    return Joi.object({
        type: Joi.string().valid('always').required(),
    });
}

function never() {
    return Joi.object({
        type: Joi.string().valid('never').required(),
    });
}

function exact() {
    return Joi.object({
        type: Joi.string().valid('exact').required(),
        value: Joi.string().required(),
    });
}

function comparison() {
    return Joi.object({
        type: Joi.string().valid('<', '<=', '>', '>=', '=', '!=').required(),
        value: Joi.number().required(),
        unit: Joi.string().required(),
        epsilon: Joi.number(),
    });
}

function not(condition) {
    return Joi.object({
        type: Joi.string().valid('not').required(),
        condition: Joi.lazy(condition).required(),
    });
}

function all(condition) {
    return Joi.object({
        type: Joi.string().valid('all').required(),
        conditions: Joi.array().min(1).items(Joi.lazy(condition)),
    });
}

function any(condition) {
    return Joi.object({
        type: Joi.string().valid('any').required(),
        conditions: Joi.array().min(1).items(Joi.lazy(condition)),
    });
}

exports.getAll = {
    description: 'Get a list of scenarios',
    notes: 'Returns a list of scenarios',
    auth: {
        strategy: 'token',
        scope: ['scenario.list'],
    },
    pre: ['scenario.list(auth)'],
    handler: function (request, reply) {
        reply(request.pre['scenario.list'].map(fixSchedule));
    }
};

exports.create = {
    description: 'Creates a new scenario',
    auth: {
        strategy: 'token',
        scope: ['scenario.create'],
    },
    validate: {
        payload: rule(),
    },
    handler: function (request, reply) {
        const context = request.context();
        return scenario.validate(context, request.payload).then(() => {
            return scenario.create(context, request.payload);
        }).then(fixSchedule).asCallback(reply);
    }
};

exports.read = {
    description: 'Get a scenario',
    notes: 'Returns the specified scenario',
    auth: {
        strategy: 'token',
        scope: ['scenario.read'],
    },
    validate: {
        params: { id: Joi.string().required() },
    },
    pre: ['scenario.read(auth, params.id)'],
    handler: function (request, reply) {
        reply(fixSchedule(request.pre['scenario.read']));
    }
};

exports.update = {
    description: 'Update a scenario',
    notes: 'Updates the specified scenario',
    auth: {
        strategy: 'token',
        scope: ['scenario.edit'],
    },
    validate: {
        params: { id: Joi.string().required() },
        payload: rule(),
    },
    pre: ['scenario.write(auth, params.id)'],
    handler: function (request, reply) {
        const context = request.context();
        return scenario.validate(context, request.payload).then(() => {
            return scenario.update(context, request.payload, request.params.id);
        }).then(fixSchedule).asCallback(reply);
    }
};

exports.delete = {
    description: 'Removes a scenario',
    auth: {
        strategy: 'token',
        scope: ['scenario.remove'],
    },
    validate: {
        params: { id: Joi.string().required() },
    },
    pre: ['scenario.write(auth, params.id)'],
    handler: function (request, reply) {
        return scenario.delete(request.context(), request.params.id)
        .then(() => reply().code(204))
        .catch(reply);
    }
};

function fixSchedule(rule) {
    if (rule.trigger.type === 'schedule') {
        const id = _.get(rule.trigger, 'schedule._id');
        if (id) {
            rule.trigger.schedule = id;
        }
    }
    return rule;
}
