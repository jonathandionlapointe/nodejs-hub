'use strict';

const scenario = require('./controllers/scenario-ctrl');
const schedule = require('./controllers/schedule-ctrl');

exports.endpoints = [
    { method: 'GET', path: '/scenarios', config: scenario.getAll },
    { method: 'POST', path: '/scenarios', config: scenario.create },
    { method: 'GET', path: '/scenarios/{id}', config: scenario.read },
    { method: 'PUT', path: '/scenarios/{id}', config: scenario.update },
    { method: 'DELETE', path: '/scenarios/{id}', config: scenario.delete },

    { method: 'GET', path: '/schedules', config: schedule.list },
    { method: 'POST', path: '/schedules', config: schedule.create },
    { method: 'GET', path: '/schedules/{id}', config: schedule.read },
    { method: 'PUT', path: '/schedules/{id}', config: schedule.update },
    { method: 'DELETE', path: '/schedules/{id}', config: schedule.delete },
];
