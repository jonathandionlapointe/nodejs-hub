'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const condition = require('../../lib/condition');
const config = require('../../lib/config');
const Device = require('../../lib/device');
const later = require('later');
const ObjectID = require('bson-objectid');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const schedule = require('../../lib/schedule');

const views = {
    update(context) {
        return context.getAppContext().collection('scenarios').create();
    }
};

const eventTriggers = {};

function getAll(context) {
    const results = [];
    return context.database.query('SELECT doc FROM scenarios', [], row => results.push(row.doc))
    .return(results)
    .map(JSON.parse)
    .map(doc => {
        if (doc.trigger.type === 'schedule' && typeof doc.trigger.schedule === 'string') {
            return schedule.read(context, doc.trigger.schedule)
            .then(schedule => {
                doc.trigger.schedule = schedule;
            })
            .catch(error => {
                // Ignore!
            })
            .return(doc);
        }
        return doc;
    });
}

function listForRequest(context, auth) {
    const results = [];
    return permission.findForType(context, auth.credentials, 'scenario')
    .filter(p => p.read)
    .map(p => p.resource)
    .then(ids => context.database.query(`
    SELECT doc
      FROM scenarios
     WHERE _id IN (SELECT value FROM json_each(?1))
    `, [JSON.stringify(ids)], row => results.push(row.doc)))
    .return(results)
    .map(JSON.parse);
}

function readForRequest(context, auth, id, access) {
    return permission.isValid(context, {
        type: 'scenario',
        resource: id,
        who: auth.credentials.who,
        whoType: auth.credentials.whoType,
        read: true,
        [access]: true,
    })
    .then(hasAccess => {
        if (!hasAccess) {
            throw Boom.forbidden();
        }
    })
    .then(() => context.collection('scenarios').get(id))
    .tap(result => {
        if (!result) {
            throw Boom.notFound();
        }
    });
}

/**
 * Validates the rule data.
 *
 * The rule is assumed to be well formed (the scenario-ctrl does this
 * with a Joi schema). This method makes sure that the device(s) and
 * capability(-ies) that the rule references exist.
 *
 * Invalid scenarios (bad device IDs, cap values, names) are allowed,
 * it is expected that the errors will be displayed to the user for
 * them to fix (select another device etc).
 *
 * TODO validation needs to be applied not only upon request to
 * create/update scenarios, for example if a device is removed/deleted
 * ths scenario must be marked as invalid as well.
 */
function validate(context, rule) {
    rule.errors = [];
    validateCooldown(context, rule);
    return Promise.all([
        validateTrigger(context, rule, rule.trigger),
        validateAction(context, rule, rule.action),
    ]);
}

function validateCooldown(context, rule) {
    if (!Number.isSafeInteger(rule.cooldown)) {
        rule.cooldown = config.scenario.cooldown.default;
    }
    rule.cooldown = Math.min(
        Math.max(rule.cooldown, config.scenario.cooldown.minimum),
        config.scenario.cooldown.maximum);
}

function validateTrigger(context, rule, trigger) {
    if (trigger.type === 'capability') {
        const deviceIds = condition.exactValues(trigger.device);
        const capNames = condition.exactValues(trigger.capability);
        trigger.condition = condition.simplify(trigger.condition);
        return Promise.map(deviceIds, id => Device.load(context, id).reflect()).map(devicePromise => {
            if (!devicePromise.isFulfilled()) {
                const reason = devicePromise.reason();
                if (reason.isBoom) {
                    rule.errors.push(reason.output);
                    return;
                } else {
                    throw reason;
                }
            }

            // For always/never, not much more validation we can do.
            if (capNames.length === 0) {
                return;
            }

            const device = devicePromise.value();
            const deviceCapNames = _.map(device.model.capabilities, 'capabilityId');
            const existingCaps = _.intersection(capNames, deviceCapNames);
            if (existingCaps.length === 0) {
                rule.errors.push(Boom.customCode(400, 'invalid trigger capability', 'SCENARIO_TRIGGER_CAP_NOT_FOUND', [], {
                    device: device._id,
                    capability: trigger.capability
                }).output);
                return;
            }

            _.forEach(device.model.capabilities, capModel => {
                if (!_.includes(existingCaps, capModel.capabilityId)) {
                    return;
                }
                const cap = device.caps[capModel.capabilityId];
                try {
                    condition.matches(cap, trigger.condition);
                } catch (error) {
                    rule.errors.push(Boom.customCode(400, 'invalid trigger condition', 'SCENARIO_TRIGGER_COND_INVALID', [], {
                        capability: trigger.capability
                    }).output);
                }
            });
        });
    } else if (trigger.type === 'startup' || trigger.type === 'shutdown') {
        // Ok, no further validation needed.
    } else if (trigger.type === 'schedule') {
        // Check that the schedule exists.
        return schedule.read(context, trigger.schedule)
        .then(schedule => {
            trigger.schedule = schedule;
        })
        .catch(error => {
            rule.errors.push(Boom.customCode(400, 'invalid trigger schedule', 'SCENARIO_TRIGGER_SCHED_INVALID', [], {
                error: error.message
            }).output);
        });
    } else if (trigger.type === 'alarmSystemStatus') {
        trigger.alarmSystem = condition.simplify(trigger.alarmSystem);
        trigger.alarmSystemStatus = condition.simplify(trigger.alarmSystemStatus);
        let conditionName;
        try {
            conditionName = 'alarmSystem';
            condition.matches({ value: '' }, trigger.alarmSystem);
            conditionName = 'alarmSystemStatus';
            condition.matches({ value: '' }, trigger.alarmSystemStatus);
        } catch (error) {
            rule.errors.push(Boom.customCode(400, 'invalid alarm system condition', 'SCENARIO_TRIGGER_ALARM_INVALID', [], {
                condition: conditionName
            }).output);
        }
    } else if (trigger.type === 'zoneStatus') {
        trigger.alarmSystem = condition.simplify(trigger.alarmSystem);
        trigger.zone = condition.simplify(trigger.zone);
        trigger.zoneStatus = condition.simplify(trigger.zoneStatus);
        let conditionName;
        try {
            conditionName = 'alarmSystem';
            condition.matches({ value: '' }, trigger.alarmSystem);
            conditionName = 'zone';
            condition.matches({ value: '' }, trigger.zone);
            conditionName = 'zoneStatus';
            condition.matches({ value: '' }, trigger.zoneStatus);
        } catch (error) {
            rule.errors.push(Boom.customCode(400, 'invalid zone condition', 'SCENARIO_TRIGGER_ZONE_INVALID', [], {
                condition: conditionName
            }).output);
        }
    } else {
        throw Boom.customCode(400, 'invalid trigger', 'SCENARIO_TRIGGER_INVALID', [], {
            type: trigger.type
        });
    }
}

function validateAction(context, rule, action) {
    if (action.type === 'setCapability') {
        return Device.load(context, action.device).then(device => {
            const capModel = _.findWhere(device.model.capabilities, { capabilityId: action.capability });
            if (!capModel) {
                throw Boom.customCode(400, 'invalid action capability', 'SCENARIO_ACTION_CAP_NOT_FOUND', [], {
                    capability: action.capability
                });
            }
            if (!_.contains(capModel.methods, 'put')) {
                throw Boom.customCode(400, 'action capability is read-only', 'SCENARIO_ACTION_CAP_READONLY', [], {
                    capability: action.capability
                });
            }
            if (action.unit) {
                if (capModel.type !== 'float') {
                    throw Boom.customCode(400, 'invalid action value', 'SCENARIO_ACTION_CAP_VALUE', [], {
                        capability: action.capability
                    });
                }
                if (capModel.range) {
                    try {
                        if (!condition.matches(action, {
                            type: 'all',
                            conditions: [
                                { type: '>=', value: capModel.range[0].value, unit: capModel.range[0].unit },
                                { type: '<=', value: capModel.range[1].value, unit: capModel.range[1].unit },
                            ]
                        })) {
                            throw Boom.customCode(400, 'out of range value for action capability', 'SCENARIO_ACTION_CAP_RANGE', [], {
                                capability: action.capability
                            });
                        }
                    } catch (error) {
                        if (error.isBoom) {
                            throw error;
                        }
                        throw Boom.customCode(400, 'invalid action value', 'SCENARIO_ACTION_CAP_VALUE', [], {
                            capability: action.capability
                        });
                    }
                }
            } else {
                if (capModel.type !== 'enum') {
                    throw Boom.customCode(400, 'invalid action value', 'SCENARIO_ACTION_CAP_VALUE', [], {
                        capability: action.capability
                    });
                }
                if (!_.contains(capModel.range, action.value)) {
                    throw Boom.customCode(400, 'out of range value for action capability', 'SCENARIO_ACTION_CAP_RANGE', [], {
                        capability: action.capability
                    });
                }
            }
        }).catch(error => {
            if (error.isBoom) {
                rule.errors.push(error.output);
            } else {
                throw error;
            }
        });
    } else if (action.type === 'sequence') {
        action.actions.forEach(action => {
            validateAction(context, rule, action);
        });
    } else {
        return Promise.reject(Boom.customCode(400, 'invalid action', 'SCENARIO_ACTION_INVALID', [], {
            type: action.type
        }));
    }
}

function create(context, rule) {
    rule._id = rule._id || ObjectID().toString();
    rule.type = 'rule';
    rule.triggeredAt = null;
    rule.gateway = context.gatewayId;

    const doc = _.omit(rule, ['_rev']);
    if (doc.trigger.type === 'schedule' && typeof doc.trigger.schedule === 'object') {
        doc.trigger = { type: 'schedule', schedule: doc.trigger.schedule._id };
    }
    return context.collection('scenarios').insert(_.omit(rule, ['_rev'])).then(result => {
        if (result.ok) {
            rule._id = result.id;
            rule._rev = result.rev;

            const credentials = permission.systemGroupCredentials(context);
            return permission.grant(context, credentials, {
                type: 'scenario',
                resource: rule._id,
            }).then(() => context.emitEvent({
                type: 'create',
                path: `/scenarios/${rule._id}`,
                resource: rule,
            })).return(rule);
        } else {
            context.server.log(['scenario','error'], 'Unable to insert rule into database');
            throw Boom.badImplementation('unable to update database');
        }
    })
    .then(rule => read(context, rule._id)) // Load schedule if necessary.
    .tap(rule => start(context, rule));
}

function read(context, id) {
    return context.collection('scenarios').get(id)
    .then(doc => {
        if (doc && doc.trigger.type === 'schedule' && typeof doc.trigger.schedule === 'string') {
            return schedule.read(context, doc.trigger.schedule)
            .then(schedule => {
                doc.trigger.schedule = schedule;
            })
            .catch(error => {
                // Ignore!
            })
            .return(doc);
        }
        return doc;
    });
}

function update(context, rule, id) {
    // Prevent update conflicts! The rule must be stopped before we
    // perform any async work; otherwise a trigger match could occur
    // while we're waiting to get the existing rule data, resulting in
    // this "thread" writing stale data:
    //
    //     this thread         event thread
    //      db.get(id)              |
    //          |              trigger match
    //          |               update data
    //     got old data             |
    //       db.update              V
    //  stale data written!
    //          X
    stop(context, { _id: id });

    return context.collection('scenarios').get(id).then(oldRule => {
        if (!oldRule || oldRule.type !== 'rule') {
            throw Boom.notFound('scenario rule not found');
        }

        rule.type = 'rule';
        rule.triggeredAt = oldRule.triggeredAt;
        rule._rev = oldRule._rev;
        rule._id = id;

        const doc = _.omit(rule, ['_rev']);
        if (doc.trigger.type === 'schedule' && typeof doc.trigger.schedule === 'object') {
            doc.trigger = { type: 'schedule', schedule: doc.trigger.schedule._id };
        }

        let restarted = false;

        return context.collection('scenarios').update(doc, id).then(result => {
            if (!result.ok) {
                context.server.log(['scenario','error'], 'Unable to update rule in database');
                throw Boom.badImplementation('unable to update database');
            }

            rule._rev = result.rev;

            start(context, rule);
            restarted = true;

            return context.emitEvent({
                type: 'update',
                path: `/scenarios/${rule._id}`,
                resource: rule,
            }).return(rule);
        }).catch(error => {
            if (!restarted) {
                start(context, oldRule);
            }
            throw error;
        });
    });
}

function deleteRule(context, id) {
    return context.collection('scenarios').delete(id).then(result => {
        if (result.ok) {
            stop(context, { _id: id });

            return context.emitEvent({
                type: 'delete',
                path: `/scenarios/${id}`,
                resource: {},
            });
        } else {
            context.server.log(['scenario','error'], 'Unable to remove rule from database');
            throw Boom.badImplementation('unable to update database');
        }
    });
}

function startAll(context) {
    return getAll(context)
    .mapSeries(doc => start(context, doc));
}

function start(context, rule) {
    if (!rule.enabled) {
        return;
    }

    if (eventTriggers[rule._id]) {
        return;
    }

    validateCooldown(context, rule);

    const triggers = addEventTrigger(context, rule, rule.trigger);
    if (triggers.length > 0) {
        eventTriggers[rule._id] = triggers;
    }
}

function stop(context, rule) {
    const triggers = eventTriggers[rule._id];
    delete eventTriggers[rule._id];
    if (triggers) {
        const system = context.server.plugins.system;
        const events = context.server.plugins.event;
        _.forEach(triggers, trigger => {
            if (trigger.type === 'capability' || trigger.type === 'alarmSystemStatus' || trigger.type === 'zoneStatus') {
                events.removeEventListener(trigger.listener);
            } else if (trigger.type === 'startup') {
                system.removeListener('startup', trigger.listener);
            } else if (trigger.type === 'shutdown') {
                system.removeListener('shutdown.default', trigger.listener);
            } else if (trigger.type === 'schedule') {
                trigger.timer.clear();
            } else if (trigger.type === 'cooldown') {
                clearTimeout(trigger.timer);
            }
        });
    }
}

function stopAll(context) {
    _.forEach(Object.keys(eventTriggers), id => {
        stop(context, { _id: id });
    });
}

function restart(context, rule) {
    context = context.getServerContext();
    stop(context, rule);
    start(context, rule);
}

function setCooldown(context, rule) {
    context = context.getServerContext();
    stop(context, rule);
    eventTriggers[rule._id] = [{
        type: 'cooldown',
        timer: context.setTimeout('scenario.cooldown', restart, rule.cooldown * 1000, rule)
    }];
}

// TODO register a single listener for each of:
//
//     - startup
//     - shutdown
//     - event
//
// and match the path and run all related events in that listener,
// rather than registering an event listener for each and every
// scenario associated with a particular type,
function addEventTrigger(context, rule, trigger) {
    const system = context.server.plugins.system;
    const events = context.server.plugins.event;
    if (trigger.type === 'capability') {
        const path = '^/devices/([^/]+)/caps/([^/]+)$';
        return [{
            type: trigger.type,
            listener: events.onEventPathMatch(new RegExp(path), (context, matches, event) => {
                if (event.type === 'update' &&
                    condition.matches({ value: matches[1] }, trigger.device) &&
                    condition.matches({ value: matches[2] }, trigger.capability) &&
                    condition.matches(event.resource, trigger.condition))
                {
                    return runAction(context, rule, rule.action);
                }
            }),
        }];
    } else if (trigger.type === 'startup') {
        return [{
            type: trigger.type,
            listener: system.onAsync('startup', context => {
                return runAction(context, rule, rule.action);
            }),
        }];
    } else if (trigger.type === 'shutdown') {
        return [{
            type: trigger.type,
            listener: system.onAsync('shutdown.default', context => {
                return runAction(context, rule, rule.action);
            }),
        }];
    } else if (trigger.type === 'schedule') {
        // Handle old schedule trigger schema with embedded schedule data.
        // Eventually those should be phased out and simply parsing the schedule
        // should suffice.
        const schedule = trigger.schedules
            ? { schedules: trigger.schedules }
            : later.parse.text(_.get(trigger.schedule, 'schedules', ''));
        return [{
            type: trigger.type,
            timer: context.setSchedule('scenario', runAction, schedule, rule, rule.action),
        }];
    } else if (trigger.type === 'alarmSystemStatus') {
        const path = '^/alarms/([^/]+)$';
        return [{
            type: trigger.type,
            listener: events.onEventPathMatch(new RegExp(path), (context, matches, event) => {
                if (event.type === 'update' &&
                    condition.matches({ value: matches[1] }, trigger.alarmSystem) &&
                    condition.matches({ value: event.resource.status || '' }, trigger.alarmSystemStatus))
                {
                    return runAction(context, rule, rule.action);
                }
            }),
        }];
    } else if (trigger.type === 'zoneStatus') {
        const path = '^/alarms/([^/]+)/zones/([^/]+)$';
        return [{
            type: trigger.type,
            listener: events.onEventPathMatch(new RegExp(path), (context, matches, event) => {
                if (event.type === 'update' &&
                    condition.matches({ value: matches[1] }, trigger.alarmSystem) &&
                    condition.matches({ value: matches[2] }, trigger.zone) &&
                    condition.matches({ value: event.resource.zoneStatus || '' }, trigger.zoneStatus))
                {
                    return runAction(context, rule, rule.action);
                }
            }),
        }];
    } else {
        throw new Error('invalid trigger type');
    }
}

function runAction(context, rule, action) {
    const errors = [];

    try {
        setCooldown(context, rule);
    } catch (error) {
        errors.push({
            phase: 'setCooldown',
            error: error,
        });
    }

    return Promise.try(() => {
        if (!rule.schedule) {
            return true;
        }
        return schedule.isValid(context, rule.schedule).catch(error => {
            if (!error.isBoom) {
                throw error;
            }
            return true;
        });
    }).then(allowedBySchedule => {
        if (!allowedBySchedule) {
            context.server.log(['scenario', 'debug'], {
                message: 'Skipping scenario action due to schedule',
                scenario: rule._id,
            });
            return;
        }

        rule.triggeredAt = new Date().toISOString();
        return context.collection('scenarios').update(rule, rule._id).then(result => {
            if (result.ok) {
                rule._rev = result.rev;
            }

            const event = {
                _id: ObjectID().toString(),
                type: 'update',
                path: `/scenarios/${rule._id}`,
                resource: { triggeredAt: rule.triggeredAt },
            };

            return context.emitEvent(event);
        }).then(event => {
            const actionContext = context.createEventContext(event);
            return executeAction(actionContext, rule, action, errors);
        });
    }).catch(error => {
        errors.push({
            phase: 'catchAll',
            error: error
        });
    }).then(() => {
        if (errors.length === 0) {
            return;
        }

        context.server.log(['scenario', 'warn'], {
            message: 'Errors during scenario.runAction',
            errors: _.map(errors, e => _.defaults({ error: e.error.stack }, e))
        });

        const event = {
            _id: ObjectID().toString(),
            type: 'update',
            path: `/scenarios/${rule._id}`,
            resource: {
                runActionErrors: _.map(errors, e => _.defaults({ error: e.error.message }, e))
            },
        };

        return context.emitEvent(event);
    }).catch(error => {
        context.server.log(['scenario', 'warn'], {
            message: 'Error in scenario action cleanup',
            reason: error.stack
        });
    });
}

function executeAction(context, rule, action, errors) {
    return Promise.try(() => {
        if (action.type === 'setCapability') {
            return Device.load(context, action.device).then(device => {
                return device.setCapability(context, action.capability, action.value, action.unit);
            });
        } else if (action.type === 'sequence') {
            return Promise.each(action.actions, action => executeAction(context, rule, action, errors));
        } else {
            throw new Error('invalid action type');
        }
    }).catch(error => {
        errors.push({
            phase: 'executeAction',
            action: action,
            error: error,
        });
    });
}

module.exports = {
    views,
    startAll, stopAll,
    getAll, validate,
    listForRequest, readForRequest,
    create, read, update,
    delete: deleteRule
};
