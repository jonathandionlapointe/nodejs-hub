'use strict';

const permission = require('../../lib/permission');
const Promise = require('bluebird');
const routes = require('./routes');
const scenario = require('./scenario');
const schedule = require('../../lib/schedule');

exports.register = function (server, options, next) {
    server.context();

    server.method('scenario.list', function (auth, next) {
        return scenario.listForRequest(this, auth).asCallback(next);
    });
    server.method('scenario.read', function (auth, id, next) {
        return scenario.readForRequest(this, auth, id, 'read').asCallback(next);
    });
    server.method('scenario.write', function (auth, id, next) {
        return scenario.readForRequest(this, auth, id, 'write').asCallback(next);
    });

    server.method('schedule.list', function (auth, next) {
        return schedule.listForRequest(this, auth).asCallback(next);
    });
    server.method('schedule.read', function (auth, id, next) {
        return schedule.readForRequest(this, auth, id, 'read').asCallback(next);
    });
    server.method('schedule.write', function (auth, id, next) {
        return schedule.readForRequest(this, auth, id, 'write').asCallback(next);
    });

    server.dependency(['system', 'event'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Scenario plugin');

        server.route(routes.endpoints);

        server.plugins.event.addResourceType('/scenarios', 'scenario');
        server.plugins.event.addResourceType('/schedules', 'schedule');
        permission.resourcePath('/scenarios/:id', 'scenario');
        permission.resourcePath('/schedules/:id', 'schedule');

        const context = server.context();
        return Promise.each([scenario, schedule], T => T.views.update(context))
        .then(() => {
            scenario.startAll(context);
            server.plugins['system'].onAsync('shutdown', (context, reason) => {
                return scenario.stopAll(context);
            });
        })
        .asCallback(done);
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
