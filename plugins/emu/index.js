'use strict';

const Boom = require('boom');
const Device = require('../../lib/device');
const Promise = require('bluebird');
const routes = require('./routes');

exports.register = function (server, options, next) {
    server.context();

    const protocol = {
        addDevice: Promise.method(function (context, device) {
            return [device];
        }),
        removeDevice: Promise.method(function (context, device) {
            return true;
        }),
        setCapability: Promise.method(function (context, device, name, value, unit) {
            return device.updateCap(context, name, value, unit, options);
        }),
        setParameters: Promise.method(function (context, device, parameters) {
            return device.updateParameters(context, parameters, options);
        })
    };

    server.method('emuDevice', function (auth, id, next) {
        return this.server.methods.device.read(auth, id).then(device => {
            if (device._doc.protocolName !== 'emu') {
                throw Boom.notFound();
            }
            return device;
        }).asCallback(next);
    });

    server.dependency(['device', 'system'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Emu plugin');

        server.route(routes.endpoints);

        return Device.registerProtocol(server.context(), 'emu', protocol).asCallback(done);
    });

    server.expose({
        fakeCapability: Promise.method(function (context, device, name, value, unit) {
            const input = { name, value, unit };
            const cap = device.validateCapability(input, { ignoreMethod: true });
            if (device.updateCap(context, input.name, input.value, input.unit)) {
                return device.save(context).then(function () { return cap; });
            } else {
                return false;
            }
        })
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
