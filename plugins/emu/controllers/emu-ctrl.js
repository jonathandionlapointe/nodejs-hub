'use strict';

const _ = require('lodash');
const Promise = require('bluebird');

exports.updateCap = {
    description: 'Emulate a device cap update',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['capability.edit'],
    },
    pre: ['emuDevice(auth, params.id)'],
    handler: function (request, reply) {
        const device = request.pre.emuDevice;
        const name = request.params.cap;
        const value = request.payload.value;
        const unit = request.payload.unit;
        const emulator = request.server.plugins.emu;
        return emulator.fakeCapability(request.context(), device, name, value, unit)
        .then(function (cap) {
            if (cap) {
                reply(_.omit(cap, ['_rev', 'type', 'dirty']));
            } else {
                reply().code(304);
            }
        }).catch(reply);
    }
};

exports.fail = {
    description: 'Emulate a device failure',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.edit'],
    },
    pre: ['emuDevice(auth, params.id)'],
    handler: function (request, reply) {
        const device = request.pre.emuDevice;
        if (device.status === 'ACTIVE') {
            device.updateStatus(request.context(), 'FAILED');
            return device.save(request.context()).then(function () {
                reply().code(204);
            }).catch(reply);
        } else {
            reply().code(304);
        }
    }
};

exports.restore = {
    description: 'Emulate end of device failure',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.edit'],
    },
    pre: ['emuDevice(auth, params.id)'],
    handler: function (request, reply) {
        const device = request.pre.emuDevice;
        if (device.status === 'FAILED') {
            device.updateStatus(request.context(), 'ACTIVE');
            return device.save(request.context()).then(function () {
                reply().code(204);
            }).catch(reply);
        } else {
            reply().code(304);
        }
    }
};

exports.cloudMessage = {
    description: 'Emulate a message from the cloud',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    pre: ['system.assertGatewayId(params.id)'],
    handler: function (request, reply) {
        return Promise.try(() => {
            const system = request.plugins.system;
            return system.handleCloudMessage(request.context(), request.payload)
            .then(() => reply().code(204));
        }).catch(reply);
    }
};
