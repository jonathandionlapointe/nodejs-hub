'use strict';

const emu = require('./controllers/emu-ctrl');

exports.endpoints = [
    // Emulation interface.
    { method: 'PUT',  path: '/devices/{id}/emu/caps/{cap}', config: emu.updateCap },
    { method: 'POST', path: '/devices/{id}/emu/fail', config: emu.fail },
    { method: 'POST', path: '/devices/{id}/emu/restore', config: emu.restore },

    // Inject cloud messages!
    { method: 'POST', path: '/devices/{id}/emu/cloud_message', config: emu.cloudMessage },
];
