'use strict';

exports.factoryReset = {
    description: 'Factory Reset',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    pre: ['system.assertFactoryReset()'],
    handler: function (request, reply) {
        const system = request.server.plugins.system;
        system.shutdown(request.context(), 'factoryReset');
        reply().code(204);
    }
};

exports.reset = {
    description: 'Reset',
    notes: 'A reset of the gateway, not a reboot of the OS',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: function (request, reply) {
        const system = request.server.plugins.system;
        return system.shutdown(request.context(), 'normal').then(() => {
            reply().code(204);
        }).catch(reply);
    }
};
