'use strict';

exports.beat = {
    description: 'System heartbeat',
    notes: 'Sends heartbeat with system health to API',
    tags: ['internal'],
    handler: beat,
};

exports.poke = {
    description: 'System heartbeat',
    notes: 'Sends heartbeat with system health to API',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: beat,
};

exports.gentleBeat = {
    description: 'System heartbeat',
    notes: 'Retrieve heartbeat with system health *without* sending to API',
    tags: ['internal'],
    handler: gentleBeat,
};

exports.gentlePoke = {
    description: 'System heartbeat',
    notes: 'Retrieve heartbeat with system health *without* sending to API',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: gentleBeat,
};

function beat(request, reply) {
    return request.server.plugins.system.heartbeat(request.context())
    .asCallback(reply);
}

function gentleBeat(request, reply) {
    return request.server.plugins.system.heartbeat(request.context(), {
        suppressForward: true,
        suppressSync: true
    })
    .asCallback(reply);
}
