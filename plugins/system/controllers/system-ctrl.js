'use strict';

const _ = require('lodash');
const os = require('os');
const pjson = require('../../../package.json');
const Promise = require('bluebird');

function get(request, reply) {
    const system = request.server.plugins.system;
    return reply(system.toJSON());
}

function update(request, reply) {
    const system = request.server.plugins.system;
    return Promise.try(() => system.update(request.context(), request.payload)).then(() => {
        return get(request, reply);
    }).catch(reply);
}

function info(request, reply) {
    const system = request.server.plugins.system;
    reply(_.merge({
        version: pjson.version,
        channel: _.get(system.device, 'config.channel'),
        app_env: process.env.NODE_ENV,
        hostname: os.hostname(),
        nodeRelease: process.release.name,
        nodeVersion: process.version,
        services: _.get(request.server.plugins, 'rpc.services', []),
    }, system.gitInfo));
}

function ping(request, reply) {
    const start = new Date();
    const context = request.context();
    return context.collection('system').upsert({
        _id: 'externalPing',
        timestamp: start.toISOString(),
    }).then(() => {
        const upsertTime = Date.now() - start.getTime();
        return { upsertTime };
    }).asCallback(reply);
}

function debugLink(request, reply) {
    const system = request.server.plugins.system;
    system.debugLink().asCallback(reply);
}

function debugData(request, reply) {
    const system = request.server.plugins.system;
    system.debugData(request.context(), request.params.code)
    .then(stream => {
        reply(stream).type('application/x-xz');
    }, reply);
}

exports.list = {
    description: 'Get system properties',
    notes: 'Returns system properties',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: get
};

exports.get = {
    description: 'Get the gateway device object',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: get
};

exports.update = {
    description: 'Update the gateway device object',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: update
};

exports.info = {
    description: 'Get system information',
    notes: 'Returns system information',
    tags: ['api'],
    handler: info
};

exports.ping = {
    description: 'Ping the controller',
    notes: 'Returns database update timing',
    tags: ['api'],
    handler: ping
};

exports.debugLink = {
    description: 'Get a one-use link to generate and download debug data',
    auth: {
        strategy: 'token',
        scope: ['gateway.manage'],
    },
    handler: debugLink
};

exports.debugData = {
    description: 'Get debug data',
    handler: debugData
};
