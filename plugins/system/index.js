'use strict';

const _ = require('lodash');
const Boom = require('boom');
const git = require('git-rev');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const System = require('../../lib/system');

exports.register = function (server, options, next) {
    server.context();

    const system = new System();
    system.options = options;

    system.on('provisioning', mac => {
        server.log(['system', 'info'], 'Provisioning with MAC address: ' + mac);
    });

    system.on('provisioned', device => {
        server.log(['system', 'info'], 'Provisioned new gateway: ' + device._id);
    });

    system.on('request', message => {
        server.log(['debug', 'api'], { message: 'API Request', request: message });
    });

    system.on('response', (response, message) => {
        server.log(['debug', 'api'], { message: 'API Response', request: message.request, response: response.statusCode });
    });

    system.on('startup', () => {
        server.log(['system', 'info'], 'System ready.');
    });

    system.on('shutdownRequested', (context, reason) => {
        server.log(['system', 'info'], `System shutdown requested with reason: ${reason}`);
    });

    system.on('shutdown.default', (context, reason) => {
        server.log(['system', 'info'], 'System shutdown imminent! Running shutdown tasks...');
    });

    system.on('shutdown.devices', (context, reason) => {
        server.log(['system', 'info'], 'System shutdown imminent! Stopping device protocols...');
    });

    system.on('shutdown', (context, reason) => {
        server.log(['system', 'info'], 'System shutdown. Cleaning up...');
    });

    server.method('system.isGatewayId', function (id) {
        return id === this.gatewayId;
    }, { callback: false });

    server.method('system.assertGatewayId', function (id) {
        if (id !== this.gatewayId) {
            throw Boom.notFound();
        }
        return true;
    }, { callback: false });

    server.method('system.assertFactoryReset', function () {
        if (!_.get(options, 'factoryReset.enable')) {
            throw Boom.notFound();
        }
        return true;
    }, { callback: false });

    system.gitInfo = {
        short: 'unknown',
        long: 'unknown',
        branch: 'unknown',
        tag: 'unknown',
    };

    try {
        _.merge(system.gitInfo, require('../../../git-info.json'));
    } catch (requireError) {
        Promise.join(
            new Promise(git.short),
            new Promise(git.long),
            new Promise(git.branch),
            new Promise(git.tag),
            (short, long, branch, tag) => _.merge(system.gitInfo, { short, long, branch, tag })
        ).catch(gitError => {
            server.log(['system','warn'], {
                message: 'Unable to determine git revision information',
                requireError, gitError
            });
        });
    }

    server.dependency(['sdk', 'event'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the System plugin');

        return System.views.update(server.context())
        .then(() => permission.views.update(server.context()))
        .then(() => system.provision(server.context()))
        .then(result => {
            const logger = server.plugins['2klic-hapi-bunyan'];
            if (logger) {
                // FIXME manage with context instead.
                logger.setGlobalGatewayId(system.device._id);
            }

            // Add routes *after* provisioning, so that we can add
            // system-device-specific routes on `/devices/{systemId}`.
            system.route = () => {
                const routes = require('./routes')(server);
                server.select('api').route(routes.endpoints);
                server.select('internal').route(routes.internalEndpoints);
            };

            system.once('startup', context => {
                // Give services a chance to reconnect before emitting the
                // startup heartbeat.
                Promise.delay(15000).then(() =>
                    system.heartbeat(context).reflect());
            });
        }).asCallback(done);
    });

    server.plugins['system'] = system;

    system.on('heartbeat', () => {
        server.plugins['event'].compact(server.context())
        .catch(error => {
            server.log(['system', 'info'], {
                message: 'Error compacting events database',
                reason: error,
            });
        });
    });

    server.on('start', () => {
        const context = server.context().createStartupContext('system');
        system.emitAllAsync('startup', context).catch(error => {
            server.log(['system', 'error'], error);
        });
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
