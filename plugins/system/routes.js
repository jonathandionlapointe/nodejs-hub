'use strict';

const heart = require('./controllers/heart');
const reset = require('./controllers/reset');
const system = require('./controllers/system-ctrl');

module.exports = function (server) {
    const device = server.plugins.system.device;
    const endpoints = [
        // Show system details
        { method: 'GET',  path: '/', config: system.list },
        { method: 'POST', path: '/reset', config: reset.reset },

        // System device
        { method: 'GET', path: `/devices/${device._id}`, config: system.get },
        { method: 'PUT', path: `/devices/${device._id}`, config: system.update },
        { method: 'GET', path: `/devices/${device._id}/ping`, config: system.ping },

        // System internal information (git revision, etc..)
        { method: 'GET',  path: '/system/info', config: system.info },

        // Generate heartbeat.
        { method: 'GET', path: '/heartbeat', config: heart.gentlePoke },
        { method: 'POST', path: '/heartbeat', config: heart.poke },

        // Get debug dump & one-use link.
        { method: 'GET', path: `/devices/${device._id}/debug`, config: system.debugLink },
        { method: 'GET', path: `/devices/${device._id}/debug/{code}`, config: system.debugData },

        // Factory reset (only enabled under test).
        { method: 'POST', path: '/factory_reset', config: reset.factoryReset },
    ];

    const internalEndpoints = [
        { method: 'GET', path: '/heartbeat', config: heart.gentleBeat },
        { method: 'POST', path: '/heartbeat', config: heart.beat },
    ];

    return { endpoints, internalEndpoints };
};
