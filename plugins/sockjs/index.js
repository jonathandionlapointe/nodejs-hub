'use strict';

const _ = require('lodash');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const sockjs = require('sockjs');

exports.register = function (server, options, next) {
    server.context();

    server.dependency(['event', 'auth'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the SockJS server');

        server.plugins.event.onEvent(function (context, event) {
            const perm = permission.pathPermission(event.path);
            broadcast(context, event, perm);
        });

        return done();
    });

    const socket = sockjs.createServer({
        sockjs_url: 'http://cdn.jsdelivr.net/sockjs/0.3.4/sockjs.min.js',
        log: function (severity, message) {
            server.log(['sockjs', 'debug'], { severity, message });
        },
    });
    const clients = {};

    // SockJS server, endpoints:
    //     - ws://localhost:2000/gw/websocket
    //     - ws://localhost:2000/gw/ in the browser
    //     - wss://${IP_ADDRESS}/v2/gw/[websocket] in production installs
    socket.installHandlers(server.select('api').listener, { prefix: '/gw' });
    socket.on('connection', conn => {
        server.log(['sockjs', 'debug'], {
            message: 'New connection',
            connectionId: conn.id,
            remoteAddress: conn.remoteAddress,
            remotePort: conn.remotePort,
        });

        const client = {
            connection: conn,
            credentials: null,
        };
        client.context = server.context().createClientContext(client);

        conn.on('close', function () {
            server.log(['sockjs', 'debug'], {
                message: 'Connection closed',
                connectionId: conn.id,
                remoteAddress: conn.remoteAddress,
                remotePort: conn.remotePort,
            });
            delete clients[conn.id];
        });

        conn.on('data', function (message) {
            return Promise.try(() => {
                message = JSON.parse(message);
                if (!message.token) {
                    throw new Error('no token');
                }
                return server.plugins.auth.validateToken(client.context, message.token)
                .then(credentials => {
                    if (!_.includes(credentials.scope, 'event.subscribe')) {
                        conn.write(JSON.stringify({ error: 'forbidden' }));
                        throw new Error('client does not have event.subscribe scope');
                    }
                    client.credentials = _.pick(credentials, ['who', 'whoType']);
                    conn.write(JSON.stringify({ ready: true }));
                });
            }).catch(error => {
                server.log(['sockjs', 'info'], {
                    message: 'Unable to authenticate',
                    reason: error,
                });
                conn.write(JSON.stringify({ error: 'not_authorized' }));
                conn.close();
            });
        });

        clients[conn.id] = client;
    });

    function emit(client, message, perm) {
        if (!client.credentials) {
            return;
        }

        // If there's no permission, it's a public broadcast to all
        // authenticated clients.  Otherwise, the message being broadcast must
        // match the client credentials.
        if (perm) {
            return permission.isValid(client.context, _.merge({}, client.credentials, perm))
            .then(valid => {
                if (!valid) {
                    return;
                }
                client.connection.write(JSON.stringify(message));
            });
        } else {
            client.connection.write(JSON.stringify(message));
        }
    }

    function broadcast(context, message, permission) {
        server.log(['sockjs', 'debug'], {
            message: 'Sending message',
            data: message,
        });
        _.forEach(clients, client => {
            return Promise.try(() => emit(client, message, permission))
            .catch(error => {
                server.log(['sockjs', 'debug'], {
                    message: 'Client emit failure',
                    reason: error,
                });
            });
        });
    }

    server.expose({ clients, emit, broadcast });

    next();
};

exports.register.attributes = {
    name: 'sockjs'
};
