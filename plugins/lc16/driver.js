'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const SerialPort = require('serialport');
const EventEmitter = require('events');
const util = require('util');

const COMMAND_TIMEOUT = 100; // milliseconds.

function Driver(options) {
    EventEmitter.call(this);

    if (typeof options === 'string') {
        options = { tty: options };
    }
    this.options = _.merge({
        tty: '/dev/ttyUSB0',
        baudrate: 115200,
    }, options);

    this.serial = Promise.promisifyAll(new SerialPort(this.options.tty, {
        autoOpen: false,
        baudrate: this.options.baudrate,
    }));

    this._buffer = '';

    this.serial.on('close', () => this.serialOnClose());
    this.serial.on('data', buffer => this.serialOnData(buffer));
    this.serial.on('error', error => this.serialOnError(error));
}
util.inherits(Driver, EventEmitter);

//
// Driver lifecycle.
//

Driver.prototype.start = function () {
    return this.serial.openAsync().then(() => {
        this.emit('start');
    });
};

Driver.prototype.stop = function () {
    if (!this.serial.isOpen()) {
        return Promise.resolve();
    }

    return this.serial.closeAsync().then(() => {
        this.emit('stop');
    });
};

Driver.prototype.isStarted = function () {
    return this.serial.isOpen();
};

Driver.prototype.flush = function () {
    this._buffer = '';

    return this.serial.writeAsync('\r\n').then(() => this.serial.flushAsync());
};

//
// LC16 functions.
//

/** Set the level for one channel.
 *
 * @param {number} channel Channel number (0-15).
 * @param {number} level Channel level (0-255).
 */
Driver.prototype.setLight = function (channel, level) {
    const type = 'setLight';
    return this.sendCommandWaitForResponse({ type, channel, level });
};

/** Set the level for all channels.
 *
 * @param {number} level Channel level (0-255).
 */
Driver.prototype.setAllLight = function (level) {
    const type = 'setAllLight';
    return this.sendCommandWaitForResponse({ type, level });
};

/** Set the RGB levels for one channel.
 *
 * setRgb(channel)
 * Turns the channel off (sets the color to r=0, g=0, b=0).
 * @param {number} channel Channel number (0-1).
 *
 * setRgb(channel, color)
 * @param {number} channel Channel number (0-1).
 * @param {string} color #f level, #ff level, #fff rgb, or
 * #ffffff rgb.
 *
 * setRgb(channel, color)
 * @param {number} channel Channel number (0-1).
 * @param {object} color `{ r, g, b }`.
 *
 * setRgb(channel, r, g, b)
 * @param {number} channel Channel number (0-1).
 * @param {number} r Red level (0-255).
 * @param {number} g Green level (0-255).
 * @param {number} b Blue level (0-255).
 *
 * @return {object} color `{ r, g, b }`.
 */
Driver.prototype.setRgb = function (channel, r, g, b) {
    const type = 'setRgb';
    const color = makeColor(r, g, b);
    return this.sendCommandWaitForResponse({ type, channel, color }).return(color);
};

/** Turn on or off one relay channel.
 *
 * @param {number} channel Channel number (0-15).
 * @param {boolean} enable Whether to turn the relay on or off.
 */
Driver.prototype.setRelay = function (channel, enable) {
    const type = 'setRelay';
    return this.sendCommandWaitForResponse({ type, channel, enable });
};

/** Turn on or off all relay channels.
 *
 * @param {boolean} enable Whether to turn the relays on or off.
 */
Driver.prototype.setAllRelay = function (enable) {
    const type = 'setAllRelay';
    return this.sendCommandWaitForResponse({ type, enable });
};

//
// Serial callbacks.
//

Driver.prototype.serialOnClose = function () {
    this.emit('stop');
};

Driver.prototype.serialOnError = function (error) {
    this.emit('error', error);
};

Driver.prototype.serialOnData = function (buffer) {
    this._buffer += buffer.toString();

    // Search for newlines. For each newline, examine the data leading
    // up to the newline. If the line is non-empty and can be parsed,
    // emit the command data.
    for (let n; (n = this._buffer.indexOf('\n')) >= 0; ) {
        const line = this._buffer.substr(0, n).trim();
        this._buffer = this._buffer.substr(n + 1);

        if (line.length === 0) {
            continue;
        }

        try {
            const command = this.parseCommand(line);
            this.emit('command', command);
        } catch (error) {
            this.emit('error', error);
        }
    }
};

//
// Command handling.
//

Driver.prototype.parseCommand = function (line) {
    if (line === 'OK') {
        return {
            type: 'OK'
        };
    } else if (line.endsWith('ERROR')) {
        return {
            type: 'ERROR'
        };
    } else {
        const error = new Error('bad line');
        error.line = line;
        throw error;
    }
};

Driver.prototype.printCommand = function (command) {
    switch (command.type) {
    case 'setLight': {
        const channel = nibbleToHex(command.channel);
        const level = intToHex(command.level, 2);
        return `SETLIGHT ${channel} ${level}`;
    }
    case 'setAllLight': {
        const level = intToHex(command.level, 2);
        return `SETALL ${level}`;
    }
    case 'setRgb': {
        const channel = clamp(command.channel, 0, 1).toString();
        const red = intToHex(command.color.r, 2);
        const green = intToHex(command.color.g, 2);
        const blue = intToHex(command.color.b, 2);
        return `SETRGBLED ${channel} ${red} ${green} ${blue}`;
    }
    case 'setRelay': {
        const channel = nibbleToHex(command.channel);
        const state = command.enable ? 'ON' : 'OFF';
        return `CONTROLRELAY ${channel} ${state}`;
    }
    case 'setAllRelay': {
        const state = command.enable ? 'ON' : 'OFF';
        return `ALLRELAY ${state}`;
    }
    default:
        throw new Error('invalid command: ' + command.type);
    }
};

Driver.prototype.sendCommand = function (command) {
    const line = this.printCommand(command) + '\r\n';
    return this.serial.writeAsync(line);
};

Driver.prototype.waitForResponse = function () {
    return new Promise((resolve, reject) => {
        const driver = this;
        const timer = setTimeout(() => finish(new Promise.TimeoutError()), COMMAND_TIMEOUT);
        this.on('command', onResponse);

        function onResponse(command) {
            if (command.type === 'OK') {
                finish(null, command.data);
            }
        }

        function finish(error, result) {
            clearTimeout(timer);
            driver.removeListener('command', onResponse);
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        }
    });
};

Driver.prototype.sendCommandWaitForResponse = function (command) {
    return Promise.all([
        this.waitForResponse(),
        this.sendCommand(command),
    ]).spread((response, _ignored) => {
        return response;
    });
};

//
// Utility functions.
//

function clamp(n, min, max) {
    if (!Number.isSafeInteger(n)) {
        return min;
    }
    return Math.max(min, Math.min(max, n));
}

function nibbleToHex(n) {
    return clamp(n, 0, 15).toString(16).toUpperCase();
}

function intToHex(n, width) {
    let result = '';
    while (width-- > 0) {
        const ch = nibbleToHex(n & 0xF);
        n >>= 4;
        result = ch + result;
    }
    return result;
}

Driver.intToHex = intToHex;

function makeColor(r, g, b) {
    if (typeof r === 'object') {
        return r;
    } else if (typeof r === 'string' && r[0] === '#') {
        if (r.length >= 7) {
            return {
                r: Number.parseInt(r.substr(1, 2), 16),
                g: Number.parseInt(r.substr(3, 2), 16),
                b: Number.parseInt(r.substr(5, 2), 16),
            };
        } else if (r.length >= 4) {
            return {
                r: Number.parseInt(r[1].repeat(2), 16),
                g: Number.parseInt(r[2].repeat(2), 16),
                b: Number.parseInt(r[3].repeat(2), 16),
            };
        } else if (r.length >= 3) {
            const shade8 = Number.parseInt(r.substr(1, 2), 16);
            return { r: shade8, g: shade8, b: shade8 };
        } else if (r.length >= 2) {
            const shade4 = Number.parseInt(r[1].repeat(2), 16);
            return { r: shade4, g: shade4, b: shade4 };
        } else {
            return { r: 0, g: 0, b: 0 };
        }
    } else {
        return {
            r: clamp(r, 0, 255),
            g: clamp(g, 0, 255),
            b: clamp(b, 0, 255),
        };
    }
}

module.exports = Driver;
