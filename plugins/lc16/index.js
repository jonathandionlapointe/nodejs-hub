'use strict';

const Device = require('../../lib/device');
const Protocol = require('./protocol');

exports.register = function (server, options, next) {
    server.context();

    server.dependency(['device'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the LC16 plugin');

        const protocol = new Protocol(options);

        protocol.on('error', error => {
            server.log(['lc16', 'error'], error);
        });

        return Device.registerProtocol(server.context(), 'lc16', protocol).asCallback(done);
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
