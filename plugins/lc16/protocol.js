'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const Device = require('../../lib/device');
const Driver = require('./driver');
const EventEmitter = require('events');
const Promise = require('bluebird');
const util = require('util');

const PROTOCOL = 'lc16';
const TYPE_MASTER = '0_master';
const TYPE_DIMMER = '1_dimmer';
const TYPE_SWITCH = '2_switch';
const TYPE_RGB    = '3_rgb___';
const MASTER_MODEL = 'lc16_v3';
const DIMMER_MODEL = 'lc16_v3_dimmer';
const SWITCH_MODEL = 'lc16_v3_switch';
const RGB_MODEL = 'lc16_v3_rgb';

function Protocol(options) {
    if (Protocol.instance) {
        throw new Error('LC16 protocol already started');
    }
    EventEmitter.call(this);

    _.defaults(options, {
        connections: [],
    });

    this.connections = _.map(options.connections, opts => new Driver(opts));
    this.options = options;

    _.forEach(this.connections, conn => {
        conn.on('error', error => {
            this.emit('error', {
                message: 'driver error',
                reason: error,
            });
        });
    });

    Protocol.instance = this;
}
util.inherits(Protocol, EventEmitter);

//
// Lifecycle.
//

Protocol.prototype.startup = function (context) {
    return Promise.map(this.connections, conn => {
        return conn.start()
            .then(() => conn.flush())
            .then(() => conn.setAllLight(0))
            .then(() => conn.setAllRelay(false))
            .then(() => conn.setRgb(0))
            .then(() => conn.setRgb(1))
            .catch(error => {
                this.emit('error', {
                    message: 'failed to initialize serial connection',
                    connection: conn.options,
                    reason: error,
                });
                return conn.stop().catchReturn();
            });
    }).then(() => this.syncDevices(context));
};

Protocol.prototype.shutdown = function (context) {
    return Promise.map(this.connections, conn => conn.stop());
};

//
// Protocol interface implementation.
//

Protocol.prototype.addDevice = Promise.method(function (context, device) {
    if (!device.protocolData) {
        throw Boom.badRequest('LC16 cannot be added manually');
    }

    const uid = device.protocolData.uid;
    return Promise.all([
        context.sdk.models.getCached(context, SWITCH_MODEL),
        context.sdk.models.getCached(context, DIMMER_MODEL),
        context.sdk.models.getCached(context, RGB_MODEL),
    ]).spread((switchModel, dimmerModel, rgbModel) => {
        const devices = [device];

        // Create sub-devices.
        for (let channel = 0; channel < 16; channel++) {
            let type = TYPE_SWITCH;
            devices.push(device.createChild(switchModel, {
                parameters: { channel },
                protocolData: {
                    id: makeProtocolId({ uid, type, channel }),
                    uid, type, channel
                },
            }));
            type = TYPE_DIMMER;
            devices.push(device.createChild(dimmerModel, {
                parameters: { channel },
                protocolData: {
                    id: makeProtocolId({ uid, type, channel }),
                    uid, type, channel
                },
            }));
            if (channel < 2) {
                type = TYPE_RGB;
                devices.push(device.createChild(rgbModel, {
                    parameters: { channel },
                    protocolData: {
                        id: makeProtocolId({ uid, type, channel }),
                        uid, type, channel
                    },
                }));
            }
        }

        return devices;
    });
});

Protocol.prototype.removeDevice = Promise.method(function (context, device) {
    if (device.status === 'ACTIVE') {
        throw Boom.customCode(400, 'device is still functioning', 'DEVICE_CANNOT_REMOVE_ACTIVE');
    }

    if (device.model._id !== MASTER_MODEL) {
        throw Boom.customCode(400, 'only the parent device can be removed', 'DEVICE_CANNOT_REMOVE_CHILD');
    }

    return true;
});

Protocol.prototype.setCapability = Promise.method(function (context, device, name, value, unit) {
    const uid = device.protocolData.uid;
    const type = device.protocolData.type;
    const channel = device.protocolData.channel;

    const conn = this.connections[uid];

    if (name === 'switch') {
        if (type == TYPE_MASTER) {
            const firstKey = makeProtocolId({ uid, type: TYPE_SWITCH, channel: 0 });
            const lastKey = makeProtocolId({ uid, type: TYPE_SWITCH, channel: 16 });
            return Device.loadProtocolRange(context, PROTOCOL, firstKey, lastKey).then(devices => {
                return conn.setAllRelay(value === 'on').then(() => {
                    return Promise.map(devices, subDevice => {
                        if (subDevice.updateCap(context, name, value)) {
                            return subDevice.save(context);
                        }
                    });
                });
            }).then(() => {
                return device.updateCap(context, name, value);
            });
        } else if (type === TYPE_SWITCH) {
            return conn.setRelay(channel, value === 'on').then(() => {
                return device.updateCap(context, name, value);
            });
        }
    } else if (name === 'level') {
        if (type === TYPE_MASTER) {
            const firstKey = makeProtocolId({ uid, type: TYPE_DIMMER, channel: 0 });
            const lastKey = makeProtocolId({ uid, type: TYPE_DIMMER, channel: 16 });
            return Device.loadProtocolRange(context, PROTOCOL, firstKey, lastKey).then(devices => {
                return conn.setAllLight((value / 100 * 255) & 0xFF).then(() => {
                    return Promise.map(devices, subDevice => {
                        if (subDevice.updateCap(context, name, value)) {
                            return subDevice.save(context);
                        }
                    });
                });
            }).then(() => {
                return device.updateCap(context, name, value);
            });
        } else if (type === TYPE_DIMMER) {
            return conn.setLight(channel, (value / 100 * 255) & 0xFF).then(() => {
                return device.updateCap(context, name, value);
            });
        }
    } else if (name === 'color') {
        if (type === TYPE_RGB) {
            return conn.setRgb(channel, value).then(color => {
                color = '#' +
                    Driver.intToHex(color.r, 2) +
                    Driver.intToHex(color.g, 2) +
                    Driver.intToHex(color.b, 2);
                return device.updateCap(context, name, color);
            });
        }
    }
});

Protocol.prototype.setParameters = Promise.method(function (context, device, parameters) {
    throw Boom.customCode(400, 'no editable parameters', 'DEVICE_NO_PARAMETERS');
});

//
// Internal methods.
//

Protocol.prototype.syncDevices = function (context) {
    return Device.loadProtocolAll(context, PROTOCOL).then(devices => {
        devices = deviceTree(devices);
        const newConnections = [];

        return Promise.map(this.connections, (conn, uid) => {
            // We cheat a little bit here ~~~~~~~~~~^^^
            //
            // We're simply using the connections index in the
            // connection array as its uid!  This makes the order of
            // connections in the hardware config significant!!
            if (conn.isStarted()) {
                const entry = devices[uid];
                if (entry) {
                    entry.found = true;
                    return initConnectionEntry(context, conn, entry);
                } else {
                    newConnections[uid] = conn;
                }
            }
        }).then(() => {
            // Mark missing entries as failed.
            return Promise.map(_.values(devices), entry => {
                if (!entry.found) {
                    return Promise.map(entry.all, device => {
                        device.updateStatus(context, 'FAILED');
                        return device.save(context);
                    });
                }
            });
        }).then(() => {
            return Promise.each(newConnections, (conn, uid) => {
                if (!conn) {
                    // Non-new connections are `undefined` here!
                    return;
                }
                return Device.add(context, MASTER_MODEL, {
                    protocolName: PROTOCOL,
                    protocolData: {
                        id: makeProtocolId({ uid, type: TYPE_MASTER, channel: null }),
                        uid: uid,
                        type: TYPE_MASTER,
                        channel: null,
                    },
                });
            });
        });
    });
};

//
// Utility functions.
//

function initConnectionEntry(context, conn, entry) {
    return Promise.each(entry.slaves[TYPE_SWITCH], (device, channel) => {
        const value = _.get(device, 'caps.switch.value', 'off');
        return conn.setRelay(channel, value === 'on');
    }).then(() => {
        return Promise.each(entry.slaves[TYPE_DIMMER], (device, channel) => {
            const value = _.get(device, 'caps.level.value', 0);
            return conn.setLight(channel, (value / 100 * 255) & 0xFF);
        });
    }).then(() => {
        return Promise.each(entry.slaves[TYPE_RGB], (device, channel) => {
            const value = _.get(device, 'caps.color.value');
            return conn.setRgb(channel, value);
        });
    }).then(() => {
        return Promise.map(entry.all, device => {
            device.updateStatus(context, 'ACTIVE');
            return device.save(context);
        });
    });
}

function deviceTree(devices) {
    const deviceTree = {};
    _.forEach(devices, device => {
        const uid = device.protocolData.uid;
        const type = device.protocolData.type;
        const channel = device.protocolData.channel;
        if (type === TYPE_MASTER) {
            deviceTree[uid] = {
                master: device,
                slaves: [],
                all: [],
                found: false,
            };
            deviceTree[uid].slaves[TYPE_SWITCH] = [];
            deviceTree[uid].slaves[TYPE_DIMMER] = [];
            deviceTree[uid].slaves[TYPE_RGB] = [];
        } else {
            deviceTree[uid].slaves[type][channel] = device;
        }
        deviceTree[uid].all.push(device);
    });
    return deviceTree;
}

function makeProtocolId(keyParts) {
    if (typeof keyParts.channel === 'number') {
        return `${hex2(keyParts.uid)}.${keyParts.type}.${hex2(keyParts.channel)}`;
    } else {
        return `${hex2(keyParts.uid)}.${keyParts.type}`;
    }
}

function hex2(n) {
    if (typeof n !== 'number' || isNaN(n) || n >= 256) {
        throw new TypeError(`cannot convert '${n}' to two-digit hex`);
    }
    const s = n.toString(16);
    if (n < 16) {
        return '0' + s;
    } else {
        return s;
    }
}

module.exports = Protocol;
