'use strict';

const Boom = require('boom');
const Device = require('../../lib/device');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const routes = require('./routes');

exports.register = function (server, options, next) {
    server.context();

    server.method('device.list', function (auth, next) {
        return Device.listForRequest(this, auth).asCallback(next);
    });

    server.method('device.read', function (auth, id, next) {
        if (id === this.gatewayId) {
            return next(null, this.server.plugins.system.device);
        }
        return Device.readForRequest(this, auth, id, 'read').asCallback(next);
    });

    server.method('device.write', function (auth, id, next) {
        if (id === this.gatewayId) {
            return next(null, this.server.plugins.system.device);
        }
        return Device.readForRequest(this, auth, id, 'write').asCallback(next);
    });

    server.method('capability.list', function (auth, device, next) {
        return Promise.try(() => {
            if (typeof device === 'string') {
                return Device.readForRequest(this, auth, device, 'read');
            }
            return device;
        }).then(device => {
            return device.caps;
        }).asCallback(next);
    });

    server.method('capability.read', function (auth, device, name, next) {
        return Promise.try(() => {
            if (typeof device === 'string') {
                return Device.readForRequest(this, auth, device, 'read');
            }
            return device;
        }).then(device => {
            const cap = device.caps[name];
            if (!cap) {
                throw Boom.notFound();
            }
            return cap;
        }).asCallback(next);
    });

    server.method('capability.write', function (auth, device, name, next) {
        return Promise.try(() => {
            if (typeof device === 'string') {
                return Device.readForRequest(this, auth, device, 'write');
            }
            return device;
        }).then(device => {
            const cap = device.caps[name];
            if (!cap) {
                throw Boom.notFound();
            }
            return cap;
        }).asCallback(next);
    });

    server.dependency(['sdk', 'system', 'event'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Device plugin');

        server.route(routes.endpoints);

        server.plugins.event.addResourceType('/devices', 'device');
        permission.resourcePath('/devices/:id', 'device');
        permission.resourcePath('/devices/:id/*path', 'device');

        server.plugins.event.onEventPathMatch(/^\/models\/([^/]+)$/, (context, matches, event) => {
            if (event.type === 'create' || event.type === 'update') {
                server.plugins.cache.inject(context, 'model', matches[1], event.resource)
                .catch(error => {
                    server.log(['device', 'info'], {
                        message: 'Failed to inject model into cache',
                        reason: error,
                    });
                });
            }
        });

        const system = server.plugins.system;
        system.onAsync('startup', context => {
            return Device.onSystemStartup(context).each(error => {
                server.log(['warn', 'device'], {
                    message: 'protocol startup failure',
                    error: error,
                });
            });
        });

        // Device protocols should be shut down after other shutdown callbacks
        // (for example scenarios). Give the device shutdown phase a low
        // priority.
        system.addShutdownPhase('devices', 0);
        system.onAsync('shutdown.devices', (context, reason) => {
            return Device.onSystemShutdownDevices(context).each(error => {
                server.log(['warn', 'device'], {
                    message: 'protocol shutdown failure',
                    error: error,
                });
            });
        });

        return Device.views.update(server.context()).asCallback(done);
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
