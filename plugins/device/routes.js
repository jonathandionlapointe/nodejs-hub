'use strict';

const device = require('./controllers/device-ctrl');

exports.endpoints = [
    // Device interface.
    { method: 'GET', path: '/devices', config: device.getAll },
    { method: 'POST', path: '/devices', config: device.create },
    { method: 'GET', path: '/devices/{id}', config: device.getOne },
    { method: 'PUT', path: '/devices/{id}', config: device.update },
    { method: 'DELETE', path: '/devices/{id}', config: device.remove },
    { method: 'GET', path: '/devices/{id}/caps', config: device.listCaps },
    { method: 'GET', path: '/devices/{id}/caps/{cap}', config: device.getCap },
    { method: 'PUT', path: '/devices/{id}/caps/{cap}', config: device.setCap },
    { method: 'GET', path: '/caps', config: device.listAllCaps },
];
