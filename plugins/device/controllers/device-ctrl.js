'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const Boom = require('super-boom')();
const Device = require('../../../lib/device');
const Joi = require('joi');

exports.getAll = {
    description: 'Get a list of devices',
    notes: 'Returns a list of devices',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.list']
    },
    pre: ['device.list(auth)'],
    handler: function (request, reply) {
        const devices = _.map(request.pre['device.list'], device => device.toJSON());
        const system = request.server.plugins.system;
        devices.unshift(_.omit(system.device, ['config', 'token']));
        reply(devices);
    }
};

exports.create = {
    description: 'Create a device',
    notes: 'Creates a new device',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.create']
    },
    validate: {
        payload: {
            model: Joi.string().required(),
        },
        options: {
            allowUnknown: true,
        }
    },
    pre: ['model(payload.model)'],
    handler: function (request, reply) {
        // TODO validate that `_id` does not conflict with
        // another resource ID.
        const doc = _.omit(request.payload, ['_rev', 'status', 'protocolData']);
        const model = request.pre['model'];
        const context = request.context();
        if (doc.isEmulated) {
            doc.protocolName = 'emu';
        } else {
            doc.protocolName = model.protocolId;
        }
        return context.withTransactionContext(context => {
            return Device.add(context, model, doc)
            .then(function (devices) {
                if (typeof devices === 'string') {
                    return reply({ _id: devices }).code(202);
                } else {
                    // FIXME: the API can only handle ONE device right now.
                    return reply(devices[0].toJSON());
                }
            }).catch(Promise.TimeoutError, error => {
                return reply(Boom.customCode(504, 'Device did not respond', 'DEVICE_TIMEOUT'));
            }).catch(reply);
        });
    }
};

exports.remove = {
    description: 'Remove a device',
    notes: 'Attempts to remove device',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.remove']
    },
    pre: ['device.write(auth, params.id)'],
    handler: function (request, reply) {
        return request.pre['device.write'].remove(request.context())
        .then(function () {
            reply().code(204);
        }).catch(function (error) {
            reply(error);
        });
    }
};

exports.getOne = {
    description: 'Get a device',
    notes: 'Returns a specific device',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.read']
    },
    pre: ['device.read(auth, params.id)'],
    handler: function (request, reply) {
        reply(request.pre['device.read'].toJSON());
    }
};

exports.update = {
    description: 'Update a device',
    notes: 'Returns the updated device',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.edit']
    },
    pre: ['device.write(auth, params.id)'],
    handler: function (request, reply) {
        const device = request.pre['device.write'];
        const params = request.payload.parameters;
        const context = request.context();
        return Promise.join(
            device.setParameters(context, params),
            device.updateData(context, request.payload),
            function (paramsChanged, dataChanged) {
                if (paramsChanged || dataChanged) {
                    reply(device.toJSON());
                } else {
                    reply().code(304);
                }
            }
        )
        .catch(function (error) {
            if (!error.isBoom) {
                request.server.log(['device', 'error'], error);
            }
            reply(error);
        });
    }
};

exports.getCap = {
    description: 'Get a capability',
    notes: 'Returns the current value of a capability',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.read']
    },
    pre: ['capability.read(auth, params.id, params.cap)'],
    handler: function (request, reply) {
        reply(_.omit(request.pre['capability.read'], ['_rev', 'type', 'dirty']));
    }
};

exports.listCaps = {
    description: 'List capabilities of a device',
    notes: 'Returns the current capabilities of a device',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.read']
    },
    pre: ['capability.list(auth, params.id)'],
    handler: function (request, reply) {
        reply(_.map(request.pre['capability.list'], cap => _.omit(cap, ['_rev', 'type', 'dirty'])));
    }
};

exports.setCap = {
    description: 'Set a capability',
    notes: 'Returns the updated capability',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['capability.edit']
    },
    pre: [
        function (request, reply) {
            if ('string' === typeof request.query.emulate &&
                    request.query.emulate.match(/^(|y(es)?|t(rue)?)$/)) {
                reply().takeover()
                .redirect(`/devices/${request.params.id}/emu/caps/${request.params.cap}`)
                .code(307);
            } else {
                reply();
            }
        },
        'device.write(auth, params.id)',
    ],
    handler: Promise.method(function (request, reply) {
        const name = request.params.cap;
        const device = request.pre['device.write'];
        const value = request.payload.value;
        const unit = request.payload.unit;
        return device.setCapability(request.context(), name, value, unit)
        .then(function (cap) {
            if (!cap) {
                reply().code(304);
            } else {
                reply(_.omit(cap, ['_rev', 'dirty', 'type']));
            }
        }).catch(Promise.TimeoutError, function (error) {
            request.server.log(['device', 'info'], {
                message: 'Timeout setting capability',
                reason: error
            });
            reply(Boom.customCode(504, 'Device did not respond', 'DEVICE_CAP_TIMEOUT'));
        }).catch(function (error) {
            reply(error);
        });
    })
};

exports.listAllCaps = {
    description: 'List all capabilities',
    notes: 'Returns current capabilities for all devices',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['device.list', 'device.read']
    },
    pre: ['device.list(auth)'],
    handler: function (request, reply) {
        const caps = _.flatten(
            _.map(
                request.pre['device.list'],
                device => _.map(
                    device.caps,
                    cap => _.omit(cap, ['_rev', 'type', 'dirty'])
                )
            )
        );
        reply(caps);
    }
};
