// Load modules

const SIP = require('./controllers/sip-ctrl.js');

exports.endpoints = [

    // get SIP configuration
    { method: 'GET',  path: '/sip/extension/{mac}', config: SIP.getSIPConfiguration },

];
