'use strict';

const config = require('../../../lib/config');

exports.getSIPConfiguration = {
    description: 'Get SIP configuration',
    notes: 'Returns SIP configuration',
    tags: ['api'],
    handler: function(request, reply) {
        const mac = config.mac.replace( /:/g, '' );
        return request.context().sdk.get(`/sip/extension/intercom/${mac}`)
        .then(result => result.data)
        .asCallback(reply);
    }
};
