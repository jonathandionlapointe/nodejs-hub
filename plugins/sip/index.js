'use strict';

const Routes = require('./routes');

exports.register = function (server, options, next) {
    server.context();

    server.dependency(['sdk'], function(server, done) {
        server.log(['plugin', 'debug'], 'Registering the SIP plugin');

        server.route(Routes.endpoints);

        done();
    });

    server.expose({});

    next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
