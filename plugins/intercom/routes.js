'use strict';

module.exports = function (server) {
    var Intercom = require('./controllers/intercom-ctrl.js')(server);

    var endpoints = [
        // Intercom interface.
        { method: 'PUT', path: '/intercom/status/{status}', config: Intercom.updateStatus },
        { method: 'GET', path: '/intercom/config', config: Intercom.getConfig },
    ];

    return { endpoints: endpoints };
};
