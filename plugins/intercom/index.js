'use strict';

var P = require('bluebird'),
    Boom = require('boom'),
    Device = require('../../lib/device');

exports.register = function (server, options, next) {

    const protocol = {
        addDevice: P.method(function (context, device) {
            return [device];
        }),
        removeDevice: P.method(function (context, device) {
            throw Boom.badRequest();
        }),
        setCapability: P.method(function (context, device, name, value, unit) {
            return device.updateCap(context, name, value, unit);
        }),
        setParameters: P.method(function (context, device, parameters) {
            return device.updateParameters(context, parameters);
        })
    };

    server.dependency(['device'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Intercom plugin');

        var Routes = require('./routes')(server);
        server.select('internal').route(Routes.endpoints);

        return Device.registerProtocol('intercom', protocol).then(done, done);
    });

    server.expose({
        setStatus: P.method(function (context, device, value) {
            var cap = device.caps['status'];

            if (device.updateCap(context, 'status', value, null)) {
                return device.save(context).then(function () { return cap; });
            } else {
                return false;
            }
        }),
        config: options,
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
