'use strict';

var Device = require('../../../lib/device');

module.exports = function (server) {
    var system = server.plugins.system;

    Device.loadProtocolId(server.context(), 'intercom', null)
    .tap( function ( device ) {
        server.log( ['intercom', 'info'], 'The intercom is already provisioned' );
    })
    .catch( function ( error ) {
        if ( !error.message.match( /not found/i ) ) {
            throw error;
        }

        server.log( ['intercom', 'info'], 'Provisioning the intercom' );

        var doc = {
            name: 'Front intercom',
            mac: system.device.mac,
            doc: '2Klic',
        };

        // model, device
        return Device.add('intercom', doc, null)
        .then(function ( devices ) {
            return devices[0];
        });
    })
    .then( function ( device ) {
        // OK, we have our device. Now what?
    });

    function withDevice(request, f) {
        return Device.loadProtocolId(request.context(), 'intercom', null).then(f);
    }

    return {
        updateStatus: {
            description: 'Update Intercom status',
            handler: function (request, reply) {

                return withDevice(request, function (device) {
                    return server.plugins.intercom.setStatus(request.context(), device, request.params.status)
                    .then(function (cap) {
                        reply();
                    });
                });
            }
        },
        getConfig: {
            description: 'Get intercom configuration',
            tags: ['api'],
            handler: function (request, reply) {
                reply(request.server.plugins.intercom.config);
            }
        },
    };
};
