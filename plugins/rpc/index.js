'use strict';

const _ = require('lodash');
const core = require('@2klic/hub-core');
const CoreProxy = require('./core-proxy');
const Promise = require('bluebird');
const WebSocket = require('ws');

exports.register = function (server, options, next) {
    options = options || {};
    const shutdownTimeout = options.shutdownTimeout || 3000;
    const factoryResetTimeout = options.factoryResetTimeout || 10000;

    const wss = new WebSocket.Server({
        server: (options.websocketServer || server).listener,
    });

    server.dependency(['system'], (server, done) => {
        const proxy = new CoreProxy(server.context(), server);

        const rpc = server.plugins.rpc = core.server(proxy, {
            context: options.context || server.context().getServerContext(),
            wss: wss,
            server: options.server || server,
            package: require('../../package.json'),
        });

        rpc.on('unhandled_connection', (ws, req) => {
            server.log(['rpc', 'debug'], {
                message: 'Unhandled RPC WebSocket path',
                path: req.url,
            });
            ws.close();
        });

        rpc.on('connection', (id, rpc2) => server.log(['rpc', 'debug'], {
            message: 'RPC client connected',
            clientId: id,
            isRpc2: rpc2,
        }));

        rpc.on('disconnect', (id, rpc2) => server.log(['rpc', 'debug'], {
            message: 'RPC client disconnected',
            clientId: id,
            isRpc2: rpc2,
        }));

        Object.defineProperty(rpc, 'services', {
            enumerable: true,
            configurable: false,
            get() {
                return _.map(rpc.peers, peer => ({
                    name: _.get(peer, 'package.name', 'unknown'),
                    version: _.get(peer, 'package.version', 'unknown'),
                    description: _.get(peer, 'package.description', ''),
                    changelog: _.get(peer, 'package.changelog'),
                    protocols: _.keys(peer.protocols),
                    connectedSince: _.get(peer, 'connectedSince', 'unknown'),
                }));
            }
        });

        const events = server.plugins.event;
        events.onEvent(rpc.notifyEvent.bind(rpc));

        const system = server.plugins.system;
        system.onAsync('shutdownRequested', (context, reason) => {
            if (!rpc.peerCount()) {
                return Promise.resolve();
            }

            const timeout = reason === 'factoryReset' ? factoryResetTimeout : shutdownTimeout;
            const deadline = new Date();
            deadline.setMilliseconds(deadline.getMilliseconds() + timeout);
            const promise = proxy.onSystemShutdown(timeout);
            rpc.systemShutdown(deadline, reason);
            return promise;
        });

        system.onAsync('shutdown', (context, reason) => {
            _.forEach(rpc.peers, peer => {
                // Cleanly close the WebSocket connections.
                peer.ws.close(1000, reason);
            });
        });

        return rpc.generateKeyPair().asCallback(done);
    });

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
