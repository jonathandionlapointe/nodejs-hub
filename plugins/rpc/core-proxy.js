'use strict';

const _ = require('lodash');
const Boom = require('super-boom')();
const config = require('../../lib/config');
const core = require('@2klic/hub-core');
const Device = require('../../lib/device');
const ObjectID = require('bson-objectid');
const path = require('path');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

// This class implements hub-core's CorePrivate interface.
function CoreProxy(context, server) {
    this.context = context;
    this.server = server;
    this.shutdownCanceller = null;
}

CoreProxy.prototype.registerDeviceProtocol = function (protocol) {
    return Promise.try(() => {
        Device.registerProtocol(this.context, protocol.name, new ProtocolProxy(this, protocol));
    });
};

CoreProxy.prototype.findDeviceProtocol = function (name) {
    return Device.getProtocol(name);
};

CoreProxy.prototype.findDeviceModel = function (context, id) {
    return context.sdk.models.getCached(context, id);
};

CoreProxy.prototype.findDevice = function (context, id) {
    return Device.load(context, id).then(deviceToJson);
};

CoreProxy.prototype.findProtocolDevices = function (context, protocolName) {
    return Device.loadProtocolAll(context, protocolName).map(deviceToJson);
};

CoreProxy.prototype.findPermission = function (context, rel) {
    return permission.find(context, rel);
};

CoreProxy.prototype.findPermissionsOfType = function (context, rel) {
    return permission.findForType(context, rel, rel.type);
};

CoreProxy.prototype.createDevice = function (context, options) {
    return Device.add(context, options.model, _.merge({}, options.properties, {
        _id: options.id || new ObjectID().toString(),
        protocolName: options.protocol.name,
        protocolData: options.data,
        parent: options.parent,
    }))
    .map(deviceToJson);
};

CoreProxy.prototype.updateDevice = function (context, device, edits) {
    if (device instanceof Device) {
        device = Promise.resolve(device);
    } else {
        device = Device.load(context, device._id);
    }
    return device.then(() => {
        const data = _.merge({}, device.value()._doc, edits);
        return device.value().updateData(context, data);
    }).then(() => {
        if (edits.model) {
            device.value().model = { _id: edits.model };
            device.value().dirty = true;
            return context.emitEvent({
                type: 'update',
                path: `/devices/${device.value()._id}`,
                resource: { model: edits.model }
            });
        }
    }).return(Object.keys(edits.capabilities)).mapSeries(name => {
        const cap = edits.capabilities[name];
        return device.value().updateCap(context, name, cap.value, cap.unit);
    }).then(() => {
        return device.value().updateParameters(context, edits.parameters);
    }).then(() => {
        if (edits.protocolData) {
            device.value().protocolData = edits.protocolData;
            device.value().dirty = true;
        }
    }).then(() => {
        if (edits.status) {
            return device.value().updateStatus(context, edits.status);
        }
    }).then(() => {
        if (edits._removed) {
            return device.value().deactivate(context);
        } else {
            return device.value().save(context);
        }
    });
};

CoreProxy.prototype.deviceLogin = function (context, id) {
    const auth = context.server.plugins['auth'];
    return auth.deviceLogin(id);
};

CoreProxy.prototype.apiRequest = function (context, method, path, body) {
    const options = {
        token: context.sdk.auth.token,
    };
    return context.sdk.http.request(method, path, body, options)
    .catch(err => {
        if (err.statusCode) {
            throw Boom.customCode(err.statusCode, err.body.message, err.code);
        }
        throw err;
    });
};

CoreProxy.prototype.preventShutdown = function (code, reason) {
    if (this.shutdownCanceller) {
        this.shutdownCanceller(Boom.customCode(400, reason, code));
        this.shutdownCanceller = null;
    }
};

CoreProxy.prototype.onSystemShutdown = function (timeout) {
    return new Promise((resolve, reject) => {
        this.shutdownCanceller = reject;
    })
    .timeout(timeout)
    .catch(Promise.TimeoutError, error => {
        this.shutdownCanceller = null;
    });
};

CoreProxy.prototype.createCallContext = function (context, params) {
    return context.createRemoteCallContext(params);
};

CoreProxy.prototype.prepareRequestPre = function (pre) {
    Object.keys(pre).forEach(key => {
        if (pre[key] instanceof Device) {
            pre[key] = deviceToJson(pre[key]);
        } else if (_.isArray(pre[key])) {
            pre[key] = _.map(pre[key], object => {
                if (object instanceof Device) {
                    return deviceToJson(object);
                } else {
                    return object;
                }
            });
        }
    });
    return pre;
};

CoreProxy.prototype.databaseFilePath = function () {
    const file = config.db.url.replace(/^sqlite3:/, '');
    if (!path.isAbsolute(file)) {
        return path.resolve(path.join(process.cwd(), file));
    }
    return file;
};

CoreProxy.prototype.isControllerBound = function (context) {
    return permission.getSystemGroup(this.context)
    .then(group => _.find(group.members, m => m.whoType === 'user'))
    .then(Boolean);
};

CoreProxy.prototype.emitEvent = function (context, event) {
    return context.emitEvent(event);
};

CoreProxy.prototype.notifyProgress = function (context, type, info) {
    const system = context.server.plugins.system;
    system.emit('progress', type, info);
};

module.exports = CoreProxy;

// Wrap some differences between the internal controller's protocol API and the
// hub-core protocol API. Mostly that the hub-core API uses pure-data (JSON)
// representations of devices, and the internal API uses instances of the Device
// class, and the set{Capability/Parameters} result values are different.
function ProtocolProxy(coreProxy, protocol) {
    this.coreProxy = coreProxy;
    this.protocol = protocol;
}

ProtocolProxy.prototype.addDevice = function (context, device) {
    return this.protocol.addDevice(context, deviceToJson(device))
    .map(json => new Device(json.model, jsonToDoc(json)));
};

ProtocolProxy.prototype.removeDevice = function (context, device) {
    return this.protocol.removeDevice(context, deviceToJson(device));
};

ProtocolProxy.prototype.setCapability = function (context, device, name, value, unit) {
    const json = deviceToJson(device);
    return this.protocol.setCapability(context, json, name, value, unit)
    .then(edits => {
        const diff = core.deviceDiff(json, edits);
        return this.coreProxy.updateDevice(context, device, diff)
        .return(Boolean(diff.capabilities[name]));
    });
};

ProtocolProxy.prototype.setParameters = function (context, device, parameters) {
    const json = deviceToJson(device);
    return this.protocol.setParameters(context, json, parameters)
    .then(edits => {
        const diff = core.deviceDiff(json, edits);
        return this.coreProxy.updateDevice(context, device, diff)
        .return(Object.keys(diff.parameters).length > 0 ? diff.parameters : false);
    });
};

function deviceToJson(device) {
    const json = device.toJSON();
    json.model = device.model;
    json.capabilities = json.caps;
    delete json['caps'];
    return json;
}

function jsonToDoc(json) {
    json.caps = json.capabilities;
    delete json['capabilities'];
    return json;
}
