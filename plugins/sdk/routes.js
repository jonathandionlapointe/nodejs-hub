'use strict';

const model = require('./controllers/model-ctrl');

exports.endpoints = [
    // Device model interface.
    { method: 'GET', path: '/models', config: model.getAll },
    { method: 'GET', path: '/models/{id}', config: model.getOne }
];
