'use strict';

exports.getAll = {
    description: 'Get a list of device models',
    notes: 'Returns a list of device models',
    tags: ['api'],
    handler: function (request, reply) {
        return request.server.plugins['cache'].itemsOfType(request.context(), 'model')
        .asCallback(reply);
    }
};

exports.getOne = {
    description: 'Get a device model',
    notes: 'Returns a specific device model',
    tags: ['api'],
    handler: function (request, reply) {
        const modelId = request.params.id;
        const noCache = request.query.no_cache;
        const context = request.context();
        return context.sdk.models.getCached(context, modelId, { force: noCache })
        .then(reply)
        .catch(() => reply().code(404));
    }
};
