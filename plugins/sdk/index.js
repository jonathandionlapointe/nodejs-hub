'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('../../lib/config');
const PlatformSdk = require('@2klic/2klic-sdk');
const routes = require('./routes');

const ONE_WEEK = 60 * 60 * 24 * 7; // In seconds.

exports.register = function (server, options, next) {
    server.context();

    server.dependency([], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the SDK plugin');

        server.route(routes.endpoints);

        return done();
    });

    const sdk = new PlatformSdk({
        app_key: '0000000000000',  // to be defined eventually
        url: process.env.API || config.system.api,
        unsafe: false, // not browser to browser, so it's safe
        apiVersion: '2.0'
    });

    sdk.logger = {};
    _.forEach(['trace', 'debug', 'info', 'warn', 'error', 'fatal'], level => {
        sdk.logger[level] = function () {
            server.log(['trace', 'sdk'], {
                message: arguments[0],
                level: level,
                args: Array.prototype.slice.call(arguments, 1),
            });
        };
    });
    sdk.http.logger = sdk.logger;

    // Augment get() methods with a caching implementation.

    function addCaching(type, resource, lifetime) {
        resource.getCached = function (context, id, options) {
            options = options || {};
            const cache = server.plugins.cache;
            return cache.get(context, type, id, function () {
                return resource.get(id)
                    .then(function (res) {
                        if (res.statusCode === 200) {
                            return res.data;
                        } else {
                            return Promise.reject();
                        }
                    });
            }, {
                noCache: options.force,
                lifetime: lifetime,
            });
        };
    }

    addCaching('model', sdk.models, options.modelLifetime || ONE_WEEK);

    sdk.models.injectCached = function (context, model, lifetime) {
        if (!lifetime) lifetime = 1;
        var cache = server.plugins.cache;
        return cache.inject(context, 'model', model._id, model, { lifetime });
    };

    server.plugins.sdk = sdk;

    server.method('model', function (id, next) {
        return this.sdk.models.getCached(this, id).asCallback(next);
    });

    // Add auth failure hook.
    const request = sdk.http.request;
    sdk.http.request = function (method, path, body, options) {
        if (arguments.length === 3) {
            options = body;
            body = undefined;
        } else if (arguments.length === 2) {
            options = {};
        }

        return request.call(sdk.http, method, path, body, options)
        .catch(error => {
            if (error.statusCode !== 401) {
                throw error;
            }
            const auth = server.plugins['auth'];
            const system = server.plugins['system'];
            if (!auth || !system) {
                throw error;
            }
            return auth.sign({ device: system.device._id }).then(token => {
                return request.call(sdk.http, 'POST', '/auth/renew', { token }).then(result => {
                    return system.updateToken(server.context(), result.data.token)
                    .return(result.data.token);
                });
            })
            .then(token => {
                // Re-attempt the original request, replacing the token parameter.
                options.token = token;
                return request.call(sdk.http, method, path, body, options);
            }, (err) => {
                server.log(['sdk'], {
                    message: 'Reauthentication failed.',
                    reason: err,
                });
                // Throw the original error.
                throw error;
            });
        });
    };

    return next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
