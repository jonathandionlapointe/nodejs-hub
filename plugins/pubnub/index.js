'use strict';

const _ = require('lodash');
const PubSub = require('./pubsub');

exports.register = function (server, options, next) {
    server.context();

    server.dependency(['system', 'auth'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Pubnub plugin');

        const system = server.plugins.system;
        const pubsub = new PubSub(system);

        pubsub.on('error', error => server.log(['pubnub', 'error'], error));
        pubsub.on('subscribe', info => server.log(['pubnub', 'debug'], _.merge({ message: 'Subscribed' }, info)));
        pubsub.on('unsubscribe', info => server.log(['pubnub', 'debug'], _.merge({ message: 'Unsubscribed' }, info)));
        pubsub.on('connect', info => server.log(['pubnub', 'debug'], _.merge({ message: 'Connected' }, info)));
        pubsub.on('reconnect', info => server.log(['pubnub', 'info'], _.merge({ message: 'Reconnected' }, info)));
        pubsub.on('disconnect', info => server.log(['pubnub', 'info'], _.merge({ message: 'Disconnected' }, info)));
        pubsub.on('configure', () => server.log(['pubnub', 'info'], 'Reconfiguring connection due to system config'));
        pubsub.on('publishing', message => {
            server.log(['pubnub', 'trace'], { message: 'Publishing', payload: message });
        });
        pubsub.on('published', message => {
            server.log(['pubnub', 'trace'], { message: 'Published', payload: message });
        });
        pubsub.on('message', message => {
            system.handleCloudMessage(server.context(), message).catch(error => {
                server.log(['pubnub', 'error'], {
                    message: 'Error handling cloud message',
                    error: error,
                });
            });
        });

        pubsub.subscribe();

        // Allow server initialization to continue before connection
        // to pubnub is complete.  We want to allow the server to
        // start without any network connection whatsoever (unless we
        // are performing initial provisioning, in which case the
        // server cannot start without connecting to the API).
        done();
    });

    next();

};

exports.register.attributes = {
    name: 'pubnub'
};
