const _ = require('lodash');
const EventEmitter = require('events');
const Promise = require('bluebird');
const util = require('util');

function PubSub(system) {
    EventEmitter.call(this);

    this.system = null;
    this.authKey = null;
    this.publishChannel = null;
    this.subscribeChannel = null;
    this.pubnub = null;

    this.configure(system);

    system.on('message', message => this.onSystemMessage(message));
    system.on('progress', (type, info) => this.onSystemProgress(type, info));
    system.on('config', config => this.onSystemConfig(config));
}
util.inherits(PubSub, EventEmitter);

PubSub.prototype.configure = function (system) {
    this.system = system;
    this.authKey = system.device.token;
    this.publishChannel = system.device.config.channel;
    this.subscribeChannel = `device_${system.device._id}`;

    this.pubnub = require('pubnub')({
        ssl: true,
        uuid: system.device._id,
        publish_key: system.device.config.publish_key,
        subscribe_key: system.device.config.subscribe_key,
    });
};

PubSub.prototype.subscribe = function () {
    this.emit('subscribe', {
        uuid: this.system.device._id,
        channel: this.subscribeChannel,
    });
    this.pubnub.subscribe({
        channel: this.subscribeChannel,
        auth_key: this.authKey,
        message: message => this.onPubnubMessage(message),
        connect: () => this.onPubnubConnect(),
        reconnect: () => this.onPubnubReconnect(),
        disconnect: () => this.onPubnubDisconnect(),
        error: error => this.onPubnubError(error),
    });
};

PubSub.prototype.unsubscribe = function () {
    this.emit('unsubscribe', { channel: this.subscribeChannel });
    this.pubnub.unsubscribe({
        channel: this.subscribeChannel,
    });
};

PubSub.prototype.onSystemMessage = function (message) {
    if (!message.request) {
        return;
    }

    _.merge(message, { token: this.system.device.token });

    return this.publish(message).catch(error => {
        this.emit('error', error);
    });
};

PubSub.prototype.onSystemProgress = function (type, info) {
    const message = {
        type: 'progress',
        progressType: type,
        info: info,
        token: this.system.device.token,
    };

    return this.publish(message).catch(error => {
        this.emit('error', error);
    });
};

PubSub.prototype.onSystemConfig = function () {
    this.emit('configure');
    this.unsubscribe();
    this.configure(this.system);
    this.subscribe();
};

PubSub.prototype.onPubnubMessage = function (message) {
    this.emit('message', message);
};

PubSub.prototype.onPubnubConnect = function () {
    this.emit('connect', { channel: this.subscribeChannel });
};

PubSub.prototype.onPubnubReconnect = function () {
    this.emit('reconnect', { channel: this.subscribeChannel });
};

PubSub.prototype.onPubnubDisconnect = function () {
    this.emit('disconnect', { channel: this.subscribeChannel });
};

PubSub.prototype.onPubnubError = function (error) {
    this.emit('error', error);
};

PubSub.prototype.publish = function (message) {
    this.emit('publishing', message);
    return new Promise((resolve, reject) => {
        this.pubnub.publish({
            channel: this.publishChannel,
            message: message,
            auth_key: this.authKey,
            callback: m => {
                if (m[1] === 'Sent') {
                    this.emit('published', message);
                    resolve();
                } else {
                    reject(m);
                }
            },
            error: reject
        });
    });
};

module.exports = PubSub;
