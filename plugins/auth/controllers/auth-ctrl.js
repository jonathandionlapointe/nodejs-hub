'use strict';

const Boom = require('boom');
const Joi = require('joi');

exports.nonce = {
    description: 'Generate an authentication nonce',
    tags: ['api'],
    auth: {
        strategy: 'token',
        scope: ['auth.nonce'],
    },
    validate: {
        payload: Joi.object({
            user: Joi.string().required(),
        }),
    },
    pre: ['system.assertGatewayId(params.id)'],
    handler: function (request, reply) {
        const auth = request.server.plugins.auth;
        const user = request.payload.user;
        return request.context().withTransactionContext(context => {
            return auth.syncUser(context, user)
            .filter(data => data._id === user);
        })
        .then(users => {
            if (users.length === 0) {
                throw Boom.notFound();
            }
            return users[0];
        })
        .then(userData => auth.nonce(userData))
        .asCallback(reply);
    }
};

exports.login = {
    description: 'Login to receive an authentication token',
    tags: ['api'],
    auth: {
        strategies: ['simple', 'token']
    },
    handler: function (request, reply) {
        const auth = request.server.plugins.auth;
        return auth.login(request.auth.credentials)
            .then(token => ({ token }))
            .asCallback(reply);
    }
};
