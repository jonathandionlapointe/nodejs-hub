'use strict';

const Boom = require('boom');

exports.get = {
    description: 'Get a user',
    tags: ['api'],
    auth: {
        strategy: 'token'
    },
    handler: function (request, reply) {
        const auth = request.server.plugins.auth;
        const context = request.context();
        return auth.getUserByName(context, request.params.username).tap(user => {
            if (context.who !== user._id) {
                throw Boom.forbidden();
            }
        }).asCallback(reply);
    }
};
