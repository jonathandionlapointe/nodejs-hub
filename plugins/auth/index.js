'use strict';

const Auth = require('../../lib/auth');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const routes = require('./routes');

exports.register = function (server, options, next) {
    server.context();

    server.dependency(['system', 'sdk', 'event', 'hapi-auth-basic', 'hapi-auth-jwt'], function (server, done) {
        server.log(['plugin', 'debug'], 'Registering the Auth plugin');

        const auth = new Auth();

        auth.on('keyError', error => server.log(['auth', 'warn'], {
            message: 'Unable to load keys',
            reason: error
        }));

        return auth.init(options).then(() => {
            server.auth.strategy('simple', 'basic', {
                validateFunc: (request, username, password, callback) => {
                    const ctx = request.server.context();
                    return validateAuth(
                        auth.validateCredentials(ctx, username, password),
                        request, callback);
                }
            });

            server.auth.strategy('token', 'jwt', {
                key: auth.publicKey,
                verifyOptions: { algorithms: [ auth.algorithm ] },
                validateFunc: (request, token, callback) => {
                    const ctx = request.server.context();
                    const who = request.headers['x-who'];
                    const whoType = request.headers['x-who-type'];
                    return validateAuth(
                        auth.validateToken(ctx, token, whoType, who),
                        request, callback);
                }
            });

            server.plugins.event.route();
            server.plugins.system.route();

            server.select('api').route(routes.endpoints);

            server.plugins.auth = auth;

            const ctx = server.context();

            server.plugins.system.on('heartbeat', () => {
                return auth.sync(ctx).catch(error => {
                    server.log(['auth', 'debug'], {
                        message: 'Error during sync',
                        reason: error,
                    });
                });
            });

            return Promise.all([
                Auth.views.update(ctx),
                permission.views.update(ctx),
            ])
            .then(() => auth.sign({ device: ctx.gatewayId }))
            .then(token => {
                server.log(['auth', 'debug'], 'Requesting a fresh API token');
                return ctx.sdk.http.request('POST', '/auth/renew', { token }).then(result => {
                    return server.plugins.system.updateToken(ctx, result.data.token)
                    .return(result.data.token);
                })
                .catch(error => {
                    server.log(['auth', 'info'], {
                        message: 'Unable to retrieve fresh token, is the API down?',
                        reason: error,
                    });
                });
            });
        }).asCallback(done);
    });

    return server.register([
        require('hapi-auth-basic'),
        require('hapi-auth-jwt'),
    ], next);
};

exports.register.attributes = {
    pkg: require('./package.json')
};

/**
 * @param {Promise<any>} authPromise
 * @param {Hapi.Request} request
 * @param {Function} callback
 * @returns {Promise<any>}
 */
function validateAuth(authPromise, request, callback) {
    return authPromise.tap(data => {
        const logger = request.server.plugins['2klic-hapi-bunyan'];
        if (data && data.who && logger) {
            logger.setRequestWhoId(data.who);
        }
    })
    .then(data => callback(null, Boolean(data), data))
    .catch(callback);
}
