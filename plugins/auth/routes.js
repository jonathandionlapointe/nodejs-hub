'use strict';

const auth = require('./controllers/auth-ctrl.js');
const user = require('./controllers/user-ctrl.js');

exports.endpoints = [
    { method: 'POST', path: '/devices/{id}/auth/nonce', config: auth.nonce },
    { method: 'POST', path: '/auth/login',              config: auth.login },
    { method: 'GET',  path: '/users/{username}',        config: user.get },
];
