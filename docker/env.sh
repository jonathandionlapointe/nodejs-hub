export HUB_SYSTEM_FILE=/var/lib/sc2klic/system.json
export HUB_DB_URL=sqlite3:/var/lib/sc2klic/app.db
export HUB_AUTH_KEYS_PUBLIC_FILE=/var/lib/sc2klic/public.pem
export HUB_AUTH_KEYS_PRIVATE_FILE=/var/lib/sc2klic/private.pem
export HUB_REMOTE_ACCESS_PUBLIC_KEY_FILE=/var/lib/ssh-proxy/id_rsa.pub
export HUB_REMOTE_ACCESS_LOCAL_PASSPHRASE_FILE=/var/lib/ssh-proxy/passwd
export HUB_AUTH_CERT=/var/lib/2klic/server.crt
export HUB_CHANGELOG=/usr/share/doc/2klic-gateway/changelog.Debian.gz
