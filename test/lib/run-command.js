/*global describe, it*/
'use strict';

const expect = require('chai').expect;
const os = require('os');
const runCommand = require('../../lib/run-command');

describe('run-command', function () {
    it('should run a command for its stdout', function () {
        return runCommand('/bin/echo', ['hello', 'world'])
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stdout).to.deep.equal(new Buffer('hello world\n', 'utf8'));
            expect(result.stderr.length).to.equal(0);
        });
    });

    it('should run a command for its stderr', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo hello world'])
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stderr).to.deep.equal(new Buffer('hello world\n', 'utf8'));
            expect(result.stdout.length).to.equal(0);
        });
    });

    it('should run a command for both stdout and stderr', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo error output; echo normal output'])
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stdout).to.deep.equal(new Buffer('normal output\n', 'utf8'));
            expect(result.stderr).to.deep.equal(new Buffer('error output\n', 'utf8'));
        });
    });

    it('should ignore stdout if specified', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo error output; echo normal output'], { ignoreStdout: true })
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stdout.length).to.equal(0);
            expect(result.stderr).to.deep.equal(new Buffer('error output\n', 'utf8'));
        });
    });

    it('should ignore stderr if specified', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo error output; echo normal output'], { ignoreStderr: true })
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stdout).to.deep.equal(new Buffer('normal output\n', 'utf8'));
            expect(result.stderr.length).to.equal(0);
        });
    });

    it('should ignore both stdout and stderr if specified', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo error output; echo normal output'], { ignoreStdout: true, ignoreStderr: true })
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Buffer);
            expect(result.stderr).to.be.instanceof(Buffer);
            expect(result.stdout.length).to.equal(0);
            expect(result.stderr.length).to.equal(0);
        });
    });

    it('should throw an error if the command fails', function () {
        return runCommand('/bin/bash', ['-c', '>&2 echo command failed; exec false'])
        .then(() => {
            throw new Error('unexpected success');
        }, error => {
            expect(error).to.be.instanceof(Error);
            expect(error).to.contain.keys('code', 'stdout', 'stderr');
            expect(error.message).to.equal('non-zero exit code');
            expect(error.code).to.equal(1);
            expect(error.stderr).to.be.instanceof(Buffer);
            expect(error.stderr).to.deep.equal(new Buffer('command failed\n', 'utf8'));
        });
    });

    it('should split lines if asked', function () {
        return runCommand('/bin/echo', ['-e', 'Hello' + os.EOL + 'world'], { splitLines: true })
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Array);
            expect(result.stdout).to.deep.equal(['Hello', 'world', '']);
        });
    });

    it('should split lines with a custom delimiter', function () {
        return runCommand('/bin/echo', ['-e', 'Hello\tworld'], { splitLines: '\t' })
        .then(result => {
            expect(result).to.have.all.keys('stdout', 'stderr');
            expect(result.stdout).to.be.instanceof(Array);
            expect(result.stdout).to.deep.equal(['Hello', 'world\n']);
        });
    });
});
