/*global describe, it, before, after*/
'use strict';

const _ = require('lodash');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const permission = require('../../lib/permission');
const ObjectID = require('bson-objectid');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const DEVICE_ID = _.map(_.range(1, 10), () => ObjectID().toString());
const USER_ID = ObjectID().toString();
const GATEWAY_ID = ObjectID().toString();
const PERMISSIONS = _.map(DEVICE_ID, id => ({
    who: USER_ID,
    whoType: 'user',
    resource: id,
    type: 'device',
    status: 'ACTIVE',
    read: true, write: true, manage: true, share: true
}));

PERMISSIONS.push({
    who: USER_ID,
    whoType: 'user',
    resource: GATEWAY_ID,
    type: 'gateway',
    status: 'ACTIVE',
    read: true, write: true, manage: true, share: true
});

const NEW_DEVICE_ID = ObjectID().toString();

const SDK = {
    permissions: {
        list(user) {
            if (user === USER_ID) {
                return Promise.resolve({
                    statusCode: 200,
                    data: PERMISSIONS,
                });
            } else {
                return Promise.resolve({
                    statusCode: 200,
                    data: [],
                });
            }
        }
    }
};

describe('Permissions', function () {
    this.slow(150);

    const context = Object.create(app.context, {
        sdk: { configurable: false, enumerable: false, value: SDK },
        gatewayId: { configurable: false, enumerable: true, value: GATEWAY_ID },
    });

    before(function () {
        return dropDbs(context)
        .then(() => permission.views.update(context));
    });

    after(function () {
        return dropDbs(context);
    });

    it('should sync permissions using the SDK', function () {
        this.slow(300);

        return permission.sync(context, USER_ID);
    });

    it('should have cached all SDK permissions', function () {
        return Promise.map(PERMISSIONS, perm => {
            return permission.get(context, `${perm.whoType}-${perm.who}_on_${perm.type}-${perm.resource}`).then(cached => {
                expect(_.omit(cached, ['_rev', 'isOwnGateway', '_id'])).to.deep.equal(perm);
            });
        });
    });

    it('should assign the isOwnGateway property to the gateway permission', function () {
        return permission.get(context, `user-${USER_ID}_on_gateway-${GATEWAY_ID}`).then(perm => {
            expect(perm.isOwnGateway).to.equal(true);
        });
    });

    it('should find permissions for a specific type', function () {
        return permission.findForType(context, { who: USER_ID, whoType: 'user' }, 'device')
        .then(results => {
            expect(results.length).to.equal(DEVICE_ID.length);
        });
    });

    it('should locally set permissions', function () {
        return permission.upsert(context, {
            who: USER_ID,
            whoType: 'user',
            resource: DEVICE_ID[0],
            type: 'device',
            status: 'ACTIVE',
            read: false, write: false, share: false, manage: false,
        })
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${DEVICE_ID[0]}`))
        .then(perm => {
            expect(perm.read).to.equal(false);
        });
    });

    it('should override locally set permissions on sync', function () {
        return permission.sync(context, USER_ID)
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${DEVICE_ID[0]}`))
        .then(perm => {
            expect(perm.read).to.equal(true);
        });
    });

    it('should grant full access for new permissions', function () {
        return permission.grant(context, { who: USER_ID, whoType: 'user' }, { resource: NEW_DEVICE_ID, type: 'device' })
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${NEW_DEVICE_ID}`))
        .then(perm => {
            expect(perm.read).to.equal(true);
            expect(perm.write).to.equal(true);
            expect(perm.share).to.equal(true);
            expect(perm.manage).to.equal(true);
        });
    });

    it('should revoke access for a permission', function () {
        return permission.revoke(context, { who: USER_ID, whoType: 'user' }, { resource: NEW_DEVICE_ID, type: 'device' })
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${NEW_DEVICE_ID}`))
        .then(perm => {
            expect(perm.status).to.equal('REMOVED');
        });
    });

    it('should revoke all permissions for a specified resource', function () {
        return permission.revokeAll(context, { resource: DEVICE_ID[0], type: 'device' })
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${DEVICE_ID[0]}`))
        .then(perm => {
            expect(perm.status).to.equal('REMOVED');
        });
    });

    it('should override locally revoked permissions on sync', function () {
        return permission.sync(context, USER_ID)
        .then(() => permission.get(context, `user-${USER_ID}_on_device-${DEVICE_ID[0]}`))
        .then(perm => {
            expect(perm.status).to.equal('ACTIVE');
            expect(perm.read).to.equal(true);
        });
    });

    it('should register resource paths', function () {
        permission.resourcePath('/devices/:id', 'device');
        permission.resourcePath('/devices/:device/caps/:name', { type: 'device', param: 'device' });
    });

    it('should get permission  for resource paths', function () {
        let p = permission.pathPermission('/devices/foo');
        expect(p).to.be.an('object');
        expect(p.type).to.equal('device');
        expect(p.resource).to.equal('foo');
        expect(p.read).to.equal(true);

        p = permission.pathPermission('/devices/foo', permission.WRITE);
        expect(p.type).to.equal('device');
        expect(p.resource).to.equal('foo');
        expect(p.write).to.equal(true);

        p = permission.pathPermission('/devices/foo/caps/sensor', [permission.READ, permission.WRITE]);
        expect(p.type).to.equal('device');
        expect(p.resource).to.equal('foo');
        expect(p.read).to.equal(true);
        expect(p.write).to.equal(true);
    });

    it('should reset the module', function () {
        permission.reset();

        const p = permission.pathPermission('/devices/foo');
        expect(p.resource).to.equal('unknown');
        expect(p.type).to.equal('unknown');
    });

});
