/*global describe, it, before, after, beforeEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const jwt = require('jsonwebtoken');
const Promise = require('bluebird');
const Provisioner = require('../../lib/provisioner');

const app = {};
app.context = context.createAppContext(app);

const token = jwt.sign({ api: 'http://127.0.0.1:3003', environment: config.node_env }, 'some not so secret secret');
const BINDING_CODE = 'testcode';
const MAC = '00:00:00:00:00:00';

let apiProvisioned = null;
let locallyProvisioned = null;

const sdk = {
    // FIXME remove this when the SDK is updated to support
    // provisioning with remoteAccess info.
    http: {
        get(path) {
            expect(path).to.equal('/models/smart_controller_v1');
            return Promise.resolve({ statusCode: 200, data: {
                _id: 'smart_controller_v1',
                productCode: 'test'
            }});
        },
        post(path, obj, options) {
            expect(path).to.equal('/provisioning');
            expect(options).to.deep.equal({ token });
            expect(obj).to.have.all.keys('name', 'model', 'lot', 'branding', 'defaultLanguage', 'remoteAccess', 'publicKey');
            expect(obj.remoteAccess).to.have.all.keys('publicKey', 'localPassphrase');
            apiProvisioned = _.merge({
                _id: 'test_system_device',
            }, _.pick(obj, ['name', 'model', 'remoteAccess', 'publicKey']));
            return Promise.resolve({
                data: {
                    provisioning: {
                        mac: MAC,
                        bindingCode: BINDING_CODE,
                    },
                    device: apiProvisioned,
                }
            });
        }
    },
    auth: {}
};

function immediateAsync() {
    return new Promise(resolve => setImmediate(resolve));
}

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        else return immediateAsync().then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

describe('Provisioner', function () {
    let server, provisioner;

    function request(options) {
        if (typeof options === 'string') {
            options = { url: options, method: 'GET' };
        }
        return new Promise(resolve => server.inject(options, resolve));
    }

    before(function () {
        try { fs.unlinkSync(config.system.file); } catch (error) {/*eslint no-empty:"warn"*/}
        config.reload();

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.plugins.sdk = sdk;
        server.connection({
            host: '127.0.0.1',
            port: config.server.port,
            labels: ['api']
        });
        app.context.createServerContext(server);
        return server.startAsync().then(() => { config.fixPorts(server); });
    });

    after(function () {
        try { fs.unlinkSync(config.system.file); } catch (error) {/*eslint no-empty:"warn"*/}
        config.reload();
        return server.stopAsync();
    });

    beforeEach(function () {
        apiProvisioned = null;
        locallyProvisioned = null;
    });

    it('should instantiate a provisioner', function () {
        provisioner = new Provisioner(server.context());
    });

    it('should add a provisioned event listener', function () {
        provisioner.on('provisioned', data => {
            locallyProvisioned = data;
        });
    });

    it('should get provisioner system info', function () {
        return request({
            url: endpoint('/system/info'),
            method: 'GET'
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.deep.equal({ provisioner: true });
        });
    });

    it('should provision', function () {
        expect(fs.existsSync(config.system.file)).to.equal(false);
        let provisioned = null;
        provisioner.once('provisioned', data => provisioned = data);
        return Promise.all([
            request({
                url: endpoint('/provisioning'),
                method: 'POST',
                payload: { token, model: 'smart_controller_v1', lot: 'xyz', branding: '2KLIC', defaultLanguage: 'en_US' }
            }),
            waitUntil(() => Boolean(provisioned)),
        ]).spread(response => {
            expect(response.statusCode).to.equal(200);
            expect(apiProvisioned).to.be.an('object');
            expect(locallyProvisioned).to.be.an('object');
            expect(response.result).to.have.all.keys('environment', 'api', 'file', 'provisioning', 'device', 'hostname', 'lot', 'branding', 'defaultLanguage');
            expect(response.result.device).to.deep.equal(apiProvisioned);
            expect(response.result.provisioning).to.contain.all.keys('mac', 'bindingCode');
            expect(response.result.hostname).to.equal('2klic-test-test_system_device');
            expect(provisioned).to.deep.equal(response.result);
            expect(fs.existsSync(config.system.file)).to.equal(true);
        });
    });
});
