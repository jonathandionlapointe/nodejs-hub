/*global describe, it, before, after, afterEach*/
'use strict';

const _ = require('lodash');
const Boom = require('boom');
const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const Events = require('../../lib/events');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const jwt = require('jsonwebtoken');
const ObjectID = require('bson-objectid');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const Provisioner = require('../../lib/provisioner');
const System = require('../../lib/system');

const app = {};
app.context = context.createAppContext(app);

let apiProvisioned = {};

const token = jwt.sign({
    api: 'http://127.0.0.1:3003',
    environment: config.node_env,
    test: true,
}, 'some not so secret secret');
const BINDING_CODE = 'testcode';
const MAC = '00:00:00:00:00:00';
const GROUP = new ObjectID().toString();

const sdk = {
    models: {
        get(id) {
            if (id === config.model_id) {
                return Promise.resolve({ data: { _id: id } });
            }
            return Promise.resolve({});
        }
    },
    devices: {
        provision(name, mac, model) {
            apiProvisioned = {
                _id: 'test_system_device',
                name, mac, model
            };
            return Promise.resolve({
                data: apiProvisioned
            });
        }
    },
    events: {
        create() {
            return Promise.resolve();
        }
    },
    // FIXME remove this when the SDK is updated to support
    // provisioning with remoteAccess info.
    http: {
        get(path) {
            expect(path).to.equal('/models/smart_controller_v1');
            return Promise.resolve({ statusCode: 200, data: {
                _id: 'smart_controller_v1',
                productCode: 'test'
            }});
        },
        post(path, obj, options) {
            expect(path).to.equal('/provisioning');
            expect(options).to.deep.equal({ token });
            expect(obj).to.have.all.keys('name', 'model', 'lot', 'branding', 'defaultLanguage', 'remoteAccess', 'publicKey');
            expect(obj.remoteAccess).to.have.all.keys('publicKey', 'localPassphrase');
            apiProvisioned = _.merge({
                _id: 'test_system_device',
            }, _.pick(obj, ['name', 'model', 'lot', 'branding', 'defaultLanguage', 'remoteAccess', 'publicKey']));
            return Promise.resolve({
                data: {
                    provisioning: {
                        mac: MAC,
                        bindingCode: BINDING_CODE,
                        group: GROUP,
                    },
                    device: apiProvisioned,
                }
            });
        }
    },
    auth: {}
};

function immediateAsync() {
    return new Promise(resolve => setImmediate(resolve));
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        else return immediateAsync().then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

describe('System', function () {
    let system, server, events;
    let request, response, message;
    let serverMethods = { GET: false, POST: false, PUT: false, DELETE: false };
    let lastEvents = [];

    function waitUntilEventCount(n, timeout) {
        return waitUntil(() => lastEvents.length >= n, timeout);
    }

    before(function () {
        events = new Events();
        events.settleTime = 50;
        events.onEvent((context, event) => {
            lastEvents.push(event);
        });

        try { fs.unlinkSync(config.system.file); } catch (error) {/*eslint no-empty:"warn"*/}
        config.reload();

        return dropDbs(app.context)
        .then(() => System.views.update(app.context))
        .then(() => events.init(app.context))
        .then(() => permission.views.update(app.context))
        .then(() => {
            server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
            server.plugins.sdk = sdk;
            server.plugins.event = events;
            server.connection({
                host: '127.0.0.1',
                port: config.server.port,
                labels: ['api']
            });
            app.context.createServerContext(server);
            server.route({
                method: Object.keys(serverMethods),
                path: '/test',
                config: {
                    handler(request, reply) {
                        const method = request.method.toUpperCase();
                        serverMethods[method] = true;
                        reply({ method: method });
                    }
                }
            });
            server.route({
                method: Object.keys(serverMethods),
                path: '/redirect_test',
                config: {
                    handler(request, reply) {
                        reply().redirect('/test').code(307);
                    }
                }
            });
            server.route({
                method: 'PUT',
                path: '/304_test',
                config: {
                    handler(request, reply) {
                        reply().code(304);
                    }
                }
            });
            server.route({
                method: 'PUT',
                path: '/error_test',
                config: {
                    handler(request, reply) {
                        reply(Boom.notFound());
                    }
                }
            });
            return server.startAsync().then(() => { config.fixPorts(server); });
        });
    });

    after(function () {
        events.settleTime = 200;
        return events.settle()
        .then(() => Promise.all([
            dropDbs(app.context),
            server.stopAsync(),
        ])).catchReturn();
    });

    afterEach(function () {
        request = response = message = false;
        lastEvents = [];
        _.forEach(Object.keys(serverMethods), method => {
            serverMethods[method] = false;
        });
    });

    it('should create a system instance', function () {
        system = new System();
        system.on('request', request_ => { request = request_; });
        system.on('response', response_ => { response = response_; });
        system.on('message', message_ => { message = message_; });
    });

    it('should not update the system device before it is provisioned', function () {
        expect(() => system.update(server.context(), {})).to.throw(/not provisioned/);
    });

    it('should provision', function () {
        this.slow(200);

        expect(fs.existsSync(config.system.file)).to.equal(false);
        const provisioner = new Provisioner(server.context(), { noServer: true });
        return provisioner.provision(server.context(), {
            token, model: 'smart_controller_v1', lot: 'xyz', branding: '2KLIC', defaultLanguage: 'en_US'
        })
        .then(() => {
            expect(fs.existsSync(config.system.file)).to.equal(true);
            config.reload();
        })
        .then(() => server.context().withTransactionContext(context => {
            return system.provision(context);
        }))
        .then(() => {
            expect(system.device).to.be.an('object');
            expect(apiProvisioned.remoteAccess).to.be.an('object');
            expect(apiProvisioned.publicKey).to.be.a('string');
        });
    });

    it('should not provision twice', function () {
        return server.context().withTransactionContext(context => system.provision(context))
        .then(() => {
            expect(system.device).to.deep.equal(apiProvisioned);
        });
    });

    it('should not provision twice even if the database is wiped', function () {
        this.slow(700);

        return dropDbs(app.context)
        .then(() => System.views.update(app.context))
        .then(() => events.init(app.context))
        .then(() => permission.views.update(app.context))
        .then(() => system.provision(server.context())).then(() => {
            expect(system.device).to.deep.equal(apiProvisioned);
        });
    });

    it('should update the system device', function () {
        this.slow(150);

        return Promise.all([
            system.update(server.context(), { location: 'foo' }),
            waitUntilEventCount(1)
        ]).then(() => {
            expect(system.device.location).to.equal('foo');
            expect(lastEvents[0].type).to.equal('update');
            expect(lastEvents[0].path).to.equal(`/devices/${system.device._id}`);
            expect(lastEvents[0].resource).to.be.an('object');
            expect(lastEvents[0].resource.location).to.equal('foo');
        });
    });

    it('should have a JSON representation', function () {
        const json = system.toJSON();
        expect(json).to.be.an('object');
        expect(json._id).to.equal(system.device._id);
        // Test any other properties?
    });

    it('should ignore cloud message with an invalid type', function () {
        return system.handleCloudMessage(server.context(), {}).then(() => {
            expect(request).to.deep.equal({});
            expect(response).to.equal(false);
            expect(message).to.equal(false);
        });
    });

    it('should handle cloud read messages', function () {
        const cloudMessage = {
            type: 'read',
            path: '/test',
            request: 'read-test',
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.result).to.be.an('object');
            expect(response.result).to.deep.equal({ method: 'GET' });
            expect(message).to.be.an('object');
            expect(_.omit(message, ['timestamp'])).to.deep.equal(_.merge({}, cloudMessage, {
                resource: { method: 'GET' },
                statusCode: 200,
            }));
            expect(serverMethods.GET).to.equal(true);
        });
    });

    it('should handle cloud create messages', function () {
        const cloudMessage = {
            type: 'create',
            path: '/test/' + ObjectID().toString(),
            request: 'create-test',
            resource: { foo: 42 },
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.result).to.be.an('object');
            expect(response.result).to.deep.equal({ method: 'POST' });
            //expect(message).to.equal(false); // cf TODO in handleCloudMessage
            expect(serverMethods.POST).to.equal(true);
        });
    });

    it('should handle cloud update messages', function () {
        const cloudMessage = {
            type: 'update',
            path: '/test',
            request: 'update-test',
            resource: { foo: 42 },
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.result).to.be.an('object');
            expect(response.result).to.deep.equal({ method: 'PUT' });
            //expect(message).to.equal(false); // cf TODO in handleCloudMessage
            expect(serverMethods.PUT).to.equal(true);
        });
    });

    it('should handle cloud delete messages', function () {
        const cloudMessage = {
            type: 'delete',
            path: '/test',
            request: 'delete-test',
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.result).to.be.an('object');
            expect(response.result).to.deep.equal({ method: 'DELETE' });
            //expect(message).to.equal(false); // cf TODO in handleCloudMessage
            expect(serverMethods.DELETE).to.equal(true);
        });
    });

    it('should handle redirected cloud update messages', function () {
        const cloudMessage = {
            type: 'update',
            path: '/redirect_test',
            request: 'redirect-test',
            resource: { foo: 42 },
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.result).to.be.an('object');
            expect(response.result).to.deep.equal({ method: 'PUT' });
            //expect(message).to.equal(false); // cf TODO in handleCloudMessage
            expect(serverMethods.PUT).to.equal(true);
        });
    });

    it('should handle 304 responses', function () {
        const cloudMessage = {
            type: 'update',
            path: '/304_test',
            request: '304-test',
            resource: { foo: 42 },
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.statusCode).to.equal(304);
            expect(message).to.be.an('object');
            expect(_.omit(message, ['timestamp'])).to.deep.equal(_.merge({}, cloudMessage, {
                resource: 'No change',
                statusCode: 304,
            }));
        });
    });

    it('should handle error responses', function () {
        const cloudMessage = {
            type: 'update',
            path: '/error_test',
            request: 'error-test',
            resource: { foo: 42 },
        };
        return system.handleCloudMessage(server.context(), cloudMessage).then(() => {
            expect(request).to.deep.equal(cloudMessage);
            expect(response.statusCode).to.equal(404);
            expect(message).to.be.an('object');
            expect(_.omit(message, ['timestamp'])).to.deep.equal({
                type: 'update',
                path: '/error_test',
                request: 'error-test',
                error: {
                    error: 'Not Found',
                    statusCode: 404,
                },
                statusCode: 404,
            });
        });
    });

    it('should emit a shutdown event', function () {
        let reason = null;
        system.on('shutdown', (context, reason_) => { reason = reason_; });
        return system.shutdown(server.context(), 'no reason').then(() => {
            expect(reason).to.equal('no reason');
        });
    });
});
