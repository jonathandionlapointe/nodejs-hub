/*global describe, it, before, beforeEach, after*/

const _ = require('lodash');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const Joi = require('joi');
const Promise = require('bluebird');
const Events = require('../../lib/events');

const app = {};
app.context = context.createAppContext(app);

describe('Events', function () {
    this.slow(200);

    var events;

    before(function () {
        return dropDbs(app.context);
    });

    after(function () {
        return dropDbs(app.context).timeout(5000);
    });

    const emittedEvent = {
        type: 'create',
        path: '/foo/bar',
        resource: { quux: 42 }
    };
    var createdEvent = null;
    var sdkBehaviour = 'success';
    const sdk = {
        events: {
            create: Promise.method(function (event) {
                if (sdkBehaviour === 'success') {
                    createdEvent = _.merge({}, event);
                } else if (sdkBehaviour === 'ignoredError') {
                    throw { statusCode: 400 };
                } else if (sdkBehaviour === 'retryError') {
                    sdkBehaviour = 'success';
                    throw { statusCode: 500 };
                }
            })
        }
    };
    sdk.context = Object.create(app.context, {
        server: { configurable: false, enumerable: false, value: true },
        sdk: { configurable: false, enumerable: false, value: sdk },
    });

    beforeEach(function () {
        createdEvent = null;
        emittedEvent._id = undefined;
    });

    function waitUntil(predicate, timeout) {
        function f() {
            if (predicate()) return Promise.resolve();
            else return new Promise(setImmediate).then(f);
        }
        return timeout ? f().timeout(timeout) : f();
    }

    function waitUntilEvent(timeout) {
        return waitUntil(() => createdEvent !== null);
    }

    describe('core', function () {
        it('should create a new events manager', function () {
            events = new Events();
            return events.init(app.context);
        });

        it('should set a timestamp on emitted events', function () {
            var ts = null;
            function listener(context, event) {
                ts = event.timestamp;
            }
            const event = {};
            events.onEvent(listener);
            return events.emitEvent(sdk.context, event)
            .then(() => waitUntilEvent())
            .then(() => {
                events.removeEventListener(listener);
                expect(ts).to.be.a('string');
                expect(ts).to.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/);
            });
        });
    });

    describe('path regexps', function () {
        var listener;
        var calledMatch = false;

        before(function () {
            listener = events.onEventPathMatch(/foo(.*)/, (context, match, event) => {
                calledMatch = match;
            });
        });

        beforeEach(function () {
            calledMatch = false;
        });

        after(function () {
            events.removeEventListener(listener);
        });

        it('should respond to events at a regexp', function () {
            return events.emitEvent(sdk.context, { path: 'foo' })
            .then(() => waitUntilEvent())
            .then(() => {
                expect(calledMatch).to.not.be.false;
            });
        });

        it('should capture groups in the regexp', function () {
            return events.emitEvent(sdk.context, { path: 'foobar' })
            .then(() => waitUntilEvent())
            .then(() => {
                expect(calledMatch[1]).to.deep.equal('bar');
            });
        });

        it('should not respond to non-matches', function () {
            return events.emitEvent(sdk.context, { path: 'bar' })
            .then(() => waitUntilEvent())
            .then(() => {
                expect(calledMatch).to.be.false;
            });
        });
    });

    describe('forwarding', function () {
        it('should send events', function () {
            const event = {
                type: 'update',
                path: '/quux',
                resource: {}
            };
            return events.emitEvent(sdk.context, event)
            .then(() => waitUntilEvent()).then(() => {
                expect(createdEvent).to.deep.equal(event);
            });
        });

        it('should assign object IDs to events', function () {
            const event = {
                type: 'update',
                path: '/quux',
                resource: {}
            };
            return events.emitEvent(sdk.context, event)
            .then(() => waitUntilEvent()).then(() => {
                expect(event._id).to.be.a('string');
                expect(event._id).to.match(/[0-9A-Fa-f]{24}/);
            });
        });

        it('should ignore 4xx responses from the sdk', function () {
            var dropped = false;
            function onDrop() {
                dropped = true;
            }
            events.on('drop', onDrop);
            sdkBehaviour = 'ignoredError';
            return events.emitEvent(sdk.context, emittedEvent)
            .then(() => {
                return waitUntil(() => dropped === true);
            }).finally(() => {
                events.removeListener('drop', onDrop);
            });
        });

        it('should retry for 5xx reponses from the sdk', function () {
            var retried = false;
            function onRetry(info) {
                retried = true;
            }
            events.backoffTimeFactor = 0.01;
            events.on('retry', onRetry);
            sdkBehaviour = 'retryError';
            return events.emitEvent(sdk.context, emittedEvent)
            .then(() => {
                return waitUntil(() => retried === true);
            }).finally(() => {
                events.removeListener('retry', onRetry);
            });
        });

        it('should check the event schema', function () {
            Joi.assert(emittedEvent, Events.schema);
        });

        it('should settle after no events', function () {
            events.settleTime = 200;
            return events.settle();
        });

        it('should stop forwarding events', function () {
            events.stop();
        });
    });
});
