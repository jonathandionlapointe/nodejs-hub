/*global describe, it, before, after, beforeEach, afterEach*/
/*eslint no-console: ["off"]*/

'use strict';

const _ = require('lodash');
const Alarm = require('../../lib/alarm');
const context = require('../../lib/context');
const Device = require('../../lib/device');
const dropDbs = require('../util/drop-dbs');
const Events = require('../../lib/events');
const expect = require('chai').expect;
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

Alarm.minimumDelay = 0.001;

function unexpected() {
    throw new Error('unexpected success');
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        return new Promise(setImmediate).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

const models = {
    motion: {
        _id: 'motion',
        protocolId: 'dumb',
        capabilities: [{
            capabilityId: 'motion',
            type: 'enum',
            methods: ['get'],
            range: ['idle', 'detected']
        }]
    },
    smoke: {
        _id: 'smoke',
        protocolId: 'dumb',
        capabilities: [{
            capabilityId: 'smoke',
            type: 'enum',
            methods: ['get'],
            range: ['idle', 'detected']
        }]
    },
    door: {
        _id: 'door',
        protocolId: 'dumb',
        capabilities: [{
            capabilityId: 'door',
            type: 'enum',
            methods: ['get'],
            range: ['closed', 'open']
        }]
    },
    window: {
        _id: 'window',
        protocolId: 'dumb',
        capabilities: [{
            capabilityId: 'window',
            type: 'enum',
            methods: ['get'],
            range: ['closed', 'open']
        }]
    },
};

const dumbProtocol = {
    addDevice(context, device) {
        return Promise.resolve([device]);
    },
    removeDevice(context, device) {
        return Promise.resolve();
    },
};

const sdk = {
    models: {
        getCached(context, id) {
            return Promise.resolve(models[id]);
        }
    },
    events: {
        create() {
            return Promise.resolve();
        }
    }
};

describe('Alarm', function () {
    this.slow(250);

    var registeredSystem;
    var unregisteredSystem;
    var evts = [], alarmError;
    var createdSystemId, createdZoneId, createdSectorId;
    var motionDevice, smokeDevice, doorDevice, windowDevice;
    var alarmErrorExpected = false;

    const events = new Events();
    events.onEvent((context, event) => evts.push(event));

    const context = Object.create(app.context, {
        events: { configurable: false, enumerable: false, value: events },
        sdk: { configurable: false, enumerable: false, value: sdk },
        server: { configurable: false, enumerable: false, value: true },
    });

    before(function () {
        return dropDbs(context)
        .then(() => events.init(app.context))
        .then(() => permission.views.update(app.context))
        .then(() => Promise.each([Alarm, Device], T => T.views.update(context)))
        .then(() => Device.registerProtocol(context, 'dumb', dumbProtocol))
        .then(() => context.withTransactionContext(context => Promise.mapSeries([
            [Device.add, [context, models.motion, {}]],
            [Device.add, [context, models.smoke, {}]],
            [Device.add, [context, models.door, {}]],
            [Device.add, [context, models.window, {}]],
            [() => context.collection('cache').create(), []],
            [() => Promise.mapSeries(_.values(models), model =>
                context.collection('cache').insert({ _id: 'model:' + model._id, data: model })
            ), []]
        ], funargs => funargs[0].apply(null, funargs[1]))))
        .spread((motion, smoke, door, window) => {
            expect(motion[0]).to.be.instanceof(Device);
            expect(smoke[0]).to.be.instanceof(Device);
            expect(door[0]).to.be.instanceof(Device);
            expect(window[0]).to.be.instanceof(Device);
            motionDevice = motion[0];
            smokeDevice = smoke[0];
            doorDevice = door[0];
            windowDevice = window[0];
        });
    });

    after(function () {
        events.settleTime = 300;
        return events.settle().then(() => Promise.all([
            Device.unregisterProtocol(context, 'dumb'),
            dropDbs(context),
        ]));
    });

    beforeEach(function () {
        evts = [];
        alarmError = null;
        alarmErrorExpected = false;
        registeredSystem = null;
        unregisteredSystem = null;
    });

    afterEach(function () {
        if (alarmError && !alarmErrorExpected) {
            throw alarmError;
        }
    });

    function waitForEventCount(n) {
        return waitUntil(() => evts.length === n);
    }

    it('should listen to events', function () {
        Alarm.on('error', error => {
            alarmError = error;
        });
        Alarm.on('register', system => {
            registeredSystem = system;
        });
        Alarm.on('unregister', system => {
            unregisteredSystem = system;
        });
    });

    it('should fail to get non existent systems', function () {
        expect(() => Alarm.System.get('foo')).to.throw(/not found/i);
    });

    it('should create an alarm system', function () {
        return context.withTransactionContext(context => {
            return Alarm.System.create(context);
        })
        .tap(() => waitForEventCount(1))
        .then(system => {
            expect(system).to.be.instanceof(Alarm.System);
            expect(system._doc._id).to.be.a('string');
            createdSystemId = system._doc._id;
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('create');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}`);
            expect(_.last(evts).resource).to.deep.equal(system.toJSON());
            expect(registeredSystem).to.equal(system);
        });
    });

    it('should get an existing system', function () {
        const system = Alarm.System.get(createdSystemId);
        expect(system).to.be.instanceof(Alarm.System);
    });

    it('should edit an alarm system', function () {
        const system = Alarm.System.get(createdSystemId);
        return Promise.all([
            waitForEventCount(1),
            context.withTransactionContext(context => {
                return system.update(context, { name: 'Test System' });
            }),
        ]).then(() => {
            const evt = _.last(evts);
            expect(evt).to.be.an('object');
            expect(evt.type).to.equal('update');
            expect(evt.path).to.equal(`/alarms/${createdSystemId}`);
            expect(evt.resource.name).to.equal('Test System');
        });
    });

    it('arming an empty system should be a no-op', function () {
        const system = Alarm.System.get(createdSystemId);
        return system.arm(context, 'armed').then(() => {
            expect(system._doc.status).to.equal('disarmed');
        });
    });

    let createdAccessId;
    it('should add an access PIN to the alarm system', function () {
        const system = Alarm.System.get(createdSystemId);
        return Alarm.Access.create(context, system, {
            pin: '0000',
            name: 'Test 1',
        }).tap(() => waitForEventCount(1)).then(access => {
            expect(access).to.be.instanceof(Alarm.Access);
            expect(access._doc._id).to.be.a('string');
            createdAccessId = access._doc._id;
            const evt = _.last(evts);
            expect(evt).to.be.an('object');
            expect(evt.type).to.equal('create');
            expect(evt.path).to.equal(`/alarms/${createdSystemId}/access/${createdAccessId}`);
            expect(evt.resource).to.deep.equal(access.toJSON());
        });
    });

    it('should get an existing access PIN', function () {
        const access = Alarm.Access.get(createdSystemId, createdAccessId);
        expect(access).to.be.instanceof(Alarm.Access);
    });

    it('should not add a duplicate PIN', function () {
        const system = Alarm.System.get(createdSystemId);
        return Alarm.Access.create(context, system, {
            pin: '0000',
            name: 'Test 2',
        }).then(unexpected, error => {
            expect(error.message).to.match(/invalid pin/i);
        });
    });

    it('should create another two access PINs for the alarm system', function () {
        const system = Alarm.System.get(createdSystemId);
        return Promise.mapSeries([
            { pin: '5432', name: 'Test 3-1' },
            { pin: '9999', name: 'Test 3-2' },
        ], doc => Alarm.Access.create(context, system, doc))
        .spread((a1, a2) => {
            expect(system.access).to.be.an('object');
            expect(system.access).to.have.all.keys(createdAccessId, a1._doc._id, a2._doc._id);
        })
        .then(() => waitForEventCount(2));
    });

    it('should update an access PIN', function () {
        const access = Alarm.Access.get(createdSystemId, createdAccessId);
        return access.update(context, { pin: '1234' }).then(() => {
            expect(access._doc.pin).to.equal('1234');
        })
        .then(() => waitForEventCount(1));
    });

    it('should not generate a token for an invalid pin', function () {
        const system = Alarm.System.get(createdSystemId);
        return system.generateToken(context, '0000').then(unexpected, error => {
            expect(error.message).to.match(/invalid pin/i);
        });
    });

    let token;
    it('should generate a token for a pin', function () {
        const system = Alarm.System.get(createdSystemId);
        return system.generateToken(context, '1234').then(token_ => {
            token = token_;
        });
    });

    it('should validate a token', function () {
        const system = Alarm.System.get(createdSystemId);
        return system.verifyToken(token);
    });

    it('should not validate a token twice', function () {
        const system = Alarm.System.get(createdSystemId);
        return system.verifyToken(token).then(unexpected, error => {
            expect(error.message).to.match(/invalid token/i);
        });
    });

    it('should delete an access PIN', function () {
        const access = Alarm.Access.get(createdSystemId, createdAccessId);
        const system = access.system;
        return access.delete(context).then(() => {
            expect(Object.keys(system.access).length).to.equal(2);
        })
        .then(() => waitForEventCount(1));
    });

    it('should fail to get non existent sectors', function () {
        expect(() => Alarm.Sector.get(createdSystemId, 'foo')).to.throw(/not found/i);
    });

    it('should create a sector', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Sector.create(context, system);
        })
        .tap(() => waitForEventCount(1))
        .then(sector => {
            expect(sector).to.be.instanceof(Alarm.Sector);
            expect(sector._doc._id).to.be.a('string');
            createdSectorId = sector._doc._id;
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('create');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/sectors/${createdSectorId}`);
            expect(_.last(evts).resource).to.deep.equal(sector.toJSON());
        });
    });

    it('should get a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        expect(sector).to.be.instanceof(Alarm.Sector);
        expect(sector._doc._id).to.equal(createdSectorId);
    });

    it('should update a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        return sector.update(context, { name: 'Test Sector' })
        .tap(() => waitForEventCount(1))
        .then(updated => {
            expect(updated).to.be.instanceof(Alarm.Sector);
            expect(updated._doc._id).to.equal(createdSectorId);
            expect(updated._doc.name).to.equal('Test Sector');
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/sectors/${createdSectorId}`);
            expect(_.last(evts).resource).to.deep.equal(sector.toJSON());
        });
    });

    it('should fail to get non existent zones', function () {
        expect(() => Alarm.Zone.get(createdSystemId, 'foo')).to.throw(/not found/i);
    });

    it('should fail to create a zone on a missing device', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system);
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.message).to.match(/not found/i);
        });
    });

    it('should fail to create a zone with the wrong capability', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system, {
                device: motionDevice._id,
            });
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.output.payload.code).to.equal('ZONE_BAD_CAP');
        });
    });

    it('should fail to create a zone with an invalid normal state', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system, {
                device: motionDevice._id,
                capability: 'motion',
            });
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.output.payload.code).to.equal('ZONE_BAD_CAP');
        });
    });

    it('should create a zone', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system, {
                device: motionDevice._id,
                capability: 'motion',
                normalState: 'idle',
            });
        })
        .tap(() => waitForEventCount(1))
        .then(zone => {
            expect(zone).to.be.instanceof(Alarm.Zone);
            expect(zone._doc._id).to.be.a('string');
            createdZoneId = zone._doc._id;
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('create');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal(zone.toJSON());
        });
    });

    it('should fail to create a duplicate zone', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system, {
                device: motionDevice._id,
                capability: 'motion',
                normalState: 'idle',
            });
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.output.payload.code).to.equal('ZONE_DUPLICATE');
        });
    });

    it('should get a zone', function () {
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        expect(zone).to.be.instanceof(Alarm.Zone);
        expect(zone._doc._id).to.equal(createdZoneId);
    });

    it('should trip when the device cap changes', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'detected')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal({ sensorStatus: 'tripped' });
        });
    });

    it('should untrip when the device cap changes', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'idle')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal({ sensorStatus: 'normal' });
        });
    });

    it('should trouble when the device fails', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'FAILED')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal({ sensorStatus: 'trouble' });
        });
    });

    it('should untrouble when the device is not failed', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'ACTIVE')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal({ sensorStatus: 'normal' });
        });
    });

    it('should ignore cap updates while troubled and be in the correct state after', function () {
        this.slow(600);

        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'FAILED')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            return context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'detected')
                .then(() => motionDevice.save(context)));
        }).then(() => Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'ACTIVE')
                .then(() => motionDevice.save(context))),
            waitForEventCount(5),
        ])).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal({ sensorStatus: 'tripped' });
        });
    });

    it('should update a zone', function () {
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        return context.withTransactionContext(context => {
            return zone.update(context, {
                exitDelay: 1
            });
        }).then(() => {
            expect(zone._doc.exitDelay).to.equal(1);
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
            expect(_.last(evts).resource).to.deep.equal(zone.toJSON());
        });
    });

    it('should fail to exclude a zone that does not belong to a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        expect(() => sector.exclude(context, zone)).to.throw(/sector does not include that zone/i);
    });

    it('should add a zone to a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        return context.withTransactionContext(context => {
            return sector.include(context, zone);
        }).then(() => {
            expect(_.includes(sector._doc.zones, createdZoneId)).to.equal(true);
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/sectors/${createdSectorId}`);
            expect(_.last(evts).resource).to.deep.equal({ zones: [createdZoneId] });
        });
    });

    it('should no-op if adding the same zone to a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        return Promise.try(() => sector.include(context, zone)).then(() => {
            expect(sector._doc.zones).to.deep.equal([createdZoneId]);
            expect(evts).to.be.empty;
        });
    });

    it('should stop an alarm system', function () {
        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        expect(unregisteredSystem).to.equal(system);
    });

    it('should start all alarm systems', function () {
        return Alarm.System.startAll(context).then(() => {
            expect(registeredSystem).to.be.instanceof(Alarm.System);
            expect(registeredSystem._doc._id).to.equal(createdSystemId);
        });
    });

    it('should sync a zone to a changed device: failure', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'FAILED')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('trouble');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should sync a zone to a changed device: active (tripped)', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'ACTIVE')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('tripped');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should sync a zone to a changed device: normal', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'idle')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('normal');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should sync a zone to a changed device: failure', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'FAILED')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('trouble');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should sync a zone to a changed device: active (normal)', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'ACTIVE')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('normal');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should sync a zone to a changed device: tripped', function () {
        this.slow(600);

        const system = Alarm.System.get(createdSystemId);
        system.stop(context);
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'detected')
                .then(() => motionDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => {
            return Alarm.System.startAll(context).then(() => {
                const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
                expect(zone._doc.sensorStatus).to.equal('tripped');
            });
        }).then(() => waitForEventCount(2));
    });

    it('should exclude a zone from a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        return context.withTransactionContext(context => {
            return sector.exclude(context, zone);
        }).then(() => {
            expect(sector._doc.zones).to.deep.equal([]);
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/sectors/${createdSectorId}`);
            expect(_.last(evts).resource).to.deep.equal({ zones: [] });
        });
    });

    it('should initialize timeouts and error on an invalid timeout', function () {
        alarmErrorExpected = true;
        const system = Alarm.System.get(createdSystemId);
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        system.stop(context);
        zone._doc.delayUntil = new Date().toISOString();
        return system.start(context).then(() => waitUntil(() => alarmError !== null)).then(() => {
            expect(alarmError).to.be.instanceof(Alarm.Zone.BadStateError);
            expect(alarmError.zone).to.equal(zone);
            expect(alarmError.message).to.match(/timeout/);
        });
    });

    it('should delete a zone', function () {
        const zone = Alarm.Zone.get(createdSystemId, createdZoneId);
        return context.withTransactionContext(context => {
            return zone.delete(context);
        }).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('delete');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/zones/${createdZoneId}`);
        });
    });

    it('should fail to create a 24/7 zone on a tripped capability', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return Alarm.Zone.create(context, system, {
                device: motionDevice._id,
                is24hours: true,
                capability: 'motion',
                normalState: 'idle',
            });
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.output.payload.code).to.equal('ZONE_BAD_STATE');
        });
    });

    it('should delete a sector', function () {
        const sector = Alarm.Sector.get(createdSystemId, createdSectorId);
        return context.withTransactionContext(context => {
            return sector.delete(context);
        }).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('delete');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}/sectors/${createdSectorId}`);
        });
    });

    it('should delete an alarm system', function () {
        const system = Alarm.System.get(createdSystemId);
        return context.withTransactionContext(context => {
            return system.delete(context);
        }).then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('delete');
            expect(_.last(evts).path).to.equal(`/alarms/${createdSystemId}`);
            expect(unregisteredSystem).to.equal(system);

            let result = {};
            return context.database.query(`
            SELECT (SELECT COUNT(*) FROM alarms) AS alarms,
                   (SELECT COUNT(*) FROM zones) AS zones,
                   (SELECT COUNT(*) FROM sectors) AS sectors
            `, [], row => result = row)
            .then(() => {
                expect(result.alarms).to.equal(0);
                expect(result.zones).to.equal(0);
                expect(result.sectors).to.equal(0);
            });
        });
    });

    let system2, zones = {}, sectors = {};

    it('should create another complete alarm system', function () {
        this.slow(500);

        return context.withTransactionContext(context => {
            motionDevice.updateCap(context, 'motion', 'idle');
            return motionDevice.save(context)
            .then(() => Alarm.System.create(context))
            .then(system => {
                expect(system).to.be.instanceof(Alarm.System);
                system2 = system;

                return [Alarm.Zone.create(context, system, {
                    device: motionDevice._id,
                    capability: 'motion',
                    normalState: 'idle',
                }), Alarm.Zone.create(context, system, {
                    device: smokeDevice._id,
                    capability: 'smoke',
                    normalState: 'idle',
                    is24hours: true,
                }), Alarm.Zone.create(context, system, {
                    device: doorDevice._id,
                    capability: 'door',
                    normalState: 'closed',
                    exitDelay: 0.1,
                    entryDelay: 0.1,
                    perimeter: true,
                }), Alarm.Zone.create(context, system, {
                    device: windowDevice._id,
                    capability: 'window',
                    normalState: 'closed',
                    perimeter: true,
                }), Alarm.Sector.create(context, system),
                    Alarm.Sector.create(context, system),
                    Alarm.Sector.create(context, system)];
            }).all().then(results => {
                let i = 0;

                expect(results[i]).to.be.instanceof(Alarm.Zone);
                zones.motion = results[i++];
                expect(results[i]).to.be.instanceof(Alarm.Zone);
                zones.smoke = results[i++];
                expect(results[i]).to.be.instanceof(Alarm.Zone);
                zones.door = results[i++];
                expect(results[i]).to.be.instanceof(Alarm.Zone);
                zones.window = results[i++];

                expect(results[i]).to.be.instanceof(Alarm.Sector);
                sectors.a = results[i++];
                expect(results[i]).to.be.instanceof(Alarm.Sector);
                sectors.b = results[i++];
                expect(results[i]).to.be.instanceof(Alarm.Sector);
                sectors.c = results[i++];

                return [
                    sectors.a.include(context, zones.motion),
                    sectors.b.include(context, zones.motion),
                    sectors.b.include(context, zones.door),
                    sectors.c.include(context, zones.window),
                ];
            }).all();
        });
    });

    it('should arm 24/7 zones by default', function () {
        expect(zones.smoke._doc.zoneStatus).to.equal('armed');
    });

    it('should not arm with an invalid status', function () {
        expect(() => system2.arm(context, 'foo')).to.throw(/Invalid status/);
    });

    it('should not arm an alarm system with a tripped zone', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'detected')
                .then(() => motionDevice.save(context))),
            waitForEventCount(2),
        ]).then(() => {
            expect(zones.motion._doc.sensorStatus).to.equal('tripped');
            return context.withTransactionContext(context => system2.arm(context, 'armed'));
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.message).to.match(/zones cannot be armed/);
            expect(error.output.payload.details.errors).to.deep.equal([{
                error: 'sensorStatus',
                zone: zones.motion.toJSON()
            }]);
        });
    });

    it('should create a conflict between two simultaneous arm requests', function () {
        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'armed'))
            .then(unexpected, error => {
                expect(error.message).to.match(/zones cannot be armed/);
            }),
            context.withTransactionContext(context => system2.arm(context, 'armed'))
            .then(unexpected, error => {
                expect(error.isBoom).to.equal(true);
                expect(error.message).to.match(/Another user is arming the system/i);
            }),
        ]);
    });

    it('should arm with bypass', function () {
        this.slow(400);

        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'armed', { bypassed: [ zones.motion._doc._id ] })),
            waitForEventCount(2),
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('armed');
            const zone = _.find(evts[0].resource.zones, { _id: zones.door._doc._id });
            expect(zone.zoneStatus).to.equal('exitDelay');
            expect(evts[1].type).to.equal('update');
            expect(evts[1].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[1].resource.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('armed');
            expect(zones.motion._doc.zoneStatus).to.equal('bypassed');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
        });
    });

    it('should disarm', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'disarmed')),
            waitForEventCount(1),
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('disarmed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.window._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
            expect(zones.smoke._doc.zoneStatus).to.equal('armed'); // 24h zone
        });
    });

    it('should queue and flush events received during arming', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'armed'))
                .then(unexpected, error => true),
            Promise.all([
                context.withTransactionContext(context =>
                    motionDevice.updateCap(context, 'motion', 'idle')
                    .then(() => motionDevice.save(context))),
                waitForEventCount(2),
            ]).then(() => {
                expect(zones.motion._doc.sensorStatus).to.equal('normal');
            }),
        ]);
    });

    it('should arm and use exit delays', function () {
        this.slow(400);

        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'armed')),
            waitForEventCount(2)
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('armed');
            expect(evts[1].type).to.equal('update');
            expect(evts[1].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[1].resource.zoneStatus).to.equal('armed');
            expect(sectors.a._doc.armed).to.equal(true);
            expect(sectors.b._doc.armed).to.equal(true);
            expect(sectors.c._doc.armed).to.equal(true);
        });
    });

    it('should not delete a zone when the system is armed', function () {
        return context.withTransactionContext(context => zones.door.delete(context))
        .then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.output.payload.code).to.equal('ALARM_STATUS_ERROR');
        });
    });

    it('should disarm', function () {
        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('disarmed');
            expect(sectors.a._doc.armed).to.equal(false);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(false);
        });
    });

    it('should arm and skip exit delays', function () {
        this.slow(800);

        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'armed', { skipExitDelays: true })),
            waitForEventCount(1)
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('armed');
            expect(_.find(evts[0].resource.zones, { _id: zones.door._doc._id }).zoneStatus).to.equal('armed');
        }).finally(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(1)
        ]));
    });

    it('should default to skipped exit delays when perimeter arming', function () {
        this.slow(800);

        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'perimeter')),
            waitForEventCount(1)
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('perimeter');
            expect(_.find(evts[0].resource.zones, { _id: zones.door._doc._id }).zoneStatus).to.equal('armed');
        }).finally(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(1)
        ]));
    });

    it('should sector arm', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'sector', {
                    sectors: [{ _id: sectors.c._doc._id, armed: true }]
                })),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('sector');
            expect(sectors.a._doc.armed).to.equal(false);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(true);
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        });
    });

    it('should sector arm a second sector', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'sector', {
                    sectors: [{ _id: sectors.a._doc._id, armed: true }]
                })),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('sector');
            expect(sectors.a._doc.armed).to.equal(true);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(true);
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        });
    });

    it('should arm if all sectors are armed (with bypass)', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'sector', {
                    sectors: [{ _id: sectors.b._doc._id, armed: true }],
                    bypassed: [ zones.door._doc._id ],
                })),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('armed');
            expect(sectors.a._doc.armed).to.equal(true);
            expect(sectors.b._doc.armed).to.equal(true);
            expect(sectors.c._doc.armed).to.equal(true);
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('bypassed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        });
    });

    it('should sector disarm', function () {
        this.slow(400);

        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'sector', {
                    sectors: [{ _id: sectors.b._doc._id, armed: false }],
                })),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('sector');
            expect(sectors.a._doc.armed).to.equal(true);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(true);
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        });
    });

    it('should disarm if all sectors are disarmed', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'sector', {
                    sectors: [
                        { _id: sectors.c._doc._id, armed: false },
                        { _id: sectors.a._doc._id, armed: false },
                    ],
                })),
            waitForEventCount(1),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('disarmed');
            expect(sectors.a._doc.armed).to.equal(false);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(false);
            expect(zones.window._doc.zoneStatus).to.equal('disarmed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        });
    });

    it('should disarm sector armed', function () {
        return context.withTransactionContext(context =>
            system2.arm(context, 'sector', {
                sectors: [{ _id: sectors.c._doc._id, armed: true }]
            }))
        .then(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(2),
        ])).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('disarmed');
            expect(sectors.a._doc.armed).to.equal(false);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(false);
            expect(zones.window._doc.zoneStatus).to.equal('disarmed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        });
    });

    it('should disarm exit delayed zones', function () {
        return context.withTransactionContext(context => system2.arm(context, 'armed'))
        .then(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(2),
        ])).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}`);
            expect(_.last(evts).resource.status).to.equal('disarmed');
            expect(sectors.a._doc.armed).to.equal(false);
            expect(sectors.b._doc.armed).to.equal(false);
            expect(sectors.c._doc.armed).to.equal(false);
            expect(zones.window._doc.zoneStatus).to.equal('disarmed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        });
    });

    it('should alarm on armed zones when tripped', function () {
        this.slow(600);

        return context.withTransactionContext(context =>
            system2.arm(context, 'armed', { bypassed: [ zones.door._doc._id ] }))
        .then(() => Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'detected')
                .then(() => motionDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.motion._doc._id}`);
            expect(_.last(evts).resource.zoneStatus).to.equal('alarm');
            return context.withTransactionContext(context =>
                motionDevice.updateCap(context, 'motion', 'idle')
                .then(() => motionDevice.save(context)));
        }).then(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(7)
        ]));
    });

    it('should alarm on 24/7 zones when system disarmed', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                smokeDevice.updateCap(context, 'smoke', 'detected')
                .then(() => smokeDevice.save(context))),
            waitForEventCount(3),
        ]).then(() => {
            expect(_.last(evts).type).to.equal('update');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(_.last(evts).resource.zoneStatus).to.equal('alarm');
        });
    });

    it('should suspend 24/7 zones, and re-alarm after timeout', function () {
        this.slow(600);

        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'suspend', {
                    zones: [ zones.smoke._doc._id ],
                    timeout: 0.1,
                })),
            waitForEventCount(2),
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[0].resource.zoneStatus).to.equal('suspended');
            expect(evts[1].type).to.equal('update');
            expect(evts[1].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[1].resource.zoneStatus).to.equal('alarm');
        });
    });

    it('should reset 24/7 zones on disarm if zone is normal', function () {
        this.slow(600);

        return Promise.all([
            context.withTransactionContext(context =>
                smokeDevice.updateCap(context, 'smoke', 'idle')
                .then(() => smokeDevice.save(context))),
            waitForEventCount(1),
        ]).then(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(3),
        ])).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('armed');
        });
    });

    it('should automatically re-arm 24/7 suspended if specified', function () {
        this.slow(600);

        expect(zones.smoke._doc.zoneStatus).to.equal('armed');
        return Promise.all([
            context.withTransactionContext(context =>
                smokeDevice.updateCap(context, 'smoke', 'detected')
                .then(() => smokeDevice.save(context))),
            waitForEventCount(3),
        ]).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('alarm');
            return Promise.all([
                context.withTransactionContext(context =>
                    system2.arm(context, 'suspend', {
                        zones: [ zones.smoke._doc._id ],
                        timeout: 5,
                        rearm: true,
                    })),
                waitForEventCount(4),
            ]);
        }).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('suspended');
            return Promise.all([
                context.withTransactionContext(context =>
                    smokeDevice.updateCap(context, 'smoke', 'idle')
                    .then(() => smokeDevice.save(context))),
                waitForEventCount(7),
            ]);
        }).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('armed');
            expect(evts[0].path).to.equal(`/devices/${smokeDevice._id}/caps/smoke`);
            expect(evts[0].resource.value).to.equal('detected');
            expect(evts[1].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[1].resource.sensorStatus).to.equal('tripped');
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[2].resource.zoneStatus).to.equal('alarm');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('suspended');
            expect(evts[4].path).to.equal(`/devices/${smokeDevice._id}/caps/smoke`);
            expect(evts[4].resource.value).to.equal('idle');
            expect(evts[5].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[5].resource.sensorStatus).to.equal('normal');
            expect(evts[6].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[6].resource.zoneStatus).to.equal('armed');
        });
    });

    it('should automatically re-arm 24/7 suspended at suspend timeout', function () {
        this.slow(1400);

        expect(zones.smoke._doc.zoneStatus).to.equal('armed');
        return Promise.all([
            context.withTransactionContext(context =>
                smokeDevice.updateCap(context, 'smoke', 'detected')
                .then(() => smokeDevice.save(context))),
            waitForEventCount(3),
        ]).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('alarm');
            return Promise.all([
                context.withTransactionContext(context =>
                    system2.arm(context, 'suspend', {
                        zones: [ zones.smoke._doc._id ],
                        timeout: 0.5,
                    })),
                waitForEventCount(4),
            ]);
        }).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('suspended');
            return Promise.all([
                context.withTransactionContext(context =>
                    smokeDevice.updateCap(context, 'smoke', 'idle')
                    .then(() => smokeDevice.save(context))),
                waitForEventCount(7),
            ]);
        }).then(() => {
            expect(zones.smoke._doc.zoneStatus).to.equal('armed');
            expect(evts[0].path).to.equal(`/devices/${smokeDevice._id}/caps/smoke`);
            expect(evts[0].resource.value).to.equal('detected');
            expect(evts[1].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[1].resource.sensorStatus).to.equal('tripped');
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[2].resource.zoneStatus).to.equal('alarm');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('suspended');
            expect(evts[4].path).to.equal(`/devices/${smokeDevice._id}/caps/smoke`);
            expect(evts[4].resource.value).to.equal('idle');
            expect(evts[5].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[5].resource.sensorStatus).to.equal('normal');
            expect(evts[6].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.smoke._doc._id}`);
            expect(evts[6].resource.zoneStatus).to.equal('armed');
        });
    });

    it('should alarm for trouble during an exit delay', function () {
        this.slow(600);

        const origDelay = zones.door._doc.exitDelay;
        zones.door._doc.exitDelay = 10;
        return context.withTransactionContext(context => system2.arm(context, 'armed'))
        .then(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateStatus(context, 'FAILED')
                .then(() => doorDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => {
            expect(evts[2].type).to.equal('update');
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[2].resource.sensorStatus).to.equal('trouble');
            expect(evts[3].type).to.equal('update');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('alarm');
            expect(zones.door._doc.zoneStatus).to.equal('alarm');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        }).finally(() => {
            zones.door._doc.exitDelay = origDelay;
            return Promise.all([
                context.withTransactionContext(context =>
                    doorDevice.updateStatus(context, 'ACTIVE')
                    .then(() => doorDevice.save(context))),
                context.withTransactionContext(context => system2.arm(context, 'disarmed')),
                waitForEventCount(7)
            ]);
        });
    });

    it('should alarm at the end of an exit delay', function () {
        this.slow(1300);

        const origDelay = zones.door._doc.exitDelay;
        zones.door._doc.exitDelay = 0.5;
        return context.withTransactionContext(context => system2.arm(context, 'armed')).then(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'open')
                .then(() => doorDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => {
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[2].resource).to.deep.equal({ sensorStatus: 'tripped' });
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[3].resource).to.deep.equal({ zoneStatus: 'alarm' });
        }).finally(() => {
            zones.door._doc.exitDelay = origDelay;
            return Promise.all([
                context.withTransactionContext(context =>
                    doorDevice.updateCap(context, 'door', 'closed')
                    .then(() => doorDevice.save(context))),
                context.withTransactionContext(context => system2.arm(context, 'disarmed')),
                waitForEventCount(7),
            ]);
        });
    });

    it('should remove an exit delay', function () {
        return context.withTransactionContext(context => {
            return zones.door.update(context, { exitDelay: 0 });
        }).then(() => {
            expect(zones.door._doc.exitDelay).to.equal(0);
        });
    });

    it('should alarm for trouble on an armed zone', function () {
        this.slow(500);

        return context.withTransactionContext(context => system2.arm(context, 'armed')).then(() => Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'FAILED')
                .then(() => motionDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => {
            expect(evts[2].type).to.equal('update');
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.motion._doc._id}`);
            expect(evts[2].resource.sensorStatus).to.equal('trouble');
            expect(evts[3].type).to.equal('update');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.motion._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('alarm');
            expect(zones.door._doc.zoneStatus).to.equal('armed');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.motion._doc.zoneStatus).to.equal('alarm');
        }).finally(() => Promise.all([
            context.withTransactionContext(context =>
                motionDevice.updateStatus(context, 'ACTIVE')
                .then(() => motionDevice.save(context))),
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(7),
        ]));
    });

    it('should alarm after an entry delay', function () {
        this.slow(1300);

        return context.withTransactionContext(context => system2.arm(context, 'armed')).then(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'open')
                .then(() => doorDevice.save(context))),
            waitForEventCount(5),
        ])).then(() => {
            expect(evts[3].type).to.equal('update');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('entryDelay');
            expect(evts[4].type).to.equal('update');
            expect(evts[4].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[4].resource.zoneStatus).to.equal('alarm');
            expect(zones.door._doc.zoneStatus).to.equal('alarm');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        }).finally(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'closed')
                .then(() => doorDevice.save(context))),
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(8),
        ]));
    });

    it('should disarm during an entry delay', function () {
        this.slow(600);

        zones.door._doc.entryDelay = 10;
        return context.withTransactionContext(context => system2.arm(context, 'armed')).then(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'open')
                .then(() => doorDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(5),
        ])).then(() => {
            expect(evts[3].type).to.equal('update');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[3].resource.zoneStatus).to.equal('entryDelay');
            expect(evts[4].type).to.equal('update');
            expect(evts[4].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[4].resource.status).to.equal('disarmed');
            expect(zones.door._doc.zoneStatus).to.equal('disarmed');
            expect(zones.window._doc.zoneStatus).to.equal('disarmed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        }).finally(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'closed')
                .then(() => doorDevice.save(context))),
            waitForEventCount(7),
        ]));
    });

    it('should alarm for trouble during an entry delay', function () {
        this.slow(600);

        return Promise.all([
            context.withTransactionContext(context => system2.arm(context, 'armed')),
            waitForEventCount(1)
        ]).then(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'open')
                .then(() => doorDevice.save(context))),
            waitForEventCount(4),
        ])).then(() => {
            expect(evts[2].type).to.equal('update');
            expect(evts[2].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[2].resource).to.deep.equal({ sensorStatus: 'tripped' });
            expect(evts[3].type).to.equal('update');
            expect(evts[3].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[3].resource).to.deep.equal({ zoneStatus: 'entryDelay' });
            return Promise.all([
                context.withTransactionContext(context =>
                    doorDevice.updateStatus(context, 'FAILED')
                    .then(() => doorDevice.save(context))),
                waitForEventCount(7),
            ]);
        }).then(() => {
            expect(evts[5].type).to.equal('update');
            expect(evts[5].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[5].resource.sensorStatus).to.equal('trouble');
            expect(evts[6].type).to.equal('update');
            expect(evts[6].path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.door._doc._id}`);
            expect(evts[6].resource.zoneStatus).to.equal('alarm');
            expect(zones.door._doc.zoneStatus).to.equal('alarm');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.motion._doc.zoneStatus).to.equal('armed');
        }).finally(() => Promise.all([
            context.withTransactionContext(context =>
                doorDevice.updateCap(context, 'door', 'closed')
                .then(() => doorDevice.updateStatus(context, 'ACTIVE'))
                .then(() => doorDevice.save(context))),
            context.withTransactionContext(context => system2.arm(context, 'disarmed')),
            waitForEventCount(11)
        ]));
    });

    it('should perimeter arm with bypass', function () {
        return Promise.all([
            context.withTransactionContext(context =>
                system2.arm(context, 'perimeter', { bypassed: [ zones.door._doc._id ] })),
            waitForEventCount(1),
        ]).then(() => {
            expect(evts[0].type).to.equal('update');
            expect(evts[0].path).to.equal(`/alarms/${system2._doc._id}`);
            expect(evts[0].resource.status).to.equal('perimeter');
            expect(zones.window._doc.zoneStatus).to.equal('armed');
            expect(zones.door._doc.zoneStatus).to.equal('bypassed');
            expect(zones.motion._doc.zoneStatus).to.equal('disarmed');
        });
    });

    it('should delete a disarmed zone when its device is removed even if the system is armed', function () {
        return context.withTransactionContext(context => motionDevice.remove(context))
        .then(() => waitForEventCount(2))
        .then(() => {
            expect(_.last(evts)).to.be.an('object');
            expect(_.last(evts).type).to.equal('delete');
            expect(_.last(evts).path).to.equal(`/alarms/${system2._doc._id}/zones/${zones.motion._doc._id}`);
            return Alarm.Zone.get(system2._doc._id, zones.motion._doc._id);
        }).then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.message).to.match(/not found/i);
        });
    });

    it('should not remove a device if its zones are armed', function () {
        return context.withTransactionContext(context => windowDevice.remove(context))
        .then(unexpected, error => {
            expect(error.isBoom).to.equal(true);
            expect(error.message).to.match(/armed/i);
        });
    });

    it('should get a list of armed zones', function () {
        return Device.getArmedZoneIds(context, windowDevice._doc._id).then(ids => {
            expect(ids).to.deep.equal([zones.window._doc._id]);
        });
    });

    it('should fail to delete an armed alarm system', function () {
        expect(() => system2.delete(context)).to.throw(/action not allowed/);
    });

    it('should delete a complete alarm system', function () {
        return system2.arm(context, 'disarmed')
        .then(() => context.withTransactionContext(context => system2.delete(context)))
        .then(() => {
            let result = {};
            return context.database.query(`
            SELECT (SELECT COUNT(*) FROM alarms) AS alarms,
                   (SELECT COUNT(*) FROM zones) AS zones,
                   (SELECT COUNT(*) FROM sectors) AS sectors
            `, [], row => result = row)
            .then(() => {
                expect(result.alarms).to.equal(0);
                expect(result.zones).to.equal(0);
                expect(result.sectors).to.equal(0);
            });
        });
    });
});
