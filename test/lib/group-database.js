/*global describe, it, before, after*/
'use strict';

const _ = require('lodash');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const GroupDatabase = require('../../lib/group-database');
const permission = require('../../lib/permission');
const ObjectID = require('bson-objectid');

const app = {};
app.context = context.createAppContext(app);

const DEVICE_ID = _.map(_.range(1, 10), () => new ObjectID().toString());
const GROUP_ID = new ObjectID().toString();
const USER_ID = new ObjectID().toString();
const GATEWAY_ID = new ObjectID().toString();
const PERMISSIONS = _.map(DEVICE_ID, id => ({
    who: GROUP_ID,
    whoType: 'group',
    resource: id,
    type: 'device',
    status: 'ACTIVE',
    read: true, write: true, manage: true, share: true
}));

PERMISSIONS.push({
    who: GROUP_ID,
    whoType: 'group',
    resource: GATEWAY_ID,
    type: 'gateway',
    status: 'ACTIVE',
    read: true, write: true, manage: true, share: true
});

const GROUPS = [{
    _id: GROUP_ID,
    members: [
        { who: USER_ID, whoType: 'user' },
        { who: GATEWAY_ID, whoType: 'gateway' },
    ],
    children: [],
}];

const SDK = {};

describe('Group database', function () {
    this.slow(150);

    let groupDb = null;

    const context = Object.create(app.context, {
        sdk: { configurable: false, enumerable: false, value: SDK },
        gatewayId: { configurable: false, enumerable: true, value: GATEWAY_ID },
    });

    before(function () {
        return dropDbs(context)
        .then(() => permission.views.update(context))
        .then(() => permission.upsertMany(context, PERMISSIONS));
    });

    after(function () {
        return dropDbs(context);
    });

    it('should create a GroupDatabase instance', function () {
        groupDb = new GroupDatabase(context);
    });

    it('should add groups', function () {
        return groupDb.upsertGroups(context, GROUPS);
    });

    it('should get groups by ID', function () {
        return groupDb.getGroups([GROUP_ID])
        .then(results => {
            expect(results.length).to.equal(1);
            expect(results[0]).to.deep.equal(GROUPS[0]);
        });
    });

    it('should get groups by user ID', function () {
        return groupDb.getMemberGroups({ who: USER_ID, whoType: 'user' })
        .then(results => {
            expect(results.length).to.equal(1);
            expect(results[0]).to.deep.equal(GROUPS[0]);
        });
    });

    it('should get permissions for a specific resource', function () {
        return groupDb.getPermissions([GROUP_ID], 'group', DEVICE_ID[0], 'device')
        .then(results => {
            expect(results.length).to.equal(1);
            expect(_.omit(results[0], '_id')).to.deep.equal(PERMISSIONS[0]);
        });
    });

    it('should get permissions by type', function () {
        return groupDb.getPermissionsByOwner([GROUP_ID], 'group', 'gateway')
        .then(results => {
            expect(results.length).to.equal(1);
            expect(_.omit(results[0], '_id', 'isOwnGateway')).to.deep.equal(PERMISSIONS[PERMISSIONS.length - 1]);
        });
    });

    it('should get all permissions by "who"', function () {
        return groupDb.getPermissionsByOwner([GROUP_ID], 'group')
        .then(results => {
            expect(results.length).to.equal(PERMISSIONS.length);
        });
    });

    it('should add a member to the group', function () {
        return groupDb.addGroupMember(context, GROUP_ID, { who: 'test_device', whoType: 'device' })
        .then(() => groupDb.getGroups([GROUP_ID]))
        .then(groups => {
            expect(groups[0].members.length).to.equal(3);
            expect(groups[0].members).to.include({ who: 'test_device', whoType: 'device' });
        });
    });

    it('should not add the same member twice', function () {
        return groupDb.addGroupMember(context, GROUP_ID, { who: 'test_device', whoType: 'device' })
        .then(() => groupDb.getGroups([GROUP_ID]))
        .then(groups => {
            expect(groups[0].members.length).to.equal(3);
        });
    });
});
