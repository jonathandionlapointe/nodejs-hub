/*global describe, it, before*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const Database = require('../../lib/sqldb');
const expect = require('chai').expect;

describe('SQL Database', function () {
    this.slow(150);

    let db;

    it('should create/open a database', function () {
        db = new Database(config.db.url, config.db.pool);
    });

    it('should initialize the database', function () {
        return db.init();
    });

    it('should execute queries', function () {
        let result = null;
        return db.query('SELECT 1', [], row => result = row)
        .then(() => {
            expect(result).to.deep.equal({ '1': 1 });
        });
    });

    describe('Couch compatibility layer', function () {
        let collection;
        const DOC = { type: 'foo', foo: 42 };
        const DOC2 = { xyz: 'blah' };
        const MANY_DOCS = [DOC, DOC2, {}];

        before(function () {
            return db.query('DROP TABLE IF EXISTS test');
        });

        it('should get a collection', function () {
            collection = db.collection('test');
        });

        it('should check if the collection exists (false)', function () {
            return collection.exists().then(exists => {
                expect(exists).to.equal(false);
            });
        });

        it('should create a collection', function () {
            return collection.create();
        });

        it('should return undefined for unknown documents', function () {
            return collection.get('foo')
            .then(doc => {
                expect(doc).to.be.undefined;
            });
        });

        it('should insert documents', function () {
            return collection.insert(DOC)
            .then(result => {
                expect(result.ok).to.equal(true);
                expect(result.id).to.be.a('string');
                DOC._id = result.id;
            });
        });

        it('should look up documents by ID', function () {
            return collection.get(DOC._id)
            .then(doc => {
                expect(doc).to.deep.equal(DOC);
            });
        });

        it('should update documents', function () {
            return collection.update(_.merge(DOC, { bar: 'baz' }), DOC._id)
            .then(result => {
                expect(result.ok).to.equal(true);
                expect(result.id).to.equal(DOC._id);
            })
            .then(() => collection.get(DOC._id))
            .then(doc => {
                expect(doc).to.deep.equal(DOC);
            });
        });

        it('should upsert a document (update)', function () {
            return collection.upsert(_.merge(DOC, { bar: 'quux' }), DOC._id)
            .then(result => {
                expect(result.ok).to.equal(true);
                expect(result.id).to.equal(DOC._id);
            })
            .then(() => collection.get(DOC._id))
            .then(doc => {
                expect(doc).to.deep.equal(DOC);
            });
        });

        it('should upsert a document (insert)', function () {
            return collection.upsert(DOC2)
            .then(result => {
                expect(result.ok).to.equal(true);
                expect(result.id).to.be.a('string');
                DOC2._id = result.id;
            });
        });

        it('should upsert many documents', function () {
            return collection.upsertMany(MANY_DOCS)
            .then(result => {
                expect(result.ok).to.equal(true);
                expect(result.ids).to.be.an('array');
                expect(result.ids.length).to.equal(MANY_DOCS.length);
            });
        });

        it('should check if the collection exists (true)', function () {
            return collection.exists().then(exists => {
                expect(exists).to.equal(true);
            });
        });

        it('should delete documents', function () {
            return collection.delete(DOC._id)
            .then(result => {
                expect(result.ok).to.equal(true);
            })
            .then(() => collection.get(DOC._id))
            .then(doc => {
                expect(doc).to.be.undefined;
            });
        });

        it('should drop a collection', function () {
            return collection.drop()
            .then(() => collection.exists())
            .then(exists => {
                expect(exists).to.equal(false);
            });
        });
    });
});
