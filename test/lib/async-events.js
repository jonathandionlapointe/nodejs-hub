/*global describe, it, afterEach*/
'use strict';

const _ = require('lodash');
const AsyncEventEmitter = require('../../lib/async-events');
const expect = require('chai').expect;
const Promise = require('bluebird');

function unexpected() {
    throw new Error('unexpected success');
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        else return new Promise(setImmediate).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

describe('Async event emitter', function () {
    let events;
    const listeners = {
        a: { called: false, fn: () => {
            listeners.a.called = true;
        } },
        b: { called: false, fn: () => {
            return Promise.delay(10).then(() => {
                listeners.b.called = true;
            });
        } },
        fail: { fn: () => {
            throw new Error('failure');
        } },
    };

    afterEach(function () {
        _.forEach(listeners, listener => {
            listener.called = false;
        });
    });

    it('should create an instance', function () {
        events = new AsyncEventEmitter();
    });

    it('should emit without any listeners', function () {
        events.emitAsync('foo', result => {
            expect(result).to.equal(false);
        });
    });

    it('should add listeners', function () {
        events.on('event_a', listeners.a.fn);
        listeners.b.fn = events.onAsync('event_a', listeners.b.fn);
        expect(listeners.b.fn).to.be.a('function');
    });

    it('should emit synchronously', function () {
        const result = events.emit('event_a');
        expect(result).to.equal(true);
        expect(listeners.a.called).to.equal(true);
        expect(listeners.b.called).to.equal(false);
        return waitUntil(() => listeners.b.called);
    });

    it('should emit asynchronously', function () {
        return events.emitAsync('event_a').then(result => {
            expect(result).to.equal(true);
            expect(listeners.a.called).to.equal(true);
            expect(listeners.b.called).to.equal(true);
        });
    });

    it('should remove listeners', function () {
        events.removeListener('event_a', listeners.b.fn);
    });

    it('should emit asynchronously with synchronous listeners', function () {
        return events.emitAllAsync('event_a').then(result => {
            expect(result).to.equal(true);
            expect(listeners.a.called).to.equal(true);
            expect(listeners.b.called).to.equal(false);
        });
    });

    it('should report all failures', function () {
        events.onAsync('event_a', listeners.fail.fn);
        events.onAsync('event_a', listeners.b.fn);
        return events.emitAllAsync('event_a').then(unexpected, error => {
            expect(listeners.a.called).to.equal(true);
            expect(listeners.b.called).to.equal(true);
            expect(error.message).to.equal('emitAllAsync failure');
            expect(error.errors).to.be.an('array');
            expect(error.errors.length).to.equal(1);
            expect(error.errors[0].message).to.equal('failure');
        });
    });
});
