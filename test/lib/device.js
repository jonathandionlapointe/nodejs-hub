/*global describe, it, before, after, beforeEach*/
'use strict';

const expect = require('chai').expect;
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const Device = require('../../lib/device');
const Events = require('../../lib/events');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const testModel = {
    _id: 'testModel',
    protocolId: 'test',
    capabilities: [{
        capabilityId: 'switch',
        type: 'enum',
        methods: ['get', 'put'],
        range: ['off', 'on'],
    }, {
        capabilityId: 'battery',
        type: 'float',
        methods: ['get'],
        units: ['%'],
        range: [
            { value: 0, unit: '%' },
            { value: 100, unit: '%' },
        ],
    }, {
        capabilityId: 'heating',
        type: 'float',
        methods: ['get','put'],
        units: ['C', 'F'],
        range: [
            { value: 10, unit: 'C' },
            { value: 30, unit: 'C' },
        ],
    }],
    parameters: [{
        parameter: 'xyz',
        value: 42,
    }],
};

const testProtocol = {
    addDevice(context, device) {
        const child = device.createChild(testModel);
        return Promise.resolve([device, child]);
    },
    removeDevice(context, device) {
        return Promise.resolve();
    },
    setCapability(context, device, name, value, unit) {
        const result = device.updateCap(context, name, value, unit);
        return Promise.resolve(result);
    },
    setParameters(context, device, params) {
        if (params.xyz.value !== device.parameters.xyz.value) {
            const ok = device.updateParameters(context, {
                xyz: { value: params.xyz.value }
            });
            if (ok) {
                return device.save(context).return(ok);
            }
            return ok;
        }
        return false;
    },
    startup(context) {
        testProtocol.started = true;
    },
    shutdown(context) {
        testProtocol.started = false;
    },
    started: false
};

const testSdk = {
    models: {
        getCached(id) {
            return Promise.resolve(testModel);
        }
    },
    events: {
        create() {
            return Promise.resolve();
        }
    }
};

const testSystemId = 'testSystem';

function unexpected() {
    throw new Error('unexpected success');
}

const ISO8601_RE = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;

describe('Device', function () {
    this.slow(150);

    const events = new Events();
    let lastEvent;
    events.onEvent((context, event) => { lastEvent = event; });

    const testContext = Object.create(app.context, {
        sdk: { enumerable: false, configurable: false, value: testSdk },
        gatewayId: { enumerable: true, configurable: false, value: testSystemId },
        events: { enumerable: false, configurable: false, value: events },
        server: { enumerable: false, configurable: false, value: true },
    });

    before(function () {
        return dropDbs(app.context)
        .then(() => Promise.all([
            Device.views.update(app.context),
            events.init(app.context),
            permission.views.update(app.context),
            app.context.collection('cache').create().then(db => {
                return db.insert({ _id: `model:${testModel._id}`, data: testModel });
            }),
        ]));
    });

    after(function () {
        events.settleTime = 300;
        return events.settle().then(() => dropDbs(app.context));
    });

    beforeEach(function () {
        lastEvent = null;
    });

    it('should provide sane defaults for enum caps', function () {
        const model = {
            capabilityId: 'sensor',
            type: 'enum',
            methods: ['get'],
            range: ['idle', 'detected'],
        };
        const cap = Device.defaultCap(model);
        expect(cap).to.be.an('object');
        expect(cap.name).to.equal('sensor');
        expect(cap.value).to.equal('idle');
        expect(cap.unit).to.be.undefined;
        expect(cap.timestamp).to.match(ISO8601_RE);
    });

    it('should provide sane defaults for float caps with range', function () {
        const model = {
            capabilityId: 'battery',
            type: 'float',
            methods: ['get'],
            units: ['%'],
            range: [
                { value: 0, unit: '%' },
                { value: 100, unit: '%' },
            ],
        };
        const cap = Device.defaultCap(model);
        expect(cap).to.be.an('object');
        expect(cap.name).to.equal('battery');
        expect(cap.value).to.equal(0);
        expect(cap.unit).to.equal('%');
        expect(cap.timestamp).to.match(ISO8601_RE);
    });

    it('should provide sane defaults for float caps without range', function () {
        const model = {
            capabilityId: 'airTemperature',
            type: 'float',
            methods: ['get'],
            units: ['C', 'F'],
        };
        const cap = Device.defaultCap(model);
        expect(cap).to.be.an('object');
        expect(cap.name).to.equal('airTemperature');
        expect(cap.value).to.equal(0);
        expect(cap.unit).to.equal('C');
        expect(cap.timestamp).to.match(ISO8601_RE);
    });

    it('should provide sane defaults for color caps', function () {
        const model = {
            capabilityId: 'color',
            type: 'color',
            range: ['red', 'green', 'blue'],
            methods: ['get', 'put'],
        };
        const cap = Device.defaultCap(model);
        expect(cap).to.be.an('object');
        expect(cap.name).to.equal('color');
        expect(cap.value).to.deep.equal({ red: 0, green: 0, blue: 0 });
        expect(cap.unit).to.be.undefined;
        expect(cap.timestamp).to.match(ISO8601_RE);
    });

    it('should provide sane defaults for uri caps', function () {
        const model = {
            capabilityId: 'serverUrl',
            type: 'uri',
            methods: ['get', 'put'],
        };
        const cap = Device.defaultCap(model);
        expect(cap).to.be.an('object');
        expect(cap.name).to.equal('serverUrl');
        expect(cap.value).to.equal('http://www.example.com');
        expect(cap.unit).to.be.undefined;
        expect(cap.timestamp).to.match(ISO8601_RE);
    });

    it('should fail to provide defaults for invalid cap types', function () {
        const model = {
            capabilityId: 'foo',
            type: 'bar',
        };
        expect(() => Device.defaultCap(model)).to.throw(/invalid capability type/);
    });

    it('should fail to add a device if there are no protocols', function () {
        return testContext.withTransactionContext(txnContext => {
            return Device.add(txnContext, testModel, {});
        }).then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/protocol not available/);
        });
    });

    it('should register a protocol', function () {
        return Device.registerProtocol(testContext, 'test', testProtocol);
    });

    it('should add a device', function () {
        this.slow(250);

        return testContext.withTransactionContext(txnContext => {
            return Device.add(txnContext, testModel, {});
        });
    });

    it('should be able to get a device ID from protocol ID', function () {
        return Device.fromProtocolId(testContext, 'test', null).then(id => {
            expect(id).to.be.a('string');
        });
    });

    it('should be able to load and delete a device', function () {
        this.slow(350);

        let id = null;
        return testContext.withTransactionContext(txnContext => {
            return Device.loadProtocolId(txnContext, 'test', null).then(device => {
                expect(device).to.be.an.instanceof(Device);
                id = device._id;
                return device.remove(txnContext);
            });
        }).then(() => {
            expect(lastEvent).to.be.an('object');
            expect(lastEvent.type).to.equal('delete');
            expect(lastEvent.path).to.equal(`/devices/${id}`);
        });
    });

    it('should fail to load a missing device by protocol ID', function () {
        return Device.loadProtocolId(testContext, 'test', null).then(unexpected, error => {
            expect(error.message).to.match(/not found/i);
        });
    });

    let testDeviceId = null;
    let childDeviceId = null;
    it('should create a device', function () {
        return testContext.withTransactionContext(txnContext => {
            return Device.add(txnContext, testModel, {}).timeout(5000);
        }).then(devices => {
            expect(devices).to.be.an('array');
            expect(devices.length).to.equal(2);
            testDeviceId = devices[0]._id;
            childDeviceId = devices[1]._id;
            expect(lastEvent.type).to.equal('create');
            expect(lastEvent.path).to.equal(`/devices/${childDeviceId}`);
        });
    });

    let testDevice = null;
    it('should load the created device', function () {
        this.slow(200);

        return Device.load(testContext, testDeviceId).then(device => {
            testDevice = device;
        });
    });

    it('should have assigned the system ID as the device gateway', function () {
        expect(testDevice._doc.gateway).to.equal(testSystemId);
    });

    let childDevice = null;
    it('should load the created child device', function () {
        this.slow(200);

        return Device.load(testContext, childDeviceId).then(device => {
            childDevice = device;
        });
    });

    it('should have assigned the parent and gateway', function () {
        expect(childDevice._doc.parent).to.equal(testDeviceId);
        expect(childDevice._doc.gateway).to.equal(testSystemId);
    });

    it('should get a tree of device IDs', function () {
        this.slow(200);

        return Device.getIdTree(testContext, testDeviceId).then(tree => {
            expect(tree).to.deep.equal([testDeviceId, [[childDeviceId, []]]]);
        });
    });

    it('should load a device tree', function () {
        this.slow(250);

        return Device.load(testContext, testDeviceId)
        .then(device => device.loadChildren(testContext))
        .then(device => {
            expect(device._doc).to.deep.equal(testDevice._doc);
            expect(device.children).to.be.an('array');
            expect(device.children.length).to.equal(1);
            expect(device.children[0]._doc).to.deep.equal(childDevice._doc);
        });
    });

    it('should update capabilities', function () {
        this.slow(200);

        return testContext.withTransactionContext(txnContext => {
            return testDevice.setCapability(txnContext, 'switch', 'on');
        }).then(cap => {
            expect(cap).to.be.an('object');
            expect(cap.name).to.equal('switch');
            expect(cap.value).to.equal('on');
            expect(lastEvent.type).to.equal('update');
            expect(lastEvent.path).to.equal(`/devices/${testDeviceId}/caps/switch`);
            expect(lastEvent.resource.value).to.equal('on');
        });
    });

    it('should not update capabilities if the value has not changed', function () {
        return testDevice.setCapability(testContext, 'switch', 'on').then(result => {
            expect(result).to.be.false;
            expect(lastEvent).to.be.null;
        });
    });

    it('should fail on non-existent capabilities', function () {
        return testDevice.setCapability(testContext, 'foo', 'bar').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/not found/i);
        });
    });

    it('should fail if the capability is read only', function () {
        return testDevice.setCapability(testContext, 'battery', 10, '%').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/capability is read-only/i);
        });
    });

    it('should fail if no value is provided', function () {
        return testDevice.setCapability(testContext, 'switch').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/bad value/);
        });
    });

    it('should fail on invalid capability values', function () {
        return testDevice.setCapability(testContext, 'switch', 'foo').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/out of range/);
        });
    });

    it('should fail on incompatible value units', function () {
        return testDevice.setCapability(testContext, 'heating', 10, '%').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/bad unit/);
        });
    });

    it('should fail on out of range values', function () {
        return testDevice.setCapability(testContext, 'heating', 0, 'F').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/out of range/);
        });
    });

    it('should fail on incompatible capability values', function () {
        return testDevice.setCapability(testContext, 'switch', 24, 'C').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/bad value/);
        }).then(() => {
            return testDevice.setCapability(testContext, 'heating', 'off');
        }).then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/bad value/);
        });
    });

    it('should update parameters', function () {
        return testContext.withTransactionContext(txnContext => {
            return testDevice.setParameters(txnContext, { xyz: { value: 0 } });
        }).then(result => {
            expect(result).to.be.deep.equal({ xyz: { name: 'xyz', value: 0 } });
            expect(lastEvent).to.be.an('object');
            expect(lastEvent.type).to.equal('update');
            expect(lastEvent.path).to.equal(`/devices/${testDeviceId}`);
            expect(lastEvent.resource).to.deep.equal({
                parameters: { xyz: { name: 'xyz', value: 0 } }
            });
        });
    });

    it('should not update unchanged parameters', function () {
        // This relies on a properly behaving protocol!
        return testDevice.setParameters(testContext, { xyz: { value: 0 } }).then(result => {
            expect(result).to.be.false;
            expect(lastEvent).to.be.null;
        });
    });

    it('should set a device status to FAILED', function () {
        this.slow(200);

        return testContext.withTransactionContext(txnContext => {
            return testDevice.updateStatus(txnContext, 'FAILED');
        }).then(() => {
            expect(lastEvent.type).to.equal('update');
            expect(lastEvent.path).to.equal(`/devices/${testDeviceId}`);
            expect(lastEvent.resource).to.deep.equal({
                status: 'FAILED',
            });
        });
    });

    it('should not emit events for a non-changed status', function () {
        testDevice.updateStatus(testContext, 'FAILED');
        expect(lastEvent).to.be.null;
    });

    it('should fail to load a non-existent device', function () {
        this.slow(100);

        return Device.load(testContext, 'foo').then(unexpected, error => {
            expect(error.isBoom).to.be.true;
            expect(error.message).to.match(/not found/i);
        });
    });

    it('should load all protocol devices', function () {
        return Device.loadProtocolAll(testContext, 'test').then(devices => {
            expect(devices).to.be.an('array');
            expect(devices.length).to.equal(2);
            expect(devices[0]).to.be.an.instanceof(Device);
            expect(devices[1]).to.be.an.instanceof(Device);
        });
    });

    it('should delete child devices when a parent is removed', function () {
        this.slow(300);

        return testContext.withTransactionContext(txnContext => {
            return testDevice.remove(txnContext);
        }).then(() => Device.loadAll(testContext)).then(devices => {
            expect(devices.length).to.equal(0);
        });
    });

    it('should look up models by ID when adding a new device', function () {
        this.slow(250);

        return testContext.withTransactionContext(txnContext => {
            return Device.add(txnContext, 'testModel', {});
        }).then(devices => {
            expect(devices).to.be.an('array');
            expect(devices.length).to.equal(2);
            testDeviceId = devices[0]._id;
            childDeviceId = devices[1]._id;
            expect(lastEvent.type).to.equal('create');
            expect(lastEvent.path).to.equal(`/devices/${childDeviceId}`);
        });
    });

    it('should unregister a protocol', function () {
        return Device.unregisterProtocol(testContext, 'test').then(() => {
            return Device.getProtocol('test').then(unexpected, error => {
                expect(error.message).to.match(/protocol not available/);
            });
        });
    });

    it('should call protocol startup callbacks', function () {
        return Device.registerProtocol(testContext, 'test', testProtocol).then(() => {
            return Device.onSystemStartup(testContext);
        }).then(() => {
            expect(testProtocol.started).to.equal(true);
        });
    });

    it('should call protocol shutdown callbacks', function () {
        return Device.onSystemShutdownDevices(testContext).then(() => {
            expect(testProtocol.started).to.equal(false);
            return Device.getProtocol('test').then(unexpected, error => {
                expect(error.message).to.match(/protocol not available/);
            });
        });
    });
});
