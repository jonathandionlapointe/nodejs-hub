/*global describe, it, before*/
'use strict';

const context = require('../../lib/context');
const expect = require('chai').expect;
const Hapi = require('hapi');

function sdk(server, options, next) { server.plugins['sdk'] = { test: 'sdk' }; next(); }
function event(server, options, next) { server.plugins['event'] = { test: 'event' }; next(); }
sdk.attributes = { name: 'sdk' };
event.attributes = { name: 'event' };

describe('Context', function () {
    const app = {};
    const server = new Hapi.Server();
    let ctx = null;

    before(function () {
        server.connection();
    });

    it('should create the application context', function () {
        ctx = context.createAppContext(app, null);
        expect(ctx.app).to.equal(app);
        expect(ctx.transaction).to.equal(false);
        expect(ctx.server).to.equal(null);
        expect(ctx.request).to.equal(null);
        expect(ctx.plugin).to.equal(null);
        expect(ctx.requestId).to.equal(null);
        expect(ctx.traceId).to.equal(null);
        expect(ctx.events).to.equal(null);
        expect(ctx.sdk).to.equal(null);
        expect(ctx.who).to.equal(null);
        expect(ctx.whoType).to.equal(null);
        expect(ctx.scope).to.equal(null);
    });

    it('should create the server context', function () {
        const serverContext = ctx.createServerContext(server);

        expect(server.context()).to.equal(serverContext);
        expect(serverContext.server).to.equal(server);

        server.route({ method: 'GET', path: '/', handler: function (request, reply) {
            return reply(request.context());
        }});
    });

    it('should get plugin state', function (done) {
        server.register([sdk, event], (err) => {
            if (err) return done(err);
            try {
                expect(server.context().sdk).to.equal(server.plugins['sdk']);
                expect(server.context().event).to.equal(server.plugins['events']);
                done();
            } catch (error) {
                done(error);
            }
        });
    });

    it('should create a per-request context', function (done) {
        server.inject({
            method: 'GET',
            url: '/',
            headers: {
                'x-request-id': 'request id',
                'x-trace-id': 'trace id'
            }
        }, response => {
            try {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result.requestId).to.equal('request id');
                expect(response.result.traceId).to.equal('trace id');
                done();
            } catch (error) {
                done(error);
            }
        });
    });
});
