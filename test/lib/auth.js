/*global describe,it,before,after */
'use strict';

const Auth = require('../../lib/auth');
const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const jwt = require('jsonwebtoken');
const ObjectID = require('bson-objectid');
const os = require('os');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const GATEWAY_ID = ObjectID().toString();
const USER = {
    _id: ObjectID().toString(),
    username: 'test@example.com',
};

describe('Auth', function () {
    let auth;
    let nonce;
    let loginCredentials;
    let token;

    const context = Object.create(app.context, {
        gatewayId: { configurable: false, enumerable: true, value: GATEWAY_ID },
    });

    before(function () {
        return dropDbs(context)
        .then(() => Promise.each([Auth, permission], T => T.views.update(context)));
    });

    after(function () {
        return dropDbs(context);
    });

    it('should create an instance', function () {
        auth = new Auth();
    });

    it('should initialize the instance', function () {
        return auth.init(config.auth);
    });

    it('should generate a nonce', function () {
        return auth.nonce(USER).then(data => {
            expect(data).to.be.an('object');
            expect(data.nonce).to.be.a('string');
            expect(data.nonce).to.not.be.empty;
            expect(data.servername).to.be.a('string');
            expect(data.servername).to.equal(os.hostname() + '.local');
            expect(data.url).to.be.a('string');
            expect(data.url).to.match(/^http:\/\//);
            expect(data.cert).to.equal(null); // We didn't provide a cert when initializing!
            nonce = data.nonce;
        });
    });

    it('should validate a nonce login', function () {
        return auth.validateCredentials(context, USER.username, nonce).then(creds => {
            expect(creds).to.be.an('object');
            expect(creds.whoType).to.equal('user');
            expect(creds.who).to.equal(USER._id);
            loginCredentials = creds;
        });
    });

    it('should grant a token for valid login credentials', function () {
        return auth.login(loginCredentials).then(tok => {
            expect(tok).to.be.a('string');
            expect(tok).to.not.be.empty;
            token = tok;
        });
    });

    it('should verify a generated token', function () {
        token = jwt.verify(token, auth.publicKey, {
            algorithms: [ auth.algorithm ],
        });
    });

    it('should validate a verified token', function () {
        return auth.validateToken(context, token).then(creds => {
            expect(creds).to.be.an('object');
            expect(creds.whoType).to.equal('user');
            expect(creds.who).to.equal(USER._id);
        });
    });
});
