/*global describe, it */
'use strict';

const expect = require('chai').expect;
const qc = require('quick_check');
const matches = require('../../lib/condition').matches;
const exactValues = require('../../lib/condition').exactValues;
const simplify = require('../../lib/condition').simplify;
const DEFAULT_EPSILON = require('../../lib/condition').DEFAULT_EPSILON;
const ConversionError = require('../../lib/condition').ConversionError;

function comparisonProperty(n, m) {
    let result = true;
    const d = n - m;
    if (d < -DEFAULT_EPSILON) {
        result = result && matches({ value: n, unit: 'C' }, { type: '<', value: m, unit: 'C' });
    }
    if (d < +DEFAULT_EPSILON) {
        result = result && matches({ value: n, unit: 'C' }, { type: '<=', value: m, unit: 'C' });
    }
    if (d > +DEFAULT_EPSILON) {
        result = result && matches({ value: n, unit: 'C' }, { type: '>', value: m, unit: 'C' });
    }
    if (d > -DEFAULT_EPSILON) {
        result = result && matches({ value: n, unit: 'C' }, { type: '>=', value: m, unit: 'C' });
    }
    if (Math.abs(d) < DEFAULT_EPSILON) {
        result = result && matches({ value: n, unit: 'C' }, { type: '=', value: m, unit: 'C' });
    } else {
        result = result && matches({ value: n, unit: 'C' }, { type: '!=', value: m, unit: 'C' });
    }
    return result;
}

function expectForAll() {
    const property = arguments[0];
    const args = Array.prototype.slice.call(arguments, 1);
    args.push(function () {
        if (!property.apply(undefined, arguments)) {
            const args = Array.prototype.slice.call(arguments);
            throw new Error(`property ${property.name} does not hold for ${JSON.stringify(args)}`);
        }
    });
    qc.forAll.apply(qc, args);
}

describe('Conditions', function () {
    it('should fail invalid conditions', function () {
        expect(() => matches({ value: 0, unit: '%' }, { type: 'foo', value: 0, unit: '%' })).to.throw(/invalid condition type/);
    });
    it('should check exact string equality', function () {
        expect(matches({ value: 'idle' }, { type: 'exact', value: 'idle' })).to.be.true;
        expect(matches({ value: 'detected' }, { type: 'exact', value: 'idle' })).to.be.false;
    });
    it('should fail exact matches on number values', function () {
        expect(() => matches({ value: 24, unit: 'C' }, { type: 'exact', value: 'idle' })).to.throw(/expected enum/);
    });
    describe('relative comparisons', function () {
        it('should check <', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '<', value: 23, unit: 'C' })).to.be.false;
            expect(matches({ value: 24, unit: 'C' }, { type: '<', value: 24, unit: 'C' })).to.be.false;
            expect(matches({ value: 24, unit: 'C' }, { type: '<', value: 25, unit: 'C' })).to.be.true;
        });
        it('should check <=', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '<=', value: 23, unit: 'C' })).to.be.false;
            expect(matches({ value: 24, unit: 'C' }, { type: '<=', value: 24, unit: 'C' })).to.be.true;
            expect(matches({ value: 24, unit: 'C' }, { type: '<=', value: 25, unit: 'C' })).to.be.true;
        });
        it('should check >', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '>', value: 23, unit: 'C' })).to.be.true;
            expect(matches({ value: 24, unit: 'C' }, { type: '>', value: 24, unit: 'C' })).to.be.false;
            expect(matches({ value: 24, unit: 'C' }, { type: '>', value: 25, unit: 'C' })).to.be.false;
        });
        it('should check >=', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '>=', value: 23, unit: 'C' })).to.be.true;
            expect(matches({ value: 24, unit: 'C' }, { type: '>=', value: 24, unit: 'C' })).to.be.true;
            expect(matches({ value: 24, unit: 'C' }, { type: '>=', value: 25, unit: 'C' })).to.be.false;
        });
        it('should check =', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '=', value: 24, unit: 'C' })).to.be.true;
        });
        it('should check !=', function () {
            expect(matches({ value: 24, unit: 'C' }, { type: '!=', value: 24, unit: 'C' })).to.be.false;
        });
        it('should use an epsilon value to check equality', function () {
            expect(matches({ value: 0, unit: 'C' }, { type: '=', value: 0.01, unit: 'C', epsilon: 0.1 })).to.be.true;
        });
    });
    it('should check relative comparisons (quickcheck, 100 samples)', function () {
        expectForAll(comparisonProperty, qc.real, qc.real);
    });
    it('should fail comparisons on non-number values', function () {
        expect(() => matches({ value: 'idle' }, { type: '<=', value: 23, unit: 'C' })).to.throw(/expected number/);
        expect(() => matches({ value: 24 }, { type: '<=', value: 23, unit: 'C' })).to.throw(/expected unit/);
    });
    describe('unit conversions', function () {
        it('should convert C to C (identity)', function () {
            expect(matches({ value: 0, unit: 'C' }, { type: '=', value: 0, unit: 'C' })).to.be.true;
        });
        it('should convert C to F', function () {
            expect(matches({ value: 0, unit: 'C' }, { type: '=', value: 32, unit: 'F' })).to.be.true;
        });
        it('should convert C to K', function () {
            expect(matches({ value: 0, unit: 'C' }, { type: '=', value: 273.15, unit: 'K' })).to.be.true;
        });
        it('should convert F to C', function () {
            expect(matches({ value: 32, unit: 'F' }, { type: '=', value: 0, unit: 'C' })).to.be.true;
        });
        it('should convert F to F (identity)', function () {
            expect(matches({ value: 32, unit: 'F' }, { type: '=', value: 32, unit: 'F' })).to.be.true;
        });
        it('should convert F to K', function () {
            expect(matches({ value: 32, unit: 'F' }, { type: '=', value: 273.15, unit: 'K' })).to.be.true;
        });
        it('should convert K to C', function () {
            expect(matches({ value: 273.15, unit: 'K' }, { type: '=', value: 0, unit: 'C' })).to.be.true;
        });
        it('should convert K to F', function () {
            expect(matches({ value: 273.15, unit: 'K' }, { type: '=', value: 32, unit: 'F' })).to.be.true;
        });
        it('should convert K to K (identity)', function () {
            expect(matches({ value: 273.15, unit: 'K' }, { type: '=', value: 273.15, unit: 'K' })).to.be.true;
        });
        it('should fail with incompatible units', function () {
            expect(() => matches({ value: 0, unit: '%' }, { type: '<', value: 0, unit: 'C' })).to.throw(ConversionError, /to unit C/);
            expect(() => matches({ value: 0, unit: 'C' }, { type: '<', value: 0, unit: '%' })).to.throw(ConversionError, /to unit %/);
        });
    });
    describe('compound conditions', function () {
        it('should negate matches', function () {
            expect(matches({ value: 'idle' }, { type: 'not', condition: { type: 'exact', value: 'idle' }})).to.be.false;
            expect(matches({ value: 'detected' }, { type: 'not', condition: { type: 'exact', value: 'idle' }})).to.be.true;
        });
        it('should check "all" matches', function () {
            expect(matches({ value: 24, unit: 'C' }, {
                type: 'all',
                conditions: [
                    { type: '>', value: 10, unit: 'C' },
                    { type: '<', value: 35, unit: 'C' },
                ]
            })).to.be.true;
            expect(matches({ value: 0, unit: 'C' }, {
                type: 'all',
                conditions: [
                    { type: '>', value: 10, unit: 'C' },
                    { type: '<', value: 35, unit: 'C' },
                ]
            })).to.be.false;
        });
        it('should check "any" matches', function () {
            expect(matches({ value: 'on' }, {
                type: 'any',
                conditions: [
                    { type: 'exact', value: 'auto' },
                    { type: 'exact', value: 'on' },
                ]
            })).to.be.true;
            expect(matches({ value: 'off' }, {
                type: 'any',
                conditions: [
                    { type: 'exact', value: 'auto' },
                    { type: 'exact', value: 'on' },
                ]
            })).to.be.false;
        });
        it('should extract "exact" values', function () {
            expect(exactValues({
                type: 'any',
                conditions: [
                    { type: 'exact', value: 'foo' },
                    { type: 'not', condition: { type: 'exact', value: 'bar' } },
                ]
            })).to.deep.equal(['foo', 'bar']);
        });
    });
    describe('simplification', function () {
        it('should apply rule: 1 -> 1', function () {
            expect(simplify({ type: 'always'})).to.deep.equal({ type: 'always' });
        });
        it('should apply rule: 0 -> 0', function () {
            expect(simplify({ type: 'never' })).to.deep.equal({ type: 'never' });
        });
        it('should apply rule: A -> A', function () {
            expect(simplify(1)).to.equal(1);
        });
        it('should apply rule: not 1 -> 0', function () {
            expect(simplify({ type: 'not', condition: { type: 'always' } })).to.deep.equal({ type: 'never' });
        });
        it('should apply rule: not 0 -> 1', function () {
            expect(simplify({ type: 'not', condition: { type: 'never' } })).to.deep.equal({ type: 'always' });
        });
        it('should apply rule: not not A -> A', function () {
            const input = {
                type: 'not',
                condition: {
                    type: 'not',
                    condition: { type: 'exact', value: 'idle' }
                }
            };
            const output = { type: 'exact', value: 'idle' };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: all (A, all (B, C)) -> all (A, B, C)', function () {
            const input = {
                type: 'all',
                conditions: [0, {
                    type: 'all',
                    conditions: [1, 2]
                }]
            };
            const output = {
                type: 'all',
                conditions: [0, 1, 2]
            };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: any (A, any (B, C)) -> any (A, B, C)', function () {
            const input = {
                type: 'any',
                conditions: [0, {
                    type: 'any',
                    conditions: [1, 2]
                }]
            };
            const output = {
                type: 'any',
                conditions: [0, 1, 2]
            };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: all (A, B, 1) -> all (A, B)', function () {
            const input = {
                type: 'all',
                conditions: [0, 1, { type: 'always' }]
            };
            const output = { type: 'all', conditions: [0, 1] };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: any (A, B, 0) -> any (A, B)', function () {
            const input = {
                type: 'any',
                conditions: [0, 1, { type: 'never' }]
            };
            const output = { type: 'any', conditions: [0, 1] };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: all (A, 0) -> 0', function () {
            expect(simplify({ type: 'all', conditions: [1, { type: 'never' }] })).to.deep.equal({ type: 'never' });
        });
        it('should apply extended rule: all (A, 0, B) -> 0', function () {
            expect(simplify({ type: 'all', conditions: [1, { type: 'never' }, 2] })).to.deep.equal({ type: 'never' });
        });
        it('should apply rule: any (A, 1) -> 1', function () {
            expect(simplify({ type: 'any', conditions: [1, { type: 'always' }] })).to.deep.equal({ type: 'always' });
        });
        it('should apply extended rule: any (A, 1, B) -> 1', function () {
            expect(simplify({ type: 'any', conditions: [1, { type: 'always' }, 2] })).to.deep.equal({ type: 'always' });
        });
        it('should apply rule: all (A) -> A', function () {
            expect(simplify({ type: 'all', conditions: [1] })).to.equal(1);
        });
        it('should apply rule: any (A) -> A', function () {
            expect(simplify({ type: 'any', conditions: [1] })).to.equal(1);
        });
        it('should apply rule: all () -> true', function () {
            const input = { type: 'all', conditions: [] };
            expect(simplify(input)).to.deep.equal({ type: 'always' });
        });
        it('should apply rule: any () -> false', function () {
            const input = { type: 'any', conditions: [] };
            expect(simplify(input)).to.deep.equal({ type: 'never' });
        });
        it('should apply rule: all (not A, not B) -> not any (A, B)', function () {
            const input = {
                type: 'all',
                conditions: [{
                    type: 'not',
                    condition: 0
                }, {
                    type: 'not',
                    condition: 1
                }]
            };
            const output = {
                type: 'not',
                condition: {
                    type: 'any',
                    conditions: [0, 1]
                }
            };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rule: any (not A, not B) -> not all (A, B)', function () {
            const input = {
                type: 'any',
                conditions: [{
                    type: 'not',
                    condition: 0
                }, {
                    type: 'not',
                    condition: 1
                }]
            };
            const output = {
                type: 'not',
                condition: {
                    type: 'all',
                    conditions: [0, 1]
                }
            };
            expect(simplify(input)).to.deep.equal(output);
        });
        it('should apply rules recursively', function () {
            const input = {
                type: 'any',
                conditions: [{
                    type: 'not',
                    condition: {
                        type: 'not',
                        condition: {
                            type: 'not',
                            condition: 1
                        }
                    }
                }, {
                    type: 'not',
                    condition: {
                        type: 'all',
                        conditions: [{
                            type: 'all',
                            conditions: []
                        }]
                    }
                }]
            };
            const output = {
                type: 'not',
                condition: 1
            };
            expect(simplify(input)).to.deep.equal(output);
        });
    });
});
