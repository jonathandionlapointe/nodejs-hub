'use strict';

const _ = require('lodash');
const Boom = require('boom');
const Calibrate = require('calibrate');
const config = require('../../lib/config');
const Hapi = require('hapi');
const ObjectID = require('bson-objectid');
const Promise = require('bluebird');

const server = Promise.promisifyAll(new Hapi.Server({ debug: false }));

server.connection({
    host: '127.0.0.1',
    port: 0,
});

let mac = 0;
function generateMac() {
    const freshMac = mac++;
    return (
        hex2((freshMac >> 40) & 0xFF) + ':' +
        hex2((freshMac >> 32) & 0xFF) + ':' +
        hex2((freshMac >> 24) & 0xFF) + ':' +
        hex2((freshMac >> 16) & 0xFF) + ':' +
        hex2((freshMac >>  8) & 0xFF) + ':' +
        hex2((freshMac >>  0) & 0xFF)
    );
}

function hex2(n) {
    if (n < 16) {
        return '0' + n.toString(16);
    }
    return n.toString(16);
}

function start() {
    return server.startAsync()
    .then(() => {
        config.system.api = 'http://127.0.0.1:' + server.listener.address().port;
    });
}

function stop() {
    return server.stopAsync();
}

const database = {
    models: [],
    devices: [],
    events: [],
    users: [],
    permissions: [],
    locations: [],
    templates: [],
};

function setDefault(option, defaultObject, object) {
    if (option === true) return defaultObject;
    if (option) return option;
    return object;
}

function reset(options) {
    _.defaults(options, {
        models: false,
        devices: false,
        events: false
    });
    database.models = setDefault(options.models, [], database.models);
    database.devices = setDefault(options.devices, [], database.devices);
    database.events = setDefault(options.events, [], database.events);
    database.users = setDefault(options.users, [], database.users);
    database.permissions = setDefault(options.permissions, [], database.permissions);
    database.locations = setDefault(options.locations, [], database.locations);
    database.templates = setDefault(options.templates, [], database.templates);
}

function doCreate(collection, object) {
    if (!object._id) {
        object._id = ObjectID().toString();
    } else {
        const conflict = _.find(database[collection], { _id: object._id });
        if (conflict) {
            throw Boom.conflict();
        }
        database[collection].push(object);
        return object;
    }
}

function create(collection) {
    return (request, reply) => {
        try { reply(doCreate(collection, request.payload)); }
        catch (error) { reply(Calibrate.error(error)); }
    };
}

function list(collection) {
    return (request, reply) => reply(database[collection]);
}

function doGet(collection, id) {
    const object = _.find(database[collection], { _id: id });
    if (!object) {
        throw Boom.notFound();
    } else {
        return object;
    }
}

function get(collection) {
    return (request, reply) => {
        try { reply(doGet(collection, request.params.id)); }
        catch (error) { reply(Calibrate.error(error)); }
    };
}

function doUpdate(collection, id, object) {
    const index = _.findIndex(database[collection], { _id: id });
    if (index === -1) {
        throw Boom.notFound();
    } else {
        object._id = id;
        database[collection][index] = object;
        return object;
    }
}

function update(collection) {
    return (request, reply) => {
        try { reply(doUpdate(collection, request.params.id, request.payload)); }
        catch (error) { reply(Calibrate.error(error)); }
    };
}

function doUpdateCap(id, name, object) {
    const device = _.find(database.devices, { _id: id });
    if (!device) {
        throw Boom.notFound();
    }
    if (!device.caps[name]) {
        throw Boom.notFound();
    }
    device.caps[name] = object;
    return object;
}

function doRemove(collection, id) {
    const index = _.findIndex(database[collection], { _id: id });
    if (index == -1) {
        throw Boom.notFound();
    } else {
        _.pullAt(database[collection], index);
    }
}

function remove(collection) {
    return (request, reply) => {
        try { doRemove(collection, request.params.id); reply().code(204); }
        catch (error) { reply(Calibrate.error(error)); }
    };
}

function provision(request, reply) {
    try {
        const object = request.payload;
        object._id = ObjectID().toString();
        object.token = ObjectID().toString();
        const mac = generateMac();
        reply({
            provisioning: {
                mac: mac,
                bindingCode: require('crypto').createHash('md5').update(mac).digest('hex'),
            },
            device: doCreate('devices', object),
        });
    } catch (error) {
        reply(Calibrate.error(error));
    }
}

function ping(request, reply) {
    reply({ frequency: 15 });
}

function heartbeat(request, reply) {
    reply({});
}

function createEvent(request, reply) {
    try {
        database.events.push(request.payload);
        const event = request.payload;
        const matches = event.path.match(/^\/(devices|models)\/(.+?)(?:\/caps\/([^\/]+))?$/);
        if (!matches) {
            throw new Error(`no match: ${event.path}`);
        }
        const resource = _.merge({}, event.resource);
        const collection = matches[1];
        const id = matches[2];
        const cap = matches[3];
        if (cap && collection === 'devices' && event.type === 'update') {
            doUpdateCap(id, cap, resource);
            throw new Error('done');
        }
        switch (event.type) {
        case 'create': resource._id = id; doCreate(collection, resource); break;
        case 'update': doUpdate(collection, id, resource); break;
        case 'delete': doRemove(collection, id); break;
        }
    } catch (error) {
        // Ignored.
    }
    reply().code(204);
}

server.route([
    { method: 'POST', path: '/models', handler: create('models') },
    { method: 'GET', path: '/models', handler: list('models') },
    { method: 'GET', path: '/models/{id}', handler: get('models') },
    { method: 'PUT', path: '/models/{id}', handler: update('models') },
    { method: 'DELETE', path: '/models/{id}', handler: remove('models') },

    { method: 'POST', path: '/devices/{id}/ping', handler: ping },
    { method: 'PUT', path: '/devices/{id}/heartbeat', handler: heartbeat },

    { method: 'POST', path: '/provisioning', handler: provision },

    { method: 'POST', path: '/events', handler: createEvent },

    { method: 'GET', path: '/users', handler: list('users') },
    { method: 'GET', path: '/permissions', handler: list('permissions') },

    { method: 'GET', path: '/locations', handler: list('locations') },
    { method: 'POST', path: '/locations', handler: create('locations') },
    { method: 'GET', path: '/locations/{id}', handler: get('locations') },
    { method: 'PUT', path: '/locations/{id}', handler: update('locations') },
    { method: 'PATCH', path: '/locations/{id}', handler: update('locations') },
    { method: 'DELETE', path: '/locations/{id}', handler: remove('locations') },

    { method: 'GET', path: '/templates', handler: list('templates') },
    { method: 'POST', path: '/templates', handler: create('templates') },
    { method: 'GET', path: '/templates/{id}', handler: get('templates') },
    { method: 'PUT', path: '/templates/{id}', handler: update('templates') },
    { method: 'PATCH', path: '/templates/{id}', handler: update('templates') },
    { method: 'DELETE', path: '/templates/{id}', handler: remove('templates') },
]);

module.exports = {
    server, start, stop, reset, database,
    get: doGet,
    get port() {
        return server.listener.address().port;
    }
};
