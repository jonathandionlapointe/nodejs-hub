'use strict';

module.exports = function (context) {
    const names = [];
    return context.withTransactionContext(context =>
        context.database.query(`
        SELECT name
        FROM sqlite_master
        WHERE type = 'table'
        `, [], row => names.push(row.name))
        .return(names)
        .mapSeries(name =>
            context.database.query(`DROP TABLE IF EXISTS "${name}"`)));
};
