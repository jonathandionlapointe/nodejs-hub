/*global describe, it, before, after*/
'use strict';

const config = require('../lib/config');
const expect = require('chai').expect;
const mockApi = require('./mock/api');
const Promise = require('bluebird');

describe('Mock API', function () {
    it('should start', function () {
        return mockApi.start();
    });

    it('should stop', function () {
        return mockApi.stop();
    });

    function request(options) {
        return new Promise(resolve => mockApi.server.inject(options, resolve));
    }

    describe('endpoints', function () {
        before(() => mockApi.start());
        after(() => mockApi.stop());

        it('POST   /models      => 200', function () {
            return request({
                method: 'POST',
                url: `${config.system.api}/models`,
                payload: {
                    _id: 'test_model'
                }
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal({ _id: 'test_model' });
            });
        });

        it('GET    /models      => 200', function () {
            return request({
                method: 'GET',
                url: `${config.system.api}/models`,
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal([{ _id: 'test_model' }]);
            });
        });

        it('GET    /models/{id} => 200', function () {
            return request({
                method: 'GET',
                url: `${config.system.api}/models/test_model`,
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal({ _id: 'test_model' });
            });
        });

        it('PUT    /models/{id} => 200', function () {
            return request({
                method: 'PUT',
                url: `${config.system.api}/models/test_model`,
                payload: {
                    _id: 'foo',
                    name: 'Test Model',
                }
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal({ _id: 'test_model', name: 'Test Model' });
            });
        });

        it('DELETE /models/{id} => 204', function () {
            return request({
                method: 'DELETE',
                url: `${config.system.api}/models/test_model`,
            }).then(response => {
                expect(response.statusCode).to.equal(204);
            });
        });

        it('GET    /models      => 200 []', function () {
            return request({
                method: 'GET',
                url: `${config.system.api}/models`,
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal([]);
            });
        });

        it('GET    /models/{id} => 404', function () {
            return request({
                method: 'GET',
                url: `${config.system.api}/models/test_model`,
            }).then(response => {
                expect(response.statusCode).to.equal(404);
            });
        });

        it('PUT    /models/{id} => 404', function () {
            return request({
                method: 'PUT',
                url: `${config.system.api}/models/test_model`,
                payload: {}
            }).then(response => {
                expect(response.statusCode).to.equal(404);
            });
        });

        it('DELETE /models/{id} => 404', function () {
            return request({
                method: 'DELETE',
                url: `${config.system.api}/models/test_model`,
            }).then(response => {
                expect(response.statusCode).to.equal(404);
            });
        });

        it('POST   /events      => 204', function () {
            return request({
                method: 'POST',
                url: `${config.system.api}/events`,
                payload: {
                    type: 'create',
                    path: '/devices/foo',
                    resource: {
                        caps: {
                            switch: {
                                value: 'off'
                            }
                        }
                    }
                }
            }).then(response => {
                expect(response.statusCode).to.equal(204);
                expect(mockApi.database.devices).to.deep.equal([{
                    _id: 'foo', caps: { switch: { value: 'off' } }
                }]);
            });
        });

        it('events should update caps', function () {
            return request({
                method: 'POST',
                url: `${config.system.api}/events`,
                payload: {
                    type: 'update',
                    path: '/devices/foo/caps/switch',
                    resource: { value: 'on' },
                }
            }).then(response => {
                expect(response.statusCode).to.equal(204);
                expect(mockApi.database.devices).to.deep.equal([{
                    _id: 'foo', caps: { switch: { value: 'on' } }
                }]);
            });
        });

        it('should provision', function () {
            return request({
                method: 'POST',
                url: `${config.system.api}/provisioning`,
                payload: { model: 'smart_controller_v1' },
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.have.all.keys('provisioning', 'device');
                expect(response.result.provisioning.mac).to.be.a('string');
                expect(response.result.provisioning.bindingCode).to.be.a('string');
                expect(response.result.device._id).to.be.a('string');
                expect(response.result.device.token).to.be.a('string');
            });
        });

        it('should have empty user and permission collections', function () {
            return Promise.join(
                request({ method: 'GET', url: `${config.system.api}/users` }),
                request({ method: 'GET', url: `${config.system.api}/permissions` }),
                (usersResponse, permissionsResponse) => {
                    expect(usersResponse.result).to.deep.equal([]);
                    expect(permissionsResponse.result).to.deep.equal([]);
                });
        });

        it('should inject users', function () {
            mockApi.database.users.push({ username: 'joe.shmoe@example.com' });
            return request({ method: 'GET', url: `${config.system.api}/users` }).then(response => {
                expect(response.result).to.deep.equal([{ username: 'joe.shmoe@example.com' }]);
            });
        });

        it('should inject permissions', function () {
            mockApi.database.permissions.push({ status: 'ACTIVE' });
            return request({ method: 'GET', url: `${config.system.api}/permissions` }).then(response => {
                expect(response.result).to.deep.equal([{ status: 'ACTIVE' }]);
            });
        });

        it('should reset', function () {
            mockApi.reset({ devices: true, users: true, permissions: true });
            expect(mockApi.database.devices).to.deep.equal([]);
            expect(mockApi.database.users).to.deep.equal([]);
            expect(mockApi.database.permissions).to.deep.equal([]);
        });
    });
});
