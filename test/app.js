/*global describe, it, before, after*/
'use strict';

const App = require('../app');
const config = require('../lib/config');
const dropDbs = require('./util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const mockApi = require('./mock/api');
const Promise = require('bluebird');

const MODELS = [{
    _id: config.model_id
}];

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        return Promise.delay(50).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

describe('App', function () {
    this.slow(150);

    let app;

    before(function () {
        if (!fs.existsSync(config.system.file)) {
            throw new Error('no provisioning data; run the lib/system tests first');
        }
        mockApi.reset({ models: MODELS });
        return mockApi.start();
    });

    after(function () {
        mockApi.reset({ models: true, devices: true, events: true });
        return Promise.all([
            mockApi.stop(),
            dropDbs(app.context),
        ]).then(() => app.context.database.conn.close()).catchReturn();
    });

    it('should create the application instance', function () {
        app = new App();
    });

    it('should start the application', function () {
        this.slow(3500);

        return app.start({
            server: { debug: false },
            emulator: { enable: true },
            good: { enable: false },
            cloudMessaging: { enable: false },
        }).then(() => {
            expect(app.isRunning()).to.equal(true);
        });
    });

    it('should stop the application', function () {
        return app.stop().then(() => {
            expect(app.isRunning()).to.equal(false);
        });
    });

    it('should restart the application', function () {
        this.slow(1000);

        return app.start({
            server: { debug: false },
            emulator: { enable: false },
            good: { enable: false },
            cloudMessaging: { enable: false },
        });
    });

    it('should stop the application on a shutdown event', function () {
        app.server.plugins.event.settleTime = 50;
        return app.shutdown('normal').then(() => {
            return waitUntil(() => !app.isRunning());
        });
    });

    it('should not factory reset if not enabled', function () {
        this.slow(1000);

        return app.start({
            server: { debug: false },
            emulator: { enable: false },
            good: { enable: false },
            cloudMessaging: { enable: false },
            factoryReset: { enable: false },
        }).then(() => {
            app.server.plugins.system.shutdown(app.server.context(), 'factoryReset').catchReturn();
            return Promise.delay(100);
        }).then(() => {
            expect(app.isRunning()).to.equal(true);
            return app.stop();
        });
    });

    it('should factory reset if enabled', function () {
        this.slow(2000);

        return app.start({
            server: { debug: false },
            emulator: { enable: false },
            good: { enable: false },
            cloudMessaging: { enable: false },
            factoryReset: { enable: true },
        }).then(() => {
            app.server.plugins.event.settleTime = 100;
            app.server.plugins.system.shutdown(app.server.context(), 'factoryReset').catchReturn();
            return waitUntil(() => !app.isRunning());
        }).then(() => {
            expect(fs.existsSync(config.system.file)).to.equal(true);
            expect(fs.existsSync(config.system.resetFile)).to.equal(true);
            fs.unlinkSync(config.system.resetFile);
            config.reload();
        });
    });
});
