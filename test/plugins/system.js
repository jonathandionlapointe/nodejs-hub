/*global describe, it, before, after, afterEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const Promise = require('bluebird');
const System = require('../../lib/system');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}];

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        return Promise.delay(50).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

describe('System plugin', function () {
    this.slow(150);

    var context, server, shutdownSent, factoryResetSent;

    function request(tag, options) {
        options = _.merge({ headers: { authorization: 'Bearer ' + server.plugins.auth.apiToken, 'x-who': 'mock-user', 'x-who-type': 'user' } }, options);
        return new Promise(resolve => server.select(tag).inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            return server.registerAsync([
                { register: require('../../plugins/cache'), options: {} },
                { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
                { register: require('../../plugins/event'), options: {} },
                { register: require('../../plugins/auth'), options: config.auth },
            ]);
        });
    });

    after(function () {
        mockApi.reset({ events: true, models: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    afterEach(function () {
        shutdownSent = false;
        factoryResetSent = false;
    });

    it('should register', function () {
        return server.registerAsync([
            { register: require('../../plugins/system'), options: { factoryReset: { enable: true } } },
        ]);
    });

    it('should start the server', function () {
        this.slow(2000);

        return server.startAsync().then(() => { config.fixPorts(server); });
    });

    let system;
    it('should expose the system instance', function () {
        server.plugins.event.settleTime = 50;
        system = server.plugins.system;
        expect(system).to.be.instanceof(System);
        system.on('shutdown', (context, reason) => {
            shutdownSent = true;
            if (reason === 'factoryReset') {
                factoryResetSent = true;
            }
        });
    });

    it('should have provisioned the system', function () {
        expect(system.device).to.be.an('object');
        expect(system.device._id).to.be.a('string');
    });

    it('should generate a heartbeat', function () {
        this.slow(500);

        return request('internal', {
            method: 'POST',
            url: `http://127.0.0.1:${config.server.internalPort}/heartbeat`,
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result).to.contain.all.keys(
                'version', 'cpus', 'filesystem', 'hostname',
                'loadavg', 'memory', 'network'
            );
        });
    });

    it('should get system information', function () {
        return request('api', {
            method: 'GET',
            url: `http://127.0.0.1:${config.server.port}/`,
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result).to.contain.all.keys('name', 'model', '_id');
            expect(response.result._id).to.equal(system.device._id);
        });
    });

    it('should get the system device', function () {
        return request('api', {
            method: 'GET',
            url: `http://127.0.0.1:${config.server.port}/devices/${system.device._id}`,
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result).to.contain.all.keys('name', 'model', '_id');
            expect(response.result._id).to.equal(system.device._id);
        });
    });

    it('should update the system device', function () {
        return request('api', {
            method: 'PUT',
            url: `http://127.0.0.1:${config.server.port}/devices/${system.device._id}`,
            payload: {
                location: 'foo',
            },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result).to.contain.all.keys('name', 'model', '_id', 'location');
            expect(response.result._id).to.equal(system.device._id);
            expect(response.result.location).to.equal('foo');
        });
    });

    it('should reset the system', function () {
        return request('api', {
            method: 'POST',
            url: `http://127.0.0.1:${config.server.port}/reset`,
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            expect(shutdownSent).to.equal(true);
        });
    });

    it('should factory reset the system', function () {
        return request('api', {
            method: 'POST',
            url: `http://127.0.0.1:${config.server.port}/factory_reset`,
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntil(() => factoryResetSent, 1000);
        });
    });
});
