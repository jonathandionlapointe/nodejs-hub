/*global describe, it, before, after, beforeEach*/
'use strict';

const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}];

const USERS = [{
    _id: 'mock-user',
    username: 'mock@example.com'
}];

describe('Auth plugin', function () {
    this.slow(150);

    var context, server;

    function request(tag, options) {
        return new Promise(resolve => server.select(tag).inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({
            models: MODELS,
            users: USERS,
        });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => server.registerAsync([
            { register: require('../../plugins/cache'), options: {} },
            { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
            { register: require('../../plugins/system'), options: {} },
            { register: require('../../plugins/event'), options: {} },
        ]));
    });

    after(function () {
        mockApi.reset({ models: true, devices: true, users: true, permissions: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    beforeEach(function () {
        mockApi.reset({ events: true });
    });

    it('should register', function () {
        return server.registerAsync([{ register: require('../../plugins/auth'), options: config.auth }]);
    });

    it('should start the server', function () {
        this.slow(1600);

        return server.startAsync().then(() => { config.fixPorts(server); })
        .then(() => {
            mockApi.database.permissions.push({
                resource: server.context().gatewayId,
                type: 'gateway',
                who: USERS[0]._id,
                whoType: 'user',
                status: 'ACTIVE',
                read: true, write: true, share: true, manage: true
            });
        });
    });

    function endpoint(path) {
        return `http://127.0.0.1:${config.server.port}${path}`;
    }

    let nonce;
    it('should get a login nonce', function () {
        this.slow(400);

        return request('api', {
            url: endpoint(`/devices/${server.context().gatewayId}/auth/nonce`),
            method: 'POST',
            headers: {
                authorization: 'Bearer ' + server.plugins.auth.apiToken,
                'x-who': USERS[0]._id,
                'x-who-type': 'user',
            },
            payload: {
                user: USERS[0]._id,
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            nonce = response.result.nonce;
            expect(nonce).to.be.a('string');
            expect(nonce).to.not.be.empty;
        });
    });

    let token;
    it('should login', function () {
        expect(nonce).to.be.a('string');
        return request('api', {
            url: endpoint('/auth/login'),
            method: 'POST',
            headers: {
                authorization: 'Basic ' +
                    new Buffer(USERS[0].username + ':' + nonce, 'ascii')
                    .toString('base64')
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            token = response.result.token;
            expect(token).to.be.a('string');
            expect(token).to.not.be.empty;
        });
    });

    it('should read own user', function () {
        expect(token).to.be.a('string');
        return request('api', {
            url: endpoint(`/users/${USERS[0].username}`),
            method: 'GET',
            headers: { authorization: 'Bearer ' + token },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
        });
    });

    it('should not read user with an invalid (non-verifiable) token', function () {
        return request('api', {
            url: endpoint(`/users/${USERS[0].username}`),
            method: 'GET',
            headers: { authorization: 'Bearer foo' },
        }).then(response => {
            expect(response.statusCode).to.equal(400);
        });
    });

    it('should not read user with an invalid (verifiable) token', function () {
        return server.plugins.auth.deviceLogin('not-existing').then(invalidToken => {
            return request('api', {
                url: endpoint(`/users/${USERS[0].username}`),
                method: 'GET',
                headers: { authorization: 'Bearer ' + invalidToken },
            });
        }).then(response => {
            expect(response.statusCode).to.equal(403);
        });
    });

    it('should read the root endpoint', function () {
        expect(token).to.be.a('string');
        return request('api', {
            url: endpoint('/'),
            method: 'GET',
            headers: { authorization: 'Bearer ' + token }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
        });
    });

    it('should not read root endpoint with an invalid (non-verifiable) token', function () {
        return request('api', {
            url: endpoint('/'),
            method: 'GET',
            headers: { authorization: 'Bearer foo' }
        }).then(response => {
            expect(response.statusCode).to.equal(400);
        });
    });

    it('should not read nonce endpoint with an invalid (verifiable) token', function () {
        return server.plugins.auth.deviceLogin('not-existing').then(invalidToken => {
            return request('api', {
                url: endpoint(`/devices/${server.context().gatewayId}/auth/nonce`),
                method: 'GET',
                headers: { authorization: 'Bearer ' + invalidToken }
            });
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });
});
