/*global describe, it, before, after, afterEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const Device = require('../../lib/device');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}, {
    _id: 'multi_sensor',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'motion',
        type: 'enum',
        methods: ['get'],
        range: ['idle', 'detected']
    }, {
        capabilityId: 'temperature',
        type: 'float',
        methods: ['get'],
        units: ['C'],
        range: [
            { value: -32, unit: 'C' },
            { value: 100, unit: 'C' }
        ]
    }]
}, {
    _id: 'light_switch',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'switch',
        type: 'enum',
        methods: ['get','put'],
        range: ['off', 'on']
    }]
}, {
    _id: 'heater_thermostat',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'heating',
        type: 'float',
        methods: ['get','put'],
        units: ['C'],
        range: [
            { value: 16, unit: 'C' },
            { value: 28, unit: 'C' }
        ]
    }]
}];

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

function unexpected() {
    throw new Error('unexpected success');
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        return new Promise(setImmediate).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

function waitUntilEventCount(n, timeout) {
    return waitUntil(() => mockApi.database.events.length >= n, timeout);
}

describe('Scenario plugin', function () {
    this.slow(150);

    var context, server, originalCooldown;

    function request(options) {
        if (typeof options === 'string') {
            options = { url: options, method: 'GET' };
        }
        options = _.merge({ headers: { authorization: 'Bearer ' + server.plugins.auth.apiToken, 'x-who': 'mock-user', 'x-who-type': 'user' } }, options);
        return new Promise(resolve => server.select('api').inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        originalCooldown = config.scenario.cooldown.minimum;
        config.scenario.cooldown.minimum = 0;

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            return server.registerAsync([
                { register: require('../../plugins/cache'), options: {} },
                { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
                { register: require('../../plugins/system'), options: {} },
                { register: require('../../plugins/event'), options: {} },
                { register: require('../../plugins/auth'), options: config.auth },
                { register: require('../../plugins/device'), options: {} },
                { register: require('../../plugins/emu') },
            ]);
        });
    });

    after(function () {
        config.scenario.cooldown.minimum = originalCooldown;
        mockApi.reset({ models: true, devices: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    afterEach(function () {
        mockApi.reset({ events: true });
    });

    it('should register', function () {
        return server.registerAsync([
            { register: require('../../plugins/scenario'), options: {} },
        ]);
    });

    it('should start the server', function () {
        this.slow(2000);

        return server.startAsync().then(() => { config.fixPorts(server); })
        .then(() => permission.upsertGroups(context, [{
            _id: server.plugins.system.group,
            members: [
                { whoType: 'user', who: 'mock-user' },
                { whoType: 'gateway', who: server.plugins.system.device._id },
            ],
            children: [],
        }]));
    });

    let multiSensorId, lightSwitchId, heaterId, multiSensor2Id, lightSwitch2Id, lightSwitch3Id;
    it('should create devices', function () {
        this.slow(800);

        return server.context().withTransactionContext(context => Promise.mapSeries([
            [Device.add, [context, 'multi_sensor', {}]],
            [Device.add, [context, 'light_switch', {}]],
            [Device.add, [context, 'heater_thermostat', {}]],
            [Device.add, [context, 'multi_sensor', {}]],
            [Device.add, [context, 'light_switch', {}]],
            [Device.add, [context, 'light_switch', {}]],
        ], (funargs => funargs[0].apply(null, funargs[1]))))
        .tap(() => waitUntilEventCount(3))
        .spread((multiSensor, lightSwitch, heater, multiSensor2, lightSwitch2, lightSwitch3) => {
            multiSensorId = multiSensor[0]._id;
            lightSwitchId = lightSwitch[0]._id;
            heaterId = heater[0]._id;
            multiSensor2Id = multiSensor2[0]._id;
            lightSwitch2Id = lightSwitch2[0]._id;
            lightSwitch3Id = lightSwitch3[0]._id;
            expect(multiSensorId).to.be.a('string');
            expect(lightSwitchId).to.be.a('string');
            expect(heaterId).to.be.a('string');
            expect(multiSensor2Id).to.be.a('string');
            expect(lightSwitch2Id).to.be.a('string');
            expect(lightSwitch3Id).to.be.a('string');
        });
    });

    let schedules;
    it('should create schedules', function () {
        this.slow(1000);

        return Promise.mapSeries(['before 1999 year', 'after 1999 year', 'after 9 hour and before 17 hour'], schedule => request({
            method: 'POST',
            url: endpoint('/schedules'),
            payload: {
                name: schedule,
                schedules: schedule,
                timezone: 'America/Montreal',
                occurrenceType: 'range',
                frequency: 'once',
            },
        })).map(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.be.a('string');
            return response.result._id;
        }).then(ids => {
            schedules = { before1999: ids[0], after1999: ids[1], from9to5: ids[2] };
            return waitUntilEventCount(3);
        });
    });

    it('should get a specific schedule', function () {
        return request({
            method: 'GET',
            url: endpoint(`/schedules/${schedules.from9to5}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(schedules.from9to5);
            expect(response.result.schedules).to.equal('after 9 hour and before 17 hour');
        });
    });

    it('should update a schedule', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/schedules/${schedules.from9to5}`),
            payload: {
                name: '9 to 5',
                schedules: 'after 9:00 am and before 5:00 pm',
                timezone: 'America/Montreal',
                occurrenceType: 'range',
                frequency: 'once',
            },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(schedules.from9to5);
            expect(response.result.name).to.equal('9 to 5');
            expect(response.result.schedules).to.equal('after 9:00 am and before 5:00 pm');
            return waitUntilEventCount(1);
        });
    });

    it('should delete a schedule', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/schedules/${schedules.from9to5}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should list schedules', function () {
        return request({
            method: 'GET',
            url: endpoint('/schedules'),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(2);
            expect(response.result[0]._id).to.equal(schedules.before1999);
            expect(response.result[1]._id).to.equal(schedules.after1999);
        });
    });

    it('should not create a schedule with an invalid timezone', function () {
        return request({
            method: 'POST',
            url: endpoint('/schedules'),
            payload: {
                name: 'invalid timezone',
                schedules: 'after 9:00 am and before 5:00 pm',
                timezone: 'quux',
                occurrenceType: 'range',
                frequency: 'once',
            }
        }).then(response => {
            expect(response.statusCode).to.equal(400);
            expect(response.result).to.be.an('object');
            expect(response.result.message).to.match(/unknown timezone/i);
        });
    });

    let enumRuleId;
    it('should create a scenario on an enum cap', function () {
        this.slow(300);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                cooldown: 5,
                name: 'Motion -> Turn On Light',
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'motion' },
                    condition: {
                        type: 'exact',
                        value: 'detected'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.be.a('string');
            expect(response.result.name).to.equal('Motion -> Turn On Light');
            expect(response.result.cooldown).to.equal(5);
            enumRuleId = response.result._id;
            return waitUntilEventCount(1);
        });
    });

    it('should read a scenario', function () {
        return request({
            method: 'GET',
            url: endpoint(`/scenarios/${enumRuleId}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.type).to.equal('rule');
            expect(response.result._id).to.equal(enumRuleId);
        });
    });

    it('should fail to read a non-existing scenario', function () {
        return request({
            method: 'GET',
            url: endpoint('/scenarios/this_rule_does_not_exist'),
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });

    let floatRuleId;
    it('should create a numerical trigger rule', function () {
        this.slow(300);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                cooldown: 0,
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'temperature' },
                    condition: {
                        type: '<',
                        value: 18,
                        unit: 'C'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: heaterId,
                    capability: 'heating',
                    value: 21,
                    unit: 'C'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.be.a('string');
            expect(response.result.cooldown).to.equal(0);
            floatRuleId = response.result._id;
            return waitUntilEventCount(1);
        });

    });

    let sequenceRuleId;
    it('should create a scenario with a sequenced action', function () {
        this.slow(300);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                cooldown: 0,
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensor2Id },
                    capability: { type: 'exact', value: 'motion' },
                    condition: { type: 'exact', value: 'detected' }
                },
                action: {
                    type: 'sequence',
                    actions: [{
                        type: 'setCapability',
                        device: lightSwitch2Id,
                        capability: 'switch',
                        value: 'on'
                    }, {
                        type: 'setCapability',
                        device: lightSwitch3Id,
                        capability: 'switch',
                        value: 'on'
                    }]
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.be.a('string');
            sequenceRuleId = response.result._id;
            return waitUntilEventCount(1);
        });
    });

    it('should list all scenarios', function () {
        return request({
            method: 'GET',
            url: endpoint('/scenarios'),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(3);
            expect(response.result[0]).to.be.an('object');
            expect(response.result[0].type).to.equal('rule');
            expect(response.result[1]).to.be.an('object');
            expect(response.result[1].type).to.equal('rule');
            expect(response.result[2]).to.be.an('object');
            expect(response.result[2].type).to.equal('rule');

            const responseIds = [response.result[0]._id, response.result[1]._id, response.result[2]._id];
            const createdIds = [enumRuleId, floatRuleId, sequenceRuleId];
            expect(responseIds.sort()).to.deep.equal(createdIds.sort());
        });
    });

    it('should trigger an enum scenario', function () {
        this.slow(1200);

        return Device.load(context, multiSensorId).then(device => {
            device.updateCap(context, 'motion', 'detected');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(3)
            ]).return(device);
        }).then(device => {
            device.updateCap(context, 'motion', 'idle');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(4)
            ]).then(() => Device.load(context, lightSwitchId));
        }).then(device => {
            expect(device.caps.switch.value).to.equal('on');
            expect(mockApi.database.events[0].path).to.equal(`/devices/${multiSensorId}/caps/motion`);
            expect(mockApi.database.events[1].path).to.equal(`/scenarios/${enumRuleId}`);
            expect(mockApi.database.events[2].path).to.equal(`/devices/${lightSwitchId}/caps/switch`);
            expect(mockApi.database.events[3].path).to.equal(`/devices/${multiSensorId}/caps/motion`);

            device.updateCap(context, 'switch', 'off');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(5)
            ]);
        });
    });

    it('should not activate a scenario on cooldown', function () {
        this.slow(1500);

        return Device.load(context, multiSensorId).then(device => {
            device.updateCap(context, 'motion', 'detected');
            device.updateCap(context, 'motion', 'idle');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(2)
            ]).then(() => {
                return waitUntilEventCount(3, 500);
            }).then(unexpected).catch(Promise.TimeoutError, error => {
                // OK, we should not hit event count of three.
            });
        });
    });

    it('should update a scenario', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/scenarios/${enumRuleId}`),
            payload: {
                enabled: false,
                cooldown: 0,
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'temperature' },
                    condition: {
                        type: '<',
                        value: 18,
                        unit: 'C'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: heaterId,
                    capability: 'heating',
                    value: 21,
                    unit: 'C'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.enabled).to.equal(false);
            return waitUntilEventCount(1);
        });
    });

    it('should create an invalid scenario', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/scenarios/${enumRuleId}`),
            payload: {
                enabled: false,
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: '000000000000000000000000' },
                    capability: { type: 'exact', value: 'temperature' },
                    condition: {
                        type: '<',
                        value: 18,
                        unit: 'C'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: '000000000000000000000000',
                    capability: 'heating',
                    value: 21,
                    unit: 'C'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.enabled).to.equal(false);
            expect(response.result.errors).to.be.an('array');
            expect(response.result.errors.length).to.equal(2);
            expect(response.result.errors[0].statusCode).to.equal(404);
            expect(response.result.errors[1].statusCode).to.equal(404);
            return waitUntilEventCount(1);
        });
    });

    it('should not trigger a disabled scenario', function () {
        this.slow(1500);

        return Device.load(context, multiSensorId).then(device => {
            device.updateCap(context, 'motion', 'detected');
            device.updateCap(context, 'motion', 'idle');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(2)
            ]).then(() => {
                return waitUntilEventCount(3, 500);
            }).then(unexpected).catch(Promise.TimeoutError, error => {
                // OK, we should not hit event count of three.
            });
        });
    });

    it('should trigger a numerical scenario', function () {
        this.slow(700);

        return Device.load(context, multiSensorId).then(device => {
            device.updateCap(context, 'temperature', 16, 'C');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(3)
            ]);
        }).then(() => {
            expect(mockApi.database.events[0].path).to.equal(`/devices/${multiSensorId}/caps/temperature`);
            expect(mockApi.database.events[1].path).to.equal(`/scenarios/${floatRuleId}`);
            expect(mockApi.database.events[2].path).to.equal(`/devices/${heaterId}/caps/heating`);
            expect(mockApi.database.events[2].resource.value).to.equal(21);
            expect(mockApi.database.events[2].resource.unit).to.equal('C');
        });
    });

    it('should trigger a sequence of actions', function () {
        this.slow(700);

        return Device.load(context, multiSensor2Id).then(device => {
            device.updateCap(context, 'motion', 'detected');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(4)
            ]);
        }).then(() => {
            expect(mockApi.database.events[0].path).to.equal(`/devices/${multiSensor2Id}/caps/motion`);
            expect(mockApi.database.events[1].path).to.equal(`/scenarios/${sequenceRuleId}`);
            expect(mockApi.database.events[2].path).to.equal(`/devices/${lightSwitch2Id}/caps/switch`);
            expect(mockApi.database.events[2].resource.value).to.equal('on');
            expect(mockApi.database.events[3].path).to.equal(`/devices/${lightSwitch3Id}/caps/switch`);
            expect(mockApi.database.events[3].resource.value).to.equal('on');
        });
    });

    it('should prevent scenario action due to a schedule', function () {
        this.slow(800);

        return request({
            method: 'PUT',
            url: endpoint(`/scenarios/${enumRuleId}`),
            payload: {
                name: 'Motion -> Turn On Light',
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'motion' },
                    condition: {
                        type: 'exact',
                        value: 'detected'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                },
                schedule: schedules.before1999,
            }
        }).then(() => Device.load(context, multiSensorId)).then(device => {
            device.updateCap(context, 'motion', 'detected');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(2)
            ]).return(device);
        }).then(device => {
            device.updateCap(context, 'motion', 'idle');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(3)
            ]).then(() => Device.load(context, lightSwitchId));
        }).then(device => {
            expect(device.caps.switch.value).to.equal('off');
            expect(mockApi.database.events[1].path).to.equal(`/devices/${multiSensorId}/caps/motion`);
            expect(mockApi.database.events[2].path).to.equal(`/devices/${multiSensorId}/caps/motion`);
        });
    });

    it('should apply a valid schedule to a scenario', function () {
        this.slow(1500);

        return request({
            method: 'PUT',
            url: endpoint(`/scenarios/${enumRuleId}`),
            payload: {
                name: 'Motion -> Turn On Light',
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'motion' },
                    condition: {
                        type: 'exact',
                        value: 'detected'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                },
                schedule: schedules.after1999,
            }
        }).then(() => Device.load(context, multiSensorId)).then(device => {
            device.updateCap(context, 'motion', 'detected');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(4)
            ]).return(device);
        }).then(device => {
            device.updateCap(context, 'motion', 'idle');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(5)
            ]).then(() => Device.load(context, lightSwitchId));
        }).then(device => {
            expect(device.caps.switch.value).to.equal('on');
            expect(mockApi.database.events[1].path).to.equal(`/devices/${multiSensorId}/caps/motion`);
            expect(mockApi.database.events[2].path).to.equal(`/scenarios/${enumRuleId}`);
            expect(mockApi.database.events[3].path).to.equal(`/devices/${lightSwitchId}/caps/switch`);
            expect(mockApi.database.events[4].path).to.equal(`/devices/${multiSensorId}/caps/motion`);

            device.updateCap(context, 'switch', 'off');
            return Promise.all([
                device.save(context),
                waitUntilEventCount(6)
            ]);
        });
    });

    it('should delete a scenario', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/scenarios/${enumRuleId}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should fail to update a non-existing scenario', function () {
        return request({
            method: 'PUT',
            url: endpoint(`/scenarios/${enumRuleId}`),
            payload: {
                enabled: true,
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'temperature' },
                    condition: {
                        type: '<',
                        value: 18,
                        unit: 'C'
                    }
                },
                action: {
                    type: 'setCapability',
                    device: heaterId,
                    capability: 'heating',
                    value: 21,
                    unit: 'C'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });

    it('should fail to delete a non-existing scenario', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/scenarios/${enumRuleId}`),
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });

    it('should trigger alarm system status scenarios', function () {
        this.slow(1000);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: {
                    type: 'alarmSystemStatus',
                    alarmSystem: { type: 'always' },
                    alarmSystemStatus: { type: 'exact', value: 'disarmed' }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return context.emitEvent({
                type: 'update',
                path: '/alarms/foo',
                resource: { status: 'disarmed' }
            });
        }).then(() => {
            return waitUntilEventCount(4);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('on');
        });
    });

    it('should trigger alarm system status scenarios on a specific alarm system', function () {
        this.slow(1000);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: {
                    type: 'alarmSystemStatus',
                    alarmSystem: { type: 'exact', value: 'bar' },
                    alarmSystemStatus: { type: 'not', condition: { type: 'exact', value: 'disarmed' } }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'off'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return context.emitEvent({
                type: 'update',
                path: '/alarms/bar',
                resource: { status: 'armed' }
            });
        }).then(() => {
            return waitUntilEventCount(4);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('off');
        });
    });

    it('should trigger zone status scenarios on a specific zone', function () {
        this.slow(1000);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: {
                    type: 'zoneStatus',
                    alarmSystem: { type: 'exact', value: 'bar' },
                    zone: { type: 'exact', value: 'quux' },
                    zoneStatus: { type: 'exact', value: 'alarm' }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return context.emitEvent({
                type: 'update',
                path: '/alarms/bar/zones/quux',
                resource: { zoneStatus: 'alarm' }
            });
        }).then(() => {
            return waitUntilEventCount(4);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('on');
        });
    });

    it('should trigger zone status scenarios', function () {
        this.slow(1000);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: {
                    type: 'zoneStatus',
                    alarmSystem: { type: 'always' },
                    zone: { type: 'always' },
                    zoneStatus: { type: 'exact', value: 'alarm' }
                },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'off'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return context.emitEvent({
                type: 'update',
                path: '/alarms/foo/zones/xyz',
                resource: { zoneStatus: 'alarm' }
            });
        }).then(() => {
            return waitUntilEventCount(4);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('off');
        });
    });

    it('should trigger startup scenarios', function () {
        this.slow(600);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: { type: 'startup' },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'on'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return Promise.all([
                server.plugins.system.emitAllAsync('startup', context),
                waitUntilEventCount(3)
            ]);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('on');
        });
    });

    it('should trigger shutdown scenarios', function () {
        this.slow(600);

        server.plugins.event.settleTime = 50;
        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: { type: 'shutdown' },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'off'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        }).then(() => {
            return Promise.all([
                server.plugins.system.shutdown(context, 'normal'),
                waitUntilEventCount(3)
            ]);
        }).then(() => Device.load(context, lightSwitchId)).then(device => {
            expect(device.caps.switch.value).to.equal('off');
        });
    });

    it('should create a scenario with a schedule trigger', function () {
        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: { type: 'schedule', schedule: schedules.before1999 },
                action: {
                    type: 'setCapability',
                    device: lightSwitchId,
                    capability: 'switch',
                    value: 'off'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result.trigger.schedule).to.be.a('string');
            expect(response.result.trigger.schedule).to.equal(schedules.before1999);
            return waitUntilEventCount(1);
        });
    });

    it('should validate a recursive condition', function () {
        this.slow(300);

        return request({
            method: 'POST',
            url: endpoint('/scenarios'),
            payload: {
                trigger: {
                    type: 'capability',
                    device: { type: 'exact', value: multiSensorId },
                    capability: { type: 'exact', value: 'temperature' },
                    condition: {
                        type: 'all',
                        conditions: [{
                            type: '<',
                            value: 18,
                            unit: 'C'
                        }, {
                            type: '>',
                            value: 0,
                            unit: 'C'
                        }]
                    }
                },
                action: {
                    type: 'setCapability',
                    device: heaterId,
                    capability: 'heating',
                    value: 21,
                    unit: 'C'
                }
            }
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        });
    });
});
