/*global describe, it, before, after*/
'use strict';

const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const Hapi = require('hapi');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

describe('Cache plugin', function () {
    this.slow(150);

    var context, server;

    before(function () {
        server = new Hapi.Server({ debug: false });
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        context = app.context.createServerContext(server);
        return dropDbs(context);
    });

    after(function (done) {
        server.stop(() => {
            return dropDbs(context).asCallback(done);
        });
    });

    it('should register', function (done) {
        server.register([
            { register: require('../../plugins/cache'), options: {} }
        ], done);
    });

    it('should start the server', function (done) {
        this.slow(500);

        server.start((err) => {
            if (err) return done(err);
            config.fixPorts(server);
            done();
        });
    });

    it('should expose a caching get', function () {
        expect(server.plugins.cache.get).to.be.a('function');
    });

    it('should call the fulfill callback to fetch a new entry', function () {
        let called = false;
        function fulfill() {
            called = true;
            return Promise.resolve({ _id: 'foo', value: 42 });
        }
        return server.plugins.cache.get(context, 'test', 'foo', fulfill).then(result => {
            expect(called).to.equal(true);
            expect(result.value).to.equal(42);
        });
    });

    it('should not call the fulfill callback for an existing entry', function () {
        function fulfill() {
            return Promise.reject(new Error('should not be called'));
        }
        return server.plugins.cache.get(context, 'test', 'foo', fulfill).then(result => {
            expect(result.value).to.equal(42);
        });
    });

    it('should always call the fulfill callback if noCache is specified', function () {
        let called = false;
        function fulfill() {
            called = true;
            return Promise.resolve({ _id: 'foo', value: 0 });
        }
        return server.plugins.cache.get(context, 'test', 'foo', fulfill, { noCache: true }).then(result => {
            expect(called).to.equal(true);
            expect(result.value).to.equal(0);
        });
    });

    it('should expire entries', function () {
        this.slow(1000);

        let count = 0;
        function fulfill() {
            count += 1;
            return Promise.resolve({ _id: 'bar', value: count });
        }
        function get() {
            return server.plugins.cache.get(context, 'test', 'bar', fulfill, { lifetime: 0.1 });
        }
        return get().then(result => {
            expect(result.value).to.equal(1);
            return get();
        }).then(result => {
            expect(result.value).to.equal(1);
            return Promise.delay(300).then(get);
        }).then(result => {
            expect(result.value).to.equal(2);
        });
    });
});
