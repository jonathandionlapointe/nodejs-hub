/*global describe, it, before, after, afterEach*/
'use strict';

const config = require('../../lib/config');
const context = require('../../lib/context');
const Device = require('../../lib/device');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const permission = require('../../lib/permission');
const Promise = require('bluebird');
const SockJS = require('sockjs-client');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}, {
    _id: 'mock-sensor',
    capabilities: [{
        type: 'enum',
        capabilityId: 'sensor',
        methods: ['get'],
        range: ['idle', 'detected'],
    }]
}];

const USERS = [{
    _id: 'mock-user',
    username: 'mock@example.com'
}];

let DEVICES = [{
    model: 'mock-sensor',
    isEmulated: true,
}];

const NO_PERM_DEVICE = {
    model: 'mock-sensor',
    isEmulated: true,
    protocolName: 'emu',
};

describe('SockJS plugin', function () {
    this.slow(150);

    var context, server;

    function request(options) {
        return new Promise(resolve => server.select('api').inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({
            models: MODELS,
            users: USERS,
        });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => server.registerAsync([
            { register: require('../../plugins/cache'), options: {} },
            { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
            { register: require('../../plugins/system'), options: {} },
            { register: require('../../plugins/event'), options: {} },
            { register: require('../../plugins/auth'), options: config.auth },
            { register: require('../../plugins/device'), options: {} },
            { register: require('../../plugins/emu') },
        ]));
    });

    after(function () {
        mockApi.reset({ models: true, devices: true, users: true, permissions: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    afterEach(function () {
        mockApi.reset({ events: true });
    });

    it('should register', function () {
        return server.registerAsync([{ register: require('../../plugins/sockjs') }]);
    });

    it('should start the server', function () {
        this.slow(2000);

        return server.startAsync().then(() => { config.fixPorts(server); })
        .then(() => {
            mockApi.database.permissions.push({
                resource: server.plugins.system.device._id,
                type: 'gateway',
                who: USERS[0]._id,
                whoType: 'user',
                status: 'ACTIVE',
                read: true, write: true, share: true, manage: true
            });
            return permission.upsertGroups(context, [{
                _id: server.plugins.system.group,
                members: [
                    { whoType: 'user', who: USERS[0]._id },
                    { whoType: 'gateway', who: server.plugins.system.device._id },
                ],
                children: [],
            }]);
        });
    });

    function endpoint(path) {
        return `http://127.0.0.1:${config.server.port}${path}`;
    }

    let token;
    it('should get a login token', function () {
        this.slow(500);

        return request({
            url: endpoint(`/devices/${server.plugins.system.device._id}/auth/nonce`),
            method: 'POST',
            headers: {
                authorization: 'Bearer ' + server.plugins.auth.apiToken,
                'x-who': USERS[0]._id,
                'x-who-type': 'user',
            },
            payload: {
                user: USERS[0]._id,
            }
        }).then(response => {
            const nonce = response.result.nonce;
            return request({
                url: endpoint('/auth/login'),
                method: 'POST',
                headers: {
                    authorization: 'Basic ' +
                        new Buffer(USERS[0].username + ':' + nonce, 'ascii')
                        .toString('base64')
                }
            });
        }).then(response => {
            token = response.result.token;
            expect(token).to.be.a('string');
        });
    });

    it('should create test devices', function () {
        this.slow(1500);

        return Promise.map(DEVICES, device => request({
            url: endpoint('/devices'),
            method: 'POST',
            headers: {
                authorization: 'Bearer ' + token,
            },
            payload: device,
        })).map(response => {
            expect(response.result._id).to.be.a('string');
            return response.result;
        }).then(devices => {
            DEVICES = devices;
            return Device.add(context, NO_PERM_DEVICE.model, NO_PERM_DEVICE);
        }).then(devices => {
            expect(devices.length).to.equal(1);
            expect(devices[0]._id).to.be.a('string');
            NO_PERM_DEVICE._id = devices[0]._id;
            return permission.upsert(context, {
                whoType: 'user',
                who: USERS[0]._id,
                type: 'device',
                resource: devices[0]._id,
                read: false, write: false, share: false, manage: false,
                status: 'ACTIVE',
            });
        });
    });

    describe('socket connection', function () {
        let sock;

        afterEach(function () {
            sock.onopen = () => {};
            sock.onmessage = () => {};
            sock.onclose = () => {
                throw new Error('socket closed');
            };
        });

        it('should open', function (done) {
            this.timeout(500);
            sock = new SockJS(endpoint('/gw'));
            sock.onopen = () => done();
        });

        it('should not receive events before authenticated', function (done) {
            this.slow(5000);

            const timeoutId = setTimeout(function () {
                done();
            }, 2000);
            sock.onmessage = function (event) {
                clearTimeout(timeoutId);
                done(new Error('event received'));
            };
            Device.load(context, DEVICES[0]._id).then(device => {
                return device.updateCap(context, 'sensor', 'detected')
                .then(result => {
                    expect(result).to.equal(true);
                    return device.save(context);
                });
            }).catch(error => {
                clearTimeout(timeoutId);
                sock.onmessage = () => {};
                done(error);
            });
        });

        it('should authenticate', function (done) {
            this.timeout(500);
            sock.onmessage = function (event) {
                try {
                    const message = JSON.parse(event.data);
                    expect(message.ready).to.equal(true);
                    done();
                } catch (error) {
                    done(error);
                }
            };
            sock.onclose = function () {
                done(new Error('socket closed'));
            };
            sock.send(JSON.stringify({ token }));
        });

        it('should receive events for a device associated with the user', function (done) {
            this.timeout(1000);
            sock.onmessage = function (event) {
                done();
            };
            context.withTransactionContext(context => {
                return Device.load(context, DEVICES[0]._id).then(device => {
                    return device.updateCap(context, 'sensor', 'idle')
                    .then(result => {
                        expect(result).to.equal(true);
                        return device.save(context);
                    });
                });
            }).catch(error => {
                sock.onmessage = () => {};
                done(error);
            });
        });

        it('should not receive events for non-associated devices', function (done) {
            this.slow(5000);

            const timeoutId = setTimeout(function () {
                done();
            }, 2000);
            sock.onmessage = function (event) {
                clearTimeout(timeoutId);
                done(new Error('event received'));
            };
            context.withTransactionContext(context => {
                return Device.load(context, NO_PERM_DEVICE._id).then(device => {
                    return device.updateCap(context, 'sensor', 'detected')
                    .then(result => {
                        expect(result).to.equal(true);
                        return device.save(context);
                    });
                });
            }).catch(error => {
                clearTimeout(timeoutId);
                sock.onmessage = () => {};
                done(error);
            });
        });

        it('should close the connection', function (done) {
            sock.onclose = () => done();
            sock.close();
        });

        it('should close the connection if authentication is invalid', function (done) {
            this.timeout(1000);
            sock = new SockJS(endpoint('/gw'));
            sock.onopen = function () {
                sock.onmessage = function (event) {
                    try {
                        const data = JSON.parse(event.data);
                        expect(data.error).to.equal('not_authorized');
                    } catch (error) {
                        done(error);
                    }
                };
                sock.onclose = () => done();
                sock.send(JSON.stringify({ token: 'foo' }));
            };
        });

    });

});
