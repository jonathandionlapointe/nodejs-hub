/*global describe, it, before, after*/
'use strict';

const SDK = require('@2klic/2klic-sdk');
const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const Promise = require('bluebird');
const request = require('request');

const app = {};
app.context = context.createAppContext(app);

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

describe('SDK plugin', function () {
    this.slow(150);

    var context, server;

    before(function (done) {
        server = new Hapi.Server({ debug: false });
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        context = app.context.createServerContext(server);
        mockApi.database.models.push({
            _id: 'generic_z_wave_device',
        });
        dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            server.register([
                { register: require('../../plugins/cache'), options: {} },
            ], done);
        }).catch(done);
    });

    after(function (done) {
        server.stop(() => {
            mockApi.reset({ models: true });
            return Promise.all([
                mockApi.stop(),
                dropDbs(context),
            ]).catchReturn().then(() => done());
        });
    });

    it('should register', function (done) {
        server.register([
            { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
        ], done);
    });

    it('should start the server', function (done) {
        this.slow(500);

        server.start((err) => {
            if (err) return done(err);
            config.fixPorts(server);
            done();
        });
    });

    it('should expose an SDK instance', function () {
        expect(server.plugins.sdk).to.be.instanceof(SDK);
    });

    it('should augment the SDK with a cached model getter', function () {
        expect(server.plugins.sdk.models.getCached).to.be.a('function');
    });

    describe('endpoints', function () {
        it('GET /models', function (done) {
            request.get(endpoint('/models'), done);
        });

        it('GET /models/{id}', function (done) {
            request.get(endpoint('/models/generic_z_wave_device'), (error, response) => {
                try {
                    expect(error).to.equal(null);
                    expect(response.statusCode).to.equal(200);
                    expect(JSON.parse(response.body)).to.be.an('object');
                    done();
                } catch (error) {
                    done(error);
                }
            });
        });

        it('GET /models/{id} (failure)', function (done) {
            request.get(endpoint('/models/this_device_does_not_exist'), (error, response) => {
                try {
                    expect(error).to.equal(null);
                    expect(response.statusCode).to.equal(404);
                    done();
                } catch (error) {
                    done(error);
                }
            });
        });
    });
});
