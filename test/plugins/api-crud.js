/*global describe, it, before, after, beforeEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}];

const LOCATIONS = [
    { _id: 'location-1', name: 'Location 1' },
    { _id: 'location-2', name: 'Location 2' },
    { _id: 'location-3', name: 'Location 3' },
];

const TEMPLATES = [
    { _id: 'template-1', name: 'Template 1' },
    { _id: 'template-2', name: 'Template 2' },
    { _id: 'template-3', name: 'Template 3' },
];

function unexpected() {
    throw new Error('unexpected success');
}

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

describe('API CRUD plugin', function () {
    this.slow(150);

    var context, server;

    function request(options) {
        if (typeof options === 'string') {
            options = { url: options, method: 'GET' };
        }
        options = _.merge({ headers: { authorization: 'Bearer ' + server.plugins.auth.apiToken, 'x-who': 'mock-user', 'x-who-type': 'user' } }, options);
        return new Promise(resolve => server.select('api').inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => server.registerAsync([
            { register: require('../../plugins/cache'), options: {} },
            { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
            { register: require('../../plugins/system'), options: {} },
            { register: require('../../plugins/event'), options: {} },
            { register: require('../../plugins/auth'), options: config.auth },
        ]));
    });

    after(function () {
        mockApi.reset({ models: true, devices: true, users: true, permissions: true, templates: true, locations: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    beforeEach(function () {
        mockApi.reset({ events: true });
    });

    it('should not register without a type', function () {
        return server.registerAsync([
            { register: require('../../plugins/api-crud'), options: {} }
        ]).then(unexpected, error => {
            expect(error.message).to.match(/api-crud requires a type option/);
        });
    });

    it('should register', function () {
        return server.registerAsync([
            { register: require('../../plugins/api-crud'), options: { type: 'location' } },
        ]);
    });

    it('should register another type', function () {
        return server.registerAsync([
            { register: require('../../plugins/api-crud'), options: { type: 'template' } },
        ]);
    });

    it('should start the server', function () {
        this.slow(2000);

        return server.startAsync().then(() => { config.fixPorts(server); });
    });

    it('should create resources', function () {
        this.slow(1200);

        return Promise.all([
            Promise.mapSeries(LOCATIONS, location => request({
                url: endpoint('/locations'),
                method: 'POST',
                payload: location
            })),
            Promise.mapSeries(TEMPLATES, template => request({
                url: endpoint('/templates'),
                method: 'POST',
                payload: template
            })),
        ]).then(() => {
            expect(mockApi.database.locations).to.deep.equal(LOCATIONS);
            expect(mockApi.database.templates).to.deep.equal(TEMPLATES);
        });
    });

    it('should update a resource', function () {
        this.slow(300);

        const newName = 'Updated location one';
        return request({
            url: endpoint(`/locations/${LOCATIONS[0]._id}`),
            method: 'PUT',
            payload: { name: newName }
        }).then(() => {
            expect(mockApi.database.locations[0].name).to.equal(newName);
        });
    });

    it('should delete a resource', function () {
        this.slow(300);

        return request({
            url: endpoint(`/locations/${LOCATIONS[2]._id}`),
            method: 'DELETE',
        }).then(() => {
            expect(mockApi.database.locations.length).to.equal(2);
        });
    });

    it('should retrieve a single resource', function () {
        mockApi.database.locations[0].name = LOCATIONS[0].name; // Restore the original name.
        return request(endpoint(`/locations/${LOCATIONS[0]._id}`)).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.deep.equal(LOCATIONS[0]);
        });
    });

    it('should update local resources on list', function () {
        this.slow(400);

        mockApi.database.locations.push(LOCATIONS[2]);
        return permission.grant(context, { who: 'mock-user', whoType: 'user' }, { resource: LOCATIONS[2]._id, type: 'location' })
        .then(() => request(endpoint('/locations')))
        .then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.deep.equal(LOCATIONS);
        });
    });

    describe('when the API is unavailable', function () {
        before(function () {
            return mockApi.stop();
        });

        it('should list cached resources', function () {
            return request(endpoint('/locations')).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal(LOCATIONS);
            });
        });

        it('should get a single cached resource', function () {
            return request(endpoint(`/locations/${LOCATIONS[0]._id}`)).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.deep.equal(LOCATIONS[0]);
            });
        });

        it('should fail to create a resource', function () {
            return request({
                url: endpoint('/locations'),
                method: 'POST',
                payload: { name: 'New Location' }
            }).then(response => {
                expect(response.statusCode).to.equal(503);
            });
        });

        it('should fail to update a resource', function () {
            return request({
                url: endpoint(`/locations/${LOCATIONS[0]._id}`),
                method: 'PATCH',
                payload: { name: 'Updated Location Name' }
            }).then(response => {
                expect(response.statusCode).to.equal(503);
            });
        });

        it('should fail to delete a resource', function () {
            return request({
                url: endpoint(`/locations/${LOCATIONS[0]._id}`),
                method: 'DELETE',
            }).then(response => {
                expect(response.statusCode).to.equal(503);
            });
        });
    });
});
