/*global describe, it, before, after, afterEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const Device = require('../../lib/device');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

const MODELS = [{
    _id: config.model_id,
}, {
    _id: 'infrared_sensor',
    capabilities: [{
        capabilityId: 'infrared',
        type: 'enum',
        methods: ['get'],
        range: ['idle', 'detected']
    }]
}];

describe('Device plugin', function () {
    this.slow(300);

    var context, server;

    function request(options) {
        if (typeof options === 'string') {
            options = { url: options, method: 'GET' };
        }
        options = _.merge({ headers: { authorization: 'Bearer ' + server.plugins.auth.apiToken, 'x-who': 'mock-user', 'x-who-type': 'user' } }, options);
        return new Promise(resolve => server.select('api').inject(options, resolve));
    }

    before(function (done) {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = new Hapi.Server({ debug: false });
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            server.register([
                { register: require('../../plugins/cache'), options: {} },
                { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
                { register: require('../../plugins/system'), options: {} },
                { register: require('../../plugins/event'), options: {} },
                { register: require('../../plugins/auth'), options: config.auth },
            ], done);
        }).catch(done);
    });

    after(function (done) {
        server.stop(() => {
            mockApi.reset({ models: true, devices: true });
            return Promise.all([
                mockApi.stop(),
                dropDbs(context),
            ]).catchReturn().then(() => done());
        });
    });

    afterEach(function () {
        mockApi.reset({ events: true });
    });

    it('should register', function (done) {
        server.register([
            { register: require('../../plugins/device'), options: {} },
            { register: require('../../plugins/emu') },
        ], done);
    });

    it('should start the server', function (done) {
        this.slow(2000);

        server.start(() => {
            config.fixPorts(server);
            permission.upsertGroups(context, [{
                _id: server.plugins.system.group,
                members: [
                    { whoType: 'user', who: 'mock-user' },
                    { whoType: 'gateway', who: server.plugins.system.device._id },
                ],
                children: []
            }]).asCallback(done);
        });
    });

    it('should register the emu protocol', function () {
        return Device.getProtocol('emu').then(protocol => {
            expect(protocol).to.be.an('object');
            expect(protocol.addDevice).to.be.a('function');
            expect(protocol.removeDevice).to.be.a('function');
            expect(protocol.setCapability).to.be.a('function');
            expect(protocol.setParameters).to.be.a('function');

            expect(server.plugins.emu.fakeCapability).to.be.a('function');
        });
    });

    describe('endpoints', function () {
        var deviceId;

        it('POST /devices', function () {
            this.slow(600);

            return request({
                method: 'POST',
                url: endpoint('/devices'),
                json: true,
                payload: {
                    model: 'infrared_sensor',
                    name: 'Test IR Sensor',
                    isEmulated: true,
                }
            }).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
                deviceId = response.result._id;
            });
        });

        it('GET /devices', function () {
            return request(endpoint('/devices')).then(response => {
                expect(response.statusCode).to.equal(200);
                const all = response.result;
                expect(all).to.be.an('array');
                expect(all.length).to.equal(2);
                expect(all[0]._id).to.equal(server.plugins.system.device._id);
                expect(all[1]._id).to.equal(deviceId);
            });
        });

        it('GET /devices/{id}', function () {
            return request(endpoint(`/devices/${deviceId}`)).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result._id).to.equal(deviceId);
                expect(response.result.model).to.equal('infrared_sensor');
                expect(response.result.caps).to.be.an('object');
                expect(response.result.caps.infrared).to.be.an('object');
                expect(response.result.caps.infrared.value).to.equal('idle');
            });
        });

        it('PUT /devices/{id}', function () {
            return request({
                url: endpoint(`/devices/${deviceId}`),
                method: 'PUT',
                payload: {
                    name: 'Test IR Sensor',
                    parameters: {}
                },
            }).then(response => {
                expect(response.statusCode).to.equal(304);
            });
        });

        it('GET /devices/{id}/caps/{name}', function () {
            return request(endpoint(`/devices/${deviceId}/caps/infrared`)).then(response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result.value).to.equal('idle');
            });
        });

        it('PUT /devices/{id}/caps/{name}', function () {
            return request({
                url: endpoint(`/devices/${deviceId}/caps/infrared`),
                method: 'PUT',
                payload: { value: 'detected' }
            }).then(response => {
                expect(response.statusCode).to.equal(405);
            });
        });

        it('PUT /devices/{id}/caps/{name}?emulate', function () {
            return request({
                url: endpoint(`/devices/${deviceId}/caps/infrared?emulate`),
                method: 'PUT',
                payload: { value: 'detected' }
            }).then(response => {
                expect(response.statusCode).to.equal(307);
                expect(response.headers.location).to.equal(`/devices/${deviceId}/emu/caps/infrared`);
            });
        });

        it('PUT /devices/{id}/emu/caps/{name}', function () {
            return request({
                url: endpoint(`/devices/${deviceId}/emu/caps/infrared`),
                method: 'PUT',
                payload: { value: 'detected' }
            }).then(response => {
                expect(response.statusCode).to.equal(200);
            });
        });

        it('POST /devices/{id}/emu/fail', function () {
            return request({
                url: endpoint(`/devices/${deviceId}/emu/fail`),
                method: 'POST',
                payload: {}
            }).then(response => {
                expect(response.statusCode).to.equal(204);
                return context.collection('devices').get(deviceId);
            }).then(device => {
                expect(device.status).to.equal('FAILED');
            });
        });

        it('POST /devices/{id}/emu/restore', function () {
            return request({
                url: endpoint(`/devices/${deviceId}/emu/restore`),
                method: 'POST',
                payload: {}
            }).then(response => {
                expect(response.statusCode).to.equal(204);
                return context.collection('devices').get(deviceId);
            }).then(device => {
                expect(device.status).to.equal('ACTIVE');
            });
        });

        it('DELETE /devices/{id}', function () {
            this.slow(500);

            return request({
                url: endpoint(`/devices/${deviceId}`),
                method: 'DELETE',
            }).then(response => {
                expect(response.statusCode).to.equal(204);
            });
        });
    });
});
