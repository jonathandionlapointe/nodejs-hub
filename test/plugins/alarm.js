/*global describe, it, before, after, beforeEach*/
'use strict';

const _ = require('lodash');
const config = require('../../lib/config');
const context = require('../../lib/context');
const Device = require('../../lib/device');
const dropDbs = require('../util/drop-dbs');
const expect = require('chai').expect;
const fs = require('fs');
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const permission = require('../../lib/permission');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}, {
    _id: 'multi_sensor',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'motion',
        type: 'enum',
        methods: ['get'],
        range: ['idle', 'detected']
    }, {
        capabilityId: 'temperature',
        type: 'float',
        methods: ['get'],
        units: ['C'],
        range: [
            { value: -32, unit: 'C' },
            { value: 100, unit: 'C' }
        ]
    }]
}, {
    _id: 'smoke_detector',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'smoke',
        type: 'enum',
        methods: ['get'],
        range: ['idle', 'detected']
    }]
}, {
    _id: 'contact',
    protocolId: 'emu',
    capabilities: [{
        capabilityId: 'doorWindow',
        type: 'enum',
        methods: ['get'],
        range: ['closed', 'open']
    }]
}];

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

function waitUntil(predicate, timeout) {
    function f() {
        if (predicate()) return Promise.resolve();
        return Promise.delay(50).then(f);
    }
    return timeout ? f().timeout(timeout) : f();
}

function waitUntilEventCount(n, timeout) {
    return waitUntil(() => mockApi.database.events.length >= n, timeout);
}

describe('Alarm plugin', function () {
    this.slow(150);

    var context, server;

    function request(options) {
        if (typeof options === 'string') {
            options = { url: options, method: 'GET' };
        }
        options = _.merge({ headers: { authorization: 'Bearer ' + server.plugins.auth.apiToken, 'x-who': 'mock-user', 'x-who-type': 'user' } }, options);
        return new Promise(resolve => server.select('api').inject(options, resolve));
    }

    before(function () {
        expect(fs.existsSync(config.system.file)).to.equal(true);

        server = Promise.promisifyAll(new Hapi.Server({ debug: false }));
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        server.connection({
            host: config.server.host,
            port: config.server.internalPort,
            labels: ['internal'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        return dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            return server.registerAsync([
                { register: require('../../plugins/cache'), options: {} },
                { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
                { register: require('../../plugins/system'), options: {} },
                { register: require('../../plugins/event'), options: {} },
                { register: require('../../plugins/auth'), options: config.auth },
                { register: require('../../plugins/device'), options: {} },
                { register: require('../../plugins/emu') },
                { register: require('../../plugins/scenario') }
            ]);
        });
    });

    after(function () {
        mockApi.reset({ models: true, devices: true });
        return Promise.all([
            server.stopAsync(),
            mockApi.stop(),
            dropDbs(context),
        ]).catchReturn();
    });

    beforeEach(function () {
        mockApi.reset({ events: true });
    });

    it('should register', function () {
        return server.registerAsync([
            { register: require('../../plugins/alarm'), options: {} },
        ]);
    });

    it('should start the server', function () {
        this.slow(2000);

        return server.startAsync().then(() => { config.fixPorts(server); }).then(() => permission.upsertGroups(context, [{
            _id: server.plugins.system.group,
            members: [
                { whoType: 'user', who: 'mock-user' },
                { whoType: 'gateway', who: server.plugins.system.device._id },
            ],
            children: [],
        }])).then(() => waitUntilEventCount(1));
    });

    let defaultSystemId;
    it('should create a default alarm system', function () {
        return request({
            method: 'GET',
            url: endpoint('/alarms'),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(1);
            defaultSystemId = response.result[0]._id;
        });
    });

    it('should be able to delete the default alarm system', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${defaultSystemId}`)
        }).then(response => {
            expect(response.statusCode).to.equal(204);
        });
    });

    const device = {};
    it('should create devices', function () {
        this.slow(800);

        return context.withTransactionContext(context => {
            return Promise.mapSeries([
                [Device.add, [context, 'multi_sensor', {}]],
                [Device.add, [context, 'smoke_detector', {}]],
                [Device.add, [context, 'contact', {}]],
                [Device.add, [context, 'contact', {}]],
            ], funargs => funargs[0].apply(null, funargs[1]));
        }).spread((multiSensor, smokeDetector, contact, contact2) => {
            device.multiSensor = multiSensor[0]._id;
            device.smokeDetector = smokeDetector[0]._id;
            device.contact = contact[0]._id;
            device.contact2 = contact2[0]._id;
            expect(device.multiSensor).to.be.a('string');
            expect(device.smokeDetector).to.be.a('string');
            expect(device.contact).to.be.a('string');
            expect(device.contact2).to.be.a('string');
            return Promise.mapSeries(_.values(device), id => {
                return permission.grant(context, { whoType: 'user', who: 'mock-user' }, { type: 'device', resource: id });
            })
            .then(() => waitUntilEventCount(4));
        });
    });

    const system = {};
    it('should create alarm systems', function () {
        this.slow(1200);

        return Promise.all(_.map(['a', 'b', 'c'], ignored => request({
            method: 'POST',
            url: endpoint('/alarms'),
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
            });
            system.a = responses[0].result._id;
            system.b = responses[1].result._id;
            system.c = responses[2].result._id;
            return waitUntilEventCount(3);
        });
    });

    it('should list alarm systems', function () {
        return request({
            method: 'GET',
            url: endpoint('/alarms'),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(3);

            const responseIds = _.map(response.result, '_id');
            const systemIds = [system.a, system.b, system.c];
            expect(responseIds.sort()).to.deep.equal(systemIds.sort());
        });
    });

    it('should get an alarm system', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(system.a);
        });
    });

    it('should edit an alarm system', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}`),
            payload: { name: 'Test System A' },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.name).to.equal('Test System A');
            return waitUntilEventCount(1);
        });
    });

    const access = {};
    const pins = { a: '0000', b: '1234', c: '9999' };
    it('should add access PINs to an alarm system', function () {
        return Promise.all(_.map(['a', 'b', 'c'], (id) => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/access`),
            payload: { pin: pins[id], name: id }
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
                expect(response.result).to.not.have.keys('pin');
            });
            access.a = responses[0].result._id;
            access.b = responses[1].result._id;
            access.c = responses[2].result._id;
            return waitUntilEventCount(3);
        });
    });

    it('should delete an access PIN', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/access/${access.c}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should get a list of access PINs', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/access`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(2);

            const responseIds = _.map(response.result, '_id');
            const accessIds = [access.a, access.b];
            expect(responseIds.sort()).to.deep.equal(accessIds.sort());
        });
    });

    it('should change an access PIN', function () {
        pins.a = '4321';
        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/access/${access.a}`),
            payload: { pin: pins.a },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            return waitUntilEventCount(1);
        });
    });

    const user = {};
    it('should add users to an alarm system (DEPRECATED)', function () {
        this.slow(600);

        return Promise.all(_.map(['a', 'b', 'c'], (id) => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/users`),
            payload: { _id: id }
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
            });
            user.a = responses[0].result._id;
            user.b = responses[1].result._id;
            user.c = responses[2].result._id;
            return waitUntilEventCount(3);
        });
    });

    it('should delete a user (DEPRECATED)', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/users/${user.c}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should get a list of users (DEPRECATED)', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/users`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(2);

            const responseIds = response.result;
            const userIds = [user.a, user.b];
            expect(responseIds.sort()).to.deep.equal(userIds.sort());
        });
    });

    it('should set a PIN for a user (DEPRECATED)', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/users/${user.a}/pin`),
            payload: { pin: '0000' }
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should change a user\'s PIN (DEPRECATED)', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/users/${user.a}/pin`),
            payload: { pin: '0101' }
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should update a user (DEPRECATED)', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/users/${user.a}`),
            payload: { name: 'User A' },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(user.a);
        });
    });

    function getToken(systemId, pin) {
        return request({
            method: 'POST',
            url: endpoint(`/alarms/${systemId}/auth/token`),
            payload: { pin: pin }
        }).then(response => {
            if (response.statusCode === 200) {
                return response.result.armingToken;
            } else {
                throw new Error(response);
            }
        });
    }

    it('should get an arming token', function () {
        return getToken(system.a, '1234');
    });

    it('should not arm without an arming token', function () {
        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}`),
            payload: { status: 'armed' },
        }).then(response => {
            expect(response.statusCode).to.equal(403);
        });
    });

    it('should not arm with an invalid token', function () {
        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=foobarbaz`),
            payload: { status: 'armed' },
        }).then(response => {
            expect(response.statusCode).to.equal(403);
        });
    });

    it('should not arm with a token from another alarm system', function () {
        return getToken(system.a, '1234').then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.b}?armToken=${token}`),
            payload: { status: 'armed' },
        })).then(response => {
            expect(response.statusCode).to.equal(403);
        });
    });

    it('should not arm an empty alarm system', function () {
        return getToken(system.a, '1234').then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=${token}`),
            payload: { status: 'armed' },
        })).then(response => {
            expect(response.statusCode).to.equal(304);
        });
    });

    const zone = {};
    it('should create zones', function () {
        this.slow(800);

        return Promise.all(_.map([{
            device: device.multiSensor,
            capability: 'motion',
            normalState: 'idle',
        }, {
            device: device.smokeDetector,
            capability: 'smoke',
            normalState: 'idle',
            is24hours: true,
        }, {
            device: device.contact,
            capability: 'doorWindow',
            normalState: 'closed',
        }], payload => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/zones`),
            payload: payload,
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
            });
            zone.motion = responses[0].result._id;
            zone.smoke = responses[1].result._id;
            zone.doorWindow = responses[2].result._id;
            return waitUntilEventCount(3);
        });
    });

    it('should list zones', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/zones`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(3);

            const responseIds = _.map(response.result, '_id');
            const zoneIds = [zone.motion, zone.smoke, zone.doorWindow];
            expect(responseIds.sort()).to.deep.equal(zoneIds.sort());
        });
    });

    it('should get a zone', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/zones/${zone.motion}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(zone.motion);
        });
    });

    it('should update a zone', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/zones/${zone.doorWindow}`),
            payload: {
                perimeter: true
            },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.perimeter).to.equal(true);
            return waitUntilEventCount(1);
        });
    });

    const sector = {};
    it('should create sectors', function () {
        this.slow(400);

        return Promise.all(_.map([
            { name: 'Sector A' },
            { name: 'Sector B' },
        ], payload => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/sectors`),
            payload: payload,
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('object');
                expect(response.result._id).to.be.a('string');
            });
            sector.a = responses[0].result._id;
            sector.b = responses[1].result._id;
            return waitUntilEventCount(2);
        });
    });

    it('should list sectors', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/sectors`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result.length).to.equal(2);

            const responseIds = _.map(response.result, '_id');
            const sectorIds = [sector.a, sector.b];
            expect(responseIds.sort()).to.deep.equal(sectorIds.sort());
        });
    });

    it('should get a sector', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result._id).to.equal(sector.a);
        });
    });

    it('should update a sector', function () {
        this.slow(300);

        return request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
            payload: { name: 'Sector A modified' },
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.name).to.equal('Sector A modified');
            return waitUntilEventCount(1);
        });
    });

    it('should add zones to a sector', function () {
        this.slow(600);

        return Promise.all(_.map([zone.motion, zone.doorWindow], id => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}/zones/${id}`),
        }))).then(responses => {
            _.forEach(responses, response => {
                expect(response.statusCode).to.equal(200);
                expect(response.result).to.be.an('array');
            });
            return request({
                method: 'GET',
                url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
            });
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('object');
            expect(response.result.zones).to.be.an('array');

            const responseIds = response.result.zones;
            const zoneIds = [zone.motion, zone.doorWindow];
            expect(responseIds.sort()).to.deep.equal(zoneIds.sort());

            return waitUntilEventCount(2);
        });
    });

    it('should arm the alarm system', function () {
        this.slow(500);

        return getToken(system.a, pins.b).then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=${token}`),
            payload: { status: 'armed' }
        })).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result.status).to.equal('armed');
            return waitUntilEventCount(1);
        }).then(() => {
            const event = mockApi.database.events[0];
            expect(event.resource.status).to.equal('armed');
            expect(event.access).to.deep.equal({
                _id: access.b,
                name: 'b',
                schedule: null,
                expiry: 'never',
                user: null,
            });
        });
    });

    it('should fail to remove an armed alarm system', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should fail to remove a zone from an armed system', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/zones/${zone.motion}`),
        }).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should fail to remove a sector from an armed system', function () {
        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should fail to add a zone to an armed system', function () {
        return request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/zones`),
            payload: {
                device: device.contact2,
                capability: 'doorWindow',
                normalState: 'closed'
            }
        }).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should fail to add a sector to an armed system', function () {
        return request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/sectors`),
            payload: { name: 'Sector C' }
        }).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should disarm the alarm system', function () {
        this.slow(300);

        return getToken(system.a, '1234').then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=${token}`),
            payload: { status: 'disarmed' }
        })).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result.status).to.equal('disarmed');
            return waitUntilEventCount(1);
        });
    });

    it('should fail to add a 24/7 zone on a tripped capability', function () {
        this.slow(500);

        return request({
            method: 'PUT',
            url: endpoint(`/devices/${device.contact2}/emu/caps/doorWindow`),
            payload: { value: 'open' }
        }).then(response => {
            expect(response.statusCode).to.be.within(200, 299);
            return waitUntilEventCount(1);
        }).then(() => request({
            method: 'POST',
            url: endpoint(`/alarms/${system.a}/zones`),
            payload: {
                device: device.contact2,
                is24hours: true,
                capability: 'doorWindow',
                normalState: 'closed',
            }
        })).then(response => {
            expect(response.statusCode).to.equal(400);
        });
    });

    it('should fail to arm a tripped zone', function () {
        this.slow(500);

        return request({
            method: 'PUT',
            url: endpoint(`/devices/${device.multiSensor}/emu/caps/motion`),
            payload: { value: 'detected' }
        }).then(response => {
            expect(response.statusCode).to.be.within(200, 299);
            return waitUntilEventCount(2);
        }).then(() => getToken(system.a, '1234')).then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=${token}`),
            payload: { status: 'armed' }
        })).then(response => {
            expect(response.statusCode).to.equal(409);
        });
    });

    it('should bypass a tripped zone', function () {
        this.slow(800);

        return getToken(system.a, '1234').then(token => request({
            method: 'PUT',
            url: endpoint(`/alarms/${system.a}?armToken=${token}`),
            payload: { status: 'armed', bypassed: [ zone.motion ] }
        })).then(response => {
            expect(response.statusCode).to.equal(200);
            const object = _.findWhere(response.result.zones, { _id: zone.motion });
            expect(object).to.be.an('object');
            expect(object.zoneStatus).to.equal('bypassed');
            return getToken(system.a, '1234').then(token => request({
                method: 'PUT',
                url: endpoint(`/alarms/${system.a}?armToken=${token}`),
                payload: { status: 'disarmed' }
            })).then(() => waitUntilEventCount(2));
        });
    });

    it('should remove a zone', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/zones/${zone.motion}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should fail to get a non-existing zone', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/zones/${zone.motion}`),
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });

    it('should remove zones from a sector', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}/zones/${zone.doorWindow}`),
        }).then(response => {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.be.an('array');
            expect(response.result).to.deep.equal([]);
            return waitUntilEventCount(1);
        });
    });

    it('should remove a sector', function () {
        this.slow(300);

        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should fail to get a non-existing sector', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}/sectors/${sector.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });

    it('should remove an alarm system', function () {
        this.slow(500);

        return request({
            method: 'DELETE',
            url: endpoint(`/alarms/${system.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(204);
            return waitUntilEventCount(1);
        });
    });

    it('should fail to get a non-existing alarm system', function () {
        return request({
            method: 'GET',
            url: endpoint(`/alarms/${system.a}`),
        }).then(response => {
            expect(response.statusCode).to.equal(404);
        });
    });
});
