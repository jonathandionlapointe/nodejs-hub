/*global describe, it, before, after*/
'use strict';

const config = require('../../lib/config');
const context = require('../../lib/context');
const dropDbs = require('../util/drop-dbs');
const Events = require('../../lib/events');
const expect = require('chai').expect;
const Hapi = require('hapi');
const mockApi = require('../mock/api');
const Promise = require('bluebird');

const app = {};
app.context = context.createAppContext(app);

const MODELS = [{
    _id: config.model_id,
}];

function endpoint(path) {
    return `http://localhost:${config.server.port}${path}`;
}

describe('Event plugin', function () {
    this.slow(150);

    var context, server;

    function request(options) {
        return new Promise(resolve => server.inject(options, resolve));
    }

    before(function (done) {
        server = new Hapi.Server({ debug: false });
        server.connection({
            host: config.server.host,
            port: config.server.port,
            labels: ['api'],
        });
        context = app.context.createServerContext(server);
        mockApi.reset({ models: MODELS });
        dropDbs(context)
        .then(() => mockApi.start())
        .then(() => {
            server.register([
                { register: require('../../plugins/cache'), options: {} },
                { register: require('../../plugins/sdk'), options: { logLevel: 0 } },
            ], done);
        }).catch(done);
    });

    after(function (done) {
        mockApi.reset({ models: true, events: true });
        server.stop(() => {
            return Promise.all([
                mockApi.stop(),
                dropDbs(context),
            ]).catchReturn().then(() => done());
        });
    });

    it('should register', function (done) {
        server.register([
            { register: require('../../plugins/event'), options: {} },
        ], done);
    });

    it('should start the server', function (done) {
        this.slow(600);

        server.start((err) => {
            if (err) return done(err);
            config.fixPorts(server);
            done();
        });
    });

    it('should expose an Events instance', function () {
        expect(server.plugins.event).to.be.instanceof(Events);
    });

    // TODO no tokens have the 'event.create' permission scope yet; nothing
    // should be able to inject new events into the system anyway (for now).
    it.skip('should create events by posting to the events endpoint', function () {
        const evt = {
            type: 'update',
            path: '/foo/bar',
            resource: { value: 42 },
        };
        const events = server.plugins.event;

        let listener;
        const eventPromise = new Promise((resolve, reject) => {
            listener = events.onEventPathMatch(/foo/, (context, matches, event) => {
                try {
                    expect(event).to.be.an('object');
                    expect(event.type).to.equal(evt.type);
                    expect(event.path).to.equal(evt.path);
                    expect(event.resource).to.deep.equal(evt.resource);
                    resolve();
                } catch (error) {
                    reject(error);
                }
            });
        }).timeout(1000).finally(() => {
            events.removeEventListener(listener);
        });

        return Promise.all([eventPromise, request({
            url: endpoint('/events'),
            method: 'POST',
            payload: evt
        }).then(response => {
            expect(response.statusCode).to.be.within(200, 299);
        }).timeout(1000)]);
    });
});
