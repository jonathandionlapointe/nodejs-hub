#
# Regular cron jobs for the 2klic-smart-controller package
#

# 60-minute interval heartbeat.
0 *	* * *	root	[ -x /usr/lib/2klic/scripts/heartbeat.sh ] && /usr/lib/2klic/scripts/heartbeat.sh
