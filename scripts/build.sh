#!/bin/bash
cd /opt/app/site;

# Inform about the KEY
echo "***** COPY THIS KEY TO BIT BUCKET REPO WEBHOOKS !!! ****";
cat /root/.ssh/id_rsa.pub;

# If $HASH is specified, then specifically get the hash commit
# else get the latest commit on the branch
# The following code pull the code from the branch and rebuild the code
git stash;
if [ -z "$HASH" ];
    then
        echo "No hash defined";
        echo "Retrieving latest code for branch: "$REPO"/"$BRANCH;
        SUBJECT='New release: '$REPO'/'$BRANCH;
        BODY='New version deployed: '$REPO'/'$BRANCH;
        git pull -f origin $BRANCH;
    else
        echo "Tag defined: "$HASH;
        echo "Retrieving latest code for branch: "$REPO"/"$BRANCH"@"$HASH;
        SUBJECT='New release: '$REPO'/'$BRANCH'@'$HASH;
        BODY='New version deployed: '$REPO'/'$BRANCH'@'$HASH;
        git checkout $HASH;
fi

# Install latest version of each package and install if not present.
npm install;
npm update;

gulp build;

echo $BODY;

# Send notification email to inform about new code loaded
curl -s --user $MAILGUN_AUTH $MAILGUN_URL -F from="$FROM" -F to="$TO" -F SUBJECT="$SUBJECT" -F text="$BODY" &
