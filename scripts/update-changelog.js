'use strict';

const dateFormat = require('dateformat');
const info = require('../package.json');
const fs = require('fs');

// Ensure the version has a pre-release tag, otherwise semver-parsed versions
// will indicate that the (not-really-pre-release-tagged) packages built by
// Jenkins are older than the NPM-tagged version.
let version = info.version;
if (!version.match(/-/)) {
    version = `${version}-1`;
}

function main() {
    if (process.argv.length < 3) {
        console.error("Usage: node update-changelog.js CHANGELOG");
        process.exit(1);
    }

    const path = process.argv[2];
    const dist = process.argv[3] || 'unstable';
    const contents = fs.readFileSync(path, 'utf8');

    // Simple check to see if this version is already in the changelog.
    if (contents.indexOf(`(${version})`) >= 0) {
        console.error(`Version ${version} is already in the changelog!`);
        process.exit(1);
    }

    let changesAdded = false;
    let newLog =
        `${info.name.replace(/@[^/]+\//, '')} (${version}) ${dist}; urgency=low\n` +
        '\n' +
        `  * Version bump to ${version}.\n`;

    process.stdin.on('readable', () => {
        const chunk = process.stdin.read();
        if (chunk !== null) {
            newLog += chunk.toString('utf8');
            changesAdded = true;
        }
    });

    process.stdin.on('end', () => {
        if (changesAdded) {
            newLog += '\n';
        }
        const date = dateFormat(new Date(), 'ddd, dd mmm yyyy HH:MM:ss o');
        newLog += '\n' +
            ` -- 2KLIC <maint@2klic.com>  ${date}\n` +
            '\n' + contents;
        try {
            fs.writeFileSync(path, newLog);
            process.exit(0);
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
    });
}

if (require.main === module) {
    try {
        main();
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}
