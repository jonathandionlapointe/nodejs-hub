#!/bin/bash

# Launch Bitbucket listener server
cd /opt/app/redeploy;
npm install;
npm update;
npm install -g gulp;
node /opt/app/redeploy/bitbucket-listener.js &

# Build
sh /opt/app/scripts/build.sh;

# Start server and reload if files changes, throttle 30 sec to avoid multiple reloads
cd /opt/app/site;
nodemon -e js,json,sh --watch ./ --delay 30 index.js;