#!/usr/bin/env node
/*eslint no-console: ["off"]*/
'use strict';

const _ = require('lodash');
const yaml = require('js-yaml');

function main()
{
    let env = 'default';
    if (process.argv.length >= 3) {
        env = process.argv[2];
    }

    let input = '';
    process.stdin.on('data', (data) => {
        if (data instanceof Buffer) {
            input += data.toString('utf8');
        } else if (typeof data === 'string') {
            input += data;
        } else {
            console.error('Unexpected input data type:', typeof data);
        }
    });

    process.stdin.on('end', () => {
        try {
            const cfg = yaml.safeLoad(input);
            if (!_.has(cfg, env)) {
                console.error(`Environment '${env}' not found in input configuration.`);
                process.exit(1);
            }
            const output = yaml.safeDump(_.pick(cfg, env));
            process.stdout.write(output, (err) => {
                if (err) {
                    console.error(err);
                    process.exit(1);
                }
                process.exit(0);
            });
        } catch (err) {
            console.error(err);
            process.exit(1);
        }
    });
}

if (require.main === module) {
    try {
        main();
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}
