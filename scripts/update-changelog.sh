#!/bin/bash

changelog="$1"
git log `git describe --tags --abbrev=0`..HEAD --pretty=format:'  * %s' | node scripts/update-changelog.js "$@" && git add "$changelog"