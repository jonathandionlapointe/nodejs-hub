#!/bin/bash

sudo apt-get install couchdb -y;
sudo npm install @2klic/2klic-nodejs-hub;
ln -s /opt/node_modules/@2klic/2klic-nodejs-hub /opt/hub;
ln -s /opt/hub/scripts/update.sh /opt;
ln -s /opt/hub/scripts/install.sh /opt;
sudo npm install -g gulp forever forever-service;
cd /opt/hub;
sudo forever-service install hub -s index.js --logrotateDateExt --logrotateCompress --logrotateFrequency daily;
sudo start hub;

