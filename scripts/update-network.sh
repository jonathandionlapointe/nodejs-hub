#!/bin/bash

if [ $(id -u) -ne 0 ]; then
    >&2 echo "Only root can run this script!"
    exit 1
fi

system=/var/lib/sc2klic/system.json

if [ ! -e "$system" ]; then
    >&2 echo "System not provisioned!"
    exit 1
fi

provisioned_mac="$(< "$system" perl -MJSON -ne 'print @{@{from_json($_)}{"provisioning"}}{"mac"}')"
provisioned_hostname="$(< "$system" perl -MJSON -ne 'print @{from_json($_)}{"hostname"}')"

current_mac="$(< /etc/network/interfaces awk '$1 == "hwaddress" { print $3; }')"
current_hostname="$(hostname)"
current_hosts_entry="$(</etc/hosts awk '$1 == "127.0.1.1" { print $2; }')"

reboot_required=no

if [ "$provisioned_mac" != "$current_mac" ]; then
    reboot_required=yes
    sed -i -e "s/$current_mac/$provisioned_mac/" /etc/network/interfaces
fi

if [ -n "$provisioned_hostname" -a "$provisioned_hostname" != "$current_hostname" ]; then
    reboot_required=yes
    hostname "$provisioned_hostname"
    echo "$provisioned_hostname" > /etc/hostname
fi

if [ -n "$provisioned_hostname" -a "$provisioned_hostname" != "$current_hosts_entry" ]; then
    sed -i -e "s/$current_hosts_entry/$provisioned_hostname/" /etc/hosts
fi

if [ $reboot_required = yes ]; then
    reboot
fi
