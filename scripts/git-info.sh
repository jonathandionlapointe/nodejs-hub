#!/bin/sh

SHORT=`git rev-parse --short HEAD`
LONG=`git rev-parse HEAD`
BRANCH=`git rev-parse --abbrev-ref HEAD`
TAG=`git describe --always --tag --abbrev=0`

echo "{
    \"short\": \"$SHORT\",
    \"long\": \"$LONG\",
    \"branch\": \"$BRANCH\",
    \"tag\": \"$TAG\"
}"
