#!/bin/bash

# Heartbeat.
#
# Triggers a heartbeat. The service will be restarted if it is non-
# responsive. The heartbeat will cause the service to report in to the
# cloud service if possible.

PIDFILE=/var/run/2klic-gateway.pid
HOST=127.0.0.1
PORT=2002
URL=http://$HOST:$PORT/heartbeat
HEADERS="-Hcontent-type:application/json -Haccept:application/json"
DAEMON=2klic-gateway

if [ ! -f $PIDFILE ]; then
    # We must be waiting for the service to start.
    exit 0
fi

if [ ! -d /proc/$(< $PIDFILE) ]; then
    # Looks like the service died. Try to restart it.
    rm $PIDFILE
    start $DAEMON
    exit 0
fi

if curl -sfm 15 -XPOST $HEADERS $URL >/dev/null ; then
    exit 0
else
    # Error posting to heartbeat!
    restart $DAEMON
fi
