[![Build Status](http://ci.services.2klic.io:8080/buildStatus/icon?job=nodejs-hub/release-17.9)](http://ci.services.2klic.io:8080/job/nodejs-hub/job/release-17.9/)

# Instructions
## Requirements
* Node.js 4
* Ubuntu

### Installing For Production

Follow the [installation instructions](https://2klic-cloud.atlassian.net/wiki/display/2S/New+HUB+Instructions).

### Installing For Development

Install prerequisites for development and packaging:

    sudo apt-get install -y devscripts build-essential libicu-dev libcurl4-gnutls-dev libtool debhelper

If you want the latest git, use the git-core ppa.

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:git-core/ppa
    sudo apt-get update
    sudo apt-get install -y git

Build and install Node.js somewhere you have write access to.
This will allow you to run `npm install -g` without root access.
For example:

    export PATH=$HOME/.local/bin:$PATH    # Add this line to your shell profile
    curl -O https://nodejs.org/dist/v4.4.3/node-v4.4.3.tar.gz
    tar xf node-v4.4.3.tar.gz
    cd node-v4.4.3
    ./configure --prefix=$HOME/.local && make -j4 && make install

The command `make -j4` will run four build tasks in parallel.
As a general rule of thumb, you can use a number that is twice the number of processor cores in your system.

Install docker:

    curl -fsSL https://get.docker.com | sh
    sudo usermod -aG docker <your username>
    # Log out and back in to have the group change take effect.

Install global npm packages:

    npm install -g forever
    npm install -g bunyan
    npm install -g gulp

Some of the hub's dependencies are private packages.
Log in with your npmjs.org credentials.

    npm login

Clone this repository and install dependencies:

    git clone git@bitbucket.org:2klicdev/nodejs-hub.git
    cd nodejs-hub
    npm install

# Configuration

## Development and test environment

Set the environment variables:

    export NODE_ENV=dev

Generally speaking this is the only thing to set up.
It should not be necessary to edit the config file or set other environment variables unless you have custom requirements.

On first start, the controller runs in "provisioning" mode.
To proceed, you must provision the controller.
This requires a running API instance.
The following bash commands will create a user and provision a local controller, assuming a local API on port 3003.
Some commands may prompt you for details (in particular, user registration).
User registration currently fails (even though it succeeds) if the API has notifications enabled (the default in the `dev` environment!).
The final command prints the code that can be used to bind the controller to a user account.
The binding user account cannot be the same account as the provisioning account.

    bash
    export NODE_ENV=dev
    export ZKLIC_API=http://localhost:3003
    eval $(bin/2klic -e register)
    eval $(bin/2klic -e validate $(bin/2klic get-code))
    bin/2klic api POST /provisioning/macs '{"mac":"00:00:00:00:00:00"}'
    token=$(bin/2klic api POST /provisioning/token '{"enableTestFeatures":true}' | jq -r .token)
    curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \ '{"token":"'"$token"'"}' \
    'http://localhost:2000/provisioning'


The above commands set some shell variables.
They can be unset automatically by running:

    eval $(bin/2klic reset)

## Use the configuration shell scripts

For users who are unsure about environment variables, it is possible to use the configuration script to set the environment variables.

To set the shell as "dev" mode, simply run:

    source ./dev_env.sh

or, to set the shell as "test" mode:

    source ./test_env.sh

*IMPORTANT:* Make sure to set the same environment on the API as well.

If you have [direnv](http://direnv.net/) installed, you can set up one of the environments to be used automatically whenever you `cd` into the project folder:

    $ ln -s ./dev_env.sh .envrc
    direnv: error .envrc is block. Run `direnv allow` to approve its content.
    $ direnv allow .
    direnv: loading .envrc
    direnv: export +NODE_ENV

## Production

The default configuration is for production. No environment settings are required.

# Run server

## Development

    node . |& bunyan -o short

Then, to get only the errors, info or warn logs, add `-l <logtype>`, such as:

    node . |& bunyan -o short -l error

You can read more about Bunyan commands [in its documentation](https://github.com/trentm/node-bunyan).

## Using Forever

The following stop any running instance, start a new one and watch the directory for changes.

Stop all running instances:

    forever stop index.js

To start an instance in the background (from the hub directory):

    forever start --watch --watchDirectory ./ index.js

Watch the log:

    forever logs -f index.js

To run in the foreground with logs on stdout:

    forever -w index.js

By default the server should be accessible at http://localhost:2000 (if you run the hub locally on your computer), or at http://<ip>:2000.

# Logging System

To access log in development, run a local ELK container.
The following [docker-compose](https://docs.docker.com/compose/) configuration works with the `dev` environment:

    elk:
        image: 2klicadmin/elk_stack
        ports:
            - "127.0.0.1:5601:5601"
            - "127.0.0.1:9200:9200"
            - "127.0.0.1:5044:5044"
            - "127.0.0.1:9998:9998"
        environment:
            LOGSTASH_ALERT_EMAIL: /dev/null

The Kibana interface will be available [locally](http://localhost:5601).

# Test the web socket

Use the chrome app "Dark WebSocket Terminal" with following parameters (protocol field is empty):

    socket: ws://127.0.0.1:2000/gw/websocket
    protocol:

In the terminal enter:

    /connect ws://127.0.0.1:2000/gw/websocket

All events generated by the hub are sent to listeners on this WebSocket.

# Test Retrofit Modules (Emulated)

Run the emulator in a terminal window.

    $ node tools/retrofit-emulator.js 2
    UUID 36B1DD746800-773B48-7E1990
    UUID 28E4A5696404-4C1D74-4D2D63
    TTY /dev/pts/49
    LISTEN http://127.0.0.1:1970

The argument (2) specifies the number of modules to emulate.
Up to four modules can be emulated (the official number of supported, chained modules).
At startup, the emulator prints the module UUIDs, the serial (TTY) device, and the address of its HTTP interface.
You can connect the retrofit service to the emulator by specifying the TTY at the command line:

    $ node ../retrofit-service /dev/pts/49 | bunyan -o short

The emulator will print messages when the controller makes changes via commands - setting its "board ID", changing output (relay) states, and changing input "normal" states.
By accessing the [HTTP interface](http://127.0.0.1:1970), you can dynamically change the input states by following the links in the JSON data.
Using a browser extension (for example [JSON Formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa?hl=en)) makes viewing and navigating this data relatively easy.

Note that input states are not persisted across starts and they all default to the "error" state.
The module UUIDs are defined statically in the emulator and do *not* change.

The emulator can also be used programatically by importing its source via `require`.
See its source code for more details.

# Tests

To run the local unit tests, run:

    make test

Alternatively you can run the tests and generate a coverage report:

    make cover

To run the integration tests, you must clone the [test repository](https://bitbucket.org/2klicdev/automated_tests).
Run two instances of the hub (in separate terminals):

    NODE_ENV=test \
    forever index.js |& bunyan -o short

    NODE_ENV=test \
    HUB_SERVER_PORT=2001 \
    HUB_SERVER_INTERNAL_PORT=2003 \
    HUB_DB_PREFIX=test2_ \
    HUB_SYSTEM_FILE=./test2-system.json \
    forever index.js |& bunyan -o short

Then run the unit tests.
See the documentation on how to run the tests.
Do not forget to configure the unit tests properly to point to this gateway (the settings above are the defaults for the integrated tests).

## Note about triggers

Tutum provides an interface to set triggers, such as remote restart of the container.
This is very useful if the hub freezes.
The URL will look like:

    https://dashboard.tutum.co/api/v1/service/b703f129-f35c-4e83-86eb-b26b37571a0a/trigger/dee4a1dc-8a67-42a2-886e-362113708e30/call/

So, at any moment, it will be possible to redeploy the container by making a POST request to:

    curl -i -X POST -d '' 'https://dashboard.tutum.co/api/v1/service/b703f129-f35c-4e83-86eb-b26b37571a0a/trigger/dee4a1dc-8a67-42a2-886e-362113708e30/call/'

# Generating a new .deb package

The packaging files are included in the debian folder.
This package includes native modules, so the production build must be performed either on a development board, or using a cross-build environment (details TBD).
Required packages:

    devscripts build-essential libicu-dev libcurl4-gnutls-dev libtool debhelper

You will also need to install Node.js 4.x on the build machine (and the install target!):

    curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
    sudo apt-get install -y nodejs

See [the installation instructions](https://nodejs.org/en/download/package-manager/) for details.

Then build the new packages:

    $ cd /path/to/nodejs-hub
    $ make package
    $ ls build/2klic-{gateway,node-}*.deb

The package builder automatically runs unit tests before building the package, so ensure no services are running on ports 2000 and 3003 before starting the package build.
